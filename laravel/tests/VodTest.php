<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class VodTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->checkout_a_vod_ulr();
    }

    public function checkout_a_vod_ulr(){
        $vod = Factory::create('Broadcasters\Models\AppVod');
        $this->visit("broadcaster/app/4/vod")
            ->verifyInDatabase('app_vod',$vod);
    }
}
