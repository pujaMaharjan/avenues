<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laracasts\TestDummy\Factory;

class ExampleTest extends TestCase
{
   public function test_Login_Work_Or_Not(){
        $this->visit('/')
                ->type("admin@example.com","email")
                ->type('pass123','password')
                ->press("Login")
                ->seePageIs("/");
    }

}
