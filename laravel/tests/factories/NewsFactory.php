<?php

$factory('Broadcasters\Models\AppNews', [
    'name'=>$faker->sentence
]);


$factory('Broadcasters\Models\AppVod', [
    'youtube_channel_id'=>$faker->sentence,
    'feature_playlist'=>$faker->name,
    'videos_count'=>4,

]);