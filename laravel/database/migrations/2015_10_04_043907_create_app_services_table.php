<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_services', function (Blueprint $table) {
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')
            ->references('service_id')->on('services')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->unique(array('app_id','service_id'));
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_services');
    }
}
