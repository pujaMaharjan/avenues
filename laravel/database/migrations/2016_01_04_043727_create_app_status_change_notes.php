<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppStatusChangeNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_status_change_notes', function (Blueprint $table) {
            $table->increments('app_status_change_notes_id');
            $table->string('title_notes');
            $table->text('app_status_change_notes');
            $table->integer('app_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('published_date');
            $table->string('device_ip');

            $table->timestamps();

            $table->foreign('app_id')
            ->references('app_id')->on('app');

            $table->foreign('user_id')
            ->references('user_id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_status_change_notes');
    }
}
