<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppNotificationChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_notification_channels', function (Blueprint $table) {
            $table->increments('app_notification_channels_id');
            $table->integer('notification_channels_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('notification_channels_id')
            ->references('notification_channels_id')->on('notification_channels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->unique(array('app_id','notification_channels_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_notification_channels');
    }
}
