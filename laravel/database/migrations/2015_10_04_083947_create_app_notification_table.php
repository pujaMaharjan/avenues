<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_notification', function (Blueprint $table) {
            $table->increments('app_notification_id');
            $table->string('type');
            $table->text('message');
            $table->string('time');
            $table->string('notification_day');
            $table->integer('status');
            $table->text('data');
            $table->string('exclude_days');
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_notification');
    }
}
