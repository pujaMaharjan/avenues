<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_history', function (Blueprint $table) {
            $table->increments('login_history_id');
            $table->integer('user_id')->unsigned();
            $table->datetime('login_time');
            $table->datetime('logout_time');
             $table->string('device_ip');
             $table->string('user_agent');
            $table->foreign('user_id')
            ->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('login_history');
    }
}
