<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppChannelEpgDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_channel_epg_details', function (Blueprint $table) {
            $table->increments('app_channel_epg_detail_id');
            $table->string('day');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('program_name');
            $table->integer('app_channel_epg_id')->unsigned();
            $table->foreign('app_channel_epg_id')
                ->references('app_channel_epg_id')->on('app_channel_epg')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_channel_epg_details');
    }
}
