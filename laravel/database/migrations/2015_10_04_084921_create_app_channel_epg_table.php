<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppChannelEpgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_channel_epg', function (Blueprint $table) {
            $table->increments('app_channel_epg_id');
            $table->integer('app_channel_id')->unsigned();
            $table->foreign('app_channel_id')
            ->references('app_channel_id')->on('app_channels')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_channel_epg');
    }
}
