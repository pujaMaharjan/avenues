<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_info', function (Blueprint $table) {
            $table->increments('company_info_id');
            $table->string('company_name');
            $table->string('company_address');
            $table->string('support_email');
            $table->string('register_name');
            $table->string('website');
            $table->integer('broadcaster_id')->unsigned();
            $table->foreign('broadcaster_id')
            ->references('broadcaster_id')->on('broadcasters')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_info');
    }
}
