<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('layout_id');
            $table->string('app_template_primary_color');
            $table->string('app_template_secondary_color');
            $table->string('app_template_tertiary_color');
            $table->tinyInteger('approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_templates');
    }
}
