<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAddUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_unit', function (Blueprint $table) {
            $table->increments('add_unit_id');
            $table->string('unit_name')->unique();
            $table->string('type');
            $table->string('app_version')->default('1.0.0');
            $table->integer('add_unit_set_id')->unsigned();
            $table->foreign('add_unit_set_id')
            ->references('add_unit_set_id')->on('add_unit_set')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('platform_id')->unsigned();
            $table->foreign('platform_id')
            ->references('platform_id')->on('platform')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('add_unit');
    }
}
