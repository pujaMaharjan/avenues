<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsReachedStep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_reached_step', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
           // $table->foreign('app_id')->references('app_id')->on('app');
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('name');
            $table->text('description');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments_reached_step');
    }
}
