<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_group', function (Blueprint $table) {
            $table->increments('route_group_id');
            //$table->string('groups_as');
            $table->string('middleware');
            $table->string('weight')->default(0);
            $table->string('namespace');
            $table->string('prefix');
            //$table->string('before');
            $table->integer('order')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')
            ->references('route_group_id')->on('route_group')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_group');
    }
}
