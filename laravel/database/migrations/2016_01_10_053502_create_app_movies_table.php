<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_movies', function (Blueprint $table) {
            $table->increments('app_movie_id');
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->enum('hosted',['youtube','ownServer']);
            $table->string('youtube_id');
            $table->string('movie_name')->nullable();
            $table->string('movie_local_url')->nullable();
            $table->string('movie_cdn_url')->nullable();
            $table->string('movie_photo');
            $table->mediumText('description');
            $table->enum('category',array('sciFi','travel','horror'))->nullable();
            
            $table->string('language');
            $table->string('url_token_key')->nullable();
            $table->integer('valid_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_movies');
    }
}
