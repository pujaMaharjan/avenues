<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('country', function (Blueprint $table) {
        $table->increments('country_id');
        $table->string('country_code');
        $table->string('country_name');
        $table->timestamps();
    });
     Schema::create('broadcasters', function (Blueprint $table) {
        $table->increments('broadcaster_id');
        $table->string('company_name');
        $table->string('display_name');
        $table->string('logo');
        $table->string('phone');
        $table->string('country_code');
       // $table->foreign('country_id')->references('country_id')->on('country');

        $table->boolean('status');
        $table->boolean('approved');
        $table->integer('user_id')->unsigned();
        $table->foreign('user_id')->references('user_id')->on('users');
        $table->timestamps();
    });

 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('broadcasters');
        Schema::drop('country');
    }
}
