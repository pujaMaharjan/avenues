<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route', function (Blueprint $table) {
            $table->increments('route_id');
            $table->string('route_name');
            $table->string('route_verb');
            $table->string('route_as');
            $table->string('route_uses');
            $table->string('middleware');
            $table->string('weight')->default(0);
            $table->integer('order')->unsigned();
            $table->integer('route_group_id')->unsigned()->nullable();
            $table->foreign('route_group_id')
            ->references('route_group_id')->on('route_group')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route');
    }
}
