<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app', function (Blueprint $table) {
            $table->increments('app_id');
            $table->string('app_name');
            $table->mediumText('description');
            $table->string('status');
            $table->enum('platform',array('ios', 'android'));
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('user_id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app');
    }
}
