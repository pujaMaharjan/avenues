<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_channels', function (Blueprint $table) {
            $table->increments('app_channel_id');
            $table->string('name');
            $table->string('sources_local_url');
            $table->string('sources_cdn_url');
            $table->string('logo');
            $table->mediumText('description');
            $table->integer('country_id')->unsigned();
            $table->enum('category',array('music', 'news','Spiritual','entertainment','sports','adventure','miscellenious'));
            //$table->enum('religion',array('hindu','muslim','buddhist','christian','others'));
            $table->string('language');
            $table->string('url_token_key');
            $table->integer('valid_time');
            $table->integer('app_id')->unsigned();
            $table->foreign('app_id')
            ->references('app_id')->on('app')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_channels');
    }
}
