<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('services')->delete();
    	$services =array(
          array('service_id' => '1','service_name' => 'vod','description' => 'vod','status' => '1','price' => '1','created_at' => '2016-01-13 07:10:02','updated_at' => '2016-01-13 07:10:02'),
          array('service_id' => '2','service_name' => 'channels','description' => 'channels','status' => '1','price' => '1','created_at' => '2016-01-13 07:10:18','updated_at' => '2016-01-13 07:10:18'),
          array('service_id' => '3','service_name' => 'movies','description' => 'movies','status' => '1','price' => '1','created_at' => '2016-01-13 07:10:34','updated_at' => '2016-01-13 07:10:34')
          );
    	DB::table('services')->insert($services);
    }

}
