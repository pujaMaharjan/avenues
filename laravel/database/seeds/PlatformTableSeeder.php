<?php

use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('platform')->delete();
    	$platforms = array(
    		array('platform_name' => 'android'),
    		array('platform_name' => 'ios'),
    		);
    	DB::table('platform')->insert($platforms);
    }

}
