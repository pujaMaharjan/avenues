<?php
return [
	'templates'=>[
		
	],
	'menus'=>[
		'Dashboard',
		'VISION NEPAL tv',
		'NEPALJAPAN.Com',
		'News NRN',
		'TV Shows',
		'Radio Aawaz',
		'MY Songs',
		'Magazine',
		'Photo Gallery',
		'About Us'
	],
	'content'=>'NITV Media Group (NMG) is solely dedicated to Nepalese community. NMG is operating multiple media platforms under the register company name of NITV MEDIA Pvt. Ltd. NMG is the first online news portal in Nepalese language.
The group also includes the other news portal such as News NRN, an online TV channel named as Vision Nepal Television and online radio.
The online news portal www.nepaljapan.com namely as NepalJapan was founded in 2002 A.D from Japan, likewise www.newsnrn.com namely News NRN a bilingual web portal was founded in 2015 A.D.
The online television channel of the NMG named as Vision Nepal Television is broadcasted in different IPTV/OTT platforms such as World On Demand, Nepal channels and NETTV Nepal.
NMG provides news about all the Nepalese doings around the world. Non-Residential Nepalese is one of the main cornerstone for development of Nepal. So, NMG provides and publishes the views, news and activities through the NMG media house. We are also working to make it possible for every NRNs all over the world to get the news and information about Nepal and other International issues in both English and Nepalese languages. This app is an online Digital News services which is going to strengthen the communication of media among the Diaspora Nepalese Community. NITV Media Pvt.Ltd is sister company of New IT Venture Corporation Japan.'

];