<?php
return [
	'channel'=>[
		'title'=>'Sample Tv',
		'live_tv'=>'live stream url',
	],
	'new_api'=>[
		'menu'=>'http://www.abctvlive.com/api/public/v1/home',
		'category'=>'http://www.abctvlive.com/api/public/v1/category/{id}/news',
		"single_news"=> "http://www.abctvlive.com/api/public/v1/news/{id}",
		"recent_news"=> "http://www.abctvlive.com/api/public/v1/recentnews?limit=5"
	],
];