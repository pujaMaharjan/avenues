<div class="table-responsive">

    <table class="table table-striped table-hover table-bordered">
       <thead>
           <tr>
               <th>Name</th>
               <td>{{$channel->name}}</td>
           </tr>
           <tr>
               <th>sources_local_url</th>
               <td>{{$channel->sources_local_url}}</td>
           </tr>
           <tr>
               <th>sources_cdn_url</th>
               <td>{{$channel->sources_cdn_url}}</td>
           </tr>
           <tr>
               <th>logo</th>
               <td><img src="{{asset($channel->logo)}}" alt="{{$channel->name}}"></td>
           </tr>
           <tr>
               <th>description</th>
               <td>{{$channel->description}}</td>
           </tr>
           <tr>
               <th>category</th>
               <td>{{$channel->category}}</td>
           </tr>
           <tr>
               <th>language</th>
               <td>{{$channel->language}}</td>
           </tr>
           <tr>
               <th>url_token_key</th>
               <td>{{$channel->url_token_key}}</td>
           </tr>
           <tr>
               <th>valid_time</th>
               <td>{{$channel->valid_time}}</td>
           </tr>

       </thead>
   </table>
</div>