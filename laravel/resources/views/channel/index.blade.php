@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<a href="{{url("broadcaster/app/$id/services/channels/create")}}" class="btn btn-danger">Add New Channel</a>

<div class="portlet box blue-madison">
 <div class="portlet-title">
  <div class="caption">
   <i class="fa fa-globe"></i>List Of Channel
 </div>
 <div class="tools">
   <a href="javascript:;" class="reload">
   </a>
   <a href="javascript:;" class="remove">
   </a>
 </div>
</div>
<div class="portlet-body">
  <table class="table table-striped table-bordered table-hover" id="sample_3">
    <thead>
      <tr>
       <th>Name</th>
       <th>Description</th>
       <th>Country</th>
       <th>Action</th>
     </tr>
   </thead>
   <tbody>

    @foreach($channels as $channel)
    <tr>
      
      <td>{{$channel->name}}</td>
      <td>{{$channel->description}}</td>
      <td>{{$channel->country}}</td>
      <td>
       <span type="button" data-href="{{url("broadcaster/app/$id/services/channels/$channel->app_channel_id")}}"
         data-toggle="modal" data-target="#myModal" class="btn btn-success btn-view"><i class="fa fa-book"></i></span>

         <a href="{{url("broadcaster/app/$id/services/channels/$channel->app_channel_id/edit")}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
         <a data-href="{{ url('broadcaster/app/'.$id.'/services/channels/delete/'.$channel->app_channel_id) }}" data-toggle="confirmation" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
         <a href="{{ url('broadcaster/app/'.$id.'/services/channel/'.$channel->app_channel_id.'/epg') }}"  class="btn btn-danger"><i class="fa">epg</i></a>
         <a href="{{ url('channels/'.$channel->app_channel_id.'/stream')}}"></a>
         <span type="button" data-href="{{ url('channels/'.$channel->app_channel_id.'/live')}}"
           data-toggle="modal" data-target="#myModal" class="btn btn-success btn-stream">live stream</span>
         </td>
       </tr>
       @endforeach
     </tbody>
   </table>
 </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->


</div>
</div>


<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of Channel</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>




    @stop
    @section('postFoot')


    <script>
      (function($){
        $('.btn-view').on('click',function(){
          $('.modal-body').load($(this).data('href'));
        });
        $('.btn-stream').on('click',function(){
          $('.modal-body').load($(this).data('href'));
          $('#myModalLabel').html("Live stream");
        });
      })(jQuery);
    </script>
    @stop
