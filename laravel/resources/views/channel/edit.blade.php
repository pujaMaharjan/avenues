@extends('apps.master')
@section('content')
<style>
  .hls,.rmtp,.satellite,.youtube{
    display: none;
  }
</style>

{!! Form::model($channel, array('route' => array('broadcaster.app.{app_id}.services.channels.update',$id, $channel->app_channel_id),'files'=>true,'method'=>'patch')) !!}


<h3>Add New Channel</h3>
<div class="form-group">
  <label class="control-label">Name*</label>
  <input type="text" name="name" value="{{ $channel->name }}" class="form-control" placeholder="name" />
</div>
<div class="form-group">
  <label class="control-label">Channel Play Back Stream Url</label>
  <select id="select_play_back_url" name="play_back_url_type" class="form-control">
    <option value="">--Choose Play Back Url--</option>  
    <option value="hls">Hls</option>
    <option value="rmtp">Rmtp</option>
    <option value="youtube">Youtube</option>
    <option value="satellite">Satellite</option>
  </select>
</div>

<div class="hls">
  <h3> HLS Playback is Available</h3>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Url Token Key</label>
    <input @if(Auth::user()->type=='broadcaster') readonly @endif class="form-control placeholder-no-fix" value="{{ $channel->url_token_key }}" type="text" placeholder="" name="url_token_key">
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Local Url</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->sources_local_url }}" type="text" placeholder="" name="sources_local_url">
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Cdn Url</label>
    <input class="form-control placeholder-no-fix" value="{{ $channel->sources_cdn_url }}" type="text" placeholder="" name="sources_cdn_url">
  </div>
</div>

<div class="rmtp">

  <h3>RTMP Origin Feed is Available</h3>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Rmtp Url</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->rmtp_url }}" type="text" placeholder="" name="rmtp_url">
  </div>

</div>

<div class="youtube">
  <h3>Youtube Feed is Available </h3>

  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Youtube</label>
    <input class="form-control placeholder-no-fix" data-required="1" value="{{ $channel->youtube_url }}" value="{{ old('youtube_url') }}" type="text" placeholder="" name="youtube_url">
  </div>

</div>

<div class="satellite">
  <h3>Request for Channel Downlink from Satellite</h3>

  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Satellite Name</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->sat_name }}" type="text" placeholder="" name="sat_name">
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Satellite Frequency</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->frequency }}" type="text" placeholder="" name="frequency">
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Satellite Polarization</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->polarization }}" type="text" placeholder="" name="polarization">
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">VPID</label>
    <input class="form-control placeholder-no-fix" data-required="1"  value="{{ $channel->pid_no }}" type="text" placeholder="" name="pid_no">
  </div>
  

</div>


<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Logo</label>
 <input class="form-control placeholder-no-fix" value="{{ old('logo') }}" type="file" placeholder="" name="logo">
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">description</label>
  <textarea name="description" class="form-control">{{ $channel->description }}</textarea>
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">Country</label>
  {!! Form::select('country_id',$countries,null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Language</label>
 <input class="form-control placeholder-no-fix" value="{{ $channel->language }}" type="text" placeholder="" name="language">
</div>
<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Channel Category</label>

 {!! Form::select('category', ['Music'=>'music','News'=>'news','Spiritual'=>'spiritual','Entertainment'=>'entertainment','Sports'=>'sports','Adventure'=>'adventure','Miscellenious'=>'miscellenious'], null, ['class' => 'form-control placeholder-no-fix']) !!}

</div>


<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">Valid Time</label>
  <input class="form-control placeholder-no-fix" value="{{ $channel->valid_time }}" type="text" placeholder="" name="valid_time">
</div>

<div class="form-actions">
  <a href="{{url('/')}}" class="btn btn-default">Back</a>
  <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>

<h2>Request For Portal Encoder</h2>
<button class="btn btn-success">Buy Now</button>

@stop
@section('postFoot')
<script>
  (function($){
    $('.btn-view').on('click',function(){
      $('.modal-body').load($(this).data('href'));
    });

    $('#select_play_back_url').on('change',function(){
      $va = $(this).val();
      if($va=='hls'){
        $('.hls').css('display','block')
        $('.rmtp').css('display','none')
        $('.youtube').css('display','none')
        $('.satellite').css('display','none')
      }
      if($va=='rmtp'){
        $('.hls').css('display','none')
        $('.rmtp').css('display','block')
        $('.youtube').css('display','none')
        $('.satellite').css('display','none')
      }
      if($va=='youtube'){
        $('.hls').css('display','none')
        $('.rmtp').css('display','none')
        $('.youtube').css('display','block')
        $('.satellite').css('display','none')
      }
      if($va=='satellite'){
        $('.hls').css('display','none')
        $('.rmtp').css('display','none')
        $('.youtube').css('display','none')
        $('.satellite').css('display','block')
      }
    });

  })(jQuery);
</script>
@stop
