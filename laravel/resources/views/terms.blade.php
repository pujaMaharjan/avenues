<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One|Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/front/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/responsive.css') }}">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <title>CastNow</title>
</head>
<body>

<div class="cn-header">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="images/logo.png" alt="Castnow">
                </a>
            </div>
            <div id="navbar1" class="navbar-collapse collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="{{url('login')}}">LOG IN</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>
</div>
<div style="margin:10%">
    <div style="">
          <b>  Introduction:</b> <br/>
        Castnow respects your privacy. This Privacy Statement applies to all of our web sites and our other
        products and services.This Privacy Statement explains what information we collect through the
        Castnow product, how we use that information, and what choices you have.
The Castnow product contains links to other web sites. This Privacy Statement does not
        apply to the practices of any company or individual that Castnow does not control, or any web
        sites or services that you link to from the Castnow products. You should use caution and review the
        privacy policies of any web sites or services that you visit from ours to learn more about their information
        practices. Please take a few moments to read this Privacy Statement. By accessing the Castnow product,
        you agree to accept the terms and conditions of this Privacy Statement and are aware that our policies may
        evolve in the future as indicated below. In the event of a conflict between this Privacy Statement and our
        Terms of Use, our Terms of Use will control.
<br/><br/>
        <b>What information we collect:</b><br/>
In various places in connection with Castnow product, we collect Personally Identifiable
        Information such as your name, Address, telephone number, email address, or other
        information that can be used to identify you or contact you. For example, we collect
        this information when you sign up for our email newsletter, purchase, trial, contact
        us or register for a Castnow Account. From time to time in the future we may offer
        additional features, services and promotional opportunities that would require you to
        submit Personally Identifiable Information to participate.
We also collect Anonymous Information such as your IP address, your
        ZIP code, your Media Access Control (MAC) address, your Castnow ID,
        the name you assigned to your Castnow, the hardware and firmware version of
        your Set Top Box, the type of browser you use, the pages on our websites that you
        visit, the identity of your broadcast television provider, the search terms that
        you enter, the advertisements that you click and the number and frequency of users
        of Castnow. Anonymous Information will not be associated with Personally Identifiable
        Information.
Like many Internet sites, we also use cookies to collect information
        when you visit our website. A cookie is a small data file that we transfer
        to your computer and is stored on your hard drive. When you visit our
        websites the cookie identifies your computer to us so that you do not have
        to re-register each time you visit. We also use cookies to measure the traffic
        to our site and its different services and features, and other miscellaneous uses.
        We do not intend that children will use the Castnow product. We do not solicit or knowingly
        collect information from children under 14 years old, nor will we knowingly accept registration
        from them.
<br/><br/>
        <b>How we use the information we collect:</b><br/>
We use Personally Identifiable Information that you provide us for such purposes
        as responding to your requests for information, products or services, customizing
        the advertising and content you see, and communicating with you about our offerings.
        For example, if you sign up for our email newsletter, we will send the newsletter to the
        email address you provided. If you so choose, we will also use Personally Identifiable
        Information to send you information about products and services that may be of interest
        to you. Once your purchase our package, you will be registered in our newsletter database
        by default so you may get promotional offerings and information everytime. Later on in this
        Privacy Statement, we provide you with more information on what choices you have. We use
        Anonymous Information to determine how people use the Castnow product. For example this data
        tells us information including how often users click on our advertisements, which files are
        downloaded most frequently, and which components or areas of Castnow product/content are the
        most popular. Analyzing this data allows us to improve the offerings on and performance of Castnow.
<br/><br/>
<b>How we share information:<b/><br>
We sometimes use third parties to help with some aspects of the Castnow, such as delivering content,
        processing payments and providing marketing assistance. When we supply Personally Identifiable
        Information to these third parties, we require them to use it only for the function they are
        helping us with. We aggregate the Anonymous Information that we collect and exchange it with
        advertisers and other partners who are interested in the use and performance of the Castnow product.
        We share this type of data so that our partners also may provide you with an optimal user experience
        and better understand how users access the Castnow product.
We will disclose Personally Identifiable Information as required by law, or if in our judgement
        it is necessary to protect Sling or our employees or users from harm, loss or liability.
        If we merge with or are acquired by another company or we sell substantially all of our assets
        , the acquirer or resulting company will receive and may continue to use the information you have
        provided as described in this policy. In the unlikely event we cease operation or enter bankruptcy,
        your information may be transferred to and used by another company that acquires our assets or offers
        similar or related products or services. You acknowledge that such transfers may occur, and that any
        such acquirer or successor may continue to use your information as set forth in this policy.
<br/><br/>
    <b>
Personally Identifiable Information you can access:
    </b><br/>
        We allow you to access the following information about you for the purpose of viewing, and
        in certain situations, updating that information. This list will change as Castnow
        product change. You may currently access the following information: user profile, and user preferences.
<br/><br/>
    <b>
What choices you have:</b>
    <br/>
When you register for the Castnow product, you have the option of checking a
        box to tell us that you do not want us to send you any information about
        our products and services, or that you do not want to receive information from
        our partners and others about products and services that might interest you. We
        will keep track of your decision in our database. Even if you choose not to receive
        information from us, we reserve the right to communicate with you on matters we consider
        especially important. Further, please note that if you do not want to receive legal
        notices from us, such as this Privacy Statement, those legal notices will still govern
        your use of the Castnow product, and you are responsible for reviewing such legal notices for
        changes. You are able to add or update certain information on pages, such as those listed in
        the Personally Identifiable Information you can access section above. When you update information,
        however, we often maintain a copy of the unrevised information in our records.
<br/><br/>
    <b>
You delete your account by calling Castnow customer support line. Please note that some information
        may remain in our records after deletion of your account.
Postings:</b> <br/>
If you use our bulletin boards, post user comments regarding content available through the
        Castnow, or engage in similar activity, please remember that anything you post is publicly
        available. Portions of your user profile will also be available to other users of our websites.
    <br/><br/>
    <b>
        Security: </b><br/>
During sessions in which you give us information such as credit card numbers, our third party payment
        system like Paypal is encrypt your transmissions using SSL (Secure Sockets Layer), and other
        security technology. This guards against interception of the information while it is on the
        Internet. They keep the Personally Identifiable Information you provide on servers that are
        protected by firewalls and other technological means against intrusion or unauthorized access. They are
        located in a physically secure facility, however, there is no such thing as perfect security on the
        Internet. And any third party involvement such as Payment Solution, Castnow has no liability for the
        data such on the credit card number or bank account that you will disclose to the third party. We
        rely on you to select passwords that cannot be guessed easily and to safeguard those passwords from disclosure.
        Please contact us if you have any information regarding unauthorized use of the Castnow.
    <br/><br/>
    <b>
Changes to this Privacy Statement:
    </b><br/>
We may make changes to this Privacy Statement from time to time for any reason. Use of information we
        collect now is subject to the Privacy Statement in effect at the time such information is used
        (though you always have the option to delete your account as described above). If we make changes
        in the way we use Personally Identifiable Information, we will notify you via e-mail or by posting an
        announcement on our website.
</div>
</div>
</body>
</html>