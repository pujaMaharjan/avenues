@extends('admin.master')
@section('content')

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Notification</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Push Notification</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				@include('admin.partials._success')
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Notification
						</div>
					</div>
					<div class="portlet-body form">
						<form action="{{ route('adminPushPost')}}" method="POST">
							<div class="form-body">


								<div class="form-group">
									<label>Select App</label>
									<div class="input-icon input-icon-sm">
										<div class="input-icon right">
											<i class="fa"></i>

											<select id="appId" class="select2 select2_sample_modal_2" multiple="multiple" name="appId[]" style="width:200px;">
												<optgroup id="all" label="All">
													<option value="0">All App </option>
												</optgroup>
												<optgroup id="one-by-one" label="Apps">
													@foreach($apps as $app)
													<option value="{{$app->app_id}}">{{ $app->app_name}}</option>
													@endforeach 
												</optgroup>

											</select>


										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Notification Message</label>
									<div class="input-icon input-icon-lg">

										<textarea rows="5" cols="23" name="notificationMessage"></textarea>
									</div>
								</div>
							</div>

							<div class="form-actions">
								<button type="submit" class="btn blue">Submit</button>
								<a href=""><button type="button" class="btn default">Cancel</button></a>
							</div>


						</form>
						@stop

						@section('postFoot')
						<script>

							(function($){

								$('#appId').on('change', function(){
									var appId = $('#appId').val();
									if(jQuery.inArray('0',appId) !== -1)
									{
										$("optgroup#one-by-one option").addClass('hide');
								//$('optgroup#one-by-one option').attr('disabled','disabled');
							}
							else if(appId == null)
							{
								$("optgroup#one-by-one option").removeClass('hide');
								$("optgroup#all option").removeClass('hide');
							}
							else
							{
								$("optgroup#all option").addClass('hide');
								//console.log('else')
							}
							// $("#appId option:selected").attr('disabled','disabled')
							// .siblings().removeAttr('disabled');
							console.log(appId);
						});
					// $('select').on('change', function() {
					// 	console.log(this.value);
					// 	if (this.value == '0') {
					// 		$('optgroup option').prop('disabled', true);
					// 	} else {
					// 		$('optgroup option').prop('disabled', false);
					// 	}
					// });
						})(jQuery);
					</script>
					@stop