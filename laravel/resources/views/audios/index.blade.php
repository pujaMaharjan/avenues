@extends('apps.master')
@section('content')

<a href="{{url("broadcaster/app/$app_id/services/audios/create")}}" class="btn btn-danger">Add New Audio</a>

	<div class="portlet box blue-madison">
		 <div class="portlet-title">
		  <div class="caption">
		   <i class="fa fa-globe"></i>List Of Audios
		 </div>
		 <div class="tools">
		   <a href="javascript:;" class="reload">
		   </a>
		   <a href="javascript:;" class="remove">
		   </a>
		 </div>
		</div>
		<div class="portlet-body">
			<table class="table table-bordered table-responsive">
				<tr>
					<td>Name</td>
					<td>Image</td>
					<td>Action</td>
				</tr>
				@foreach($audios as $audio)
				<tr>
					<td>{{$audio->title}}</td>
					<td>{{$audio->image}}</td>
					<td>
						<a href="{{url('broadcaster/app/'.$app_id.'/services/audios/'.$audio->id.'/edit')}}" class="btn btn-success">Edit</a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
@stop