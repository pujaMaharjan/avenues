@extends('apps.master')
@section('postHead')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')



@if(isset($audio))
{!! Form::model($audio, array('url' => array("broadcaster/app/".$app_id."/services/audios/$id"),'files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate')) !!}
<input type="hidden" name="_method" value="PATCH">
@else
{!! Form::open(array('url' => 'broadcaster/app/'.$app_id.'/services/audios','files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate','files'=>true)) !!}
@endif

<div ng-app="broadcasterApp" ng-controller="audioCtrl">
  <h3>Add New Audio</h3>
  <div class="form-group" >
    <label class="control-label">Title</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('title', null,['class'=>'form-control','placeholder'=>'name']); !!}
    </div>
  </div>





<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">audio Photo </label>
 @if(isset($audio))
 <div class="row">
  <div class="col-md-4">
    <img src="{{ asset('uploads/audios/'.$audio->image) }}" width="100" height="150" alt="">
  </div>
</div>
@endif
{!! Form::file('image', null,['class'=>'form-control','placeholder'=>'logo']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">Stream Url</label>

  {!! Form::text('live_stream', null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">description</label>

  {!! Form::textarea('description', null,['class'=>'form-control']); !!}
</div>


<div class="form-actions">
  <a href="{{url('/')}}" class="btn btn-default">Back</a>
  <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>
</div>
@stop
