@if(\Session::has('message'))
<p class="alert alert-success">{{ \Session::get('message') }}</p>
@endif
@if($errors->any())
<ul class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	@foreach($errors->all() as $error)
	<li>{{$error}}</li>
	@endforeach

</ul>
@endif