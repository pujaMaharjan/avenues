@extends('apps.master')
@section('content')

<form action="{{url('broadcaster/app/'.$app_id.'/services/news/'.$news_id.'/newsapi')}}" enctype="multipart/form-data" method="post">
	{!! csrf_field() !!}


	<h3>Add New News External Api</h3>
	<!-- <div class="form-group">
		<label class="control-label">External News Api*</label>
		<textarea name="app_external_news" class="form-control" rows="10"></textarea>
	</div>
	<div class="form-actions">
		<a href="{{url('/')}}" class="btn btn-default">Back</a>
		<button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
	</div> -->
	<span class="btn btn-success add_btn">Add</span>
	<table class="table table-bordered">
		<tr>
			<th>Key</th>
			<th>Value</th>
			<th>Action</th>
		</tr>
		<tr>
			<td><input type="text" name="app_external_news[key][]"  class="form-control" /></td>
			<td><input type="text" name="app_external_news[value][]"  class="form-control" /></td>
			<td><span class="btn btn-danger remove-btn">&times</span></td>
		</tr>
	</table>
	<!-- <input type="submit" name="save" value="Save" class="btn btn-primary btn-lg" /> -->
	<button class="btn btn-primary">Save</button>
</form>

@stop
@section('postFoot')


<script>
	(function($){
		function addRow(){
			var new_row = '<tr>';
			new_row += '<td><input type="text" name="app_external_news[key][]" class="form-control" /></td>';
			new_row += '<td><input type="text" name="app_external_news[value][]"  class="form-control" /></td>';
			new_row += '<td><span class="btn btn-danger remove-btn">&times</span></td>';
			new_row += '</tr>';
			return new_row;
		}
		$('.add_btn').on('click',function(){
			
			$(this).siblings('table').children('tbody').append(addRow());
		});
		$(document).on('click','table .remove-btn',function(){
			$(this).closest('tr').remove();
		});


	})(jQuery);
</script>
@stop
