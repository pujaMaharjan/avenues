@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->
@include('admin.partials._success')

<a href='{{ url("broadcaster/app/$app_id/services/news/$news_id/newsapi/create") }}' class="btn btn-danger">Add New News Api</a>

<div class="portlet box blue-madison">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-globe"></i>List Of News Api
    </div>
    <div class="tools">
      <a href="javascript:;" class="reload">
      </a>
      <a href="javascript:;" class="remove">
      </a>
    </div>
  </div>
  <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_3">
      <thead>
        <tr>
          <th>News api</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

        @foreach($newsApis as $newsApi)

        <tr> 

          <td>  
            <dl class="dl-horizontal">
              @for($i=0; $i<count($newsApi->app_external_news['key']); $i++)
              <dt style="text-align:left;">{{ $newsApi->app_external_news['key'][$i] }}</dt>
              <dd>{{ $newsApi->app_external_news['value'][$i] }}</dd>
              @endfor
            </dl>
          </td>
          <td>
            <a href="{{ url('broadcaster/app/'.$app_id.'/services/news/'.$news_id.'/newsapi/'.$newsApi->app_external_api_id.'/edit') }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
            <a data-href="{{ url('broadcaster/app/'.$app_id.'/services/news/'.$news_id.'/newsapi/'.$newsApi->news_api_id.'/delete')  }}" data-toggle="confirmation" data-placement="left" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->


</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of News Api</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>




    @stop
    @section('postFoot')


    <script>
      (function($){
        $('.btn-view').on('click',function(){
          $('.modal-body').load($(this).data('href'));
        });

      })(jQuery);
    </script>
    @stop
