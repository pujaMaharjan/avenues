@extends('apps.master')
@section('content')



<form action="{{url('broadcaster/app/'.$app_id.'/services/news/'.$news_id.'/newsapi/'.$newsApi->app_external_api_id)}}" enctype="multipart/form-data" method="post">
{!! csrf_field() !!}

<input type="hidden" name="_method" value="PATCH">
<h3>Edit External News Api</h3>


<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <span class="btn btn-success add_btn">Add</span>
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th>Key</th>
        <th>Value</th>
        <th>Action</th>
      </tr>
      @for($i=0; $i<count($newsApi->app_external_news['key']); $i++)

      <tr> 
        <td><input type="text" name="app_external_news[key][]" value="{{ $newsApi->app_external_news['key'][$i] }}"  class="form-control" /></td>

        <td><input type="text" name="app_external_news[value][]" value="{{ $newsApi->app_external_news['value'][$i] }}"  class="form-control" /></td>

        <td><span class="btn btn-danger remove-btn">&times</span></td>
      </tr>
      @endfor
    </tbody>
  </table>


  <div class="form-actions">
    <a href="" class="btn btn-default">Back</a>
    <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
  </div>
</form>

@stop

@section('postFoot')


<script>
  (function($){
    function addRow(){
      var new_row = '<tr>';
      new_row += '<td><input type="text" name="app_external_news[key][]" class="form-control" /></td>';
      new_row += '<td><input type="text" name="app_external_news[value][]"  class="form-control" /></td>';
      new_row += '<td><span class="btn btn-danger remove-btn">&times</span></td>';
      new_row += '</tr>';
      return new_row;
    }
    $('.add_btn').on('click',function(){

      $(this).siblings('table').children('tbody').append(addRow());
    });
    $(document).on('click','table .remove-btn',function(){
      $(this).closest('tr').remove();
    });


  })(jQuery);
</script>
@stop