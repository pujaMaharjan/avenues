@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

       <!-- BEGIN EXAMPLE TABLE PORTLET-->

                <div class="portlet box blue-madison">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="fa fa-globe"></i>List Of Epg
                    </div>
                    <div class="tools">
                      <a href="javascript:;" class="reload">
                      </a>
                      <a href="javascript:;" class="remove">
                      </a>
                    </div>
                  </div>
                  <div class="portlet-body">



                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="" method="post" action="{{ url("broadcaster/app/$app_id/services/channel/$channel_id/epg/store") }}" enctype="multipart/form-data">
                                            <lable class="form-label"><strong>Import File</strong></lable>
                                            <input type="file" name="schedule" class="form-control" />
                                            <input type="text" name="url" class="form-control" />
                                            <input type="submit" name="submit" class="btn btn-primary" value="Save" />
                                        </form>
                                    </div>
                                </div>

                                 <div class="row">
                                     <div class="col-md-12">
                                      @if(count($epgs))
                                            <table class="table table-bordered table-hover">
                                                   <tr>
                                                    <th>Day</th>
                                                    <th>Program Name</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                   </tr>
                                                   @if(count($epgs))
                                                   @foreach($epgs as $epg)
                                                   <tr>
                                                        <td>{{$epg->day}}</td>
                                                        <td>{{$epg->program_name}}</td>
                                                        <td>{{$epg->start_time}}</td>
                                                        <td>{{$epg->end_time}}</td>
                                                   </tr>
                                                   @endforeach
                                                   @endif
                                            </table>
                                      @endif
                                    </div>
                                </div>


                                <div class="row">
                                <div class="col-md-12">
                                <!--<form class="" method="post" action="{{ url("broadcaster/app/$app_id/services/channel/$channel_id/epg/store") }}">
                                <h4>Sunday</h4>
                                 <span class="add_btn btn btn-success" data-day="1">Add</span>
                                <table class="table table-bordered">
                                  <thead>
                                        <tr>
                                            <th>Programs</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th>Action</th>
                                        </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                         <td><input type="text" name="schedule[1][name][]" class="form-control"  /></td>
                                         <td><input type="text" name="schedule[1][start_time][]" class="form-control"  /></td>
                                         <td> <input type="text" name="schedule[1][end_time][]" class="form-control"  /></td>
                                         <td><span class="btn btn-danger remove-btn">&times</span></td>
                                    </tr>
                                  </tbody>
                                </table>



                                </div>

                                 <div class="col-md-12">
                                    <h4>Monday</h4>
                                     <span class="add_btn btn btn-success" data-day="2">Add</span>
                                    <table class="table table-bordered">
                                    <thead>
                                          <tr>
                                              <th>Programs</th>
                                              <th>Start Time</th>
                                              <th>End Time</th>
                                              <th>Action</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                           <td><input type="text" name="schedule[1][name][]" class="form-control"  /></td>
                                           <td><input type="text" name="schedule[1][start_time][]" class="form-control"  /></td>
                                           <td> <input type="text" name="schedule[1][end_time][]" class="form-control"  /></td>
                                           <td><span class="btn btn-danger remove-btn">&times</span></td>
                                      </tr>
                                    </tbody>

                                    </table>



                                    </div>
                                <div class="col-md-6">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Save" />
                                </div>
                                 </form>
                                </div>-->


                  </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of Channel</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>


</div>
@stop
@section('postFoot')


    <script>
        (function($){
        function addRow(i){
            var new_row = '<tr>';
            new_row += '<td><input type="text" name="schedule['+i+'][name][]" class="form-control"  /></td>';
            new_row += '<td><input type="text" name="schedule['+i+'][start_time][]" class="form-control"  /></td>';
            new_row += '<td><input type="text" name="schedule['+i+'][end_time][]" class="form-control"  /></td>';
            new_row += '<td><span class="btn btn-danger remove-btn">&times</span></td>';
             new_row += '</tr>';
             return new_row;
        }
            $('.add_btn').on('click',function(){
            var $day = $(this).data('day');
              $(this).siblings('table').children('tbody').append(addRow($day));
            });
            $(document).on('click','table .remove-btn',function(){
               console.log($(this).closest('tr').fadeOut(1000));
            });

        })(jQuery);
    </script>
@stop
