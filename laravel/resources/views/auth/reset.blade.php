@extends('admin.login')
@section('content')
<form method="POST" action="{{url('password/reset')}}" class="login-form">
    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $token }}">
    <h3 class="form-title">Reset Password</h3>
    <div class="form-group">
     <label class="control-label visible-ie8 visible-ie9">Email</label>
     <input class="form-control form-control-solid placeholder-no-fix" type="email" name="email" value="{{ old('email') }}" placeholder="Email">
 </div>

   <div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Password</label>
    <input class="form-control form-control-solid placeholder-no-fix" type="password" name="password" placeholder="password">
</div class="form-group">

<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
    <input class="form-control form-control-solid placeholder-no-fix" type="password" name="password_confirmation" placeholder="retype password">
</div>

<div class="form-group">
    <button type="submit" class="btn btn-success uppercase">
        Reset Password
    </button>
</div>
</form>
@stop