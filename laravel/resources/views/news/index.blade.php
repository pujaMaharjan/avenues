@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

       <!-- BEGIN EXAMPLE TABLE PORTLET-->

       <a href="{{url("broadcaster/app/$id/services/news/create")}}" class="btn btn-danger">Add New News</a>

       					<div class="portlet box blue-madison">
       						<div class="portlet-title">
       							<div class="caption">
       								<i class="fa fa-globe"></i>List Of News
       							</div>
       							<div class="tools">
       								<a href="javascript:;" class="reload">
       								</a>
       								<a href="javascript:;" class="remove">
       								</a>
       							</div>
       						</div>
       						<div class="portlet-body">
       							<table class="table table-striped table-bordered table-hover" id="sample_3">
       							<thead>
       							<tr>
                      <th>Name</th>
       								<th>Logo</th>

       								<th>Action</th>
       							</tr>
       							</thead>
       							<tbody>

                            @foreach($news as $new)
       							<tr>
       								<td>{{$new->name}}</td>
                      <td><img class="img-responsive" src="{{asset('uploads/'.$new->logo)}}" alt=""></td>
       								<td>app/{app_id}/services/news
       								  <a href="{{url("broadcaster/app/$id/services/news/$new->app_news_id/newsapi")}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
       								   <a href="{{url("broadcaster/app/$id/services/news/$new->app_news_id/edit/")}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                         <a href="{{ url('broadcaster/app/'.$id.'/services/news/'.$new->app_news_id.'/delete') }}" class="btn btn-primary"><i class="fa fa-trash"></i></a>
                                        <a data-href="{{ url('broadcaster/app/'.$id.'/services/news/'.$new->app_news_id.'/delete') }}" data-toggle="confirmation" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
       								</td>
       							</tr>
                            @endforeach
       							</tbody>
       							</table>
       						</div>
       					</div>
       					<!-- END EXAMPLE TABLE PORTLET-->


    </div>
</div>
app/{app_id}/services/news/{news_id}/delet

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of Channel</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>




@stop
@section('postFoot')


    <script>
        (function($){
            $('.btn-view').on('click',function(){
                $('.modal-body').load($(this).data('href'));
            });

        })(jQuery);
    </script>
@stop
