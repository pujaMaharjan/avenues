@extends('apps.master')
@section('content')
 <h3>Add New Channel</h3>
    <form action='{{ url("broadcaster/app/$id/services/news") }}'  method="post" enctype="multipart/form-data" >
    {!! csrf_field() !!}


     
            <div class="form-group">
                <label class="control-label">Name*</label>
                <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="name" />
            </div>
            <div class="form-group">
                <label class="control-label">Link</label>
                <input type="text" name="web_link" value="{{ old('web_link') }}" class="form-control" placeholder="name" />
            </div>
             <div class="form-group">
                <label class="control-label">Logo</label>
                <input type="file" name="logo"  class="form-control" placeholder="name" />
            </div>


        <div class="form-actions">
            <a href="{{url('/')}}" class="btn btn-default">Back</a>
            <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>

@stop
