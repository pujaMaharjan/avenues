@extends('apps.master')
@section('content')
broadcaster/app/$id/services/news
    <form class="login-form" action="{{ url("broadcaster/app/$id/services/news/$news_id") }}" enctype="multipart/form-data" method="post"  class="register-form" novalidate="novalidate">
  
    {!! csrf_field() !!}

<input type="hidden" name="_method" value="PATCH">
      <h3>Edit Channel</h3>
            <div class="form-group">
                <label class="control-label">Name</label>

            
                <input type="text" name="name" class="form-control" value="{{$news->name}}">
            </div>
             <div class="form-group">
                <label class="control-label">Link</label>
                <input type="text"  value="{{$news->web_link}}" name="web_link"  class="form-control" placeholder="name" />
            </div>
             <div class="form-group">
                <label class="control-label">Logo</label>
                <input type="file" name="logo"  class="form-control" placeholder="name" />
            </div>

        <div class="form-actions">
            <a href="{{url('/')}}" class="btn btn-default">Back</a>
            <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>

@stop