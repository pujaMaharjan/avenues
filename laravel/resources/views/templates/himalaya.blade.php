<!DOCTYPE html>
<html ng-app="app">

<head>
    <title>Himalaya TV App Template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('himal-new/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('himal-new/css/slick.css')}}">
     <link rel="stylesheet" type="text/css" href="{{asset('himal-new/vendor/slick/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{asset('himal-new/vendor/slick/slick-theme.css') }}" />
<style>
.primary_bg_color{
  background:  {{ $_GET['p_color'] }} !important;
}
.primary_text_color{
  color: {{ $_GET['p_color'] }} !important;
}
.primary_tab_border{
  border-bottom-color: {{ $_GET['p_color'] }} !important;
}


.secondary_bg_color{
  background:{{ $_GET['s_color'] }} !important;
}
.secondary_text_color{
  color:{{ $_GET['s_color'] }}  !important;
}
.secondary_tab_border{
  border-bottom-color:{{ $_GET['s_color'] }} !important;
}


.tertiary_bg_color{
  background: {{ $_GET['t_color'] }} !important;
}
.tertiary_text_color{
  color: {{ $_GET['t_color'] }} !important;
}


.selected_bg_color{
  background: {{ $_GET['t_color'] }} !important;
}
.selected_text_color{
  color: {{ $_GET['t_color'] }} !important;
}
  
.primary_bg_text_color{
  color: {{ $_GET['t_color'] }};
}

</style>
</head>

<body ng-controller="SiteCtrl">
    <header class="primary_bg_color">
        <h1><img src="@{{app_info.app_logo}}" alt="" class="logo">HIMALAYA</h1>
        <img src="{{ asset('himal-new/img/fblike.png') }}" alt=" " class="fblike">
    </header>
    <div class="wrapper-main">
        <div class="wrapper">
            <div class="sub-header ">
                <div class="swipe-tabs">
                    <div class="swipe-tab main_tab">CHANNELS</div>
                    <div class="swipe-tab main_tab">VOD</div>
                    <div class="swipe-tab main_tab">RADIO</div>
                    <div class="swipe-tab main_tab">NEWS</div>
                    <div class="swipe-tab main_tab">PHOTO GALLERY</div>
                    <div class="swipe-tab main_tab">ABOUT US</div>
                </div>
            </div>
            <div class="main-container">
                <div class="swipe-tabs-container ">
                    <div class="swipe-tab-content">
                        <div class="video_player">
                            <img src="{{ asset('himal-new/img/video1.jpg') }}" alt="" class="width-100">
                        </div>
                        <ul class="epg primary_bg_color">
                            <li><h3 class="active secondary_text_color secondary_tab_border">MONDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">TUESDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">WEDNESDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">THURSDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">FRIDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">SATURDAY</h3></li>
                            <li><h3 class="primary_bg_text_color">SUNDAY</h3></li>
                        </ul>
                        <ul class="epg_list primary_bg_color">  
                            <li class=""><span class="time">6:00</span> <span class="show_name">'Movie Trailer'</span></li>
                            <li class=""><span class="time">7:00</span> <span class="show_name">''Pop Songs</span></li>
                            <li class="active secondary_bg_color"><span class="time">8:00</span> <span class="show_name">'Nrn Playlist</span></li>
                            <li class=""><span class="time">9:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">10:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">11:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">12:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">1:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">2:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">3:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">4:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">5:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">6:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">6:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">6:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
                            <li class=""><span class="time">6:00</span> <span class="show_name">Teej Bisesh Geet</span></li>
       
                    </div>
                    <div class="swipe-tab-content">
                        <div class="tab tab_2 " id="vod">
                            <div class="inner-tab-wrapper">
                                <ul class="vod_subcategory primary_bg_color">
                                    <li><h3 class="active secondary_text_color secondary_tab_border">RONAGONER</h3></li>
                                    <li><h3 class="primary_bg_text_color">NEWS INSIDE</h3></li>
                                    <li><h3 class="primary_bg_text_color">GALLERY</h3></li>
                                    <li><h3 class="primary_bg_text_color">BIZ TIME</h3></li>
                                    <li><h3 class="primary_bg_text_color">FRANKLY SPEAKING</h3></li>
                                    <li><h3 class="primary_bg_text_color">MARKET WATCH</h3></li>
                                    <li><h3 class="primary_bg_text_color">EL SOMOY L TALK SHOW </h3></li>
                                    <li><h3 class="primary_bg_text_color">NEWS CLIPS</h3></li>
                                </ul>
                            </div>
                            <div class="vod_listings">
                                <ul>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color ">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="popup"></div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="{{asset('himal-new/img/gallery/img2.jpg')}}" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img3.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img4.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img5.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img6.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img7.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img8.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img9.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <li class="vod_list">
                                        <div class="thumbnail">
                                            <img src="img/gallery/img4.jpg" alt="" class="width-100">
                                        </div>
                                        <div class="info primary_text_color">
                                            <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                            <span>2015-11-09</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="swipe-tab-content">
                        <div class="tab tab_3 set_height_1 " id="radio">
                            <h3 class="live"><div class="red_dot"></div> Live</h3>
                            <h1 class="text-center mg-0"><img src="{{ asset('himal-new/img/logo.png') }}" class="radio_logo" alt=""></h1>
                            <h3 class="text-center">Radio Awaz</h3>
                            <h3 class="text-center play_button"><img src="{{ asset('himal-new/img/play.png')}}" alt=""></h3>
                        </div>
                    </div>
                    <div class="swipe-tab-content">
                        <div class="tab tab_4 " id="news">
                            <div class="inner-tab-wrapper">
                                <ul class="vod_subcategory primary_bg_color">
                                    <li ng-repeat="c in cats"><h3 class="active secondary_text_color secondary_tab_border">@{{c.menu_title}}</h3></li>
                                </ul>
                            </div>
                            <ul class="news-lisitng">

                                <li ng-repeat="n in recent_news">
                                    <div class="box">
                                        <h5 class="primary_text_color" ng-bind="n.title"></h5>
                                        <span ng-bind="n.time"></span>
                                        <img src="@{{n.img}}" alt="" class="width-100">
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="swipe-tab-content">
                        <div class="tab tab_5" id="gallery">
                            <ul>
                                <li><img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{asset('himal-new/img/gallery/img2.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img9.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{asset('himal-new/img/gallery/img2.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img4.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img5.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img6.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img7.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img6.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img5.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img9.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{asset('himal-new/img/gallery/img2.jpg')}}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img6.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img5.jpg') }}" alt="" class="gallery_image"></li>
                                <li><img src="{{ asset('himal-new/img/gallery/img8.jpg') }}" alt="" class="gallery_image"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="swipe-tab-content about_us">
                        <p>Himalaya TV smart phone app enables users to watch the live streaming of the Himalaya Television in their smart devices connected to the internet anywhere in the globe . This app is not only limited to the live streaming video but also includes the VOD and the news from the Himalaya television . Himalaya TV app is a joint initiative of Himalaya Television and New IT Venture Corp. In the user 's quest , more features will be added in near future , helping viewers to stay close to Himalaya Television</p>
                        <p>Powered by</p>
                        <div class="text-center">
                            <img src="{{ asset('himal-new/img/nitv.png') }}" class="text-center" alt="">
                        </div>
                        <div class="text-center copyright primary_bg_color">
                            <span>Copyright &copy; 2015 NITV Media.</span>
                            <br>
                            <span>All Rights Reserved.</span>
                            <br>
                            <span class="link_about_color secondary_text_color">http://newitventure.com</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup news_popup" style="display:none;">
            <div class="single-news">
                <img src="img/news1.jpg" class="width-100" alt="">
                <i class="demo-icon icon-share secondary_bg_color"></i>
                <div class="news_box">
                    <h5 class="primary_text_color">à¤—à¥‹à¤°à¥à¤–à¤¾ à¤­à¤°à¥à¤¤à¥€ : à¤›à¤¨à¥‹à¤Ÿ à¤¹à¥à¤¨à¥‡ à¤®à¤¾à¤²à¤¾à¤®à¤¾à¤²- à¤¨à¤¹à¥à¤¨à¥‡ à¤¬à¥‡à¤•à¤¾à¤° (à¤­à¤¿à¤¡à¤¿à¤¯à¥‹)</h5>
                    <span>2017-03-21</span>
                    <p>à¤¨à¤¿à¤œà¥€ à¤­à¥à¤°à¤®à¤£à¤•à¤¾ à¤•à¥à¤°à¤®à¤®à¤¾ à¤ªà¥‹à¤–à¤°à¤¾à¤®à¤¾ à¤°à¤¹à¤¨à¥à¤­à¤à¤•à¤¾ à¤ªà¥‚à¤°à¥à¤µ à¤ªà¥à¤°à¤§à¤¾à¤¨à¤®à¤¨à¥à¤¤à¥à¤°à¥€ à¤¹à¤¾à¤¤à¥‹à¤¯à¤¾à¤®à¤¾à¤¸à¤‚à¤— à¤¨à¥‡à¤ªà¤¾à¤² à¤ªà¤¤à¥à¤°à¤•à¤¾à¤° à¤®à¤¹à¤¾à¤¸à¤‚à¤˜ à¤•à¤¾à¤¸à¥à¤•à¥€à¤²à¥‡ à¤®à¤‚à¤—à¤²à¤¬à¤¾à¤° à¤ªà¥‹à¤–à¤°à¤¾à¤®à¤¾ à¤†à¤¯à¥‹à¤œà¤¨à¤¾ à¤—à¤°à¥‡à¤•à¥‹ à¤ªà¤¤à¥à¤°à¤•à¤¾à¤° à¤¸à¤®à¥à¤®à¥‡à¤²à¤¨à¤®à¤¾ à¤¸à¥‹ à¤•à¥à¤°à¤¾ à¤¬à¤¤à¤¾à¤‰à¤¨à¥à¤­à¤à¤•à¥‹ à¤¹à¥‹ à¥¤</p>
                    <p>à¤¤à¥à¤¯à¤¸ à¤•à¥à¤°à¤®à¤®à¤¾ à¤‰à¤¹à¤¾à¤à¤²à¥‡ à¤œà¤¾à¤ªà¤¾à¤¨à¥€ à¤²à¤—à¤¾à¤¨à¥€ à¤¨à¥‡à¤ªà¤¾à¤²à¤®à¤¾ à¤¹à¥à¤¨ à¤¨à¤¸à¤•à¥à¤¨à¥à¤•à¥‹ à¤•à¤¾à¤°à¤£ à¤¦à¥‹à¤¹à¥‹à¤°à¥‹ à¤•à¤° à¤ªà¥à¤°à¤£à¤¾à¤²à¤ªà¥ à¤°à¤¹à¥‡à¤•à¥‹ à¤¬à¤¤à¤¾à¤‰à¤à¤¦à¥ˆ à¤¯à¥‹ à¤¹à¤Ÿà¥à¤¨à¤¾à¤¸à¤¾à¤¥ à¤²à¤—à¤¾à¤¨à¥€ à¤¬à¤¢à¥à¤¨à¥‡ à¤° à¤†à¤«à¥‚à¤²à¥‡ à¤®à¤‚à¤—à¤²à¤¬à¤¾à¤° à¤•à¤¾à¤ à¤®à¤¾à¤¡à¥Œà¤‚à¤®à¤¾ à¤ªà¥à¤°à¤§à¤¾à¤¨à¤®à¤¨à¥à¤¤à¥à¤°à¥€ à¤ªà¥à¤°à¤šà¤£à¥à¤¡à¤¸à¤‚à¤—à¤•à¥‹ à¤­à¥‡à¤Ÿà¤®à¤¾ à¤¯à¥‹ à¤•à¥à¤°à¤¾ à¤‰à¤ à¤¾à¤‰à¤¨à¥‡ à¤¬à¤¤à¤¾à¤‰à¤¨à¥à¤­à¤¯à¥‹ à¥¤</p>
                    <p>à¤‰à¤¹à¤¾à¤à¤²à¥‡ à¤œà¤¾à¤ªà¤¾à¤¨à¤²à¥‡ à¤§à¥‡à¤°à¥ˆ à¤¦à¥‡à¤¶à¤¸à¤‚à¤— à¤¦à¥‹à¤¹à¥‹à¤°à¥‹ à¤•à¤° à¤¹à¤Ÿà¤¾à¤‰à¤¨à¥‡ à¤¸à¤®à¥à¤¬à¤¨à¥à¤§à¤®à¤¾ à¤¸à¤®à¥à¤à¥Œà¤¤à¤¾ à¤—à¤°à¤¿à¤¸à¥‡à¤•à¥‡à¤•à¥‹ à¤° à¤¨à¥‡à¤ªà¤¾à¤²à¤¸à¤‚à¤— à¤ªà¤¨à¤¿ à¤¤à¥à¤¯à¤¸à¥à¤¤à¥‹ à¤¸à¤®à¥à¤à¥Œà¤¤à¤¾ à¤­à¤ à¤œà¤¾à¤ªà¤¾à¤¨à¥€ à¤²à¤—à¤¾à¤¨à¥€ à¤¬à¤¢à¥à¤¨à¥‡ à¤¸à¥à¤ªà¤·à¥à¤Ÿ à¤ªà¤¾à¤°à¥à¤¨à¥à¤­à¤¯à¥‹ à¥¤</p>
                    <img src="img/news1.jpg" class="width-100" alt="">
                    <p>à¤¨à¥‡à¤ªà¤¾à¤²à¤•à¥‹ à¤¸à¤¬à¤­à¤¨à¥à¤¦à¤¾ à¤ à¥‚à¤²à¥‹ à¤¦à¤¾à¤¤à¤¾ à¤®à¤¿à¤¤à¥à¤°à¤°à¤¾à¤·à¥à¤Ÿà¥à¤°à¤•à¥‹ à¤°à¥à¤ªà¤®à¤¾ à¤°à¤¹à¥‡à¤•à¥‹ à¤œà¤¾à¤ªà¤¾à¤¨à¤¬à¤¾à¤Ÿ à¤…à¤ªà¥‡à¤•à¥à¤·à¤¾à¤•à¥ƒà¤¤ à¤²à¤—à¤¾à¤¨à¥€ à¤¹à¥à¤¨ à¤¨à¤¸à¤•à¥à¤¨à¥à¤•à¥‹ à¤•à¤¾à¤°à¤£ à¤¦à¥‹à¤¹à¥‹à¤°à¥‹ à¤•à¤° à¤°à¤¹à¥‡à¤•à¥‹ à¤› à¥¤</p>
                    <p>à¤¤à¥à¤¯à¤¸à¥à¤¤à¥ˆ à¤‰à¤¹à¤¾à¤à¤²à¥‡ à¤¨à¥‡à¤ªà¤¾à¤² à¤° à¤œà¤¾à¤ªà¤¾à¤¨à¤®à¤¾ à¤¸à¤¿à¤§à¤¾ à¤‰à¤¡à¤¾à¤¨ à¤¹à¥à¤¨à¤¸à¤•à¥‡ à¤¨à¥‡à¤ªà¤¾à¤²à¤®à¤¾ à¤ªà¤°à¥à¤¯à¤Ÿà¤• à¤…à¤ à¤¬à¤¢à¥à¤¨à¥‡ à¤¬à¤¤à¤¾à¤‰à¤à¤¦à¥ˆ à¤ªà¥‹à¤–à¤°à¤¾à¤•à¥‹ à¤ªà¥à¤°à¤¾à¤•à¥ƒà¤¤à¤¿à¤• à¤¸à¥Œà¤¨à¥à¤¦à¤°à¥à¤¯à¤•à¥‹ à¤–à¥à¤²à¥‡à¤° à¤ªà¥à¤°à¤¶à¤‚à¤¸à¤¾ à¤—à¤°à¥à¤¨à¥à¤­à¤¯à¥‹ à¥¤</p>
                    <p>à¤ªà¤¤à¥à¤°à¤•à¤¾à¤° à¤¸à¤®à¥à¤®à¥‡à¤²à¤¨à¤®à¤¾ à¤‰à¤¹à¤¾à¤à¤²à¥‡ à¤ªà¥‹à¤–à¤°à¤¾ à¤®à¤¾à¤‰à¤¨à¥à¤Ÿ à¤«à¥à¤œà¥€ à¤­à¤¨à¥à¤¦à¤¾ à¤ªà¤¨à¤¿ à¤°à¤¾à¤®à¥à¤°à¥‹ à¤°à¤¹à¥‡à¤•à¥‹ à¤ªà¥à¤°à¤¤à¤¿à¤•à¥à¤°à¤¿à¤¯à¤¾ à¤¦à¤¿à¤‚à¤¦à¥ˆ à¤œà¥€à¤µà¤¨à¤®à¤¾ à¤à¤• à¤ªà¤Ÿà¤• à¤ªà¥‹à¤–à¤°à¤¾à¤•à¥‹ à¤­à¥à¤°à¤®à¤£ à¤—à¤°à¥à¤¨à¥ˆà¤ªà¤°à¥à¤¨à¥‡ à¤ªà¤¨à¤¿ à¤¬à¤¤à¤¾à¤‰à¤¨à¥à¤­à¤¯à¥‹ à¥¤</p>
                    <p>à¤¨à¥‡à¤ªà¤¾à¤²à¤®à¤¾ à¤µà¤¿à¤­à¤¿à¤¨à¥à¤¨ à¤•à¥à¤·à¥‡à¤¤à¥à¤°à¤®à¤¾ à¤¸à¤¾à¤¨à¥‹à¤¤à¤¿à¤¨à¥‹ à¤²à¤—à¤¾à¤¨à¥€ à¤œà¤¾à¤ªà¤¾à¤¨à¥€à¤¹à¤°à¥à¤¬à¤¾à¤Ÿ à¤­à¥ˆà¤°à¤¹à¥‡ à¤ªà¤¨à¤¿ à¤”à¤¦à¥à¤¯à¥‹à¤—à¤¿à¤•à¤• à¤° à¤œà¤²à¤¸à¥à¤°à¥‹à¤¤à¥‹ à¤•à¥à¤·à¥‡à¤¤à¥à¤°à¤®à¤¾à¤¾ à¤œà¤¾à¤ªà¤¾à¤¨à¤²à¥‡ à¤²à¤—à¤¾à¤¨à¥€à¤•à¥‹ à¤¸à¤¾à¤Ÿà¥‹ à¤¸à¤¹à¤¾à¤¯à¤¤à¤¾ à¤‰à¤ªà¤²à¤¬à¥à¤§ à¤—à¤°à¤¾à¤‰à¤à¤¦à¥ˆ à¤†à¤à¤•à¥‹ à¤› à¥¤</p>
                    <p>à¤¨à¥‡à¤ªà¤¾à¤² à¤ªà¤¤à¥à¤°à¤•à¤¾à¤° à¤®à¤¹à¤¾à¤¸à¤‚à¤˜ à¤•à¤¾à¤¸à¥à¤•à¥€à¤•à¤¾ à¤…à¤§à¥à¤¯à¤•à¥à¤· à¤¦à¥€à¤ªà¥‡à¤¨à¥à¤¦à¥à¤° à¤¶à¥à¤°à¥‡à¤·à¥à¤ à¤•à¥‹ à¤…à¤§à¥à¤¯à¤•à¥à¤·à¤¤à¤¾à¤®à¤¾ à¤­à¤à¤•à¥‹ à¤•à¤¾à¤°à¥à¤¯à¤•à¥à¤°à¤®à¤•à¥‹ à¤¸à¤žà¥à¤šà¤¾à¤²à¤¨ à¤¸à¤šà¤¿à¤µ à¤°à¤¾à¤®à¤ªà¥à¤°à¤¸à¤¾à¤¦ à¤¤à¤¿à¤®à¤¿à¤²à¥à¤¸à¤¿à¤¨à¤¾à¤²à¥‡ à¤—à¤°à¥‡à¤•à¤¾ à¤¥à¤¿à¤ à¥¤</p>
                </div>
                <i class="fa fa-arrow-circle-left news_close" style="position:absolute;bottom:5px;font-size: 40px;right: 5px;color: #ff4081;background: #ffffff;border-radius: 58%;padding: 1px 3px;"></i>
            </div>
        </div>
        <div class="popup vod_popup primary_bg_color" style="display:none;">
            <div class="single-news">
                <img src="img/video1.jpg" class="width-100" alt="">
                <h5 class="single_vod_title secondary_bg_color primary_text_color">Ronanogoner Dinculi | Episod 1 | Talk Show | News & Current Affairs</h5>
                <div class="vod_single_listings">
                    <ul>
                        <li>
                            <div class="thumbnail">
                                <img src="{{asset('himal-new/img/gallery/img1.jpg')}}" alt="" class="width-100">
                            </div>
                            <div class="info tertiary_text_color">
                                <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                <span>2015-11-09</span>
                            </div>
                            <div class="popup"></div>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <div class="thumbnail">
                                <img src="{{asset('himal-new/img/gallery/img2.jpg')}}" alt="" class="width-100">
                            </div>
                            <div class="info tertiary_text_color">
                                <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                <span>2015-11-09</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <div class="thumbnail">
                                <img src="img/gallery/img3.jpg" alt="" class="width-100">
                            </div>
                            <div class="info tertiary_text_color">
                                <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                <span>2015-11-09</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        <li>
                            <div class="thumbnail">
                                <img src="img/gallery/img3.jpg" alt="" class="width-100">
                            </div>
                            <div class="info tertiary_text_color">
                                <h5>Gallery | Sports Talk Show | Episod 554 | Conversation with Jahid Hasan Emili and Faruk Ahamed</h5>
                                <span>2015-11-09</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    </ul>
                </div>
                <i class="fa fa-arrow-circle-left vod_close" style="position:fixed;bottom:5px;font-size: 40px;right: 5px;color: #ff4081;background: #ffffff;border-radius: 58%;padding: 1px 3px;"></i>
            </div>
        </div>
        <div class="popup gallery_popup" style="display:none;">
            <div class="full_gallery">
                <div>
                    <img src="{{asset('himal-new/img/gallery/img1.jpg')}}" class='width-100' alt="">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto sit temporibus mollitia amet laudantium hic soluta.</h4>
                </div>
                <div>
                    <img src="{{asset('himal-new/img/gallery/img2.jpg')}}" class='width-100' alt="">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto sit temporibus mollitia amet laudantium hic soluta.</h4>
                </div>
                <div>
                    <img src="{{ asset('himal-new/img/gallery/img3.jpg') }}" class='width-100' alt="">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto sit temporibus mollitia amet laudantium hic soluta.</h4>
                </div>
                <div>
                    <img src="{{ asset('himal-new/img/gallery/img4.jpg') }}" class='width-100' alt="">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto sit temporibus mollitia amet laudantium hic soluta.</h4>
                </div>
                <div>
                    <img src="{{ asset('himal-new/img/gallery/img5.jpg') }}" class='width-100' alt="">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto sit temporibus mollitia amet laudantium hic soluta.</h4>
                </div>
            </div>
            <i class="fa fa-arrow-circle-left gallery_close" style="position:fixed;bottom:5px;font-size: 40px;right: 5px;color: #ff4081;background: #ffffff;border-radius: 58%;padding: 1px 3px;"></i>
        </div>
    </div>
    <script src="{{ asset('himal-new/js/jquery.js') }}"></script>
    <script src="{{ asset('himal-new/js/slickjs.js') }}"></script>
    <!-- <script type="text/javascript" src="vendor/slick/slick.min.js"></script> -->
    <script src="{{ asset('himal-new/js/script.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script>
        var app = angular.module('app',[]);
        app.controller("SiteCtrl",function($scope,$http){
                $http.get('http://newbroad.newitventure.com/api/broadcasters/15/apilist?platform=ios').then(function(result){
                    // console.log(result);
                   $scope.app_info = result.info;
                });
                $http.get('http://nepaljapan.com/api/public/v1/recentnews').then(function(res){
                    
                    $scope.recent_news = res.data;
                    console.log(res.data);
                });
                $http.get("http://nepaljapan.com/api/public/v1/home").then(function(res){
                     $scope.cats = res.data.data.sidenav;
                });
                $http.get('https://www.instagram.com/nitvmediagroup/media/').then(function(res){
                    console.log(res);
                });
        });
    </script>
</body>


</html>
