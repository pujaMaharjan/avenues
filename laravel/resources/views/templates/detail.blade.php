@if($data['layout_id']==1)

<style>
    header{
        margin:0;
        width:100%;
        height:100px;
        position:fixed;
        top:0;
        left:0;
        z-index:10;
    }
    /* primary Color */

    .banner{
        height:60px;
        text-align: center;
        font-size: 2.7em;
        background-color: {{ $data['app_template_primary_color'] }};
    }
    .divlogo{
        width:80px;
        height:58px;
        float: left;                                                        
    }
    .divlogo>img{
        width:100%;
        height:100%;
    }
    .title-news {
        width: 70%;
        float: left;   
        color: white;
        text-transform: uppercase;
        font-size: 120%;
    }
    .menu{
        width:inherit;
        height:40px;
        background-color: {{ $data['app_template_secondary_color'] }}; /*remove*/
    }
    ul{
        height: 40px;
        list-style-type: none;
        padding-left:0;
        padding-right:0;
    }
    li{
        height:40px;
        line-height: 40px;
        width:24.9%;
        float: left;
        text-transform: uppercase;
        text-align:center;
    }
    .active{
        border-bottom:2px solid #FFC400;
    }
    a{  
        text-decoration: none;
        color:#868686;
    }

    /*----container part----*/

    .divlive{ 
        max-height:400px;
        max-width:100%;
        /*box-shadow: 0 0 5px black;*/
    }
    .divlive img{
        max-height: 400px;
        width: 100%;
    }
    .program{
        height:30px;
        line-height: 30px;
        width: 100%;
        text-align: center;
        background-color: {{ $data['app_template_tertiary_color'] }};
    }
    .divtable{
        width:100%;
        max-height:45vh;
        overflow:auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        margin-top:2px;
        float:left;
        padding:10px 5px;
        background-color: white; /*remove*/;
        margin-bottom:50px;
    }
    table{
        width:100%;
        margin:0 0 20px 0;
    }
    tr{
        height:40px;
        border:1px solid #9E9E9E;
    }
    td{
        height: 20px;
    }
    tr>td:first-child{
        text-align: left;
        color:black;
        padding-left:20px;
    }
    tr>td:last-child{
        text-align: right;
        color:#019fe8;
        padding-right:20px;
    }
    tr:nth-child(3){
        background-color: blue;
    }
    tr:nth-child(3)>td:first-child,
    tr:nth-child(3)>td:last-child{
        color:white;
    }

    /*----CSS used for Latest.html---*/
    /*.container{
        padding:0 5px;
        height:500px;
        overflow:auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
    .row{
        margin:0;
    }
    */
    .news-wrap{
        width:100%;
        height:200px;
        background-color: black;
    }
    .col-xs-1,.col-xs-2,.col-xs-3,
    .col-xs-4,.col-xs-5,.col-xs-6,
    .col-xs-7,.col-xs-8,.col-xs-9,
    .col-xs-10,.col-xs-11,.col-xs-12{
        padding:2px;
        margin:0;
        text-align: left;
    }
    .img-adjust{
        min-width:100%;
        height: 100px;

    }
    hr{
        margin:5px 0;
        border-top:1px solid #9E9E9E;
    }


    /*----CSS for related to news tab----*/
<<<<<<< HEAD

    .news-menu{
        width:100%;
        height:40px;
        background-color: #F7F8E0; /*remove*/
    }
    .news-menu>ul>li{
        width:20%;
        float: left;
        text-transform: uppercase;
        text-align:center;

    }


    footer{
        height:40px;
        width:100%;
        background-color:black;
        
        
        z-index: 10;
    }
    footer>img{
        width:100%;
        height:100%;
    }

</style>
<script type="text/javascript" src="{{ asset('jwplayer/jwplayer.js') }}"></script>
<script type="text/javascript">jwplayer.key="o+Weqr3g2fK+GuH/lEv+GmnjDZmj8IOa0Mwc+2kc6Bs=";</script>
<script type="text/javascript">
    var url = '{{ $url }}';
    console.log(url);
    function play() {
        var cnt = document.getElementById("myElement");
        jwplayer(cnt).setup({
            file: url,
            autostart: true,
            height: 380,
            width: 567
        });
    }

    play();
</script>
<div >
    <header>
        <div class="banner">
            <div class="divlogo">
                @foreach($channels as $chan)
                
                <img src="{{asset("uploads/".$chan->logo)}}" alt="Himalayan TV">
                
                @endforeach
            </div>
            <div class="title-news">
                <h3>{{ $chan->name }}</h3>
            </div>
            
        </div>
        <div class="menu">
            <ul class="menu menu-layout">
                <li><a href="#live">live</a></li>
                <li><a href="#latest" ng-click="clickLatest()" >latest</a></li>
                <li><a href="#national" >news</a></li>
                <li><a href="#popular" >popular</a></li>
            </ul>
        </div>
    </header>
    <div >
        <div id="live" class="container-full">
            <div class="row" style="margin: 0; padding:0;">
                <div class="col-xs-12">
                    <div class="divlive">

                        <div id="myElement">
                            <p>Loading the player...</p>
                        </div>

                        
                        
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="program">

                    <strong>Program Guide</strong>
                </div>
                <div class="divtable">
                    <table>
                        <tr><td>New Entry</td><td>12:00-12:50</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                        <tr><td>English Samachar</td><td>16:00-16:30</td></tr>
                        <tr><td>New Entry</td><td>12:00-12:50</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                    </table>
                </div>
            </div> 

        </div>

        <div id="latest" class="container-full" style="display:none;">
            
            <?php foreach($recent_news as $recent) { ?>
            <div class="row">
                <div class="col-xs-5">
                   
                    <img src="{{$recent->img}}" class="img-responsive img-adjust">  
                </div>
                <div class="col-xs-7">
                    <a href="#watch"><h4>{{$recent->title}}</h4></a>
                    <small>2016-01-11<br>137 views</small>
                </div>
            </div>
            <hr>
            <?php } ?>
            
            
        </div>

        <div id="popular" class="container-full" style="display:none;">
            
            <?php for($i=0; $i<6; $i++) { ?>
            <div class="row">
                <div class="col-xs-5">
                    <img src="http://www.statref.com/images/content/products/SR%20Online/MedNews-Banner%20956x256.gif" class="img-responsive img-adjust">  
                </div>
                <div class="col-xs-7">
                    <a href="#read" id="readmore"><h4>Youth dies of cold in Manang</h4></a>
                    <small>2016-01-11<br>137 views</small>
                </div>
            </div>
            <hr>
            <?php } ?>
            
        </div>
        
        <div class="container-full" id="national" style="display:none;">
            
         <?php for($i=0; $i<6; $i++) { ?>
         <div class="row">
            <div class="col-xs-5">
                <img src="media/video1.jpg" class="img-responsive img-adjust">  
            </div>
            <div class="col-xs-7">
                <a href="#read"><h4>Youth dies of cold in Manang</h4></a>
                <small>2016-01-11<br>137 views</small>
            </div>
        </div>
        <hr>
        <?php } ?>

        
    </div>
    
</div>
<div class="lang">
    <img src="images/nep.png">
</div>
</div>



<footer>
    <img src="images/footer.jpg">
</footer>
</div>


@elseif($data['layout_id']==2)
<style>

    body{
        margin:0;
        padding:0;
        background-color:#9F0A10;
        overflow-x:hidden;
    }
    .side-menu{
        width:60%;
        height:100%;
        background-color: #9F0A10;
        position:fixed;
        overflow: auto;
    }
    .visible{
        margin-left:60%;
    }
    .container-fluid{
        z-index:10;
        position: absolute;
        left:0px;
        padding:0px;
        box-shadow: 0 2px 5px 3px black;
    }
    ul{
        list-style-type: none;
        margin-left:-40px;

    }
    li{
        float:left;
        width:25%;
        text-align: center;
        /*border:1px solid orange;*/
    }
    a{
        color:white;
        cursor: pointer;
    }
    .active{
        border-bottom: 2px solid blue;
    }

    .row{
        margin:0;
        padding:0;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1,
    .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2,
    .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3,
    .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4,
    .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5,
    .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6,
    .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7,
    .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8,
    .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9,
    .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10,
    .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11,
    .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
        margin:0;
        padding:0;
    }
    .menu{
=======

    .news-menu{
>>>>>>> 8bc90d08885f21d32ce492f2e31e60ce96680670
        width:100%;
        height:40px;
        background-color: #F7F8E0; /*remove*/
    }
    .news-menu>ul>li{
        width:20%;
        float: left;
        text-transform: uppercase;
        text-align:center;

    }


    footer{
        height:40px;
        width:100%;
        background-color:black;
        
        
        z-index: 10;
    }
    footer>img{
        width:100%;
        height:100%;
    }

</style>
<<<<<<< HEAD


<div class="side-menu  text-center" style="color:white;">   
    <div class="row">
        <div class="col-sm-12" style="border-bottom:1px solid #EFEFF4;" >
            <img src="images/Logo.png" style="max-height:70px;width:50%">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12" >
            <img src="images/home.png" style="max-height:70px;width:50%;margin-bottom:2px;">
            <br>HOME
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <img src="images/epg.png" style="max-height:70px;width:50%">
            <br>SCHEDULE
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <img src="images/videos.png" style="max-height:70px;width:50%">
            <br>VIDEOS
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <img src="images/settings.png" style="max-height:70px;width:50%">
            <br>SEETINGS
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <img src="images/aboutus.png" style="max-height:70px;width:50%">
            <br>ABOUT US
        </div>
    </div>
</div>
<div class="container-fluid" id="" style="background-color:#9F0A10;padding:0px;">
    <div class="row">
        <div class="col-sm-12">
            <span id="menu-icon" class="glyphicon glyphicon-menu-hamburger" style="position:absolute;top:10px;left:20px;color:white;cursor:pointer;"></span>
            <img src="images/videoplay.jpg" class="img-responsive" style="min-width:320px;max-height:350px;">
        </div>
    </div>
    <div class="menu">
        <ul>
            <li class="active"><a href="#">RECENT</a> </li>
            <li><a href="#">मुख्य समाचार</a> </li>
            <li><a href="#"> एबिसि बिज</a> </li>
            <li><a href="#">बिचार</a></li>
        </ul>
    </div>
    <div class="news-container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <a href="newsinfo.html" style="color:black">
                    <div class="news-preview">
                        <div class="news-preview-img">
                            <img src="images/Logo.png" class="img-responsive">
                        </div>
                        <div class="news-preview-heading">
                            It is a news heading
                            <small style="color:#CFCFCF">2016-02-4</small>
                        </div>
                    </div>
                </a>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>

                <div class="news-preview">
                    <div class="news-preview-img">
                        <img src="images/Logo.png" class="img-responsive">
                    </div>
                    <div class="news-preview-heading">
                        It is a news heading
                        <small style="color:#CFCFCF">2016-02-4</small>
                    </div>
                </div>


=======
<script type="text/javascript" src="{{ asset('jwplayer/jwplayer.js') }}"></script>
<script type="text/javascript">jwplayer.key="o+Weqr3g2fK+GuH/lEv+GmnjDZmj8IOa0Mwc+2kc6Bs=";</script>
<script type="text/javascript">
    var url = '{{ $url }}';
    console.log(url);
    function play() {
        var cnt = document.getElementById("myElement");
        jwplayer(cnt).setup({
            file: url,
            autostart: true,
            height: 380,
            width: 567
        });
    }

    play();
</script>
<div >
    <header>
        <div class="banner">
            <div class="divlogo">
                @foreach($channels as $chan)
                
                <img src="{{asset("uploads/".$chan->logo)}}" alt="Himalayan TV">
                
                @endforeach
            </div>
            <div class="title-news">
                <h3>{{ $chan->name }}</h3>
            </div>
            
        </div>
        <div class="menu">
            <ul class="menu menu-layout">
                <li><a href="#live">live</a></li>
                <li><a href="#latest" ng-click="clickLatest()" >latest</a></li>
                <li><a href="#national" >news</a></li>
                <li><a href="#popular" >popular</a></li>
            </ul>
        </div>
    </header>
    <div >
        <div id="live" class="container-full">
            <div class="row" style="margin: 0; padding:0;">
                <div class="col-xs-12">
                    <div class="divlive">

                        <div id="myElement">
                            <p>Loading the player...</p>
                        </div>

                        
                        
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="program">

                    <strong>Program Guide</strong>
                </div>
                <div class="divtable">
                    <table>
                        <tr><td>New Entry</td><td>12:00-12:50</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                        <tr><td>English Samachar</td><td>16:00-16:30</td></tr>
                        <tr><td>New Entry</td><td>12:00-12:50</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                        <tr><td class="nowplay">Sports</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>14:50-15:00</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:00-15:30</td></tr>
                        <tr><td>Nepali Samachar</td><td>15:45-16:30</td></tr>
                    </table>
                </div>
            </div> 

        </div>

        <div id="latest" class="container-full" style="display:none;">

            <?php foreach($recent_news as $recent) { ?>
            <div class="row">
                <div class="col-xs-5">

                    <img src="{{$recent->img}}" class="img-responsive img-adjust">  
                </div>
                <div class="col-xs-7">
                    <a href="#watch"><h4>{{$recent->title}}</h4></a>
                    <small>2016-01-11<br>137 views</small>
                </div>
            </div>
            <hr>
            <?php } ?>
            
            
        </div>

        <div id="popular" class="container-full" style="display:none;">

            <?php for($i=0; $i<6; $i++) { ?>
            <div class="row">
                <div class="col-xs-5">
                    <img src="http://www.statref.com/images/content/products/SR%20Online/MedNews-Banner%20956x256.gif" class="img-responsive img-adjust">  
                </div>
                <div class="col-xs-7">
                    <a href="#read" id="readmore"><h4>Youth dies of cold in Manang</h4></a>
                    <small>2016-01-11<br>137 views</small>
                </div>
            </div>
            <hr>
            <?php } ?>
            
        </div>
        
        <div class="container-full" id="national" style="display:none;">

           <?php for($i=0; $i<6; $i++) { ?>
           <div class="row">
            <div class="col-xs-5">
                <img src="media/video1.jpg" class="img-responsive img-adjust">  
            </div>
            <div class="col-xs-7">
                <a href="#read"><h4>Youth dies of cold in Manang</h4></a>
                <small>2016-01-11<br>137 views</small>
>>>>>>> 8bc90d08885f21d32ce492f2e31e60ce96680670
            </div>
        </div>
        <hr>
        <?php } ?>

        
    </div>
<<<<<<< HEAD
</div>
=======
    
</div>
<div class="lang">
    <img src="images/nep.png">
</div>
</div>



<footer>
    <img src="images/footer.jpg">
</footer>
</div>


@elseif($data['layout_id']==2)
@include('templates.abc')
>>>>>>> 8bc90d08885f21d32ce492f2e31e60ce96680670
@elseif($data['layout_id']==3)
<h3>Avebues Templates</h3>


@else 



@endif


