@extends('apps.master')
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('dist/css/bootstrap-colorpicker.min.css') }}">
<div class="row">
	<div class="col-md-4">
		{{$template}}
		@if($template)
		<form action="{{url('broadcaster/app/'.$app_id.'/template/'.$template->id.'/update')}}" class="from-vertical" method="post">
			<div class="form-group">
				<label for="">Choose Layout</label>
				{!! Form::select('layout_id', $layouts, $template->layout_id, ['class'=>'form-control layout_id']); !!}
			</div>
			<label for="">Primary Color</label>
			<div class="input-group demo2">
				<input type="text" id="position-top-right" class="form-control minicolors-input app_template_primary_color" name="app_template_primary_color" value="{{$template->app_template_primary_color}}">
				<span class="input-group-addon"><i></i></span>
			</div>
			<label for="">Secondar Color</label>
			<div class="input-group demo2">
				<input type="text" class="form-control app_template_secondary_color" name="app_template_secondary_color" value="{{$template->app_template_secondary_color}}"/>
				<span class="input-group-addon"><i></i></span>
			</div>
			<label for="">Tertiray Color</label>
			<div class="input-group demo2">
				<input type="text" class="form-control app_template_tertiary_color" name="app_template_tertiary_color" value="{{$template->app_template_tertiary_color}}"/>
				<span class="input-group-addon"><i></i></span>
			</div>
			<label for="">app_template_highlighted_color</label>
			<div class="input-group demo2">
				<input type="text" class="form-control app_template_highlighted_color" name="app_template_highlighted_color"  value="{{$template->app_template_highlighted_color}}"/>
				<span class="input-group-addon"><i></i></span>
			</div>
			<label for="">app_template_text_color</label>
			<div class="input-group demo2">
				<input type="text" class="form-control app_template_text_color" value="{{$template->app_template_text_color}}" name="app_template_text_color" />
				<span class="input-group-addon"><i></i></span>
			</div>

			<input type="hidden" name="app_id" id="app_id" value="{{$app_id}}" />
			<div class="form-group">
				<a  class="btn btn-primary btn-prev btn-lg"  data-href="{{ url('broadcaster/app/3/template/preview') }}" data-toggle="modal" data-target="#myModal" href="#" data-href="">Preview Your App</a>
				<button class="btn btn-save btn-danger">Confirm</button>
			</div>

		</form>
		@endif
	</div>
	<div class="col-md-8">
		<div class="preview" ></div>
		<div class="result">

		</div>
	</div>

</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div  class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

@stop
@section('postFoot')
<script src="{{ asset('dist/js/bootstrap-colorpicker.js') }}"></script>
<script src="{{ asset('src/js/docs.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>


<script type="text/javascript">
	(function($){
		$('.layout_id').on('change',function(){
			var id = $(this).val();
			//$('.result').hide();
			if(id!='')
			{


				var url = '<?= url('layout') ?>/'+id;
					//console.log(url);
					$.ajax({
						url: url,
						data:{id:id},
						success: function(res) {

							//console.log(res);
							$('.result').show();
							if(res)
								$('.result').html('<img src="<?php echo url('/') ?>/'+res+'" />');
							


						},
						error: function (request, status, error) {
							console.log(request.responseText);
						},
						type: 'post'
					});
				}
			});
		$(document).on('click','.menu-layout li a',function(e){
			$var = $(this).attr('href');
			console.log($var);
			$($var).show().siblings().hide();

			e.preventDefault();
		});
		$('.demo2').colorpicker();
		
		$(document).on(".modal-body .container-fluid .row .col-sm-12 #menu-icon",'click',function(){
			$(".container-fluid").toggleClass("visible");		
		});
		
		
		$('.btn-prev').on('click',function(){
			
			var url = $(this).data('href');
			console.log(url);
			var app_id = $('#app_id').val();
			var app_template_secondary_color = $('.app_template_secondary_color').val();
			var layout_id = $('.layout_id').val();
			var app_template_primary_color = $('.app_template_primary_color').val();
			var app_template_tertiary_color = $('.app_template_tertiary_color').val();
			
			$.ajax({
				url: url,
				data:{app_id:app_id,layout_id:layout_id,app_template_secondary_color: app_template_secondary_color,app_template_tertiary_color: app_template_tertiary_color,
					app_template_primary_color: app_template_primary_color
				},
				success: function(res) {
					$('.modal-body').html(res);
					     // $('.preview').html(res);
					 },
					 type: 'post'
					});

			
		});
	})(jQuery);
</script>
@stop