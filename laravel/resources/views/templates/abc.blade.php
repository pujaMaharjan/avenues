<!DOCTYPE html>
<html lang="en" class="no-js" ng-app="app">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sidebar Transitions</title>
    <meta name="description" content="Sidebar Transitions: Transition effects for off-canvas views" />
    <meta name="keywords" content="transition, off-canvas, navigation, effect, 3d, css3, smooth" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{ asset('av/css/normalize.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('av/css/demo.css"') }} />
    <link rel="stylesheet" type="text/css" href="{{ asset('av/css/icons.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('av/css/component.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('av/css/style.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('av/vendor/scrollbar/jquery.mCustomScrollbar.css') }}">
    <script src="{{ asset('av/js/modernizr.custom.js') }}"></script>
    <style>
.primary_bg_color{
  background:  {{ $_GET['p_color'] }} !important;
}
.primary_text_color{
  color: {{ $_GET['p_color'] }} !important;
}
.primary_tab_border{
  border-bottom-color: {{ $_GET['p_color'] }} !important;
}


.secondary_bg_color{
  background:{{ $_GET['s_color'] }} !important;
}
.secondary_text_color{
  color:{{ $_GET['s_color'] }}  !important;
}
.secondary_tab_border{
  border-bottom-color:{{ $_GET['s_color'] }} !important;
}


.tertiary_bg_color{
  background: {{ $_GET['t_color'] }} !important;
}
.tertiary_text_color{
  color: {{ $_GET['t_color'] }} !important;
}


.selected_bg_color{
  background: {{ $_GET['t_color'] }} !important;
}
.selected_text_color{
  color: {{ $_GET['t_color'] }} !important;
}
  
.primary_bg_text_color{
  color: {{ $_GET['t_color'] }};
}

</style>
</head>

<body ng-controller="SiteCtrl">
    <div id="st-container" class="st-container">
        <nav class="st-menu st-effect-1 tertiary_bg_color" id="menu-1">
            <h2 class="text-center"><img src="{{ asset('av/img/logo.png') }}" alt="" class="sidebar_logo"></h2>
            <ul class="side-bar customizer_tabs">
                <li class="active" data-tab="tab-1">
                <img class="hide" src="{{ asset('av/img/channels.png') }}" alt=""><span>Channels</span></li>
                <li data-tab="tab-2"><img class="hide" src="{{ asset('av/img/vod.png') }}" alt=""><span>Vod</span></li>
                <li data-tab="tab-3"><img class="hide" src="{{ asset('av/img/radio.png') }}" alt=""><span>Radio</span></li>
                <li data-tab="tab-4"><img class="hide" src="{{ asset('av/img/news.png') }}" alt=""><span>News</span></li>
                <li data-tab="tab-5"><img class="hide" src="{{ asset('av/img/about.png') }}" alt=""><span>About Us</span></li>
                <li><img class="hide" src="{{ asset('av/img/settings.png') }}" alt=""><span>Settings</span></li>
            </ul>
        </nav>
        <div class="st-pusher">
            <div class="st-content">
                <!-- this is the wrapper for the content -->
                <div class="st-content-inner">
                    <!-- extra div for emulating position:fixed of the menu -->
                    <div class="main clearfix">
                        <div id="st-trigger-effects" class="column">
                            <button class="hamburger-menu" data-effect="st-effect-1"><img src="{{asset('av/img/hamberger.png') }}" alt=""></button>
                            <div class="header primary_bg_color">
                                <h2 class="text-center header"><img src="{{ asset('av/img/logo.png') }}" alt=""></h2>
                            </div>
                            <img src="{{ asset('av/img/fblike.png') }}" class="fb-like" alt="">
                            <div class="dynamic_contetnts">
                                <div class="channels tab-content info_tab active" id="tab-1">
                                    <img src="{{ asset('av/img/video1.jpg') }}" class="width-100" alt="">

                                    <div class="box_news" ng-repeat="news in recent_news">
                                        <h5>New <span class="arrow primary_text_color">></span></h5>
                                        <div class="news_list text-center">
                                            <img src="{{ asset('av/img/news.jpg') }}" alt="">
                                            <div class="overlay">
                                                <span class="title">पुनःनिर्माणको गति सुस्त</span>
                                                <br>
                                                <span class="date primary_text_color">2017-03-22</span>
                                            </div>
                                        </div>
                                        <div class="news_list text-center">
                                            <img src="{{ asset('av/img/news.jpg') }}" alt="">
                                            <div class="overlay">
                                                <span class="title">पुनःनिर्माणको गति सुस्त</span>
                                                <br>
                                                <span class="date primary_text_color">2017-03-22</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="vod tab-content" id="tab-2">
                                    <ul class="vod_subcategory">
                                        <li>
                                            <h3 class="active primary_text_color primary_tab_border">RONAGONER</h3></li>
                                        <li>
                                            <h3>NEWS INSIDE</h3></li>
                                        <li>
                                            <h3>GALLERY</h3></li>
                                        <li>
                                            <h3>BIZ TIME</h3></li>
                                        <li>
                                            <h3>FRANKLY SPEAKING</h3></li>
                                        <li>
                                            <h3>MARKET WATCH</h3></li>
                                        <li>
                                            <h3>EL SOMOY L TALK SHOW </h3></li>
                                        <li>
                                            <h3>NEWS CLIPS</h3></li>
                                    </ul>
                                    <ul class="vod_listings">
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span></div>
                                            <div class="clearfix"></div>
                                        </li>
                                        <li class="vod-lists">
                                            <div class="thumbanail">
                                                <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                            </div>
                                            <div class="title_date primary_text_color">
                                                <h5>Karobar News | 2073-12-15</h5>
                                                <span>2017-03-21</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="vod_fullscreen full_screen hide">
                                    <div class="wrapper_fullscreen">
                                        <iframe width="320" height="200" src="https://www.youtube.com/embed/sQGgDWx0WXc" frameborder="0" allowfullscreen></iframe>
                                        <h5 class="title primary_bg_color">Karobar News 2017 13 15</h5>
                                        <ul class="vod_listings">
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <div class="thumbanail">
                                                    <img src="{{ asset('av/img/news.jpg') }}" class="width-100" alt="">
                                                </div>
                                                <div class="title_date primary_text_color primary_text_color">
                                                    <h5>Karobar News | 2073-12-15</h5>
                                                    <span>2017-03-21</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="radio tab-content" id="tab-3">
                                    <h3 class="live"><div class="red_dot"></div> Live</h3>
                                    <h1 class="text-center mg-0"><img src="{{ asset('av/img/logo.png') }}" class="radio_logo" alt=""></h1>
                                    <h3 class="text-center">Radio Awaz</h3>
                                    <h3 class="text-center play_button"><img src="{{ asset('av/img/play.png') }}" alt=""></h3>
                                </div>
                                <div class="news tab-content" id="tab-4" ">
                                    <div class="wrap ">
                                        <div class="block ">
                                            <img src="{{ asset('av/img/nepjap.png')}}" alt=" " class="width-100 news_head ">
                                            <h5>Nepaljapan</h5>
                                        </div>
                                        <div class="block ">
                                            <img src="{{ asset('av/img/nrn.png') }}" alt=" " class="width-100 news_head ">
                                            <h5>News NRN</h5>
                                        </div>
                                        <div class="block ">
                                            <img src="{{ asset('av/img/media.png') }}" alt=" " class="width-100 news_head ">
                                            <h5>Gallery</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="news_fullscreen full_screen hide " >
                                    <ul class="newslist_subcategory primary_bg_color">
                                        <li>
                                            <h3 class="active secondary_text_color secondary_tab_border ">जापान</h3></li>
                                        <li>
                                            <h3>जन सरोकार</h3></li>
                                        <li>
                                            <h3>विश्व समाचार</h3></li>
                                        <li>
                                            <h3>समाज</h3></li>
                                        <li>
                                            <h3>अर्थ</h3></li>
                                        <li>
                                            <h3>प्रबास</h3></li>
                                        <li>
                                            <h3>सूचना/प्रविधि</h3></li>
                                    </ul>

                                    <ul class="news-lisitng ">
                                        <li>
                                            <div class="box ">
                                                <h5>गोर्खा भर्ती : छनोट हुने मालामाल- नहुने बेकार (भिडियो)</h5>
                                                <span>Few Seconds ago</span>
                                                <img src="{{ asset('av/img/news1.jpg') }}" alt=" " class="width-100 ">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box ">
                                                <h5>निर्वाचन आयोग घेर्ने राप्रपाको कार्यक्रममा अश्रुग्यास प्रहार</h5>
                                                <span>Few Minutes Ago</span>
                                                <img src="{{ asset('av/img/news2.jpg') }}" alt=" " class="width-100 ">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box ">
                                                <h5>गोर्खा भर्ती : छनोट हुने मालामाल- नहुने बेकार (भिडियो)</h5>
                                                <span>A Day ago</span>
                                                <img src="{{ asset('av/img/news1.jpg') }}" alt=" " class="width-100 ">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box ">
                                                <h5>गोर्खा भर्ती : छनोट हुने मालामाल- नहुने बेकार (भिडियो)</h5>
                                                <span>Date</span>
                                                <img src="{{ asset('av/img/news1.jpg') }}" alt=" " class="width-100 ">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pop_news_box">
                                    <div class="popup news_popup " style="display:none; ">
                                        <div class="single-news ">
                                            <img src="{{asset('av/img/news1.jpg') }}" class="width-100 " alt=" ">
                                            <i class="demo-icon icon-share primary_bg_color"></i>
                                            <div class="news_box ">
                                                <h5>गोर्खा भर्ती : छनोट हुने मालामाल- नहुने बेकार (भिडियो)</h5>
                                                <span>2017-03-21</span>
                                                <p>निजी भ्रमणका क्रममा पोखरामा रहनुभएका पूर्व प्रधानमन्त्री हातोयामासंग नेपाल पत्रकार महासंघ कास्कीले मंगलबार पोखरामा आयोजना गरेको पत्रकार सम्मेलनमा सो कुरा बताउनुभएको हो ।</p>
                                                <p>त्यस क्रममा उहाँले जापानी लगानी नेपालमा हुन नसक्नुको कारण दोहोरो कर प्रणालप् रहेको बताउँदै यो हट्नासाथ लगानी बढ्ने र आफूले मंगलबार काठमाडौंमा प्रधानमन्त्री प्रचण्डसंगको भेटमा यो कुरा उठाउने बताउनुभयो ।</p>
                                                <p>उहाँले जापानले धेरै देशसंग दोहोरो कर हटाउने सम्बन्धमा सम्झौता गरिसेकेको र नेपालसंग पनि त्यस्तो सम्झौता भए जापानी लगानी बढ्ने स्पष्ट पार्नुभयो ।</p>
                                                <img src="{{ asset('av/img/news1.jpg') }}" class="width-100 " alt=" ">
                                                <p>नेपालको सबभन्दा ठूलो दाता मित्रराष्ट्रको रुपमा रहेको जापानबाट अपेक्षाकृत लगानी हुन नसक्नुको कारण दोहोरो कर रहेको छ ।</p>
                                                <p>त्यस्तै उहाँले नेपाल र जापानमा सिधा उडान हुनसके नेपालमा पर्यटक अझ बढ्ने बताउँदै पोखराको प्राकृतिक सौन्दर्यको खुलेर प्रशंसा गर्नुभयो ।</p>
                                                <p>पत्रकार सम्मेलनमा उहाँले पोखरा माउन्ट फुजी भन्दा पनि राम्रो रहेको प्रतिक्रिया दिंदै जीवनमा एक पटक पोखराको भ्रमण गर्नैपर्ने पनि बताउनुभयो ।</p>
                                                <p>नेपालमा विभिन्न क्षेत्रमा सानोतिनो लगानी जापानीहरुबाट भैरहे पनि औद्योगिकक र जलस्रोतो क्षेत्रमाा जापानले लगानीको साटो सहायता उपलब्ध गराउँदै आएको छ ।</p>
                                                <p>नेपाल पत्रकार महासंघ कास्कीका अध्यक्ष दीपेन्द्र श्रेष्ठको अध्यक्षतामा भएको कार्यक्रमको सञ्चालन सचिव रामप्रसाद तिमिल्सिनाले गरेका थिए ।</p>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>

                                <div class="about_us tab-content " id="tab-5">
                                    <h5 class="text-center ">About us</h5>
                                    <p>Aveneus smart phone app enables users to watch the live streaming of the Aveneus Television in their smart devices connected to the internet anywhere in the globe . This app is not only limited to the live streaming video but also includes the VOD and the news from the Aveneus television . Aveneus TV app is a joint initiative of Aveneus Television and New IT Venture Corp. In the user 's quest , more features will be added in near future , helping viewers to stay close to Aveneus Television</p>
                                    <p>Powered by</p>
                                    <div class="text-center ">
                                        <img src="{{ asset('av/img/nitv.png')}}" class="text-center " alt=" ">
                                    </div>
                                    <div class="text-center copyright primary_bg_color">
                                        <span>Copyright &copy; 2015 NITV Media.</span>
                                        <br>
                                        <span>All Rights Reserved.</span>
                                        <br>
                                        <span class="link_about_color">http://newitventure.com</span>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <!-- /main -->
                </div>
                <!-- /st-content-inner -->
            </div>
            <!-- /st-content -->
        </div>
        <!-- /st-pusher -->
        <div class="navigation">
            <div class="wrap-1">
                <div class="block " >
                    <img src="{{asset('av/img/back.png') }}" alt=" " class="back ">
                    
                </div>
                <div class="block ">
                    <img src="{{asset('av/img/home.png') }}" alt=" ">
                    
                </div>
                <div class="block ">
                    <img src="{{asset('av/img/preview.png') }} " alt=" ">
                    
                </div>
                <img src="{{asset('av/img/back.png') }}" alt=" " class="news_back" >
            </div>
        </div>
    </div>
    <!-- /st-container -->
    <script src="{{ asset('av/js/jquery.js') }}"></script>
    <script src="{{ asset('av/js/classie.js') }}"></script>
    <script src="{{ asset('av/js/sidebarEffects.js') }}"></script>
    <script src="{{ asset('av/vendor/scrollbar/jquery.mCustomScrollbar.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.3/angular.min.js"></script>
    <script>
       var app = angular.module('app',[]);
        app.controller("SiteCtrl",function($scope,$http){
                $http.get('http://www.castnow.tv/api/broadcasters/15/apilist?platform=ios').then(function(result){
                    // console.log(result);
                   $scope.app_info = result.info;
                });
                $http.get('http://nepaljapan.com/api/public/v1/recentnews').then(function(res){
                    
                    $scope.recent_news = res.data;
                    console.log(res.data);
                });
                $http.get("http://nepaljapan.com/api/public/v1/home").then(function(res){
                     $scope.cats = res.data.data.sidenav;
                });
                
        });
    </script>
    <script>
    (function($) {
        $(window).on("load ", function() {
            $(".st-content ").mCustomScrollbar();
            $(".full_screen ").mCustomScrollbar();
            $(".news_popup ").mCustomScrollbar();
           
        });
        $('.vod-lists').click(function(){
            $('.vod_fullscreen').toggleClass('hide');
        });
        $('.news_list').click(function(){
            $('.vod_fullscreen').toggleClass('hide');
        });
        $('.news_head').click(function(){
            $('.news_fullscreen').toggleClass('hide');
        });
        $('.back').click(function(){
            console.log('abc');
            $('.full_screen').addClass('hide');
        });
        $('.box').click(function(){
            $('.news_popup').css('display','block');
            $('.news_back').css('display','block');
        });

        $('.news_back').click(function(){
            $('.news_popup').css('display','none');
            $(this).css('display','none');
        });


        $('ul.customizer_tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.customizer_tabs li').removeClass('active');
        $('.tab-content').removeClass('active');
        $('.st-container').removeClass('st-menu-open');

        $(this).addClass('active');
        $("#"+tab_id).addClass('active');
    })
    })(jQuery);
    </script>
</body>

</html>
