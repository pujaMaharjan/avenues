@extends('apps.master')
@section('content')
<?php $p_color = $template->app_template_primary_color; 
$s_color = $template->app_template_secondary_color;
$t_color = $template->app_template_tertiary_color;
?>

@if($template->layout_id==2)
<iframe src="{{url('template/himalaya?app_id='.urlencode($id).'&p_color='.urlencode($p_color).'&s_color='.urlencode($s_color).'&t_color='.urlencode($t_color)) }}" height="500px" width="400px"></iframe>
@elseif($template->layout_id==1)

<iframe src="{{url('template/abc?p_color='.urlencode($p_color).'&s_color='.urlencode($s_color).'&t_color='.urlencode($t_color)) }}" height="500px" width="400px"></iframe>
@else
<iframe src="{{ url('template/nitvmedia?p_color='.urlencode($p_color).'&s_color='.urlencode($s_color).'& t_color='.urlencode($t_color))}}" height="500px" width="400px"></iframe>
@endif
<br>
<a href="{{ url('broadcaster/app/'.$template->app_id.'/template/'.$template->id.'/edit') }}" class="btn btn-success">edit</a>

@stop
@section('postFoot')
<script>
	$(function() {
		$( "#tabs" ).tabs();

	});
	$(document).ready(function(){
		$("#menu-icon").on('click',function(){
			$(".container-fluid").toggleClass("visible");		
		});
	});
</script>
@stop