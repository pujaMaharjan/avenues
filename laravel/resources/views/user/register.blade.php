@extends('admin.login')

@section('content')

<form class="login-form" action="{{ url('user/register') }}" method="post" class="register-form" novalidate="novalidate">
 {!! csrf_field() !!}
 <h3>Sign Up</h3>
 <div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">user Name</label>
    <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="username" />
</div>

<div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label visible-ie8 visible-ie9">Email</label>
    <input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email">
</div>
<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Password</label>
    <input class="form-control placeholder-no-fix" type="password" placeholder="Password" name="password">
</div>


<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">CPassword</label>
    <input class="form-control placeholder-no-fix" type="password" placeholder="Password" name="password_confirmation">
</div>

<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Phone</label>
    <input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="password_confirmation">
</div>

<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Country</label>
    
    <input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="password_confirmation">
</div>





<div class="form-actions">

    <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>
@stop