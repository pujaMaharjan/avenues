@extends('admin.login')
@section('content')
<form method="POST" action="{{ url('user/login') }}" class="login-form" novalidate="novalidate" id="form_sample_2">
    {!! csrf_field() !!}

    <h3 class="form-title">Sign In</h3>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="input-icon right">
            <i class="fa"></i>
            <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="input-icon right">
            <i class="fa"></i>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required>
        </div>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-success uppercase">Login</button>
        <label class="rememberme check">
         <span class="checker"><input type="checkbox" name="remember"></span>Remember </label>
         <a href="{{ url('password/email') }}" id="forget-password" class="forget-password">Forgot Password?</a>
     </div>
     <div class="create-account">
        <p>
            <a href="{{ url('broadcaster/create') }}" id="register-btn" class="uppercase">Create an account</a>
        </p>
    </div>
</form>

@stop