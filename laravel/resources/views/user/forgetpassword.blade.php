<!-- resources/views/auth/password.blade.php -->
@extends('admin.login')
@section('content')
<form method="POST" action="{{url('password/email')}}" class="login-form">
    {!! csrf_field() !!}
    <h3 class="form-title">Forget Password ?</h3>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="email" name="email" value="{{ old('email') }}" placeholder="Email">
    </div>

    <div class="form-actions">
        <button class="btn btn-success uppercase" type="submit">
            Send Password Reset Link
        </button>
    </div>
</form>
@stop