@extends('broadcaster.master')
@section('postHead')

<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/pages/css/profile.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/pages/css/tasks.css') }}" rel="stylesheet" type="text/css"/>

@stop
@section('content')
<div class="portlet-title">

	<div class="tools">
		<a href="javascript:;" class="collapse">
		</a>
		<a href="#portlet-config" data-toggle="modal" class="config">
		</a>
		<a href="javascript:;" class="reload">
		</a>
		<a href="javascript:;" class="remove">
		</a>
	</div>
</div>
<div class="page-content-wrapper">
	<div class="page-content">
		
		@include('admin.partials._success')
		@include('admin.partials._failure')
		@include('errors.error')
		<!-- BEGIN PAGE HEADER-->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterHome') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Broadcaster</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">User Account</a>
				</li>
			</ul>

		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row margin-top-20">
			<div class="col-md-12">
				<!-- BEGIN PROFILE SIDEBAR -->
				<div class="profile-sidebar">
					<!-- PORTLET MAIN -->
					<div class="portlet light profile-sidebar-portlet">
						<!-- SIDEBAR USERPIC -->

						<div class="profile-userpic">

							@if($broadcaster->logo)
							<img src="{{ asset($broadcaster->logo) }}" class="img-responsive oldImg" alt="">
							@else
							<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" class="img-responsive oldImg" alt="">
							@endif
							<div id="preview">
								
							</div>
							<img src ="" id="thumb_img" class="img-responsive hide"/>


						</div>
						<!-- END SIDEBAR USERPIC -->
						<!-- SIDEBAR USER TITLE -->
						<div class="profile-usertitle">
							<div class="profile-usertitle-name">
								{{ $broadcaster->company_name }}
							</div>
							<div class="profile-usertitle-job">
								{{ $broadcaster->display_name }}
							</div>
						</div>
						<!-- END SIDEBAR USER TITLE -->
						<!-- SIDEBAR BUTTONS -->
						<div class="profile-userbuttons">
							<form action="{{ route('chanageImage') }}" role="form" enctype="multipart/form-data" method="post" id="imageform">
								<span class="btn btn-default btn-file">
									Change Image <input type="file" name="profile_image" id="profile_image">
								</span>
								<input type="hidden" value="{{ $broadcaster->broadcaster_id }}" name="broadcaster_id">
							</form>
						</div>


						<!-- END SIDEBAR BUTTONS -->
						<!-- SIDEBAR MENU -->
						<div class="profile-usermenu">
							<ul class="nav">
								<li>
									<a href="">
										<i class="icon-home"></i>
										Overview </a>
									</li>
									<li class="active">
										<a href="#">
											<i class="icon-settings"></i>
											Account Settings </a>
										</li>
										<li>
											<a href="#">
												<i class="icon-check"></i>
												Tasks </a>
											</li>
											<li>
												<a href="#">
													<i class="icon-info"></i>
													Help </a>
												</li>
											</ul>
										</div>
										<!-- END MENU -->
									</div>
									<!-- END PORTLET MAIN -->
									<!-- PORTLET MAIN -->
									<div class="portlet light">
										<!-- STAT -->
										<div class="row list-separated profile-stat">
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="uppercase profile-stat-title">
													{{$apps->count()}}
												</div>
												<div class="uppercase profile-stat-text">
													Apps
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="uppercase profile-stat-title">
													51
												</div>
												<div class="uppercase profile-stat-text">
													Tasks
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-6">
												<div class="uppercase profile-stat-title">
													61
												</div>
												<div class="uppercase profile-stat-text">
													Uploads
												</div>
											</div>
										</div>
										<!-- END STAT -->
										<div>

											
											<div class="margin-top-20 profile-desc-link">
												<i class="fa fa-globe"></i>
												<a href="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->website}}@endif">@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->website}}@endif</a>

											</div>
											<div class="margin-top-20 profile-desc-link">
												<i class="fa fa-envelope"></i>
												<a href="">@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->support_email}}@endif</a>
												
											</div>
											<div class="margin-top-20 profile-desc-link">
												<i class="fa fa-phone"></i>
												<a href="">@if(count($companyInfo->companyInfo)){{$companyInfo->phone}}@endif</a>
												
											</div>
											
										</div>
									</div>
									<!-- END PORTLET MAIN -->
								</div>
								<!-- END BEGIN PROFILE SIDEBAR -->
								<!-- BEGIN PROFILE CONTENT -->
								<div class="profile-content">
									<div class="row">
										<div class="col-md-12">
											<div class="portlet light">
												<div class="portlet-title tabbable-line">
													<div class="caption caption-md">
														<i class="icon-globe theme-font hide"></i>
														<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
													</div>
													<ul class="nav nav-tabs">
														<li class="active">
															<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
														</li>

														<li>
															<a href="#tab_1_2" data-toggle="tab">Broadcaster Details</a>
														</li>
														<li>
															<a href="#tab_1_3" data-toggle="tab">Change Password</a>
														</li>

													</ul>
												</div>
												<div class="portlet-body">
													<div class="tab-content">
														<!-- PERSONAL INFO TAB -->
														<div class="tab-pane active" id="tab_1_1">
															<form action="{{ route('profileStore') }}" method="post"  id="" novalidate="novalidate" >
																<div class="form-group">
																	<input type="hidden" value="@if(count($broadcasterDetails[0]->BroadcasterDetails)){{$broadcasterDetails[0]->BroadcasterDetails->broadcaster_details_id}}@endif" name="Broadcaster_Details_id" />
																	<label class="control-label">First Name</label>
																	<input type="text" placeholder="First Name" class="form-control" name="first_name" 
																	value="@if(count($broadcasterDetails[0]->BroadcasterDetails)){{$broadcasterDetails[0]->BroadcasterDetails->first_name}}@endif" />
																</div>
																<div class="form-group">
																	<label class="control-label">Last Name</label>
																	<input type="text" placeholder="Last Name" class="form-control" name="last_name" value="@if(count($broadcasterDetails[0]->BroadcasterDetails)){{$broadcasterDetails[0]->BroadcasterDetails->last_name}}@endif" />
																</div>
																<div class="form-group">
																	<label class="control-label">Mobile Number</label>
																	<input type="text" placeholder="+977 000-0000000" class="form-control" name="mobile" value="@if(count($broadcasterDetails[0]->BroadcasterDetails)){{$broadcasterDetails[0]->BroadcasterDetails->mobile}}@endif" />
																</div>

																<div class="form-group">
																	<label class="control-label">Address</label>
																	<input type="text" placeholder="address" class="form-control" name="address" value="@if(count($broadcasterDetails[0]->BroadcasterDetails)){{$broadcasterDetails[0]->BroadcasterDetails->address}}@endif" />
																</div>

																<div class="margiv-top-10">

																	<button type="submit" id="register-submit-btn" class="btn btn-success uppercase">Save</button>


																</div>
																<input type="hidden" value="{{ $broadcaster->broadcaster_id }}" name="broadcaster_id">
															</form>
														</div>
														<!-- END PERSONAL INFO TAB -->

														<!-- CHANGE PASSWORD TAB -->
														<div class="tab-pane" id="tab_1_3">
															<form action="{{ url('profile/changepassword') }}" method="POST">
																<div class="form-group">
																	<label class="control-label">Current Password</label>
																	<input type="password" class="form-control" name="oldPassword" />
																</div>
																<div class="form-group">
																	<label class="control-label">New Password</label>
																	<input type="password" class="form-control" name="password" />
																</div>
																<div class="form-group">
																	<label class="control-label">Re-type New Password</label>
																	<input type="password" class="form-control" name="password_confirmation" />
																</div>


																<div class="margin-top-10">
																	<button class="btn green-haze">Change Password</button> 
																	<a href="#" class="btn default">
																		Cancel </a>
																	</div>
																</form>
															</div>
															<!-- END CHANGE PASSWORD TAB -->
															<!-- broadcaster TAB -->
															<div class="tab-pane" id="tab_1_2">
																<form action="{{ route('storeCompanyInfo')}}" method="POST" id="form_sample_2" novalidate="novalidate" >
																	<div class="form-group">
																		<label class="control-label">Company Name</label>


																		<input type="text" class="form-control" name="company_name" placeholder="Company Name" value="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->company_name}}@endif" required />

																	</div>
																	<div class="form-group">
																		<label class="control-label">Company Address</label>
																		<input type="text" class="form-control" name="company_address" placeholder="Company Address" value="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->company_address}}@endif"/>
																	</div>
																	<div class="form-group">
																		<label class="control-label">Support Email</label>
																		<input type="text" class="form-control" name="support_email" placeholder="Support Email" 
																		value="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->support_email}}@endif"/>
																	</div>
																	<div class="form-group">
																		<label class="control-label">Register Name</label>
																		<input type="text" class="form-control" name="register_name" placeholder="Register Name"  value="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->register_name}}@endif"/>
																	</div>
																	<div class="form-group">
																		<label class="control-label">Website</label>
																		<input type="text" class="form-control" name="website" placeholder="Website" value="@if(count($companyInfo->companyInfo)){{$companyInfo->companyInfo->website}}@endif" />
																	</div>


																	<div class="margin-top-10">
																		<button class="btn green-haze">save</button> 
																		<a href="#" class="btn default">
																			Cancel </a>
																		</div>
																	</form>
																</div>
																<!-- END broacaster TAB -->

															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


								@stop

								@section('postFoot')
								<script type="text/javascript" src="{{ asset('assets/js/jquery.form.js') }}"></script>
								<script type="text/javascript" src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>

								<script type="text/javascript" src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"></script>
								<script src="{{ asset('assets/admin/pages/scripts/form-validation.js') }}"></script>

								<script type="text/javascript" >

									(function($){
										$('#profile_image').live('change', function()			{ 
																//$("#preview").html('');
																//alert($('#profile_image').val());
																if($('#profile_image').val() !== "" ) {
																	$(".oldImg").hide();
																	$("#preview").html('Image uploading...........');
																	$("#imageform").ajaxForm({
																		target: '#preview', 
																		success : function (response) {
																			//alert(response);
																			$('#preview').hide();
																			
																			$('#thumb_img').attr('src',response);
																			$('#thumb_img').removeClass('hide');
																			
																		}
																	}).submit();
																}
															});

									})(jQuery);
									FormValidation.init();

								</script>
								@stop

