@extends('admin.login')

@section('content')
<div class="portlet-title">

    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="#portlet-config" data-toggle="modal" class="config">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<form class="login-form" action="{{ url('broadcaster/store') }}" method="post" class="register-form" novalidate="novalidate" id="form_sample_2">
 {!! csrf_field() !!}

 <div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        You must fill all records. Please check below.
    </div>
    <h3>Sign Up</h3>
    <div class="form-group" id="user">
        <label class="control-label visible-ie8 visible-ie9">user Name <span class="required">
            * </span> </label>
            <div class="input-icon right">
                <i class="fa"></i>
                <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="username" required id="userName" />
                <div id="checkUser">

                </div>
            </div> 
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email<span class="required">
                * </span></label>

                <div class="input-icon right">
                    <i class="fa"></i>
                    <input class="form-control placeholder-no-fix" value="{{ old('email') }}" type="email" placeholder="Email" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password <span class="required">
                    * </span></label>
                    <div class="input-icon right">
                        <i class="fa"></i>
                        <input class="form-control placeholder-no-fix" type="password" placeholder="Password" name="password" required id="password">
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">CPassword <span class="required">
                        * </span></label>
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <input class="form-control placeholder-no-fix" type="password" placeholder="ReType-Password" name="password_confirmation" data-match="#password" required>
                        </div>
                    </div>
                    <h3>Broadcaster Information</h3>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Company Name <span class="required">
                            * </span></label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" name="company_name" value="{{ old('company_name') }}" class="form-control" placeholder="company name" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Display Name <span class="required">
                                * </span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" name="display_name" value="{{ old('display_name') }}" class="form-control" placeholder="display name" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Phone <span class="required">
                                    * </span></label>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input class="form-control placeholder-no-fix" type="text" placeholder="Phone" name="phone" value="{{ old('phone') }}" required>
                                    </div>
                            </div>


                                <div class="form-group">

                                    <label class="control-label visible-ie8 visible-ie9">Country <span class="required">
                                        * </span></label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            {!!Form::select('country_code',[""=>"Select Country"]+$country,null,['class'=>'form-control','id'=>'select2_sample4','required'])!!}
                                        </div>
                                </div>
                                 <div class="form-group">

                                    <label class="control-label">Is Oem/Is ssingle Channel <span class="required">
                                        * </span></label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="checkbox" name="is_oem" value="1">is_oem<br />
                                            <input type="checkbox" name="is_worldtv" value="0">is_channel
                                        </div>
                                </div>

                                <div class="form-actions">
                                        <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
                                    </div>
                                </div>
                            </form>


                            @stop

                            @section('postFoot')
<script>
    (function($){

        $('#userName').on('blur',function(){
         $.ajax({
            url: "{{route('checkUserAvailable')}}",
            data:'username='+$("#userName").val(),
            type: "POST",
            success:function(data){
             $('#checkUser').html(data.message);
             if(data.flag){
                $('#checkUser').siblings('.fa').addClass('fa-check');
            }
            else{
                $('#checkUser').siblings('.fa').addClass('fa-warning');
           // $('#user').removeClass('has-success');

           $('#user').addClass('has-error');
       }
       
       console.log(data.message);
   },
   error:function (){}
});

});

})(jQuery);


</script>


@stop