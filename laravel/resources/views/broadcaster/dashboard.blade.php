@extends('broadcaster.master')
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-android"></i>Apps
                    </div>
                </div>
                <div class="portlet-body">
                    @if(\Auth::user()->type == 'broadcaster')
                        @if(\Auth::user()->broadcaster->is_oem)
                            @include('broadcaster.partials._failure')
                            @include('apps.partials._cart')
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            @if(\Auth::user()->type=='broadcaster')
                                                <a href="{{url('apps/create')}}" class="btn btn-success">Add New </a>
                                            @else
                                                <a href="{{url('admin/broadcaster/'.$id.'/apps/create')}}"
                                                   class="btn btn-success">Add New </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet">

                                <div class="portlet-body table-striped table-responsive">

                                    <table class="table" id="sample_1">
                                        <thead>

                                        <tr>
                                            <th class="table-checkbox">
                                                <input type="checkbox" class="group-checkable"
                                                       data-set="#sample_1 .checkboxes"/>
                                            </th>
                                            <th>
                                                Name
                                            </th>

                                            <th>
                                                Created Date
                                            </th>
                                            <th>
                                                App Code
                                            </th>

                                        </tr>

                                        </thead>
                                        <tbody>

                                        @foreach($apps as $app)


                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    <div class="app_name" data-id="{{$app->app_id}}"
                                                         contenteditable="true">
                                                        {{$app->app_name}}
                                                    </div>
                                                </td>

                                                <td>
                                                    {{$app->created_at}}
                                                </td>

                                                <td>
                                                    {{$app->app_code}}
                                                </td>

                                                @if($app->status && $app->expire_at > date('Y-m-d H:i:s') || $app->expire_at == '0000-00-00 00:00:00' )
                                                    <td>

                                                        <a href="{{url("broadcaster/app/$app->app_id")}}"
                                                           class="label label-lg label-primary">Dashboard</a>

                                                    <td>
                                                        @if(\Auth::user()->type=='admin')
                                                            <a class="label label-lg label-primary btn-notes-add"
                                                               data-id="{{$app->app_id}}"
                                                               data-href="{{ route('statusNotes',$app->app_id)}}"
                                                               data-toggle="modal" data-target=".bs-example-modal-lg">Approve</a>
                                                        @else
                                                            <label class="label label-lg label-primary btn-notes-add">Approve</label>
                                                    @endif
                                                    <!-- <a data-btnOkLabel="ok" class="label label-lg label-danger" data-toggle="confirmation" data-placement="left" data-href="{{route('appStatus',$app->app_id)}}">Disable</a> -->

                                                    </td>
                                                    </td>
                                                @elseif(Auth::user()->subscribed('main'))
                                                    <td>
                                                        <a href="{{url("broadcaster/app/$app->app_id")}}"
                                                           class="label label-lg label-primary">Dashboard</a>
                                                    </td>
                                                @else
                                                    <td>
                                                        <a href="{{url('apps/analysis-step/'.$app->app_id)}}"
                                                           data-toggle="tooltip" data-placement="top"
                                                           title="Payment not confirmed"
                                                           class="label label-lg label-primary">Dashboard</a>
                                                    </td>

                                                    <td>

                                                    <!-- <a data-btnOkLabel="ok" data-toggle="confirmation" data-placement="left" data-href="{{route('appStatus',$app->app_id)}}"class="label label-lg label-primary">Approve</a> -->
                                                        @if(\Auth::user()->type=='admin')

                                                            <a class="label label-lg label-danger btn-notes-add"
                                                               data-id="{{$app->app_id}}"
                                                               data-href="{{ route('statusNotes',$app->app_id)}}"
                                                               data-toggle="modal" data-target=".bs-example-modal-lg">Disable</a>
                                                        @else
                                                            <label class="label label-lg label-danger btn-notes-add">Disable</label>
                                                        @endif
                                                    </td>

                                                @endif
                                                <td>

                                                    <a class="label label-lg label-primary btn-notes"
                                                       data-href="{{ route('viewNotes',$app->app_id)}}"
                                                       data-toggle="modal" data-target=".bs-example-modal-lg">View
                                                        Notes</a>
                                                </td>
                                                <td>

                                                    <a class="label label-lg label-primary btn-view"
                                                       data-href="{{ route('paymentIndex',$app->app_id)}}"
                                                       data-toggle="modal" data-target=".bs-example-modal-lg">payment
                                                        history</a>
                                                </td>
                                                <td>

                                                    @if($app->expire_at < date('Y-m-d H:i:s'))
                                                        <a data-href=""
                                                           href="{{ url('broadcaster/'.$app->app_id.'/renew')}}"
                                                           class="label label-lg label-danger btn-renew">Renew</a>
                                                    @elseif(Auth::user()->subscribed('main'))
                                                        <a class="label label-lg label-success btn-renew">Running</a>
                                                    @else
                                                        <a class="label label-lg label-success btn-renew">Running</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{url('broadcaster/app/'.$app->app_id.'/change-logo')}}"
                                                       class="btn btn-default">Change Logo</a>

                                                </td>
                                                <td>
                                                    <input type="hidden" name="serverKey"
                                                           class="serverKey{{$app->app_id}}"
                                                           value="{{ $app->app_key }}"/>
                                                    <a href="#" data-toggle="modal" data-target="#serverKeyModal"
                                                       class="btn btn-default server" id="s"
                                                       data-id="{{ $app->app_id }}">server
                                                        key</a>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->

                        @endif
                    @endif

                </div>

                @if(\Auth::user()->broadcaster->is_worldtv)
                    <h3>
                        <a href="{{url('broadcaster/channels')}}" class="btn btn-danger">List Of Channel</a>
                    </h3>
                @endif

            <!-- END CONTENT -->


                <div class="modal fade bs-example-modal-lg" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Payment Details</h4>
                            </div>
                            <div class="modal-body" class="scroller" style="max-height:500px;overflow:auto;">
                                Modal body goes here
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


                <!-- Server key Modal -->
                <div id="serverKeyModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Set your firebase server key here</h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" name="appKey" id="appKey" class="form-control"
                                       placeholder="server key"/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success keySubmit" data-dismiss="modal">
                                    save
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                @stop

                @section('postFoot')

                    <script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"
                            type="text/javascript"></script>

                    <script>
                        var app_id = '';
                        $('.server').click(function () {
                            app_id = $(this).data('id');
                            $("#appKey").val($(".serverKey" + app_id).val());

                        });
                        $('.keySubmit').click(function () {
                            $.ajax({
                                url: "{{ url('broadcaster/serverkey') }}",
                                data: {
                                    'appId': app_id,
                                    'appKey': $("#appKey").val()
                                },
                                type: "POST",
                                success: function () {
                                    $(".serverKey" + app_id).val($("#appKey").val());
                                }
                            });
                        });
                    </script>
                    <script>
                        (function ($) {
                            $('.btn-view').on('click', function () {
                                $('.modal-body').load($(this).data('href'));
                                $('.modal-title').html("Payment Details");
                            });

                            $('.btn-notes').on('click', function () {
                                $('.modal-body').load($(this).data('href'));
                                $('.modal-title').html("View Notes");
                            });
                            $('.btn-renew').on('click', function () {
                                $('.modal-body').load($(this).data('href'));
                                $('.modal-title').html("View Notes");
                            });

                            $('.btn-notes-add').on('click', function () {
                                // var this = $(this);
                                var id = $(this).data('id');
                                var href = $(this).data('href');

                                $.ajax({
                                    url: "{{route('checkAppPayment')}}",
                                    data: 'id=' + id,
                                    type: "POST",
                                    success: function (data) {

                                        if (data.flag) {
                                            $('.modal-body').html(' ');
                                            $('.modal-title').html(data.message);
                                            $('.modal-body').load(href);

                                        }
                                        else {
                                            $('.modal-title').html(data.message);
                                            $('.modal-body').html(' ');

                                            console.log(data.message);


                                        }

                                    },
                                    error: function (err) {
                                        console.log(err);
                                    }
                                });

                                // $('.modal-body').load($(this).data('href'));
                                // $('.modal-title').html("Add notes");
                            });
                            $('body').on('click', 'table tr td  button.btn-status', function () {
                                var url = $(this).attr('data-href');
                                $.ajax({
                                        url: url, success: function (result) {
                                            console.log(result);
                                        },
                                        error: function (request, status, error) {
                                            console.log(request.responseText);
                                        }
                                    }
                                );
                            });

                        })(jQuery);

                        // UIConfirmations.init();

                    </script>
@stop