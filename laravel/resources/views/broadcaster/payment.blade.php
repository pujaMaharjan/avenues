@extends('broadcaster.master')
@section('postHead')
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')
<div class="page-content-wrapper" ng-app="broadcasterApp" ng-controller="PaymentCtrl">

        <div class="page-content">
        <div class="portlet">
                       
                    <label for=""><h3>Choose Payment Method</h3></label>
                           
                    <select class="form-control" name="payment_method" ng-model="payment_method" id="payment_method">
                        <option  value="">--Choose Payment--</option>
                        <option  value="bank_payment">Bank</option>
                        <option  value="paypal_payment">Paypal</option>
                        <option  value="stripe_payment">Credit Card</option>
                    </select>    
                    
                    <?php $total_price=0; ?>
                      
                <div class="portlet-body">
           
                 <!-- <h2>Broadcaster Need To charge</h2> -->
                  
                 <table class="table table-borderd table-hover">
                     <tr>
                         <td>BroadCaster Services Fees</td>
                         <td>{{$payment_details->total_charge}}</td>
                     </tr>
                 </table>
                     
                   
                       <div ng-show="payment_method=='paypal_payment'" class="box green">
                           
                           <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

                            <!-- Identify your business so that you can collect the payments. -->
                            <input type="hidden" name="business" value="info@codexworld.com">
                            
                            <!-- Specify a Buy Now button. -->
                            <input type="hidden" name="cmd" value="_xclick">
                            
                            <!-- Specify details about the item that buyers will purchase. -->
                            <input type="hidden" name="item_name" value="Broadcaster Register">
                            <input type="hidden" name="item_number" value="1">
                            <input type="hidden" name="amount" value="1">
                            <input type="hidden" name="currency_code" value="USD">
                            
                            <!-- Specify URLs -->
                            <input type='hidden' name='cancel_return' value='http://localhost/mine/pay/cancel.php'>
      <input type='hidden' name='return' value='{{ url('broadcaster/paypal') }}'>

                            
                            <!-- Display the payment button. -->
                            <input type="image" name="submit" border="0"
                            src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
                            <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                        
                        </form> 
                           
                            <button class="btn btn-danger btn-lg" onClick="document.paypalform.submit();">Paypal</button>
                                                      
                       </div>

                        <div ng-show="payment_method=='bank_payment'" class="box green">
                            <form action = "{{ url('broadcaster/gateway/') }}" method = "post" enctype="multipart/form-data">
                            <input type="hidden" name="gateway"  value="bank" />
                                    <div class="form-group">
                                        <label for="">Bank Name</label>
                                        <input type="text" name="bank_name" class="form-control">
                                    </div>
                                    <input type="hidden" name="total" value="{{$payment_details->total_charge}}">
                                     <div class="form-group">
                                        <label for="">Attachemnt (For Proved)</label>
                                        <input type="file" name="bank_credential" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">...</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                   
                                    <div class="form-group">
                                        <button class="btn btn-danger btn-success">Submit</button>
                                    </div>
                            </form>
                        </div>

                        <div ng-show="payment_method=='stripe_payment'" class="box green">
                            <form action="{{url('broadcaster/gateway')}}" method="POST">
                            <input type="hidden" name="gateway" value="stripe" />
                             <input type="hidden" name="amount" value="100" />
                            <script
                              src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                              data-key="pk_test_voimralm3mu3DSaeRqK6qI8F"
                              data-amount="100"
                              data-name="Demo Site"
                              data-description="Widget"
                              data-image="/img/documentation/checkout/marketplace.png"
                              data-locale="auto">
                            </script>
                          </form>
                        </div>
                    
                </div>

            </div>
        
        <!-- <a href="{{url('broadcaster/cart/')}}" class="btn btn-danger">Back To the cart Page</a>    -->
</div>
</div>
@stop
@section('postFoot')

    <script type="text/javascript">
     angular.module("broadcasterApp",[])
     .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
     })
      .controller('PaymentCtrl',function($scope){

      });
    </script>
@stop