
<!-- BEGIN SIDEBAR -->

<div class="page-sidebar-wrapper">
	<!-- BEGIN HORIZONTAL RESPONSIVE MENU -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<!-- DOC: This is mobile version of the horizontal menu. The desktop version is defined(duplicated) in the header above -->
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
				<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<form class="sidebar-search sidebar-search-bordered" action="extra_search.html" method="POST">
					<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn submit"><i class="icon-magnifier"></i></button>
						</span>
					</div>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
			@if(Auth::user()->type != "admin")
			<li class="{{ setBroadcasterActive('/') }}">
				<a href="{{ route('broadcasterHome') }}">
					<i class="icon-home"></i>
					Dashboard <span class="selected">
				</span>
			</a>
		</li>
		@else
	
		<li class="">
				<a href="{{url('admin')}}">
					<i class="icon-home"></i>
					Dashboard <span class="selected">
				</span>
			</a>
		</li>
		@endif
		<li class="{{setBroadcasterActive('/'.$broadcaster->broadcaster_id.'/unit')}}">
			<a>
				<i class="icon-settings"></i>
				Broadcaster Config <span class="arrow">
			</span>
		</a>
		<ul class="sub-menu">
			<li class="{{setBroadcasterActive('/'.$broadcaster->broadcaster_id.'/unit/admob')}}">
				@if(\Auth::user()->type == 'admin')
				<a href="{{ route('unitBroadcasterIndex',$broadcaster->broadcaster_id) }}">
					@else
					<a href="{{ route('broadcasterAdmob',$broadcaster->broadcaster_id)}}">
						@endif

						<i class=""></i>
						AdMob <span class="arrow">
					</span>
				</a>	

			</li>


		</ul>
	</li>
</ul>
</div>
<!-- END HORIZONTAL RESPONSIVE MENU -->
</div>
	<!-- END SIDEBAR -->