<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
	<ul class="nav navbar-nav pull-right">
		
<li class="dropdown dropdown-user">
	<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

		<span class="username username-hide-on-mobile">
			{{ $broadcaster->display_name }} 
		</span>


		@if($broadcaster->logo)
		<img src="{{ asset($broadcaster->logo) }}" class="img-circle" alt="">
		@else
		<i class="icon-user"></i>
		@endif

		<i class="fa fa-angle-down"></i>

	</a>

	<ul class="dropdown-menu dropdown-menu-default">
		@if(Auth::user()->type != "admin")
		<li>
			<a href="{{ route('broadcasterProfile') }}">
				<i class="icon-user"></i> My Profile </a>
			</li>
			@endif
			<li>
				<a href="{{ route('logout') }}">
					<i class="icon-key"></i> Log Out </a>
				</li>
				@if( Session::has('orig_user') )
				<li>
					<a href="{{ url('admin/user/switch/stop') }}"><i class="icon-logout""></i> Switch back </a>
				</li>
				@endif
			</ul>
		</li>
	</ul>
</div>
