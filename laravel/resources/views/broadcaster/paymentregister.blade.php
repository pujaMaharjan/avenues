<div class="table-responsive">
<span class="btn btn-info"> {{$registerStep->name}}</span>
  <div class="clearfix">
      </div>

      @if($payment)
    <table class="table table-striped table-hover table-bordered">
       <thead>
           <tr>
               <th>Payment Gateway</th>
               <td>{{$payment->payment_gateway}}</td>
           </tr>
           <tr>
               <th>Payment Status</th>
               <td>{{$payment->payment_status}}</td>
           </tr>
           <tr>
            @if(isset($payment_details->payment_meta_data['bank_name']))
                            {{$payment_details->payment_meta_data['bank_name']}}
                        @endif
             <tH>Details</tH>
            @if(isset($payment->payment_meta_data['bank_name']))
             <td>{{$payment->payment_meta_data['bank_name']}}</td>
             @endif
           </tr>
           <tr>
             <th>Paid Information</th>
             <td><img src="{{asset('uploads/payments/'.$payment->paid_information)}}" alt="{{$payment->paid_information}}"></td>
           </tr>
   <tr>
     <th>Total Charge</th>
     <td>{{$payment->total_charge}}</td>
   </tr>
         

       </thead>
       @if($payment->payment_status == 0)
       <tfoot>
         <tr>
           <td><a href="{{route('approveBroadcaster',$broadcasterId)}}" class="btn bth-success">conform</a></td>
         </tr>
       </tfoot>
       @endif
   </table>
   @endif
</div>