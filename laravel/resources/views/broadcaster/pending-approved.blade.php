@extends('broadcaster.master')

@section('content')
<div class="page-content-wrapper">

        <div class="page-content">
        <div class="portlet">
         <div class="portlet-title">
                        <div class="caption">
                            You have Provided Payment Information
                        </div>
                    </div>   
        	<table class="table table-responsive table-bordered">
        		<tr>
                {{dd()}}
        			<td>GateWay</td>
        			<td>{{$payment_details->payment_gateway}}</td>
        		</tr>
        		<tr>
        			<td>Payment Status</td>
        			<td>
        				@if($payment_details->payment_status)
        					paid
						@else
							Unpaid
						@endif
        			</td>
        		</tr>
        		<tr>
        			<td>Payment Image</td>
        			<td><img src="{{ asset('uploads/payments/'.$payment_details->paid_information) }}" alt="" class="img-responsive" height="150" height="200"></td>
        		</tr>
        		<tr>
        			<td>Payment Information</td>
        			<td>
						<p>{{$payment_details->payment_meta_data['information_submit_date']}}</p>
						<p>
                        @if(isset($payment_details->payment_meta_data['bank_name']))
                            {{$payment_details->payment_meta_data['bank_name']}}
                        @endif
                        </p>
                        <p>
                        @if(isset($payment_details->payment_meta_data['txn_id']))
                            {{$payment_details->payment_meta_data['txn_id']}}
                        @endif
                        </p>
        			
        			</td>
        		</tr>
        	</table>
        </div>
        </div>
</div>
@stop