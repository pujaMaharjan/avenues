@extends('broadcaster.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Edit Unit Set
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				@include('errors.error')
				<form action="{{ route('unitBroadcasterUpdate',$units->add_unit_set_id)}}" class="horizontal-form" method="POST">
					<div class="form-body">

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name for Admob</label>
									<input type="text" id="name" class="form-control" placeholder="Name for Admob" name="name" value="{{ old('name',$units->name)}}">
									<span class="help-block">
									</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Broadcaster</label>

									{!!Form::select('app_id', [""=>"None"]+$apps,$units->app_id,['class'=>'select2me form-control','disabled'])!!}
									<input type="hidden" value="{{$authBroadcaster->broadcaster_id}}" name="broadcaster_id">
									<span class="help-block">
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							@foreach ($units->addUnit as $i=>$list)


							<div class="col-md-6">
								<div class="form-group">
									<div class="form-group">
										@foreach ($platformList as $platform)
										@if($platform->platform_id == $list->platform_id)
										<h3>{{$platform->platform_name}}</h3>
										@endif
										@endforeach
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">{{ $list->type }}</label>
									<input type="text" id="Banner" class="form-control" placeholder="Banner" name="img[]" value="{{ isset(old('img')[$i])?old('img')[$i]:$list->unit_name }}">
									<input type="hidden" name="type[]" id="" value="{{ $list->type }}">
									<input type="hidden" name="id[]" value="{{ $list->add_unit_id}}">
									<input type="hidden" value="{{$list->platform_id}}" name="platform_id[]" id="platform_id">
									<span class="help-block">
									</span>
								</div>
							</div>

							@endforeach
						</div>

					</div>
					<div class="form-actions right">

						<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
	@stop
