@extends('broadcaster.master')
@section('preHead')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@stop
@section('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      @if($units)
      <h3>{{ $units->app->app_name }}</h3>
      <div class="page-toolbar">
        <div class="btn-group pull-left">
          <form action="" class="" method="post">
           <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
            <span></span> <b class="caret"></b>
          </div>
        </form>
      </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
      <thead>
       <tr>
        <th>Type</th>
        <th>
          Earning
        </th>
      </tr>
    </thead>

    <tbody>
      @foreach ($units->addUnit as $i=>$list)

      <tr>

        <td>
          @foreach ($platformList as $platform)
          @if($platform->platform_id == $list->platform_id)
          {{$platform->platform_name}}
          @endif
          @endforeach
          {{ $list->type }}
        </td>
        

        <td>
          <i class="fa fa-jpy"></i>
          {{ $earning[$i] }}</td>
        </tr>

        @endforeach
        <tr>
          <td>Total Earning</td>
          <td>
            <i class="fa fa-jpy"></i>
            {{$total}}
          </td>
        </tr>
      </tbody>
    </table>
    <a href="{{ route('unitEaringDetails',$id) }}"><button type="button" class="btn default blue">View Details</button></a>



    @endif

  </div>

  @stop

  @section('postFoot')


  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

  <script>
    (function($) {

      function cb(start, end) {
        $('#reportrange span').html(start.format('YYYY,MMMM D') + ' - ' + end.format('YYYY,MMMM D'));
      }
      cb(moment().subtract(29, 'days'), moment());

      $('#reportrange').daterangepicker({
        ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       },
       change: function(){
        console.log("changed");

      }
    }, cb);
      $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        $start = picker.startDate.format('YYYY-MM-DD');
        $end = picker.endDate.format('YYYY-MM-DD');
        var url = $('from').attr('action');
        $.post( url,{from: $start, to: $end} ,function( data ) {
          var tr = '';
          var i = 0;
          var platform_name;
          $.each( data.earning, function( key, value ) {

            tr +='<tr>';
            
            $.each(data.platformList,function(key,v){
              if(v.platform_id == data.units[i].platform_id )
              {
               platform_name = v.platform_name;
             }
           });
            tr += '<td>'+platform_name+' '+data.units[i].type+'</td>';

            tr += '<td><i class="fa fa-jpy"></i> '+value+'</td>';
            
              //console.log(data.units[i]);
              
              tr += '</tr>';
              i++;
              //console.log(i);
            });
          tr +='<tr>'
          tr +='<td>'+'Total Earning'+'</td>';
          tr +='<td><i class="fa fa-jpy"></i> '+data.total+'</td>';

          tr +='</tr>'
          $('tbody').html(tr);
          
        });
      });



})(jQuery);

</script>
@stop
