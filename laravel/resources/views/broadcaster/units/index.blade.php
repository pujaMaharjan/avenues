@extends('broadcaster.master')
@section('content')


<div class="page-content-wrapper">
	<div class="page-content">

		@include('broadcaster.partials._success')
		
		@if(count($units))
		<div class="table-responsive"> 
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>App</th>
						<th>Name</th>
						<th>Android Banner</th>
						<th>Ios Banner</th>
						<th>Android Interstitial</th>
						<th>Ios Interstitial</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($units as $i=>$unit)
					<tr>
						<td>{{ $unit[0]->app->app_name }}</td>
						<td>{{ $unit[0]->name }}</td>
						@foreach ($unit[0]->addUnit as $list)
						<td>


							{{ $list->unit_name }}



						</td>
						@endforeach
						<td>

							@if($authUser->type == 'admin')


							<a href="{{route('unitBroadcasterEdit',[$authBroadcaster->broadcaster_id,$list->add_unit_set_id])}}" class="btn btn-icon-only blue">
								<i class="fa fa-edit"></i> 
							</a>

							@endif
							<a href="{{route('unitEarningBroadcaster',[$authBroadcaster->broadcaster_id,$list->add_unit_set_id])}}" class="btn btn-icon-only red">
								<i class="fa fa-share"></i> 
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@else 
		No data
		@endif

		@stop


