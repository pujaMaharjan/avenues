
<div class="form-body">


    <div class="form-group">
        <label>Service Name <span class="font-red alert font-size">{{$errors->first()}}</span></label>
        <div class="input-icon input-icon-sm">
         <div class="input-icon right">
            <i class="fa"></i>
            {!! Form::text('service_name',null,['class'=>'form-control','required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label>Description</label>
    <div class="input-icon input-icon-lg">

        {!! Form::textarea('description',null,['class'=>'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label >Price</label>
    <div class="input-icon right">
        <i class="fa"></i>
        {!! Form::text('price',null,['class'=>'form-control','required']) !!}
    </div>
</div>
<div class="form-group">
    <label>status</label>
    <div class="input-icon right">

        <i class="fa"></i>
        {!! Form::select('status',['Deactive','Active'],null,['class'=>'form-control','required']) !!}

    </div>
</div>
</div>

</div>