
@extends('admin.master')
@section('content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ route('broadcasterIndex') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Service</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                
            </ul>
            
        </div>
        <div class="col-md-12 col-sm-12 margin-top-20">
            <div class="row">
                <a href="{{url('admin/services/create')}}" class="btn btn-primary">Add New</a>
                <div class="portlet box green">
                  <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> List Of All Service
                    </div>
                    <div class="tools">
                        <a href="" class="collapse" data-original-title="" title="">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
                        </a>
                        <a href="" class="reload" data-original-title="" title="">
                        </a>
                        <a href="" class="remove" data-original-title="" title="">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <table class="table table-hover">
                        <tr>
                            <th>Name</th>
                            <th>Desciption</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        @foreach($services as $service)
                        <tr>
                            <td>{{$service->service_name}}</td>
                            <td>{{$service->description}}</td>
                            <td>{{$service->price}}</td>
                            <td>

                                <a href="{{url("admin/services/$service->service_id/edit")}}" class="btn btn-icon-only red">
                                    <i class="fa fa-edit"></i> 
                                </a>
                                <button class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{url("admin/services/$service->service_id/delete")}}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach


                    </table>
                </div>
            </div>
        </div>
    </div>
    @stop

    @section('postFoot')
    <script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>
    @stop