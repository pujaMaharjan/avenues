@extends('admin.master')
@section('content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ route('broadcasterIndex') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Services</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Add Services</a>
                </li>
            </ul>
            
        </div>
        <div class="col-md-12 col-sm-12 margin-top-20">
            <div class="row">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> Default Form
                        </div>
                        <div class="tools">
                            <a href="" class="collapse" data-original-title="" title="">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
                            </a>
                            <a href="" class="reload" data-original-title="" title="">
                            </a>
                            <a href="" class="remove" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form role="form" method="post" action="{{url('admin/services/store')}}" novalidate="novalidate" id="form_sample_2">
                            {{csrf_field()}}
                            @include('services._form')
                            <div class="form-actions">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="{{ url('admin/services/list')}}"><button type="button" class="btn default">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>


        @stop