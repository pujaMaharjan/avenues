<div class="table-responsive">

	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>name</th>
				<td>{{$radio->name}}</td>
			</tr>
			<tr>
				<th>sources_local_url</th>
				<td>{{$radio->sources_local_url}}</td>
			</tr>
			<tr>
				<th>sources_cdn_url</th>
				<td>{{$radio->sources_cdn_url}}</td>
			</tr>
			<tr>
				<th>description</th>
				<td>{{$radio->description}}</td>
			</tr>
			<tr>
				<th>category</th>
				<td>{{$radio->category}}</td>
			</tr>
			<tr>
				<th>url_token_key</th>
				<td>{{$radio->url_token_key}}</td>
			</tr>
			<tr>
				<th>valid_time</th>
				<td>{{$radio->valid_time}}</td>
			</tr>


		</thead>
	</table>
</div>