@extends('apps.master')
@section('content')



@if(isset($radio))
{!! Form::model($radio, array('url' => array("broadcaster/app/".$app_id."/services/radios/$id"),'files' => true, 'id'=>'form_sample_2','novalidate' =>'novalidate','method'=>'post')) !!}
<input type="hidden" name="_method" value="PATCH">
@else
{!! Form::open(array('url' => 'broadcaster/app/'.$app_id.'/services/radios','files' => true,'id' => 'form_sample_2', 'novalidate' =>'novalidate','method'=>'post')) !!}
@endif

<div>
  <h3>Add New Radio</h3>
  <div class="form-group" >
    <label class="control-label">Name</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('name', null,['class'=>'form-control','placeholder'=>'name']); !!}
    </div>
  </div>

  <div class="">
    <div class="form-group">
      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
      <label class="control-label">Url</label>
      <div class="input-icon right">
        <i class="fa"></i>
        {!! Form::text('sources_local_url', null,['class'=>'form-control','placeholder'=>'url']); !!}
      </div>
    </div>
    <div class="form-group">
      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
      <label class="control-label">sources_local_url</label>
      <div class="input-icon right">
        <i class="fa"></i>
        {!! Form::text('sources_cdn_url', null,['class'=>'form-control','placeholder'=>'sources_cdn_url']); !!}
      </div>
    </div>
    <div class="form-group">
      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
      <label class="control-label">Url Token Key</label>
      <div class="input-icon right">
        <i class="fa"></i>
        {!! Form::text('url_token_key', null,['class'=>'form-control','placeholder'=>'url_token_key']); !!}
      </div>
    </div>
    <div class="form-group">
      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->

      <label class="control-label">Valid Time</label>
      <div class="input-icon right">
        <i class="fa"></i>

        {!! Form::text('valid_time', null,['class'=>'form-control','placeholder'=>'valid_time']); !!}
      </div>
    </div>

  </div>

  <div class="form-group">
   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
   <label class="control-label">Movie Photo </label>
   @if(isset($radio))
   <div class="row">
    <div class="col-md-4">
      <img src="{{ asset('uploads/radios/'.$radio->logo) }}" width="100" height="150" alt="">
    </div>
  </div>
  @endif
  {!! Form::file('logo', null,['class'=>'form-control','placeholder'=>'logo']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">description</label>

  {!! Form::textarea('description', null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">Country</label>
  <div class="input-icon right">
    <i class="fa"></i>
    {!! Form::select('country_id', $countries, null, ['class'=>'form-control']); !!}
  </div>
</div>
<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Language</label>

 {!! Form::text('language', null,['class'=>'form-control','placeholder'=>'language']); !!}
</div>
<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Genere</label>

 {!! Form::text('genere', null,['class'=>'form-control','placeholder'=>'genere']); !!}
</div>


<div class="form-actions">
  <a href="{{url('/')}}" class="btn btn-default">Back</a>
  <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>
</div>
@stop
@section('postFoot')

@stop