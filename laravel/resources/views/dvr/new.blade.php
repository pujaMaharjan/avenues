@extends('apps.master')
@section('content')
    <style>
        .hls, .rmtp, .satellite, .youtube {
            display: none;
        }
    </style>

    <form class="login-form" id="" action="{{ url("broadcaster/app/$app_id/services/dvr") }}"
          enctype="multipart/form-data" method="post" class="register-form" novalidate="novalidate">
        {!! csrf_field() !!}

        <h3>Add New Dvr</h3>
        <div class="form-group">
            <label class="control-label">Name*</label>
            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="name"/>
        </div>
        <div class="form-group">
            <label class="control-label">Url*</label>
            <input type="text" name="url" value="{{ old('url') }}" class="form-control" placeholder="url"/>
        </div>
        <div class="form-group">
            <label class="control-label">Created by*</label>
            <input type="text" name="created_by" value="{{ old('created_by') }}" class="form-control"
                   placeholder="Created by"/>
        </div>
        <div class="form-group">
            <label class="control-label">Option</label>
            <input type="text" name="option" value="{{ old('option') }}" class="form-control" placeholder="option"/>
        </div>
        <div class="form-actions">
            <a href="{{url('/')}}" class="btn btn-default">Back</a>
            <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>
@stop