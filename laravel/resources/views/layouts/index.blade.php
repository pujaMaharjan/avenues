@extends('admin.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Layout</a>
					<i class="fa fa-angle-right"></i>
				</li>

			</ul>

		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">

				<div>
					<a href="{{url('admin/layouts/create')}}" class="btn btn-primary">Create</a>
				</div>
				<table class="table table-bordered">
					<tr>
						<th>Sn</th>
						<th>Name</th>
						<th>Status</th>
						<th>Screen Shots</th>
						<th>Action</th>
					</tr>
					@foreach($layouts as $i=>$layout)
					<tr>
						<td>{{++$i}}</td>
						<td>{{$layout->layout_name}}</td>
						<td>{{$layout->layout_status}}</td>
						<td><img src="{{asset($layout->screen_shot)}}" height="100px" width="50px"></td>
						<td>
							<a href="{{url('admin/layouts/'.$layout->layout_id.'/edit')}}" class="btn btn-success"> <i class="fa fa-edit"></i></a>
							<a class="btn btn-danger" data-toggle="confirmation" data-placement="left" data-href="{{url('admin/layouts/'.$layout->layout_id.'/delete')}}"><i class="fa fa-times"></i></a>
							
						</td>
					</tr>
					@endforeach
				</table>

			</div>
		</div>
	</div>
</div>


@stop

@section('postFoot')
<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>

<script>




					// UIConfirmations.init(); 	
				</script>
				@stop
