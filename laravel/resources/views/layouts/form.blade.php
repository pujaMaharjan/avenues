@extends('admin.master')
@section('content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{ route('broadcasterIndex') }}">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Layout</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Add Layout</a>
                </li>
            </ul>
            
        </div>
        <div class="col-md-12 col-sm-12 margin-top-20">
            <div class="row">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> Layout Form
                        </div>
                        <div class="tools">
                            <a href="" class="collapse" data-original-title="" title="">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
                            </a>
                            <a href="" class="reload" data-original-title="" title="">
                            </a>
                            <a href="" class="remove" data-original-title="" title="">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($layout))
                        {!! Form::model($layout, array('route' => array('admin.layouts.update', $layout->layout_id),'method' => 'patch','files' => true)) !!}
                        @else
                        {!! Form::open(array('route' => 'admin.layouts.store','files' => true)) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label >Layout Name</label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    {!! Form::text('layout_name',null,['class'=>'form-control','required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>status</label>
                                <div class="input-icon right">

                                    <i class="fa"></i>
                                    {!! Form::select('status',['Deactive','Active'],null,['class'=>'form-control','required']) !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label>Screen Shot</label>
                                <div class="input-icon right">

                                    <i class="fa"></i>
                                    <input type="file" name="screen_shot"></input>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="input-icon right">

                                    <i class="fa"></i>
                                    <button class="btn btn-success">Save</button>

                                </div>
                            </div>
                        </form>
                    </div>



                </div>
            </div>

        </div>
    </div>


    @stop