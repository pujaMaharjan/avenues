@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<a href="{{url("broadcaster/app/$id/services/vod/create")}}" class="btn btn-danger">Add New Vod</a>

<div class="portlet box blue-madison">
 <div class="portlet-title">
  <div class="caption">
   <i class="fa fa-globe"></i>List Of News
 </div>
 <div class="tools">
   <a href="javascript:;" class="reload">
   </a>
   <a href="javascript:;" class="remove">
   </a>
 </div>
</div>
<div class="portlet-body">
  <table class="table table-striped table-bordered table-hover" id="sample_3">
    <thead>
      <tr>
       <th>youtube_channel_id</th>
       <th>feature_playlist</th>

       <th>Action</th>
     </tr>
   </thead>
   <tbody>

    @foreach($vods as $vod)
    <tr>
     <td>{{$vod->youtube_channel_id}}</td>
     <td>{{$vod->feature_playlist}}</td>

     <td>
       
      <span type="button" data-href="{{url("broadcaster/app/$id/services/vod/show/$vod->app_vod_id")}}"
       data-toggle="modal" data-target="#myModal" class="btn btn-success btn-view"><i class="fa fa-book"></i></span>

       <a href="{{url("broadcaster/app/$id/services/vod/$vod->app_vod_id/edit")}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
       <a data-href="{{ url('broadcaster/app/'.$id.'/services/vod/delete/'.$vod->app_vod_id) }}" data-toggle="confirmation" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
     </td>
   </tr>
   @endforeach
 </tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->


</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of Vod</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>




    @stop
    @section('postFoot')


    <script>
      (function($){
        $('.btn-view').on('click',function(){
          $('.modal-body').load($(this).data('href'));
        });

      })(jQuery);
    </script>
    @stop
