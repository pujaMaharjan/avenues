@extends('apps.master')
@section('content')

    {{--<form class="login-form" action="{{ url("broadcaster/app/$id/channels/upadte/$channel_id") }}" enctype="multipart/form-data" method="post" class="register-form" novalidate="novalidate">--}}
    {!! Form::model($vod, array('route' => array("broadcaster.app.{app_id}.services.vod.update",$id, $vod->app_vod_id),'method'=>'PATCH')) !!}
   
      <h3>Edit Vod</h3>
            <div class="form-group">
                <label class="control-label">youtube channel id</label>

                {!! Form::text('youtube_channel_id', null,['class'=>'form-control','placeholder'=>'youtube_channel_id']); !!}
            </div>
            <div class="form-group">
                <label class="control-label">feature playlist</label>

                {!! Form::text('feature_playlist', null,['class'=>'form-control','placeholder'=>'feature_playlist']); !!}
            </div>
            <div class="form-group">
                <label class="control-label">playlist</label>

                {!! Form::text('videos_count[playlist]', null,['class'=>'form-control','placeholder'=>'playlist']); !!}
            </div>
            <div class="form-group">
                            <label class="control-label">latest</label>

                            {!! Form::text('videos_count[lasted]', null,['class'=>'form-control','placeholder'=>'lasted']); !!}
            </div>


            <div class="form-group">
                    <label class="control-label">featured</label>

                    {!! Form::text('videos_count[featured]', null,['class'=>'form-control','placeholder'=>'featured']); !!}
            </div>
              <div class="form-group">
                        <label class="control-label">popular</label>

                        {!! Form::text('videos_count[popular]', null,['class'=>'form-control','placeholder'=>'popular']); !!}
                        </div>



        <div class="form-actions">
            <a href="{{url('/')}}" class="btn btn-default">Back</a>
            <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form>

@stop