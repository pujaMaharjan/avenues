<div class="table-responsive">

	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>youtube_channel_id</th>
				<td>{{$vod->youtube_channel_id}}</td>
			</tr>
			<tr>
				<th>feature_playlist</th>
				<td>{{$vod->feature_playlist}}</td>
			</tr>
			<tr>
				<th>videos_count</th>
				<td>@foreach($vod->videos_count as $list)
					{{$list}},
					@endforeach</td>
				</tr>


			</thead>
		</table>
	</div>