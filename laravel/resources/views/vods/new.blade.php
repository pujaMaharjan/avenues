@extends('apps.master')
@section('content')

<form class="login-form" id="" action="{{ url("broadcaster/app/$id/services/vod") }}"  method="post" class="register-form" novalidate="novalidate">
    {!! csrf_field() !!}


    <h3>Add New Vod</h3>
    <div class="form-group">
        <label class="control-label">youtube channel id*</label>
        <input type="text" name="youtube_channel_id" value="{{ old('youtube_channel_id') }}" class="form-control" placeholder="youtube_channel_id" />
    </div>

    <div class="form-group">
        <label class="control-label">feature playlist*</label>
        <input type="text" name="feature_playlist" value="{{ old('feature_playlist') }}" class="form-control" placeholder="feature_playlist" />
    </div>
    <div class="form-group">
        <label class="control-label">playlist*</label>
        <input type="text" name="videos_count[playlist]" value="{{ old('playlist') }}" class="form-control" placeholder="playlist" />
    </div>
    <div class="form-group">
     <label class="control-label">latest*</label>
     <input type="text" name="videos_count[lasted]" value="{{ old('lasted') }}" class="form-control" placeholder="lasted" />
 </div>
 <div class="form-group">
   <label class="control-label">featured*</label>
   <input type="text" name="videos_count[featured]" value="{{ old('featured') }}" class="form-control" placeholder="featured" />
</div>
<div class="form-group">
   <label class="control-label">popular*</label>
   <input type="text" name="videos_count[popular]" value="{{ old('popular') }}" class="form-control" placeholder="popular" />
</div>


<div class="form-actions">
    <a href="{{url('/')}}" class="btn btn-default">Back</a>
    <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>

@stop
@section('postFoot')


<script>
    (function($){
        $('.btn-view').on('click',function(){
            $('.modal-body').load($(this).data('href'));
        });
    })(jQuery);
</script>
@stop
