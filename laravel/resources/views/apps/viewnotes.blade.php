

@if($notes)
<table class="table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Details</th>
			<th>Created Date</th>
		</tr>
	</thead>
	<tbody>
		
		@foreach($notes as $note)

		<tr>
			
			<td>{{$note->title_notes}}</td>
			<td>{{$note->app_status_change_notes}}</td>
			<td>{{$note->created_at}}</td>
		</tr>

		



		@endforeach

	</tbody>
	<tfoot>
		<tr>
			<td>
			<button class="btn">
			<a href="{{route('pdfPost',$appId)}}" target="_blank">

			Save To Pdf</a></button></td>
		</tr>
	</tfoot>
</table>
@endif