@extends('apps.master')
@section('content')
<form action="{{url('broadcaster/app/'.$app_id.'/splashimage')}}" enctype="multipart/form-data" method="post">
	<div class="form-group">
		<label for="">Logo</label>
		<input type="file" name="splashimage" >
	</div>
	<button type="submit" class="btn blue">Update</button>
</form>
@stop