<form action="{{route('appNotePost',$appId)}}" method="POST" id="form_sample_2" novalidate="novalidate">
	<div class="form-body">
		<div class="form-group">
			<label>Note Title </label>
			<div class="input-icon input-icon-sm">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="noteTitle" class="form-control" id="noteTitle">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Note Details</label>
			<div class="input-icon input-icon-lg">

				<textarea rows="5" cols="23" class="form-control" name="noteDetails"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label>Set Up Expire Date</label>
			<div class="input-icon input-icon-sm">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="exipre_at" class="form-control date-picker" id="expirre_at">
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-actions">
		<button type="submit" class="btn blue">Submit</button>
		<a href=""><button type="button" class="btn default">Cancel</button></a>
	</div>


</form>