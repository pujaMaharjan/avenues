<style type="text/css">
 td, th {    
    border: 1px solid #ddd;
  width: 300px;
  margin: auto;
  padding: 10px;
}

table {
    border-collapse: collapse;
    width: 900px;
    border: 1px solid #ddd;
    text-align: left;
    margin: auto;
}


img {
  width: 100%;
    height: auto;
}
</style>

@if($payment)
<table class="table">
	<tr>
		<td>app</td>
		<td>{{ $payment->app->app_name }}</td>
	</tr>
	<tr>
		<td>payment_status</td>
		<td>
			@if($payment->payment_status == 0)
			pending
			@else
			paid
			@endif
		</td>

	</tr>
	<tr>
		<td>paid_date</td>
		<td>{{ $payment->paid_date }}</td>
	</tr>
	<tr>
		<td>payment_gateway</td>
		<td>{{ $payment->payment_gateway}}</td>
	</tr>
	<tr>
		<td>payment_description</td>
		<td>{{ $payment->payment_description}}</td>
	</tr>
	<tr>
		<td>Status</td>
		<td>{{ $payment->payment_status}}</td>
	</tr>
	<tr>
		<td>device_ip</td>
		<td>{{ $payment->device_ip}}</td>
	</tr>
	<tr>
		<td>Image </td>
		<td><img src="{{ url('uploads/payments/'.$payment->payment_information) }}" alt="{{ $payment->paid_date }}"></td>
	</tr>
	<tr>
		<td>Total Amount</td>
		<td>{{$payment->total}}</td>
	</tr>


</table>
@endif