
@if($paymentDetails)
@foreach($paymentDetails as $paymentDetail)
<table class="table">
	<tr>
		<td>app</td>
		<td>{{ $paymentDetail->app->app_name }}</td>
	</tr>
	<tr>
		<td>payment_status</td>
		<td>
			@if($paymentDetail->payment_status == 0)
			pending
			@else
			paid
			@endif
		</td>

	</tr>
	<tr>
		<td>paid_date</td>
		<td>{{ $paymentDetail->paid_date }}</td>
	</tr>
	<tr>
		<td>payment_gateway</td>
		<td>{{ $paymentDetail->payment_gateway}}</td>
	</tr>
	<tr>
		<td>payment_description</td>
		<td>{{ $paymentDetail->payment_description}}</td>
	</tr>
	<tr>
		<td>Status</td>
		<td>{{ $paymentDetail->payment_status}}</td>
	</tr>
	<tr>
		<td>device_ip</td>
		<td>{{ $paymentDetail->device_ip}}</td>
	</tr>
	<tr>
		<td>Image </td>
		<td><img class="img-responsive" src="{{ url('uploads/payments/'.$paymentDetail->payment_information) }}" alt="{{ $paymentDetail->paid_date }}"></td>
	</tr>
	<tr>
		<td>Total Amount</td>
		<td>{{$paymentDetail->total}}</td>
	</tr>
	<tr>
		<td><button class="btn btn-status" data-href="{{ url("apps/$paymentDetail->app_id/payment_confirmed/$paymentDetail->payment_detail_id") }}">Confirm</button></td>

		<td><a class="btn btn-default" href="{{ route('paymentPdfPost',[$paymentDetail->app_id,$paymentDetail->payment_detail_id])}}" target="_blank">PDF</a></td>

	</tr>


</table>
@endforeach
@endif