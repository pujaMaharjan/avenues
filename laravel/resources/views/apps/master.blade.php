@include('apps.partials._header')
<div class="page-content-wrapper">
         <div class="page-content">
            @include('errors.error')
            @yield('content')
        </div>
    </div>
@include('apps.partials._quick-sidebar')
@include('apps.partials._footer')
