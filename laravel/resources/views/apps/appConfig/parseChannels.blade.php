@extends('admin.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Notification</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Notification Channels</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Add Notification Channels for All App
						</div>
					</div>
					<div class="portlet-body form">
						<form class="login-form" id="" action="{{route('parseChannelsSave') }}" enctype="multipart/form-data" method="post" class="register-form" novalidate="novalidate">
							{!! csrf_field() !!}

							<!-- <span class="btn btn-success add_btn">Add</span> -->

							
							<table class="table table-bordered">
								<tr>
									<th>Parse Channels <span class="font-red alert font-size">{{$errors->first()}}</span></th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								<tr>
									<td><input type="text" name="parseChannels"  class="form-control" /></td>

									<td><input type="checkbox" class="make-switch"  name="statusParseChannels" value="1"  checked /></td>
									<td><input type="submit" name="save" value="Save" class="btn btn-primary btn-lg" /></td>
								</tr>
							</table>

							

						</form>
					</div>
				</div>


				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i> List of Notification Channels
						</div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title="">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a>
							<a href="" class="reload" data-original-title="" title="">
							</a>
							<a href="" class="remove" data-original-title="" title="">
							</a>
						</div>
					</div>
					<div class="portlet-body form">
						<table class="table table-hover">
							<tr>
								<th>#</th>
								<th>Parse Channels</th>
								<th>Status</th>
								<th>Action</th>
							</tr>

							@foreach($parseChannels as $i=>$parseChannel)
							<tr>
								<td>{{++$i}}</td>
								<td>{{$parseChannel->channels}}</td>


								<td>
									@if($parseChannel->status == 1)
									<a href="{{route('parseChannelsStatus',$parseChannel->notification_channels_id)}}" class="label label-lg label-primary">
										Active
									</a>
									@else
									<a href="{{route('parseChannelsStatus',$parseChannel->notification_channels_id)}}" class="label label-lg label-danger">
										Deactive
									</a>
									@endif

								</td>
								<td>
									<button class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{ route('parseChannelsDelete',$parseChannel->notification_channels_id)}}">
										<i class="fa fa-times"></i>
									</button>
								</td>
							</tr>
							@endforeach


						</table>
					</div>
				</div>
			</div>
		</div>


		@stop
		@section('postFoot')


		<script>
			(function($){
				function addRow(){
					var new_row = '<tr>';
					new_row += '<td><input type="text" name="parseChannels[]" class="form-control" /></td>';
					new_row += '<td><input type="checkbox" class="make-switch"  name="statusParseChannels[]" value="1"  checked /></td>';

					new_row += '<td><span class="btn btn-danger remove-btn">&times</span></td>';
					new_row += '</tr>';
					return new_row;
				}
				$('.add_btn').on('click',function(){

					$(this).siblings('table').children('tbody').append(addRow());
				});
				$(document).on('click','table .remove-btn',function(){
					$(this).closest('tr').remove();
				});


			})(jQuery);
		</script>
		<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>
		@stop
