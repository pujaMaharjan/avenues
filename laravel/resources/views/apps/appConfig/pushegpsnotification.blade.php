@extends('apps.master')
@section('preHead')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>

@stop
@section('content')
@include('apps.partials._success')
@include('apps.partials._failure')

<form class="login-form" id="" action="{{ route('epgNotification',$appId)}}" method="POST"  novalidate="novalidate" id="form">
	{!! csrf_field() !!}
	<table class="table table-striped table-hover table-bordered" class="" id="sample_2">
		<thead>
			<tr>
				<th>#</th>
				<th>type</th>
				<th>Program Time(24hr)</th>
				<th>Message</th>
				<th>Notification Days</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>

			@foreach($epgLists as $i=>$list)
			<tr>
				<td>{{++$i}}</td>
				
				
				<td class="type">{{$list->type}}</td>
				<td class="time">{{$list->time}}</td>
				<td class="message">{{$list->message}}</td>

				<td class="notificationDays">@foreach(json_decode($list->notification_day,true) as $day)
					@if($day == 7)
					Daily
					@elseif($day == 0)
					Sunday
					@elseif($day == 1)
					Monday
					@elseif($day == 2)
					Tuesday
					@elseif($day == 3)
					Wednesday
					@elseif($day == 4)
					Thursday
					@elseif($day == 5)
					Friday
					@else
					Saturday
					@endif
					@endforeach
				</td>
				@if($list->status == 1)
				<td class="notificationId"><input type="checkbox" class="make-switch epgDetailIds"  name="status" value="{{$list->app_notification_id}}" checked /></td>
				@else
				<td class="notificationId"><input type="checkbox" class="make-switch epgDetailIds"  name="status" value="{{$list->app_notification_id}}" /></td>
				@endif
				
				
			</tr>

			@endforeach
		</tbody>
		<tr><td><a href="{{ route('post.pushEpgNotification',$appId)}}" class="btn btn-success">Push</a></td></tr>
	</table>
</form>
@stop

@section('postFoot')

<script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/pages/scripts/table-managed.js') }}"></script>


<script>
	(function($){
		$('.epgDetailIds').on('switchChange.bootstrapSwitch', function(event, state) {
			// var time = $(this).closest("tr")   // Finds the closest row <tr> 
   //                     .find(".time")     // Gets a descendent with class="nr"
   //                     .text();         // Retrieves the text within <td>

   //                     var message = $(this).closest("tr").find(".message").text();
   //                     var type = $(this).closest("tr").find(".type").text();
   //                     var notificationDays = $(this).closest("tr").find(".notificationDays").text();
   var id = $(this).closest("tr").find(".epgDetailIds").val();
                     //  var notificationId = $(this).closest("tr").find(".epgDetailIds").val();
		//console.log(notificationDays); // DOM element

		// console.log(event); // jQuery event
		console.log(id); // true | false
		
		
		$.ajax({
			url: "{{route('updateNotificationStatus',$appId)}}",
			method: "POST",
			data:{id:id}, 
			success: function(response){
				console.log(response);
			}
		});
		

	});
})(jQuery);
TableManaged.init();	

</script>
@stop

