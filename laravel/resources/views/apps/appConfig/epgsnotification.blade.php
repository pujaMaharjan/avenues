@extends('apps.master')
@section('preHead')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>

<link href="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/pages/css/profile.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/admin/pages/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
	
	.nav-item-days li {
		padding: 8px 0px;

	}
	

</style>
@stop
@section('content')
<ul class="nav nav-tabs nav-days" id="tabs">
	<li class="active">
		<a href="#tab_0" data-toggle="tab">Sunday</a>
	</li>

	<li>
		<a href="#tab_1" data-toggle="tab">Monday</a>
	</li>
	<li>
		<a href="#tab_2" data-toggle="tab">Tuesday</a>
	</li>
	<li>
		<a href="#tab_3" data-toggle="tab">Wednesday</a>
	</li>
	<li>
		<a href="#tab_4" data-toggle="tab">Thursday</a>
	</li>
	<li>
		<a href="#tab_5" data-toggle="tab">Friday</a>
	</li>
	<li>
		<a href="#tab_6" data-toggle="tab">Saturday</a>
	</li>


</ul> 


<form class="login-form" id="" action="{{ route('epgNotification',$appId)}}" method="POST"  novalidate="novalidate" id="form">
	{!! csrf_field() !!}
	@if(count($epgsLists) != 0)
	<div class="tab-content">

		@for($j=0; $j<7; $j++)
		<div class="tab-pane row" id="tab_{{$j}}">
			
			<ul class="nav nav-bar nav-item-days">
				<li class="col-md-2">Program</li>
				<li class="col-md-2">Program Time</li>
				<li class="col-md-2">Daily</li>
				<li class="col-md-3">Select Days</li>
				<li class="col-md-2">Message</li>
			</ul>

			@foreach($epgsLists as $i=>$epgList)
			<ul class="nav nav-bar nav-item-days">
				@if($epgList->day == $j)

				<li class="col-md-2 ">{{$epgList->program_name}}</li>
				<li class="col-md-2 startTime">{{$epgList->start_time}}</li>

				<li class="col-md-2 daily-checkbox ">

					<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

				</li>
				<li class="col-md-2 notificationDays">
					<select id="days" class="form-control select2 select2_sample_modal_2 days" multiple>
						<option value="0">Sunday</option>
						<option value="1">Monday</option>
						<option value="2">Thusday </option>
						<option value="3">Wednesday</option>
						<option value="4">Thursday</option>
						<option value="5">Friday</option>
						<option value="6">Saturday</option>

					</select>
					<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

				</li>
				<li class="col-md-3" style="margin-left:10px;">
					<input type="text"  name="message[]" id="message" class="form-control message">
				</li>
			</ul>
			@endif
			@endforeach 
		</div>
		@endfor

		@else 
		<div class="tab-content">

			@foreach($externalEpg as $j=>$epgList)
			
			<div class="tab-pane row" id="tab_{{$j}}">

				<ul class="nav nav-bar nav-item-days">
					<li class="col-md-2">Program</li>
					<li class="col-md-2">Program Time</li>
					<li class="col-md-2">Daily</li>
					<li class="col-md-3">Select Days</li>
					<li class="col-md-2">Message</li>
				</ul>

				@foreach($epgList as $i=> $list)
				<ul class="nav nav-bar nav-item-days">
					@if($j==0)

					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text"  name="message[]" id="message" class="form-control message">
					</li>

					@elseif($j == 1)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="form-control message">
					</li>
					@elseif($j == 2)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="form-control message">
					</li>

					@elseif($j == 3)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="message form-control">
					</li>
					@elseif($j == 4)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>

					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="form-control message">
					</li>

					@elseif($j == 5)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="form-control message">
					</li>
					@elseif($j == 6)
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="form-control message">
					</li>



					@else
					<li class="col-md-2 ">{{$list['name']}}</li>
					<li class="col-md-2 startTime">{{ $list['start']}}</li>

					<li class="col-md-2 daily-checkbox ">

						<input type="checkbox" class="make-switch daily"  name="daily" value="7" id="daily" /></td>

					</li>
					<li class="col-md-2 notificationDays">
						<select id="days" data-target="{{$j.$i}}" class="form-control select2 select2_sample_modal_2 days" multiple>
							<option value="0">Sunday</option>
							<option value="1">Monday</option>
							<option value="2">Thusday </option>
							<option value="3">Wednesday</option>
							<option value="4">Thursday</option>
							<option value="5">Friday</option>
							<option value="6">Saturday</option>

						</select>
						<!-- <input type="hidden" class="days" id="select2_sample_modal_5" class="form-control" value="Sunday" multiple="multiple"> -->

					</li>
					<li class="col-md-3" style="margin-left:10px;">
						<input type="text" name="message[]" id="message" class="message form-control">
					</li>


					@endif
				</ul>
				@endforeach
			</div>


			@endforeach


		</div>
		@endif

	</form>










	@stop

	@section('postFoot')
	<script type="text/javascript" src="{{ asset('assets/js/jquery.form.js') }}"></script>


	<script>

		(function($){

			console.log('active');
			$('#tab_0').addClass('active');





		//for switch change 
		var daily = "10";
		$('.daily').on('switchChange.bootstrapSwitch', function(event, state) {
			if(state == true)
			{
				daily = $(this).closest("li").find(".daily").val();
				$(this).closest('li').next('li').find('select').prop('disabled', true);
				// console.log($(this).parent('td').siblings('.notificationDays'));
				// $(this).parent('td').siblings('.notificationDays').children('.days').prop('disabled', true);
				//$('.days').prop('disabled', true);
			}
			else
			{
				$(this).closest('li').next('li').find('select').prop('disabled', false);
				//$('.days').prop('disabled', false);
			}
		});


		$('.message').on('blur',function(){
			$this = $(this);	
			var message = $(this).val();
			var startTime = $this.parent('li').siblings('.startTime').text();

			var notificationDays = $this.parent('li').siblings('.notificationDays').children('select').val();

			//var daily = $(this).closest("tr").find(".daily").val();
			console.log(daily);



			if(message != "")
			{
				$.ajax({
					url: "{{route('epgNotification',$appId)}}",
					method: "POST",
					data: {message:message,startTime:startTime,notificationDays:notificationDays,daily:daily},
					success: function(response){
						console.log(response);
					}
				});
			}


		});
		
		
		$('.nav-days a').click(function (e) {
			$(this).tab('show')
			e.preventDefault();
			var  now_tab = e.target;
			var div_id = $(now_tab).attr('href').substr(1);
			// $('#'+div_id).load('http://localhost/nitv/broadcasters-backend/public_html/broadcaster/app/1/epgs/notification');
			$('#'+div_id).siblings().hide();
			$('#'+div_id).show();



		})

	})(jQuery);
	TableManaged.init();	

</script>
@stop

