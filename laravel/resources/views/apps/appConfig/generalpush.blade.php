@extends('apps.master')
@section('content')
@include('apps.partials._success')
<form action="{{ route('generalPush',$appId)}}" method="POST">
	<div class="form-body">


		<div class="form-group">
			<label>Notification Title </label>
			<div class="input-icon input-icon-sm">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="notificationTitle" id="notificationTitle">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Notification Message</label>
			<div class="input-icon input-icon-lg">

				<textarea rows="5" cols="23" name="notificationMessage"></textarea>
			</div>
		</div>
	</div>
	
	<div class="form-actions">
		<button type="submit" class="btn blue">Submit</button>
		<a href=""><button type="button" class="btn default">Cancel</button></a>
	</div>


</form>
@stop