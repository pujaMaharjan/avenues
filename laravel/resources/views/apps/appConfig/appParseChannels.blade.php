@extends('apps.master')
@section('content')
<form class="login-form" id="" action="{{ route('parseChannelsSubcribe',$appId) }}" method="POST"  novalidate="novalidate">
	{!! csrf_field() !!}
	<table class="table">

		<tr>
			<td id="channels">Notification Channels</td>
			<td>subscribe</td>
		</tr>

		@foreach($parseChannels as $i=>$parseChannel)
		<tr>

			<td>{{$parseChannel->channels}}</td>
			
			<td>
				@if(in_array($parseChannel->notification_channels_id,$subscribeChannels))
				<input type="checkbox" class="make-switch"  name="channelsId[]" value="{{$parseChannel->notification_channels_id}}"  checked />
				@else
				<input type="checkbox" class="make-switch"  name="channelsId[]" value="{{$parseChannel->notification_channels_id}}" />
				@endif
			</td>
			
			
			
		</tr>
		@endforeach
		<tr>
			<td><input type="submit" name="save" value="subscribe" class="btn btn-primary btn-mg" /></td>
		</tr>

	</table>
</form>
@stop

