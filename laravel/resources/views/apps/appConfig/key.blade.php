@extends('apps.master')
@section('content')

<form class="login-form" id="form_sample_1" action="{{ route('appConfigStore',$appId) }}" enctype="multipart/form-data" method="post"  novalidate="novalidate">
	{!! csrf_field() !!}
	<div class="form-body">
		

		<div class="form-group">
			<label class="control-label col-md-3">Application ID</label>

			<input type="text" class="form-control" name="applicationId" value="@if(!$parseKey->isEmpty()){{$parseKey[0]->value}}@endif">

		</div>


		<!--/span-->
		
		<div class="form-group">
			<label class="control-label col-md-3">Client Key</label>

			<input type="text" class="form-control" name="clientKey" value="@if(!$parseKey->isEmpty()){{$parseKey[1]->value}}@endif">

		</div>

		<!--/span-->

		<div class="form-group">
			<label class="control-label col-md-3">REST API Key </label>
			<input type="text" class="form-control" name="restApiKey" value="@if(!$parseKey->isEmpty()){{$parseKey[2]->value}}@endif">
		</div>

		<!--/span-->

		<div class="form-group">
			<label class="control-label col-md-3">Master Key</label>

			<input type="text" class="form-control" name="masterKey" value="@if(!$parseKey->isEmpty()){{$parseKey[3]->value}}@endif">

			<span class="help-block">
			</span>


		</div>

	</div><!--End Form Body-->
	<div class="form-actions">


		<button type="submit" class="btn blue">Submit</button>
		<button type="reset" class="btn  default">Cancel</button>


	</div>
</form>
</div><!--end page content-->
</div><!--end page content wrapper-->
</form>

@stop
@section('postFoot')



@stop
