@extends('broadcaster.master')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-android"></i>Create New App
                </div>
            </div>
            <div class="portlet-body">                   
                <div class="">

                    <form class="form-horizontal" action="{{ url('apps/postCreate') }}" method="post" role="form">
                     {!! csrf_field() !!}
                     <div class="form-body">
                         <div class="form-group">
                            <label class="col-md-3 control-label">App Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="app_name" placeholder="Enter text" value="{{old('app_name')}}">
                                <span class="font-red alert font-size">
                                   {{$errors->first('app_name')}}
                               </span>
                           </div>
                       </div>

                   </div>
                   <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Description</label>
                        <div class="col-md-9">

                            <textarea class="form-control" name="description"></textarea>
                            <span class="help-block">
                                A block of help text. </span>
                            </div>
                        </div>

                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Months</label>
                            <div class="col-md-9">


                                <span class="help-block">
                                    A block of help text. </span>
                                </div>
                            </div>

                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Choose Services</label>
                                <div class="col-md-9">


                                   @foreach($services as $service)
                                   <input type="checkbox" class="service" name="services[]" 
                                   
                                   value="{{$service->service_id}}">{{$service->service_name}}
                                   @endforeach
                               </div>
                               <span class="font-red alert font-size">
                                   {{$errors->first('services')}}
                               </span>
                           </div>

                       </div>
                       <div class="form-actions">
                         <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                               <button type="submit" class="btn green">Submit</button>
                           </div>
                       </div>
                   </div>
               </form>

           </div>   
       </div>
   </div>
</div>
</div>
@stop
@section('postFoot')
<script>
    (function($){
        function countTd(){
            var tr =  $('.table > tbody > tr').length;
            return tr;
        }

        function totalPrice(price){
            var totals;
            totals += parseInt(price);
            
            return totals;
        }

        $(document).on('click','table .btn-remove',function(){
            $(this).closest('tr').remove();
        });

        $('.service').on('click',function(){
            if ($(this).is(':checked')) {
                var id = $(this).val();
                $.ajax({
                    url: "{{url('apps/service-detail')}}/"+id,
                    type: 'GET',
                    data: {id:id},
                    success: function(res){
                        var tr = "<tr>";
                        tr += "<td>"+countTd()+"</td>";
                        tr += ("<td>"+res.service_name+"</td>");
                        tr += "<td class='price'>"+res.price+"</td>";
                        tr += "<td class='price'>"+res.price+"</td> ";
                        tr += "<td><span class='btn btn-remove btn-danger'>&times;</span></td>";
                        tr += "</tr>";
                        $('table tbody').append(tr);
                        console.log($('.table > tbody > tr').length);
                        $('.total').text(res.price);
                    }

                });
            } 


            if ($(this).is(':checked')) {



            }





        });
    })(jQuery);
</script>
@stop