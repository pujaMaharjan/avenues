	
<!-- BEGIN HORIZANTAL MENU -->
<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
<div class="hor-menu hidden-sm hidden-xs">
	<ul class="nav navbar-nav">
		<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
		<li class="classic-menu-dropdown {{setBroadcasterActive('/app')}}">
			<a href="{{url('broadcaster/app/'.$menus[0]->pivot->app_id)}}">
				Dashboard <span class="">
			</span>
		</a>
	</li>		

	<li class="mega-menu-dropdown {{setBroadcasterActive('/app/'.$menus[0]->pivot->app_id.'/services')}}">
		<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
			Services
			<i class="fa fa-angle-down"></i></a>

			<ul class="dropdown-menu pull-left ">

				@if($menus)
				@foreach($menus as $i=> $menu)

				<li class="{{ setBroadcasterActive('/app/'.$menu->pivot->app_id.'/services/'.$menu->service_name)}}"><a href="{{ url('broadcaster/app/'.$menu->pivot->app_id.'/services/'.$menu->service_name) }}" class="ajaxify">{{ $menu->service_name }}</a></li>
				@endforeach
				@endif
				

			</ul>
		</li>
		<li class="classic-menu-dropdown">
			<a href="{{url('broadcaster/app/'.$menus[0]->pivot->app_id)}}">
				Notification 
			</a>
		</li>
		<li class="classic-menu-dropdown">
			<a href="{{ route('broadcasterHome') }}">
				Broadcaster 
			</a>
		</li>
		<li class="classic-menu-dropdown {{setBroadcasterActive('/app/'.$menus[0]->pivot->app_id.'/template')}}">
			<a href="{{ url('broadcaster/app/'.$menus[0]->pivot->app_id.'/template') }}">
				Template 
			</a>
		</li>
		<li class="classic-menu-dropdown {{setBroadcasterActive('/app/'.$menus[0]->pivot->app_id.'/notification')}}">
			<a href="{{ url('broadcaster/app/'.$menus[0]->pivot->app_id.'/notification') }}">
				Notification 
			</a>
		</li>
		<li class="classic-menu-dropdown {{setBroadcasterActive('/app/'.$menus[0]->pivot->app_id.'/menus')}}">
			<a href="{{ url('broadcaster/app/'.$menus[0]->pivot->app_id.'/menus') }}">
				Menus 
			</a>
		</li>

	</ul>
</li>
</ul>
</div>
		<!-- END HORIZANTAL MENU -->