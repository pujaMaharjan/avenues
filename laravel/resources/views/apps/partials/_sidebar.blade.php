	<!-- BEGIN SIDEBAR -->


	<div class="page-sidebar-wrapper">
		<!-- BEGIN HORIZONTAL RESPONSIVE MENU -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<!-- DOC: This is mobile version of the horizontal menu. The desktop version is defined(duplicated) in the header above -->
				<li class="">
					<a href="#"> General </span> </a>
				</li>
				
				<li class="{{setBroadcasterActive('/app/'.$appId.'/appconfig')}}">
					<a>
						<i class=""></i> App Config <span class=""> </span>
					</a>
					<ul class="sub-menu ">
						<li class="{{setBroadcasterActive('/app/'.$appId.'/appconfig/key')}}">
							
							<a href="{{ route('appConfigCreate',$appId) }}">
								<i class=""></i>
								Parse Keys 
							</a>

						</li>
						

						<li class="{{setBroadcasterActive('/app/'.$appId.'/appconfig/notificationchannels')}}">
							<a href="{{route ('parseChannels',$appId)}}">
								Notification Channels  <span class="">
							</span>
						</a>
					</li>
					<li  class="{{setBroadcasterActive('/app/'.$appId.'/appconfig/epgsetting')}}">
						<a href="{{ route('epgNotification',$appId) }}">
							epg notification setting
						</a>
					</li>
					
				</ul>
			</li>
			<li class="{{setBroadcasterActive('/app/'.$appId.'/push')}}">
				<a href="#">
					Push
				</span>
			</a>
			<ul class="sub-menu">
				<li class="{{setBroadcasterActive('/app/'.$appId.'/push/general')}}">
					<a href="{{ route('generalPushView',$appId)}}">
						General push
					</a>
				</li>

				<li class="{{setBroadcasterActive('/app/'.$appId.'/push/epg')}}">
					<a href="{{ route('pushEpgNotification',$appId)}}">
						Push epg notification 
					</a>
				</li>
			</ul>
		</li>								
	</ul>
</div>
<!-- END HORIZONTAL RESPONSIVE MENU -->
</div>
	<!-- END SIDEBAR -->