<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
	<ul class="nav navbar-nav">
		<!-- BEGIN NOTIFICATION DROPDOWN -->
		<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
		<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-android"></i><span class="badge badge-default">
				{{$selectApp->app_name}}</span>
			</a>
			<ul class="dropdown-menu">
				<li class="external">
					<h3><span class="bold">Your App List</span></h3>
					
				</li>
				<li>
					<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
						@foreach($apps as $app)
						<li>
							<a href="{{ url('broadcaster/app/'.$app->app_id)}}">
								<span class="time">{{$app->created_at}}</span>
								<span class="details">
									<span class="label label-sm label-icon label-success">
										<i class="fa fa-android"></i>
									</span>
									{{$app->app_name}}</span>
								</a>
							</li>
							@endforeach
							<!-- END QUICK SIDEBAR TOGGLER -->
						</ul>
					</div>
<!-- END TOP NAVIGATION MENU -->