		<!-- BEGIN TOP NAVIGATION MENU -->

		<div class="top-menu">

			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-bell"></i>
						<span class="badge badge-default">
							7 </span>
						</a>
						<!--<ul class="dropdown-menu">
							<li class="external">
								<h3><span class="bold">12 pending</span> notifications</h3>
								<a href="extra_profile#">view all</a>
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283"> 
									<li>
										<a href="javascript:;">
											<span class="time">14 hrs</span>
											<span class="details">
												<span class="label label-sm label-icon label-info">
													<i class="fa fa-bullhorn"></i>
												</span>
												Application error. </span>
											</a>
										</li> 
										<li>
											<a href="javascript:;">
												<span class="time">9 days</span>
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Storage server failed. </span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>-->
							<!-- END NOTIFICATION DROPDOWN -->
							<!-- BEGIN INBOX DROPDOWN -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
							<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<i class="icon-envelope-open"></i>
									<span class="badge badge-default">
										4 </span>
									</a>
									<!--<ul class="dropdown-menu">
										<li class="external">
											<h3>You have <span class="bold">7 New</span> Messages</h3>
											<a href="#">view all</a>
										</li>
										<li>
											<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">

												<li>
													<a href="#">
														<span class="photo">
															<img src="{{ asset('assets/admin/layout3/img/avatar3.jpg')}}" class="img-circle" alt="">
														</span>
														<span class="subject">
															<span class="from">
																Richard Doe </span>
																<span class="time">46 mins </span>
															</span>
															<span class="message">
																Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
															</a>
														</li>
													</ul>
												</li>
											</ul>-->
										</li>

										<!-- END INBOX DROPDOWN -->
										<!-- BEGIN TODO DROPDOWN -->
										<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
										<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
											<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
												<i class="icon-calendar"></i>
												<span class="badge badge-default">
													3 </span>
												</a>
												<!--<ul class="dropdown-menu extended tasks">
													<li class="external">
														<h3>You have <span class="bold">12 pending</span> tasks</h3>
														<a href="page_todo#">view all</a>
													</li>
													<li>
														<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283"> 
															<li>
																<a href="javascript:;">
																	<span class="task">
																		<span class="desc">Mobile development</span>
																		<span class="percent">85%</span>
																	</span>
																	<span class="progress">
																		<span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">85% Complete</span></span>
																	</span>
																</a>
															</li>
															
														</ul>
													</li>
												</ul>-->
											</li>
											<!-- END TODO DROPDOWN -->
											<!-- BEGIN USER LOGIN DROPDOWN -->
											<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
											<li class="dropdown dropdown-user ">
												<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													
													<span class="username username-hide-on-mobile">
														{{ $broadcaster->display_name }} 
													</span>
													@if($broadcaster->logo)

													<img src="{{ asset($broadcaster->logo) }}" class="img-circle" alt="">
													@else
													<i class="icon-user"></i>
													@endif
													<i class="fa fa-angle-down"></i>

												</a>

												<ul class="dropdown-menu dropdown-menu-default">
													@if(Auth::user()->type != "admin")
										<li>
											<a href="{{ route('broadcasterProfile') }}">
												<i class="icon-user"></i> My Profile </a>
											</li>
											@endif
														<li>
															<a href="{{ route('logout') }}">
																<i class="icon-key"></i> Log Out </a>
															</li>
														</ul>
													</li>
													<!-- END USER LOGIN DROPDOWN -->
													<!-- BEGIN QUICK SIDEBAR TOGGLER -->
													<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
													<li class="dropdown dropdown-quick-sidebar-toggler">
														<a href="javascript:;" class="dropdown-toggle">
															<i class="icon-logout"></i>
														</a>
													</li>
													<!-- END QUICK SIDEBAR TOGGLER -->
												</ul>
											</div>
		<!-- END TOP NAVIGATION MENU -->