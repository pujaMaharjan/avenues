</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		2014 © NewItVenture.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</div>
<!-- END CONTAINER -->
@include('apps.partials._foot-scripts')
@yield('postFoot')
</body>
<!-- END BODY -->
</html>