
@extends('broadcaster.master')
@section('postHead')
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')
<div class="page-content-wrapper" ng-app="broadcasterApp" ng-controller="PaymentCtrl">

        <div class="page-content">
        <div class="portlet">
         <?php $total_price=0; ?>
                        @foreach($services as $i=>$service)
                        
                        <?php  $total_price += $service->price;  ?>
                        
                     @endforeach
                      
                      <h3>BroadCaster Services Fees: {{$total_price}}</h3>
                    <label for="">Choose Payment Method</label>
                           
                    <select class="form-control" name="payment_method" ng-model="payment_method" id="payment_method">
                        <option  value="">--Choose Method--</option>
                        <option  value="bank_payment">Bank</option>
                        <option  value="paypal_payment">Paypal</option>
                        <option  value="stripe_payment">Credit Card</option>
                    </select>    
                    
                   
                <div class="portlet-body">
           
                   
                       <div ng-show="payment_method=='paypal_payment'" class="box green">

                          <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="amitrajbaral@gmail.com">

                        <!-- Specify a Subscribe button. -->
                        <input type="hidden" name="cmd" value="_xclick-subscriptions">
                        <!-- Identify the subscription. -->
                        <input type="hidden" name="item_name" value="Alice's Weekly Digest">
                        <input type="hidden" name="item_number" value="DIG Weekly">

                        <!-- Set the terms of the regular subscription. -->
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="a3" value="1.00">
                        <input type="hidden" name="p3" value="1">
                        <input type="hidden" name="t3" value="D">
                        <input type="hidden" name="custom" value="{{ $app_id }}" >
                        <!-- Set recurring payments until canceled. -->
                        <input type="hidden" name="src" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit"
                        src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_LG.gif"
                        alt="PayPal - The safer, easier way to pay online">
                        <img alt="" width="1" height="1"
                        src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                    </form>

                                <button class="btn btn-danger btn-lg" onClick="document.paypalform.submit();">Paypal</button>
                                                      
                       </div>

                        <div ng-show="payment_method=='bank_payment'" class="box green">
                            <form action = "{{ url('payment/bank/'.$app_id) }}" method = "post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="">Bank Name</label>
                                        <input type="text" name="bank_name" class="form-control">
                                    </div>
                                    <input type="hidden" name="total" value="{{$total_price}}">
                                     <div class="form-group">
                                        <label for="">Attachemnt (For Proved)</label>
                                        <input type="file" name="bank_credential" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">...</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                   
                                    <div class="form-group">
                                        <button class="btn btn-danger btn-success">Submit</button>
                                    </div>
                            </form>
                        </div>
                        <div ng-show="payment_method=='stripe_payment'" class="box green">
                            <form action="{{ url('payment/stripe/'.$app_id) }}" method="POST">
                               <input type="hidden" name="gateway" value="stripe" />
                               <input type="hidden" name="amount" value="{{$total_price*100}}" />
                                <script
                                  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                  data-key="pk_test_voimralm3mu3DSaeRqK6qI8F"
                                  data-amount="1000"
                                  data-name="Demo Site"
                                  data-description="Widget"
                                  data-image="/img/documentation/checkout/marketplace.png"
                                  data-locale="auto">
                                </script>
                          </form>
                        </div>
                    
                </div>

            </div>
        
        <a href="{{url('apps/cart/'.$app_id)}}" class="btn btn-danger">Back To the cart Page</a>   
</div>
</div>
@stop
@section('postFoot')

    <script type="text/javascript">
     angular.module("broadcasterApp",[])
     .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
     })
      .controller('PaymentCtrl',function($scope){

      });
    </script>
@stop