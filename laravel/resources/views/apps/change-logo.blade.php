@extends('apps.master')
@section('content')
	<form action="{{url('broadcaster/app/'.$app_id.'/change-logo')}}" enctype="multipart/form-data" method="post">
		<div class="form-group">
			<label for="">Logo</label>
			<input type="file" name="logo" >
		</div>
		<div class="form-group">
			<label for="">About us Link</label>
			<input type="text" name="web_link" >
		</div>
		<button type="submit" class="btn blue">Update</button>
	</form>
@stop