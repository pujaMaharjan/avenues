@extends('broadcaster.master')
@section('content')

<div class="page-content-wrapper">

<div class="page-content">
<div class="portlet">

         <div class="portlet-body">
               
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
              
                <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- BEGIN PAGE HEADER-->
                    
                <div class="invoice">
                       
                <form class="form-horizontal" action="{{ url('apps/postCart/'.$app_id) }}" method="post" role="form">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         #
                                    </th>
                                    <th>
                                         Name
                                    </th>
                                    <th class="hidden-480">
                                         Price
                                    </th>

                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                   @if($payment_details)
                                    <tr>

                                        <td></td>
                                        <td>Registration Charge</td>
                                        <td>{{$payment_details->total_charge}}</td>
                                        
                                        <td></td>
                                    </tr>
                                    @endif
                                    @foreach($services as $i=>$service)
                                        <tr>
                                        <td>{{++$i}}</td>
                                        <td>
                                            {{ $service->service_name }}
                                            <input type="hidden" name="service[]" value="{{ $service->service_id }}" >
                                        </td>
                                        <td>{{ $service->price }}</td>
                                        
                                        <td>
                                        <!-- <a href="{{url('apps/update-cart/'.$service->pivot->app_id."/".$service->service_id)}}" class="btn btn-danger btn-remove">&times;</a> -->
                                        </td>
                                            
                                        </tr>
                                      @endforeach

                                    </tbody>
                               
                                 
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                               
                            </div>
                            <div class="col-xs-8 invoice-block">
                                
                                <br>
                               <!--  <a class="btn  blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                Print <i class="fa fa-print"></i>
                                </a> -->
                              
                                </a>
                            </div>
                        </div>
                    </div>
                     <button class="btn btn-success" > Process</button>
                </form>
              
          
                <!-- END PAGE CONTENT-->
          
         </div>

    </div>
</div>
</div>
</div>
</div>
@stop
@section('postFoot')
<script>
    (function($){
        function countTd(){
            var tr =  $('.table > tbody > tr').length;
            return tr;
        }

        function totalPrice(price){
            var totals;
            totals += parseInt(price);
            
            return totals;
        }

        $(document).on('click','table .btn-remove',function(){
            $(this).closest('tr').remove();
        });

        $('.service').on('click',function(){
            if ($(this).is(':checked')) {
            var id = $(this).val();
            $.ajax({
                url: "{{url('apps/service-detail')}}/"+id,
                type: 'GET',
                data: {id:id},
                success: function(res){
                    var tr = "<tr>";
                        tr += "<td>"+countTd()+"</td>";
                        tr += ("<td>"+res.service_name+"</td>");
                        tr += "<td class='price'>"+res.price+"</td>";
                        tr += "<td class='price'>"+res.price+"</td> ";
                        tr += "<td><span class='btn btn-remove btn-danger'>&times;</span></td>";
                        tr += "</tr>";
                        $('table tbody').append(tr);
                        console.log($('.table > tbody > tr').length);
                        $('.total').text(res.price);
                }

            });
        } 


     if ($(this).is(':checked')) {
            


     }





    });
    })(jQuery);
</script>
@stop