@extends('broadcaster.master')
@section('content')
<div class="row">
<div class="page-content-wrapper">

	<div class="page-content">
		<div class="portlet">
   				<div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-android"></i>Create New App
                        </div>
                </div>   
		        <div class="portlet-body">
	         		<table class="table table-responsive">
	         			
	         			<tr>
	         				<td>GateWay</td>
	         				<td>{{$payment_details->payment_gateway}}</td>
	         			</tr>
	         			<tr>
	         				<td>Document</td>
	         				<td>
	         				<img src="{{asset('uploads/payments/'.$payment_details->payment_information)}}" alt=""></td>
	         			</tr>
	         		</table>
		        </div>
		</div>
	</div>
</div>
</div>