@extends('broadcaster.master')
@section('postHead')
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')
<div class="page-content-wrapper" ng-app="broadcasterApp" ng-controller="PaymentCtrl">

        <div class="page-content">
        <div class="portlet">
                     <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-android"></i>Create New App
                        </div>
                    </div>    
                    <label for="">Choose Payment Method</label>
                           
                    <select name="payment_method" ng-model="payment_method" id="payment_method">
                        <option  value="">--Choose --</option>
                        <option  value="bank_payment">Bank</option>
                        <option  value="paypal_payment">Paypal</option>
                    </select>    
                    
                    <?php $total_price=0; ?>
                        @foreach($services as $i=>$service)
                        
                        <?php  $total_price += $service->price;  ?>
                        
                     @endforeach
                <div class="portlet-body">
           
                 <h2>Broadcaster Need To charge</h2>
                  @include('apps.partials._cart')
                 <table class="table table-borderd table-hover">
                     <tr>
                         <td>BroadCaster Services Fees</td>
                         <td>{{$total_price}}</td>
                     </tr>
                 </table>
                     {% payment_method %}
                   
                       <div ng-show="payment_method=='paypal_payment'" class="box green">

                          <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

                                <!-- Identify your business so that you can collect the payments. -->
                                <input type="hidden" name="business" value="mdh_anil-facilitator-1@hotmail.com">

                                <!-- Specify an Installment Plan button. -->
                                <input type="hidden" name="cmd" value="_xclick-payment-plan">

                                <!-- Specify details about the item being purchased under the plan. -->
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="item_name" value="Electric Oven">
                                <input type="hidden" name="disp_tot" value="Y">

                                <!-- Make sure you get the buyer's address during checkout. -- >
                                <input type="hidden" name="no_shipping" value="2">

                                <!-- Set up the plan with equal 4 payments, starting at checkout. -->
                                <input type="hidden" name="option_select0" value="option_0">
                                <input type="hidden" name="option_select0_name"
                                value="Pay in 4 installments">
                                <input type="hidden" name="option_select0_type" value="E">
                                <input type="hidden" name="option_select0_a0" value="{{$total_price}}">
                                <input type="hidden" name="option_select0_p0" value="1">
                                <input type="hidden" name="option_select0_t0" value="M">
                                <input type="hidden" name="option_select0_n0" value="4">
                                <input type="hidden" name="option_index" value="0">

                                <!-- Display the plan description above the button. -->
                                <table class="table ">
                                <tr>
                                    <td><input type="hidden" name="on0" value="plan"></td>
                                </tr>
                                <tr>
                                    <td><input type="hidden" name="os0" value ="option_0"></td>
                                    <td><b>Electic Oven</b></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Number of payments: 4</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Start payments at checkout</td>
                                </tr>
                                <table>
                                <tr>
                                <th align="left">Due*</th><th align="right">Amount</th></tr>
                                <tr>
                                <td colspan="2"><hr /></td></tr>
                                <tr>
                                <td>At checkout</td>
                                <td align="right">$75.00 USD</td></tr>
                                <tr>
                                <td>Every 1 month (x 3)</td>
                                <td align="right">$74.75 USD</td></tr>
                                <tr>
                                <td colspan="2"><hr /></td></tr>
                                <tr>
                                <td colspan="2"
                                align="right"><b>Total $300.00 USD</b></td></tr>
                                </table></td></tr>
                                <tr>
                                <td colspan="3"><i>* We calculate payments from the
                                   date of checkout.</i></td></tr>
                                </table>

                                <!-- Display the Installment Plan button. -->
                                <table>
                                <tr>
                                <td align=center><i>Sign up for</i></td></tr>
                                <tr>
                                <td><input type="image" border="0" name="submit"
                                src="https://www.paypalobjects.com/en_US/i/btn/btn_installment_plan_LG.gif"
                                alt="PayPal - The safer, easier way to pay online!"></td></tr>
                                </table>
                                <img alt="" border="0" width="1" height="1"
                                src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">

                            </form>

                                <button class="btn btn-danger btn-lg" onClick="document.paypalform.submit();">Paypal</button>
                                                      
                       </div>

                        <div ng-show="payment_method=='bank_payment'" class="box green">
                            <form action = "{{ url('payment/bank/'.$app_id) }}" method = "post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="">Bank Name</label>
                                        <input type="text" name="bank_name" class="form-control">
                                    </div>
                                    <input type="hidden" name="total" value="{{$total_price}}">
                                     <div class="form-group">
                                        <label for="">Attachemnt (For Proved)</label>
                                        <input type="file" name="bank_credential" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">...</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                   
                                    <div class="form-group">
                                        <button class="btn btn-danger btn-success">Submit</button>
                                    </div>
                            </form>
                        </div>
                    
                </div>

            </div>
        
        <a href="{{url('apps/cart/'.$app_id)}}" class="btn btn-danger">Back To the cart Page</a>   
</div>
</div>
@stop
@section('postFoot')

    <script type="text/javascript">
     angular.module("broadcasterApp",[])
     .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
     })
      .controller('PaymentCtrl',function($scope){

      });
    </script>
@stop