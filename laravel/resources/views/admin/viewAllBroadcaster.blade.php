@extends('admin.master')
@section('content')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="page-content">
		<!-- /.modal -->

		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE HEADER-->
		
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="#">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Admin</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Broadcaster</a>
				</li>
			</ul>
			
		</div>
		<!-- END PAGE HEADER-->
		<div class="row margin-top-20">
			
			<div class="portlet box green-haze tasks-widget" style="border: none;">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check"></i>Broadcasters
					</div>
				</div>
				<div class="portlet-body">
					<!-- BEGIN DASHBOARD STATS -->

					<!-- END DASHBOARD STATS -->
					<div class="task-content">
						<div class="scroller" data-always-visible="1" data-rail-visible="1">
							<!-- START TASK LIST -->
							<ul class="task-list">
								@foreach($broadcasters as $broadcaster)
								<li>

									<span class="profile-userpic">
										@if($broadcaster->logo)
										
										<img src="{{asset($broadcaster->logo)}}" class="img-circle" alt="" width="100px" height="100px" alt="{{$broadcaster->display_name}}">
										@else
										<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" class="img-circle" alt="no image" width="100px" height="100px">
										@endif
									</span>
									
									
									<div class="task-title">
										<span class="task-title-sp">
											{{ $broadcaster->display_name }}</span>
											<span class="label label-sm label-success">{{ $broadcaster->company_name}}</span>
										</div>

										<div class="task-config show">
											<div class="task-config-btn btn-group">
												<a href="{{ route('broadcaster',$broadcaster->broadcaster_id) }}" class="btn btn-success">login as broadcaster</a>

											</div>
											<div class="task-config-btn btn-group">
												@if($broadcaster->approved == 1)

												<a data-btnOkLabel="yes" data-btnCancelLabel="No" class="btn btn-danger" data-toggle="confirmation" data-placement="left" data-href="{{route('approveBroadcaster',$broadcaster->broadcaster_id)}}" title="Are you sure to unsubcribe this broadcaster">Unsubcribe</a> 
												@else
												<a data-btnOkLabel="yes" data-btnCancelLabel="No" class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{route('approveBroadcaster',$broadcaster->broadcaster_id)}}" title="Are you sure to approve this broadcaster">Subcribe</a> 
												@endif
											</div>
											
<div class="task-config-btn btn-group">
												
<a class="btn btn-success btn-view" data-href="{{url('broadcaster/'.$broadcaster->broadcaster_id.'/payment-register')}}"
                  data-toggle="modal" data-target=".bs-example-modal-lg">Payment</a>
											</div>
										</div>
									</li>
									@endforeach 
								</ul>

								<!-- END START TASK LIST -->
							</div>
						</div>
						
					</div>
				</div>
			</div>

			<div class="clearfix">
			</div>

			<div class="modal fade bs-example-modal-lg" id="basic" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Payment Details</h4>
          </div>
          <div class="modal-body" class="scroller" style="max-height:500px;overflow:auto;">
           Modal body goes here
         </div>

       </div>
       <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
   </div>
			@stop

			@section('postFoot')

			<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript">
				
			</script>
			<script>
    (function($){
      $('.btn-view').on('click',function(){
      	$('.modal-body').html('');
        $('.modal-body').load($(this).data('href'));
        $('.modal-title').html("Register Payment");
      });

      

    })(jQuery);

   // UIConfirmations.init(); 
			


 </script>

		
		@stop