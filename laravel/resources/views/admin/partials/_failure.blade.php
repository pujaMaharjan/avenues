@if(Session::has('failure'))
<div class="alert alert-danger alert-dismissable">
	<i class="fa fa-check-circle"></i>
	{{Session::get('failure')}}
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>
@endif