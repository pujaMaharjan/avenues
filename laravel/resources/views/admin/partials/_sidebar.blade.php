	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- BEGIN HORIZONTAL RESPONSIVE MENU -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">

				<li class="{{setActive('/')}}">
					<a href="{{url('../public_html/admin')}}">
						Dashboard <span class="selected">
					</span>
				</a>
			</li>
			
			<li class="{{setActive('/unit')}}">
				<a href="">
					Units <span class="arrow">
				</span>
			</a>

			<ul class="sub-menu">

				<li class="{{setActive('/unit/list')}}"><a href="{{route('unitIndex') }}" class=" ">View Units</a></li>
				<li class="{{setActive('/unit/create')}}"><a href="{{ route('unitCreate') }}" class=" ">Add Units</a></li>

			</ul>
		</li>

		<li class="{{setActive('/route')}} ">
			<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
				Routes <span class="arrow">
			</span>

			<ul class="sub-menu">
				<li class="{{setActive('/route/list')}}"><a href="{{url('admin/route/list')}}" class=" ">Routes</a></li>
				<li class="{{setActive('/route/group')}}"><a href="{{url('admin/route/group')}}" class="">Route Groups</a></li>
				<li class="{{setActive('/route/order')}}"><a href="{{ route('routeOrder') }}" class="">Route Order</a>
				</ul>
			</li>

			<li class="{{setActive('/services')}}">
				<a data-toggle="dropdown" href="javascript:;" data-hover="" data-close-others="true">
					Services<span class="arrow">
				</span></a>
				<ul class="sub-menu">
					<li class="{{setActive('/services/list')}}"><a href="{{route('serviceIndex') }}" class="">View Services</a></li>
					<li class="{{setActive('/services/create')}}"><a href="{{ route('serviceCreate') }}" class="">Add Services</a></li>
				</ul>


			</li>
			<li class="mega-menu-dropdown {{setActive('/notificaiton')}}">
				<a data-toggle="dropdown" href="javascript:;" data-hover="" data-close-others="true">
					Notification<span class="arrow">
				</span></a>
				<ul class="sub-menu">
					<li class="{{setActive('/notificaiton/list')}}"><a href="{{route('parseChannelsCreate') }}" class="">Notificaiton Channels</a></li>
					<li class="{{setActive('/notificaiton/push')}}"><a href="{{ route('adminPushView') }}" class="">Push Notificaiton</a></li>

				</ul>


			</li>
		</ul>
	</div>
	<!-- END HORIZONTAL RESPONSIVE MENU -->
</div>
	<!-- END SIDEBAR -->