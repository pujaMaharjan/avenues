		<!-- BEGIN HORIZANTAL MENU -->
		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
		<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
		<div class="hor-menu hidden-sm hidden-xs">
			<ul class="nav navbar-nav">
				<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
				<li class="classic-menu-dropdown {{setActive('/')}}">
					<a href="{{url('../public_html/admin')}}">
						Dashboard <span class="selected">
					</span>
				</a>
			</li>
			<li class="mega-menu-dropdown {{setActive('/unit')}}">
				<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
					Admob<span class="selected">
				</span>
				<i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu pull-left">
					<li class="{{setActive('/unit/list')}}"><a href="{{route('unitIndex') }}" class="ajaxify ">Admob</a></li>
					<li class="{{setActive('/unit/create')}}"><a href="{{ route('unitCreate') }}" class="ajaxify ">Add Admob</a></li>
					<li class="{{setActive('/nitv/list')}}"><a href="{{ url('admin/nitv/list') }}" class="ajaxify ">Manage Nitv Ad</a></li>
					<li class="{{setActive('/nitv/list')}}"><a href="{{ url('admin/ad-manage/create') }}" class="ajaxify ">Manage Ad</a></li>
				</ul>
				

			</li>
			<li class="mega-menu-dropdown {{setActive('/route')}} ">
				<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
					Routes<span class="selected">
				</span>
				<i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu pull-left">
					<li class="{{setActive('/route/list')}}"><a href="{{url('admin/route/list')}}" class="ajaxify ">Routes</a></li>
					<li class="{{setActive('/route/group')}}"><a href="{{url('admin/route/group')}}" class="ajaxify">Route Groups</a></li>
					
					</ul>
				</li>

				<li class="mega-menu-dropdown {{setActive('/services')}}">
					<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
						Services<span class="selected">
					</span>
					<i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu pull-left">
						<li class="{{setActive('/services/list')}}"><a href="{{route('serviceIndex') }}" class="ajaxify ">View Services</a></li>
						<li class="{{setActive('/services/create')}}"><a href="{{ route('serviceCreate') }}" class="ajaxify ">Add Services</a></li>
					</ul>


				</li>
				<li class="mega-menu-dropdown {{setActive('/notificaiton')}}">
					<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
						Notification<span class="selected">
					</span>
					<i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu pull-left">
						<li class="{{setActive('/notificaiton/list')}}"><a href="{{route('parseChannelsCreate') }}" class="ajaxify ">Notificaiton Channels</a></li>
						<li class="{{setActive('/notificaiton/push')}}"><a href="{{ route('adminPushView') }}" class="ajaxify ">Push Notificaiton</a></li>
						
					</ul>


				</li>
				<li class="mega-menu-dropdown {{setActive('/layouts')}}">
					<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
						Layouts<span class="selected">
					</span>
					<i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu pull-left">
						<!-- <li class="{{setActive('/layouts/create')}}"><a href="{{ url('admin/layouts/create')}}" class="ajaxify ">Add layouts</a></li> -->
						<li class="{{setActive('/layouts')}}"><a href="{{ url('admin/layouts')}}" class="ajaxify ">View layouts</a></li>
						
					</ul>


				</li>
				
			</ul>

		</li>


	</ul>
</div>
		<!-- END HORIZANTAL MENU -->