@extends('admin.master')
@section('preHead')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/jquery-nestable/jquery.nestable.css') }}"/>

@stop

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN STYLE CUSTOMIZER -->

		@include('admin.partials._success')
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Routes</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Order</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">

				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i>Route Ordering
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body ">
						<div class="dd" id="nestable_list_1">
							<h2>Route without group</h2>
							{!! Form::open(['route'=>'updateOrder']) !!}
							<ol class="dd-list" >

								@foreach ($routes as $route)
								<li  class="dd-item">
									<div class="dd-handle listRoute">
										{{$route->route_name}}
									</div>
									<input type="hidden" name="routeOrder[]" value="{{$route->route_id}}">
								</li>
								@endforeach
								<h2>Route with group</h2>
								@foreach ($routeGroups as $group)
								<li class="dd-item">
									<div class="dd-handle">

										{{ $group->prefix }}
										<input type="hidden" name="groupOrder[]" value="{{ $group->route_group_id }}">
									</div>
									<ol class="dd-list">
										@foreach($group->children as $child)
										<li class="dd-item">
											<div class="dd-handle">
												{{ $child->prefix }}
												<input type="hidden" name="childGroupOrder[]" value="{{ $child->route_group_id }}">
											</div>
											<ol class="dd-list">
												@foreach($child->routes as $childRoute)
												<li class="dd-item">
													<div class="dd-handle">
														{{ $childRoute->route_name }}
														<input type="hidden" name="groupChildOrder[]" value="{{$childRoute->route_id}}" >
													</div>
												</li>
												@endforeach
											</ol>
										</li>
										@endforeach
										@foreach ($group->routes as $route)
										<li>
											<div class="dd-handle">
												{{ $route->route_name}}
												<input type="hidden" name="groupRouteOrder[]" value="{{$route->route_id}}">
											</div>
										</li>
										@endforeach


									</ol>
								</li>
								@endforeach
								<div class="form-actions">

									<button type="submit" class="btn blue">Submit</button>
									<button type="reset" class="btn  default">Cancel</button>

								</div>

							</ol>
							{!! Form::close() !!}
						</div>


					</div>
				</div>




				<!-- END PAGE CONTENT-->

				@stop

				@section('postFoot')
				<script src="{{ asset('assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"></script>
				<script src="{{ asset('assets/admin/pages/scripts/ui-nestable.js') }}"></script>

				<script>

					UINestable.init();

				</script>
				@stop
