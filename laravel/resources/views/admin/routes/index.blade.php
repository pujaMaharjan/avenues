@extends('admin.master')
@section('preHead')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>

@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Routes</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">view</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				@include('admin.partials._success')
				<!-- BEGIN LIST OF ROUTES-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-edit"></i>List of Routes
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
										<a href="{{url('admin/route/create')}}" class="btn btn-icon-only green">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>

						</div>
						<table class="table table-striped table-hover table-bordered" id="sample_2">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Verb</th>
									<th>Route As</th>
									<th>Route Uses</th>
									<th>Route Middleware</th>
									<th>Route Group</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($routes as $i=>$route)
								<tr>
									
									
									<td>{{ $i+1}}</td>
									<td>{{ $route->route_name }}</td>
									<td>{{ $route->route_verb }}</td>
									<td>{{ $route->route_as }}</td>
									<td>{{ $route->route_uses }}</td>
									<td>{{ $route->middleware }}</td>
									<td>{{ $route->group?$route->group->prefix:null }}</td>
									<td>
										<a href="{{route('routeEdit',$route->route_id)}}" class="btn btn-icon-only red">
											<i class="fa fa-edit"></i> 
										</a>
										<button class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{route('routeDelete',$route->route_id)}}"><i class="fa fa-times"></i></button>
									</td>
									
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		@stop

		@section('postFoot')

		<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>
		<script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/admin/pages/scripts/table-managed.js') }}"></script>


		<script>

			TableManaged.init();	
			
			
		</script>
		@stop
