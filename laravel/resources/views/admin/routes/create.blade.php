@extends('admin.master')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Routes</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Add</a>
				</li>
			</ul>
			
		</div>

		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>New Route
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->

						{!! Form::open(['url'=>'admin/route/create','novalidate'=>'novalidate','id'=>'form_sample_2']) !!}
						<div class="form-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Name</label>
										<div class="col-md-9">
											<div class="input-icon right">
												<i class="fa"></i>
												<input type="text" class="form-control" name="route_name">
												<span class="help-block alert-danger">
													{{$errors->first('route_name')}}</span>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3">Route Verb</label>
											<div class="col-md-9">
												<div class="input-icon right">
													<i class="fa"></i>
													<input type="text" class="form-control" name="route_verb">
													<span class="help-block alert-danger">
														{{$errors->first('route_verb')}}
													</span>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3">Route As</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="route_as">
												<span class="help-block">
												</span>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3">Route Uses</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="route_uses">
												<span class="help-block alert-danger">
													{{$errors->first('route_uses')}} </span>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Route Middleware</label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="middleware">
													<span class="help-block">
													</span>
												</div>
											</div>
										</div>
										<!--/span-->																										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Route Group</label>
												<div class="col-md-9">
													{!!Form::select('route_group_id', [""=>"None"]+$routeGroups,null,['class'=>'select2me form-control'])!!}

													<span class="help-block">
													</span>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
								</div><!--End Form Body-->
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-2 col-md-9">
											<button type="submit" class="btn blue">Submit</button>
											<button type="button" class="btn  default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div><!--end page content-->
					</div><!--end page content wrapper-->
					@stop