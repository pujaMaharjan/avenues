@extends('admin.master')

@section('postHead')
	<script>
		var app = angular.module('app',[]);

		app.controller('AdSettingCtrl',function($scope,AdSetting){
			// $scope.admob_type_value = false;
			$scope.changeValue = function(adtype,app_id,country_id,admob_type_value){
				$scope.data = {'adtype':adtype,'app_id':app_id,'country_id':country_id,'admob_type_value':$scope.admob_type_value};
				// console.log($scope.data);
				AdSetting.update($scope.data).then(function(res){
					console.log(res.data);
				});
			}
		});

		app.service('AdSetting',function($http){
			this.update = function(data){
					
					return $http({
						    url: '{{url()}}'+'/admin/ad-manage/update-setting',
						    method: "POST",
						    data: data
						    });
			}
		})
	</script>
@stop
@section('content')
	<div class="page-content-wrapper" ng-app="app" ng-controller="AdSettingCtrl">

		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
				<h3>Ad Manger</h3>
			@foreach($apps as $app)
				<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_{{$app->app_id}}" aria-expanded="false">
										Add setting for app Id {{App\App::find($app->app_id)->app_name}} </a>
										</h4>
									</div>
									<div id="collapse_{{$app->app_id}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
										<div class="panel-body">
											<table class="table table-hovered table-bordered">
												<tr>
													<td>Country</td>
													<td>Admob</td>
													<td>Nitv Ad</td>
												</tr>
												
												@foreach(['Nepal','Japan','Chaina'] as $i=>$c)
													<tr>
														<td>{{$c}}</td>

														@if($app->adSetings()->where(['country_id'=>$i])->first())

															<td>
														
														<input type="checkbox" ng-checked="{{$app->adSetings()->where(['country_id'=>$i])->first()->admob}}==1" ng-model="admob_type_value"  ng-click="changeValue('admob',{{$app->app_id}},{{$i}},admob_type_value)" name="admob"></td>

														<td><input type="checkbox" ng-checked="{{$app->adSetings()->where(['country_id'=>$i])->first()->nitvad}}==1" ng-model="admob_type_value" ng-click="changeValue('nitvad',{{$app->app_id}},{{$i}},admob_type_value)" name="nitvad"></td>
														@else
														<td>
															<input type="checkbox" ng-model="admob_type_value"  ng-click="changeValue('admob',{{$app->app_id}},{{$i}},admob_type_value)" name="admob">
														</td>

														<td>
															<input type="checkbox" ng-model="admob_type_value" ng-click="changeValue('nitvad',{{$app->app_id}},{{$i}},admob_type_value)" name="nitvad">
														</td>
														@endif
													</tr>

												@endforeach
												
											</table>
											@{{ admob_type_value }}
										</div>

									</div>
								</div>
					
						
							
						@endforeach
					
				</div>
			</div>
		</div>
	</div>

@stop