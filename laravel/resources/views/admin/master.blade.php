@include('admin.partials._header')
@yield('content')
@include('admin.partials._quick-sidebar')
@include('admin.partials._footer')