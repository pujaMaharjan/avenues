@extends('admin.master')
@section('content')]
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Nitv Ad Unit Set
				</div>
			</div>
			<div class="portlet-body form">
{!! Form::model($adKey, array('url' => array('admin/nitv/list/'.$adKey->id),'files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate','method'=>'post')) !!}
	<input type="hidden" name="_method" value="PATCH" />

	<div class="row">

    <div class="col-md-12">
     <div class="form-group">
       <label for="">App List</label>
        {!! Form::select('app_id',$apps,null,['class'=>'form-control']); !!}
     </div>
   </div>

		<h3>Ios</h3>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Banner</label>
				 {!! Form::text('ios_banneradunitid', null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial status</label>
				 {!! Form::select('ios_banneradunitid_status',['No','Yes'],null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial</label>
				 {!! Form::text('ios_interestialadunitid', null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial status</label>
				 {!! Form::select('ios_interestialadunitid_status', ['No','Yes'],null,['class'=>'form-control']); !!}
			</div>
		</div>
	</div>
	<div class="row">
		<h3>Android</h3>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Banner</label>
				 {!! Form::text('android_banneradunitid', null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial status</label>
				 {!! Form::select('android_banneradunitid_status', ['No','Yes'],null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial</label>
				 {!! Form::text('android_interestialadunitid', null,['class'=>'form-control']); !!}
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Interstial status</label>
				 {!! Form::select('android_interestialadunitid_status', ['No','Yes'],null,['class'=>'form-control']); !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
					<button class="btn btn-danger">Save</button>
		</div>
	</div>
</form>
</div>
</div>
</div>
</div>
@stop
