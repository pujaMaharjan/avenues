@extends('admin.master')
@section('content')

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Units</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Unit Set</a>
				</li>
			</ul>

		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Nitv Ad Unit Set
						</div>
					</div>
					<div class="portlet-body form">

						<a href="{{url('admin/nitv/list/create')}}" class="btn btn-success">Create a Nitv Ad set</a>

						<table class="table table-responsive">
							<tr>
								<th>App ID</th>
								<th>Ios Banner</th>
								<th>Ios Interestial</th>
								<th>Android Banner</th>
								<th>Android Interestial</th>
								<th>Action</th>
							</tr>

							@foreach($adKeys as $key)
								<tr>
									<td>{{App\App::find($key->app_id)->app_name}}</td>
									<td>{{$key->ios_banneradunitid}}</td>
									<td>{{$key->ios_interestialadunitid}}</td>
									<td>{{$key->android_banneradunitid}}</td>
									<td>{{$key->android_interestialadunitid}}</td>
									<td>
										<a href="{{url('admin/nitv/list/'.$key->id.'/edit')}}">Edit</a>
										<a href="">Delete</a>
									</td>
								</tr>
							@endforeach
						</table>

					</div>


					@stop
