@extends('admin.master')
@section('content')

<div id="myElement">Loading the player...</div>
<script type="text/javascript">
	var playerInstance = jwplayer("myElement");
	playerInstance.setup({
		file: "https://www.youtube.com/watch?v=Dp6lbdoprZ0",
		image: "http://example.com/uploads/myPoster.jpg",
		width: 640,
		height: 360,
		title: 'Basic Video Embed',
		description: 'A video with a basic title and description!',
		mediaid: '123456'
	});
</script>
@stop

