@extends('admin.master')
@section('content')

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Units</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">View Unit Set</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				
				@include('admin.partials._success')
				<!-- BEGIN LIST OF ROUTES-->
				<div class="portlet box green-haze tasks-widget">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-edit"></i>List of unit sets
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
										<a href="{{ route('unitCreate')}}" class="btn btn-icon-only green">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>App</th> 
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($units as $i=>$list)
								<tr>


									<td>{{ $i+1}}</td>
									<td>{{ $list->name }}</td>
									<td>{{ $list->app?$list->app->app_name:null }}</td>

									<td>
										<span type="button" data-href="{{ route('unitShow',$list->add_unit_set_id)}}"
											data-toggle="modal" data-target="#myModal" class="btn btn-success btn-view"><i class="fa fa-book"></i></span>

											<a href="{{ route('unitEdit',$list->add_unit_set_id)}}" class="btn btn-icon-only red">
												<i class="fa fa-edit"></i> 
											</a>
											<button class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{ route('unitDelete',$list->add_unit_set_id) }}"><i class="fa fa-times"></i></button>
											<a href="{{ route('unitEarning',$list->add_unit_set_id)}}" class="btn btn-icon-only red">
												<i class="fa fa-share"></i> 
											</a>
											<a href="{{ route('versionUpdate',$list->add_unit_set_id)}}"><span class="btn default">Update version</span></a>
										</td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

			<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Detail of Units</h4>
						</div>
						<div class="modal-body">
							.....
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
				@stop


				@section('postFoot')
				<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>

				<script>


					(function($){
						$('.btn-view').on('click',function(){
							$('.modal-body').load($(this).data('href'));
						});

					})(jQuery);

					// UIConfirmations.init(); 	
				</script>
				@stop
