@extends('admin.master')
@section('content')

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Units</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Unit Set</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Ad Unit Set
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->
						@include('errors.error')
						@if($selectApp)
						<form action="{{ route('unitStore') }}" class="horizontal-form" method="POST" novalidate="novalidate" id="form_sample_2">
							<div class="form-body">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Name for Admob <span class="text-danger">*
											</span></label>
											<div class="input-icon right">
												<i class="fa"></i>
												<input type="text" id="name" class="form-control" placeholder="Name for Admob" name="name" value="{{old('name')}}"  required>

											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Broadcaster's App <span class="text-danger">*
											</span></label>
											<div class="input-icon right">
												<i class="fa"></i>
												{!!Form::select('app_id',[""=>"None"]+$selectApp,null,['class'=>'select2me form-control','required'])!!}
											</div>
										</div>
									</div>
									<!--/span-->

									<!--/span-->

								</div>

								@foreach ($platformList as $i=>$list)
								<div class="row">

									<div class="col-md-6">
										<div class="form-group">
											<h3>{{ $list->platform_name }}</h3>
										</div>
										<div class="form-group">
											<label class="control-label">Banner <span class="text-danger">*
											</span></label>
											<div class="input-icon right">
												<i class="fa"></i>
												<input type="text" id="Banner" class="form-control" placeholder="Banner" name="imgBanner[]" value="{{ old('imgBanner')[$i] }}" required>
												<input type="hidden" name="typeBanner[]" id="" value="Banner">
												<input type="hidden" value="{{$list->platform_id}}" name="platform_id[]" id="platform_id">
												<input type="checkbox" name="status[]">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><h3>{{ $list->platform_name }}</h3></div>
										<div class="form-group">
											<label class="control-label">Interstitial <span class="text-danger">*
											</span></label>
											<div class="input-icon right">
												<i class="fa"></i>
												<input type="text" id="" class="form-control" placeholder="Interstitial" name="imgInterstitial[]" value="{{ old('imgInterstitial')[$i]}}" required>
												<input type="hidden" name="typeInterstitial[]" id="" value="Interstitial">
												<input type="checkbox" name="status[]">

											</div>
										</div>
									</div>
								</div>

								@endforeach
								<!--/row-->


							</div>
							<div class="form-actions right">

								<a href="{{ route('unitIndex') }}"><button type="button" class="btn default"> Cancel</button></a>
								<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
							</div>
						</form>
						<!-- END FORM-->
						@else
						<div class="row">
							<div class="col-md-6">
								<h3 class="control-label">All broadcaster have their units </h3>
							</div>

						</div>

						@endif
					</div>


					@stop