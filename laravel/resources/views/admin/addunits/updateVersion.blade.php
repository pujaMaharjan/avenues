@extends('admin.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>update app version
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				@include('admin.partials._failure')
				@include('errors.error')
				<form action="{{ route('versionUpdate',$id)}}" class="horizontal-form" method="POST">
					<div class="form-body">
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">App</label>
									<input type="text" id="appName" class="form-control" placeholder="" name="appName" value="{{ $app->app_name}}" disabled>
									<span class="help-block">
									</span>
								</div>
							</div>
							
						</div>
						<div class="row">
							@foreach($platformList as $i=>$platform)
							<div class="col-md-6">
								<div class="form-group">
									
								</div>
								<div class="form-group">
									<label class="control-label">App Version {{$platform->platform_name}}</label>                                  
									<input type="text" id="appVersion[]" class="form-control" placeholder="appVersion" name="appVersion[]" value="{{isset(old('appVersion')[$i])?old('appVersion')[$i]:$units->addUnit[$i]->app_version}}">
									<input type="hidden" id="appLastVersion[]" name="appLastVersion[]" value="{{ $units->addUnit[$i]->app_version }}">
									<input type="hidden" id="adUnitSetId[]" value="{{$units->addUnit[$i]->add_unit_set_id}}" name="adUnitSetId[]">
									<input type="hidden" value="{{$units->addUnit[$i]->platform_id}}" name="platformId[]" id="platformId[]">
									<span class="help-block">
									</span>
								</div>
							</div>
							@endforeach

							
						</div>


					</div>
					<div class="form-actions right">
						<a href="{{ route('unitIndex') }}"><button type="button" class="btn default"> Cancel</button></a>
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
					</div>
				</form>
				<!-- END FORM-->
			</div>
			@stop