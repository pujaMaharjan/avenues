@extends('admin.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Ad Units</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Edit Unit Set</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">

				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Edit Unit Set
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->
						@include('errors.error')
						<form action="{{ route('unitUpdate',$units->add_unit_set_id)}}" class="horizontal-form" method="POST">
							<div class="form-body">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Name for Admob</label>
											<input type="text" id="name" class="form-control" placeholder="Name for Admob" name="name" value="{{ old('name',$units->name)}}">
											<span class="help-block">
											</span>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">App</label>
											{!!Form::select('app_id', [""=>"None"]+$apps, $units->app_id,['class'=>'select2me form-control','disabled'])!!}


											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<div class="row">
									@foreach ($units->addUnit as $i=>$list)
									<div class="col-md-6">
										<div class="form-group">
											@foreach ($platformList as $platform)
											@if($platform->platform_id == $list->platform_id)
											<h3>{{$platform->platform_name}}</h3>
											@endif
											@endforeach
										</div>
										<div class="form-group">
											<label class="control-label">{{ $list->type }}</label>
											<input type="text" id="Banner" class="form-control" placeholder="Banner" name="img[]" value="{{ isset(old('img')[$i])?old('img')[$i]:$list->unit_name }}">
											<input type="hidden" name="type[]" id="" value="{{ $list->type }}">
											<input type="hidden" name="id[]" value="{{ $list->add_unit_id}}">
											<input type="hidden" value="{{$list->platform_id}}" name="platform_id[]" id="platform_id">
											
											<select name="status[]" id="">
												<option  @if($list->status==1) selected @endif  value="1">Yes</option>
												<option @if($list->status==0) selected @endif value="0">No</option>
											</select>
											<span class="help-block">
											</span>
										</div>
									</div>

									@endforeach
								</div>

							</div>
							<div class="form-actions right">
								<a href="{{ route('unitIndex') }}"><button type="button" class="btn default"> Cancel</button></a>
								<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
							</div>
						</form>
						<!-- END FORM-->
					</div>
					@stop