@extends('admin.master')
@section('preHead')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>

@stop
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					Details of Unit Earning
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-hover table-bordered" class="" id="sample_2">

					<thead>
						<tr>
							<th>Date</th>
							<th>ad_unit_name</th>
							<th>page_view</th>
							<th>ad_request</th>
							<th>ad_request_coverage</th>
							<th>clicks</th>
							<th>ad_request_ctr</th>
							<th>cost_per_click</th>
							<th>ad_request_rpm</th>
							<th>earning</th>
						</tr>
					</thead>
					<tbody>

						@foreach($earningDetails as $earning)

						@foreach($earning as $list)
						<tr>


							<td>{{$list->date}}</td>
							<td>{{$list->ad_unit_name}}</td>
							<td>{{$list->page_view}}</td>
							<td>{{$list->ad_request}}</td>
							<td>{{$list->ad_request_coverage}}</td>
							<td>{{$list->clicks}}</td>
							<td>{{$list->ad_request_ctr}}</td>
							<td>{{$list->cost_per_click}}</td>
							<td>{{$list->ad_request_rpm}}</td>
							<td>{{$list->earning}}</td>
						</tr>
						@endforeach



						@endforeach
					</tbody>
				</table>
			</div>         
				         </div>         <!-- END EXAMPLE TABLE
				         PORTLET-->    
				     </div>
				 </div>

				 @stop

				 @section('postFoot')

				 <script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
				 <script type="text/javascript" src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
				 <script type="text/javascript" src="{{ asset('assets/admin/pages/scripts/table-managed.js') }}"></script>


				 <script>
				 	$('.sample_5').DataTable({

				 		pageLength: 10
				 	});
				 	TableManaged.init();	
				 	UIConfirmations.init(); 

				 </script>
				 @stop