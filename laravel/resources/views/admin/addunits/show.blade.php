<div class="table-responsive"> 
  <table class="table table-striped table-hover table-bordered">
    <thead>
      <tr>
        <th>Admob name</th>
        <th>App</th>
        
        <th>Android Banner</th>
        <th>Ios Banner</th>
        <th>Android Interstitial</th>
        <th>Ios Interstitial</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{ $units->name }}</td>
        <td>{{ $units->app->app_name }}</td>
        @foreach ($units->addUnit as $list)
        <td>{{$list->unit_name}}</td>
        @endforeach
      </tr>
    </tbody>
  </table>
</div>
