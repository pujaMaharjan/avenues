@extends('admin.master')
@section('content')

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		@include('admin.partials._failure')
		@include('admin.partials._success')
		<!-- BEGIN LIST OF ROUTES-->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Routes Groups</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">view</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-edit"></i>List of Route Groups
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-6">
									<div class="btn-group">
										<a href="{{ route('routeGroupCreate') }}" class="btn btn-icon-only green">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
								<tr>
									<th>#</th>
									<th>Group As</th>
									<th>Group middleware</th>
									<th>weight </th>
									<th>namespace</th>
									<th>prefix</th>
									<th>Parent Group</th>
									<th></th>
								</tr>
							</thead>
							<tbody>

								@foreach ($routegroups as $i=>$routegroup)
								<tr>
									<td>{{ $i+1}}</td>
									<td>{{ $routegroup->prefix }}</td>
									<td>{{ $routegroup->middleware }}</td>
									<td>{{ $routegroup->weight }}</td>
									<td>{{ $routegroup->namespace }}</td>
									<td>{{ $routegroup->prefix }}</td>
									<td>{{ $routegroup->parent['prefix'] }}</td>
									<td>

										<a href="{{ route('routeGroupEdit',$routegroup->route_group_id) }}" class="btn btn-icon-only red">
											<i class="fa fa-edit"></i>
										</a>
										<button class="btn btn-success" data-toggle="confirmation" data-placement="left" data-href="{{route('routeGroupDelete',$routegroup->route_group_id)}}"><i class="fa fa-times"></i></button>
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		@stop

		@section('postFoot')
		<script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>

		<script>
			//UIConfirmations.init(); 	
		</script>
		@stop


