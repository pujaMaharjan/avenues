@extends('admin.master')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{ route('broadcasterIndex') }}">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Routes Groups</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Edit</a>
				</li>
			</ul>
			
		</div>
		<div class="col-md-12 col-sm-12 margin-top-20">
			<div class="row">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Update Route Group
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->

						{!! Form::open(['route'=>['routeGroupUpdate',$routeGroup->route_group_id],'method' => 'post']) !!}
						<div class="form-body">
							<div class="row">

								<!--span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">prefix</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="prefix"  value="{{$routeGroup->prefix}}">
											<span class="help-block">
												This is inline help </span>
											</div>
										</div>
									</div>
									<!--/span-->

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3">namespace</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="namespace"  value="{{$routeGroup->namespace}}">
												<span class="help-block">
													This is inline help </span>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">middleware</label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="middleware"  value="{{$routeGroup->middleware}}">
													<span class="help-block">
														This is inline help </span>
													</div>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Route Group</label>
													<div class="col-md-9">

														{!!Form::select('parent_id', [""=>"None"]+$selectedGroups,$routeGroup->parent_id,['class'=>'select2me form-control'])!!}

														<span class="help-block">
														</span>
													</div>
												</div>
											</div>


										</div><!--End Form Body-->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-2 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<button type="button" class="btn  default">Cancel</button>
												</div>
											</div>
										</div>
									</form>
								</div><!--end page content-->
							</div><!--end page content wrapper-->
							@stop