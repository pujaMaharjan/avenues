@extends('broadcaster.master')
@section('content')
	<div class="page-content-wrapper">
		 <div class="page-content">
		   <!-- BEGIN EXAMPLE TABLE PORTLET-->
		   <div class="portlet">
		     	<div class="portlet-body">
					{!! Form::model($channel,['url'=>'broadcaster/channels/'.$channel->id,'method'=>'Patch']) !!}
						@include('world-tv._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@stop