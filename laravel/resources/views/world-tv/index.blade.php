@extends('broadcaster.master')
@section('content')
	<div class="page-content-wrapper">
		 <div class="page-content">
		   <!-- BEGIN EXAMPLE TABLE PORTLET-->
		   <div class="portlet">
		     	<div class="portlet-body">
		     	<a href="{{url('broadcaster/channels/create')}}" class="btn btn-success">Create New Play Back Url</a>
		     	@if($channels)
		     		<table class="table table-responsive">
		     			<tr>
		     				<th>Name</th>
		     				<th>Action</th>
		     			</tr>
		     			@foreach($channels as $channel)
		     			<tr>
		     				<td>{{$channel->name}}</td>
		     				<td>
		     					<a href="{{url('broadcaster/channels/'.$channel->id.'/edit')}}" class="btn btn-primary">Edit</a>
		     				</td>
		     			</tr>
		     			@endforeach
		     		</table>
		     	@endif
		     	</div>
		    </div>
		</div>
	</div>
@stop