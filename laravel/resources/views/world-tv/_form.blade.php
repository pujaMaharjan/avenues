<p>
	<label for="">Channel Name</label>
	{!! Form::text('name', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Play Back Url</label>
	{!! Form::text('play_back_url', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Channel Logo</label>
	{!! Form::file('logo', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Admin Play Back Url</label>
	{!! Form::text('playback_url_amin', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">token</label>
	{!! Form::text('token', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Valid Time</label>
	{!! Form::text('valid_time', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Description</label>
	{!! Form::textarea('description', null,['class'=>'form-control']) !!}
</p>
<p>
	<label for="">Do you want to enable Admin url ?</label>
	<input type="checkbox" name="admin_url_enabled" value="1" />
</p>
<p>
	<input type="submit" class="btn btn-danger" name="submit" value="Save/Create">
</p>