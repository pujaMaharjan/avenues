@extends('apps.master')
@section('postHead')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')



@if(isset($menu))
{{$menu}}
{!! Form::model($menu, array('url' => array("broadcaster/app/".$appId."/menus/".$menu->menu_id),'files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate')) !!}
<input type="hidden" name="_method" value="PATCH"></input>
@else
{!! Form::open(array('url' => 'broadcaster/app/'.$appId.'/menus','id'=>'form_sample_2','novalidate' =>'novalidate','files'=>true)) !!}
@endif

<div ng-app="broadcasterApp" ng-controller="MovieCtrl">
  <h3>Add New Menu</h3>
 
  <div class="form-group">
   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
   <label class="control-label">Title</label>

   {!! Form::text('menu_title', null,['class'=>'form-control','placeholder'=>'menu title']); !!}
  </div>


  <div class="form-group">
   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
   <label class="control-label">Title</label>

   {!! Form::file('logo', null,['class'=>'form-control','placeholder'=>'menu logo']); !!}
  </div>
  <div class="form-group">
   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
   <label class="control-label">Status</label>

   {!! Form::text('status', null,['class'=>'form-control','placeholder'=>'status']); !!}
  </div>
    <div class="form-group">
   <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
   <label class="control-label">Order</label>

   {!! Form::text('menu_order', null,['class'=>'form-control','placeholder'=>'status']); !!}
  </div>



<div class="form-actions">
  <a href="{{url('/')}}" class="btn btn-default">Back</a>
  <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>
</div>
@stop
@section('postFoot')

<script type="text/javascript">
 angular.module("broadcasterApp",[])
 .config(function($interpolateProvider){
  $interpolateProvider.startSymbol('{%');
  $interpolateProvider.endSymbol('%}');
})
 .controller('MovieCtrl',function($scope){
  $scope.streamTypes = [
  {
    'name': 'youtube',
    'label': 'Youtube'
  },
  {
    'name': 'ownServer',
    'label': 'Server'
  }
  ]
});
</script>
@stop