@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<a href="{{url("broadcaster/app/$appId/menus/create")}}" class="btn btn-danger">Add New menu</a>

<div class="portlet box blue-madison">
 <div class="portlet-title">
  <div class="caption">
   <i class="fa fa-globe"></i>List Of menu
 </div>
 <div class="tools">
   <a href="javascript:;" class="reload">
   </a>
   <a href="javascript:;" class="remove">
   </a>
 </div>
</div>
<div class="portlet-body">
  <table class="table table-striped  table-hovered" id="sample_3">
    <thead>
      <tr>
       <th>Name</th>
       
       <th>Action</th>
     </tr>
   </thead>
   <tbody>

    @foreach($menus as $menu)
    <tr>
     <td>{{$menu->menu_title}}</td>
    
     <td>
      

       <a href="{{url("broadcaster/app/$appId/menus/$menu->menu_id/edit")}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
       <a data-href="{{ url('broadcaster/app/'.$appId.'/menus/delete/'.$menu->menu_id) }}" data-toggle="confirmation" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
       
     </tr>
     @endforeach
   </tbody>
 </table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of menu</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    @stop
    @section('postFoot')
    <script>
      (function($){
        $('.btn-view').on('click',function(){
          $('.modal-body').load($(this).data('href'));
        });
        $('.btn-stream').on('click',function(){
          $('.modal-body').load($(this).data('href'));
          $('#myModalLabel').html("Live stream");
        });
      })(jQuery);
    </script>
    @stop
