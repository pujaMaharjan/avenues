<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One|Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/front/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/responsive.css') }}">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <title>CastNow</title>
</head>
<body>

<div class="cn-header">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="images/logo.png" alt="Castnow">
                </a>
            </div>
            <div id="navbar1" class="navbar-collapse collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="{{url('login')}}">LOG IN</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>
</div>

<section class="cn-slider">
    <div class="main-slider">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/banner.jpg" alt="banner" style="width:100%; overflow: hidden;">
                    <div class="carousel-caption">
                        <h3>Create Your Own App</h3>
                        <h1>Take Control</h1>
                        <p><a href="#"><img src="images/create.png"></a></p>
                    </div>
                </div>

                <div class="item">
                    <img src="images/banner2.jpg" alt="banner" style="width:100%; overflow: hidden;">
                    <div class="carousel-caption">
                        <h3>Create Your Own App</h3>
                        <h1>Take Control</h1>
                        <p><a href="#"><img src="images/create.png"></a></p>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>


<section class="cn-service">
    <div class="col-service1 col-md-4 col-sm-4">
        <img src="images/wand.png"/>
        <h4>Boost Your Content</h4>
        <p>With Castnow, your content will now be targeted towards a larger audience</p>
    </div>
    <div class="col-service2 col-md-4 col-sm-4">
        <img src="images/net.png"/>
        <h4>Get Connected To Your Users</h4>
        <p>Target a large number of users with your mobile app</p>
    </div>
    <div class="col-service3 col-md-4 col-sm-4">
        <img src="images/promote.png"/>
        <h4>Promote Your Brand</h4>
        <p>Now your contents will be your brand, Castnow helps you promote them</p>
    </div>

</section>
<div class="clearfix"></div>

<section class="cn-steps">
    <h2>Create Mobile App In 4 Easy Steps</h2>
    <div class="cn-img-steps">
        <div class="phone">
            <img src="images/phone.png"/>
        </div>
        <div class="inside-icons">
            <ul>
                <li><img src="images/user.png"></li>
                <li><img src="images/windows.png"></li>
                <li><img src="images/$.png"></li>
                <li><img src="images/infinity.png"></li>
            </ul>
        </div>
    </div>
    <div class="cn-4steps">
        <div class="itemss">
            <div id="stepsline">
                <div>

                    <section class="year">

                        <section>

                            <ul>
                                <li>Step 1: Create an account with Castnow</li>
                            </ul>
                        </section>
                        <section>

                            <ul>
                                <li>Step 2: Choose the template and contents you would like to have in your app</li>
                            </ul>
                        </section>
                        <section>

                            <ul>
                                <li>Step 3: Choose a payment option</li>
                            </ul>
                        </section>
                        <section>

                            <ul>
                                <li>Step 4: Connect with your users</li>
                            </ul>
                        </section>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cn-features">
    <h2 style="margin-top: 0px;">App Features</h2>
    <img src="images/sept.png">
    <div class="cn-temp-feat">
        <ul>
            <li>
                <img src="images/youtube.png">
                <div class="desc">
                    <h4>Youtube</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/live.png">
                <div class="desc">
                    <h4>Live TV</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/fm.png">
                <div class="desc">
                    <h4>FM Radio</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/epg.png">
                <div class="desc">
                    <h4>EPG</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/vod.png">
                <div class="desc">
                    <h4>VOD</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/dvr.png">
                <div class="desc">
                    <h4>DVR</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
            <li>
                <img src="images/news.png">
                <div class="desc">
                    <h4>News</h4>
                    <p>
                        With this feature, upload your news content and make it accessible to the world which users can
                        access through your app.
                    </p>
                </div>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cn-templates">
    <h2>Use Templates</h2>
    <div class="phone-img"><img src="images/iphone.png"></div>
    <div class="cn-temp-img">
        <div class="owl-carousel owl-theme" id="owl1">
            <div class="item">
                <img src="images/screenshots/unnamed.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed2.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed3.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed4.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed5.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed6.png">
            </div>
            <div class="item">
                <img src="images/screenshots/unnamed7.png">
            </div>
        </div>
        <script>
            $(document).ready(function () {
                var owl = $('#owl1');
                owl.owlCarousel({
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    margin: 10,
                    navRewind: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 5
                        }
                    }
                })
            })
        </script>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cn-forwhom">
    <h2>Who Can Sign Up For Our Service</h2>
    <div class="itemss">
        <div id="workline">
            <div>
                <section class="year">
                    <section>
                        <ul>
                            <li>Television Stations</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>News Agencies</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>Movie Distributors</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>Youtube Channels</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>FM Radio Stations</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>Content Creators</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>Online News Portals</li>
                        </ul>
                    </section>
                </section>
                <section class="year">
                    <section>
                        <ul>
                            <li>Book Publications</li>
                        </ul>
                    </section>
                </section>

            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cn-subscribe">
    <div>
        <input type="text" name="subscribe"/>
        <input type="button" name="submit" value="subscribe"/>
    </div>
    <div class="cn-social">
        <ul>
            <li><a href="#"><img src="images/Yo.png"></a></li>
            <li><a href="#"><img src="images/fb.png"></a></li>
            <li><a href="#"><img src="images/twitter.png"></a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
</section>

<footer>
    <section class="cn-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.02416265374!2d85.32259541454764!3d27.716540231695795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb190fef7e6b3d%3A0xd646510b5dc0bc1a!2sNITV+Nepal+(Software+Development)!5e0!3m2!1sen!2snp!4v1502183734137"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="clearfix"></div>
    </section>

    <section class="cn-contact">
        <div class="column3 col-md-4">
            <img src="images/logo.png"/>
            <p>With <a href="#">Castnow,</a> creating an app to showcase your contents and to boost your business is now
                made easier. You can now develop apps for android as well as iOS, with the contents of your choice, with
                the template of your choice.<br>
                Castnow provides you with a platform that would help you to broadcast your content to your targeted
                audience.</p>
            <div style="margin-top:5%">
                <a href="{{url('terms')}}"> terms and condition </a>
            </div>
        </div>
        <div class="column3 col-md-4 col-sm-6">
            <h4 class="contact-hdr">Give Us A Call</h4>
            <h5>Our Head Office</h5>
            NEW IT VENTURE CORP.<br>
            Kopo Yamato Bldg 1F&2F, Tateishi 5-24-8<br>
            Katsushika-Ku, Tokyo 124-0012<br>
            <i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +81-3-5650-5430<br>
            <i class="fa fa-globe" aria-hidden="true"></i>&nbsp; info@newitventure.com
            <h5>Our Regional Office</h5>
            NITV Nepal (Software Development)<br>
            Address : Naxal 1, Eastgate, Nagpokhari Marg, TBi plaza bldg 1F<br>
            <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;01- 2298587<br>
            <i class="fa fa-mobile" aria-hidden="true"></i>&nbsp; +977 9849771614<br>
            <i class="fa fa-globe" aria-hidden="true"></i>&nbsp; info@newitventure.com<br>

        </div>
        <div class="column3 col-md-4 col-sm-6">
            <h4 class="contact-hdr">Write To Us</h4>
            <form action="#">
                <input type="text" name="name" placeholder="Name">
                <input type="text" name="phone" placeholder="Phone">
                <input type="text" name="email" placeholder="Email">
                <textarea placeholder="Message"></textarea>
                <input type="submit" value="Send Message">
            </form>
        </div>
        <div class="clearfix"></div>
    </section>

    <section class="cn-copyrights">
        <br/>
        &copy;&nbsp;Copyrights 2017,&nbsp;<a href="#">CASTNOW.</a>&nbsp;All rights reserved.
    </section>
</footer>
<script src="{{ asset('assets/front/js/owl.carousel.js') }}"></script>

</body>
</html>