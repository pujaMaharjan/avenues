@extends('apps.master')
@section('postHead')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script>
@stop
@section('content')



@if(isset($movie))
{!! Form::model($movie, array('url' => array("broadcaster/app/".$app_id."/services/movies/update/$id"),'files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate')) !!}
@else
{!! Form::open(array('url' => 'broadcaster/app/'.$app_id.'/services/movies/store','files' => true,'id'=>'form_sample_2','novalidate' =>'novalidate')) !!}
@endif

<div ng-app="broadcasterApp" ng-controller="MovieCtrl">
  <h3>Add New Movie</h3>
  <div class="form-group" >
    <label class="control-label">Name</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('movie_name', null,['class'=>'form-control','placeholder'=>'name']); !!}
    </div>
  </div>

  <div class="form-group">
   <label class="control-label">Movie Streaming</label>
   <div class="input-icon right">
    <i class="fa"></i>
    <select ng-model="hosted" class="form-control" name="hosted" id="hosted">
      @if(isset($movie))
      <option ng-selected="' {{$movie->hosted}} ' =='{% streamType.name %}'" value="{% streamType.name %}" ng-repeat="streamType in streamTypes" >{% streamType.label %}</option>
      @else
      <option  value="{% streamType.name %}" ng-repeat="streamType in streamTypes" >{% streamType.label %}</option>
      @endif
    </select>
  </div>
  

</div>


<div class="" ng-show="hosted =='youtube'">
 <div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <div class="input-icon right">
    <i class="fa"></i>
    <label class="control-label">Youtube Id</label>
    {!! Form::text('youtube_id', null,['class'=>'form-control','placeholder'=>'Ypu Tube Url']); !!}
  </div>
</div>
</div>
<div class="" ng-show="hosted =='ownServer'">
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">sources_local_url</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('movie_local_url', null,['class'=>'form-control','placeholder'=>'sources_local_url']); !!}
    </div>
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">sources_local_url</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('movie_cdn_url', null,['class'=>'form-control','placeholder'=>'sources_cdn_url']); !!}
    </div>
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Url Token Key</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('url_token_key', null,['class'=>'form-control','placeholder'=>'url_token_key']); !!}
    </div>
  </div>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label">Valid Time</label>
    <div class="input-icon right">
      <i class="fa"></i>
      {!! Form::text('valid_time', null,['class'=>'form-control','placeholder'=>'valid_time']); !!}
    </div>
  </div>

</div>

<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Movie Photo </label>
 @if(isset($movie))
 <div class="row">
  <div class="col-md-4">
    <img src="{{ asset('uploads/movies/'.$movie->movie_photo) }}" width="100" height="150" alt="">
  </div>
</div>
@endif
{!! Form::file('movie_photo', null,['class'=>'form-control','placeholder'=>'logo']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">description</label>

  {!! Form::textarea('description', null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
  <label class="control-label">Country</label>

  {!! Form::select('country_id', $countries, null, ['class'=>'form-control']); !!}
</div>
<div class="form-group">
 <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
 <label class="control-label">Language</label>

 {!! Form::text('language', null,['class'=>'form-control','placeholder'=>'language']); !!}
</div>


<div class="form-actions">
  <a href="{{url('/')}}" class="btn btn-default">Back</a>
  <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
</div>
</form>
</div>
@stop
@section('postFoot')

<script type="text/javascript">
 angular.module("broadcasterApp",[])
 .config(function($interpolateProvider){
  $interpolateProvider.startSymbol('{%');
  $interpolateProvider.endSymbol('%}');
})
 .controller('MovieCtrl',function($scope){
  $scope.streamTypes = [
  {
    'name': 'youtube',
    'label': 'Youtube'
  },
  {
    'name': 'ownServer',
    'label': 'Server'
  }
  ]
});
</script>
@stop