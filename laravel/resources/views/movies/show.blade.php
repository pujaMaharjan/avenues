<div class="table-responsive">
	
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>hosted</th>
				<td>{{$movie->hosted}}</td>
			</tr>
			<tr>
				<th>youtube_id</th>
				<td>{{$movie->youtube_id}}</td>
			</tr>
			<tr>
				<th>movie_name</th>
				<td>{{$movie->movie_name}}</td>
			</tr>
			<tr>
				<th>movie_local_url</th>
				<td>{{$movie->movie_local_url}}</td>
			</tr>
			<tr>
				<th>movie_cdn_url</th>
				<td>{{$movie->movie_cdn_url}}</td>
			</tr>
			<tr>
				<th>movie_photo</th>
				<td><img src="{{$movie->movie_photo}}" alt="{{$movie->movie_name}}"> </td>
			</tr>
			<tr>
				<th>description</th>
				<td>{{$movie->description}}</td>
			</tr>
			<tr>
				<th>category</th>
				<td>{{$movie->category}}</td>
			</tr>
			<tr>
				<th>url_token_key</th>
				<td>{{$movie->url_token_key}}</td>
			</tr>
			<tr>
				<th>valid_time</th>
				<td>{{$movie->valid_time}}</td>
			</tr>


		</thead>
	</table>
</div>