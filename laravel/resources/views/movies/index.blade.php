@extends('apps.master')
@section('content')
<!-- BEGIN CONTENT -->

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<a href="{{url("broadcaster/app/$app_id/services/movies/create")}}" class="btn btn-danger">Add New movie</a>

<div class="portlet box blue-madison">
 <div class="portlet-title">
  <div class="caption">
   <i class="fa fa-globe"></i>List Of movie
 </div>
 <div class="tools">
   <a href="javascript:;" class="reload">
   </a>
   <a href="javascript:;" class="remove">
   </a>
 </div>
</div>
<div class="portlet-body">
  <table class="table table-striped  table-hovered" id="sample_3">
    <thead>
      <tr>
       <th>Name</th>
       <th>Description</th>
       <th>Country</th>
       <th>Action</th>
     </tr>
   </thead>
   <tbody>

    @foreach($movies as $movie)
    <tr>
     <td>{{$movie->movie_name}}</td>
     <td>{{$movie->description}}</td>
     <td>{{$movie->country}}</td>
     <td>
      <span type="button" data-href="{{url("broadcaster/app/$app_id/services/movies/show/$movie->app_movie_id")}}"
       data-toggle="modal" data-target="#myModal" class="btn btn-success btn-view"><i class="fa fa-book"></i></span>

       <a href="{{url("broadcaster/app/$app_id/services/movies/edit/$movie->app_movie_id")}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
       <a data-href="{{ url('broadcaster/app/'.$app_id.'/services/movies/delete/'.$movie->app_movie_id) }}" data-toggle="confirmation" data-original-title="Are you sure ?" class="btn btn-danger"><i class="fa fa-times"></i></a>
       
     </tr>
     @endforeach
   </tbody>
 </table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->


</div>
</div>


<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail of movie</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    @stop
    @section('postFoot')
    <script>
      (function($){
        $('.btn-view').on('click',function(){
          $('.modal-body').load($(this).data('href'));
        });
        $('.btn-stream').on('click',function(){
          $('.modal-body').load($(this).data('href'));
          $('#myModalLabel').html("Live stream");
        });
      })(jQuery);
    </script>
    @stop
