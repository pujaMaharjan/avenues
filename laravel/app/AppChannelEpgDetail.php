<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppChannelEpgDetail extends Model
{
	protected $primaryKey = 'app_channel_epg_detail_id';
	protected $fillable = ['app_channel_epg_id','program_name','start_time','end_time','day'];

	public function AppChannelEpg()
	{
		return $this->belongsTo('Broadcasters\Models\AppChannelEpg', 'app_channel_epg_id');
	}
}
