<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    protected $table = 'app_audios';
    
    protected $fillable = ['title','image','description','live_stream','playlist_id'];

    protected $hidden = [];

}
