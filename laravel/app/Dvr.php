<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dvr extends Model
{
    protected $fillable = ['name', 'url', 'app_id', 'created_by', 'option'];
}