<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentReachStep extends Model
{
    protected $table = "payments_reached_step";
    protected $fillable = ['app_id','name','description'];
    public $timestamps = false;
}
