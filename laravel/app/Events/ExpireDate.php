<?php

namespace App\Events;

use App\Events\Event;
use Broadcasters\App\Models\App;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExpireDate extends Event
{
    use SerializesModels;
    public $app;
  
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
