<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Broadcasters\User\Models\User;
use Broadcasters\Models\Broadcaster;

class WhileBroadcasterRegister extends Event
{
    use SerializesModels;
    public $user;
    public $broadcaster;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user )
    {
       // dd($user);
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
