<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirebaseToken extends Model
{
	
    protected $fillable = ['app_id','token','country'];

}
