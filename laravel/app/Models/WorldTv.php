<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldTv extends Model
{
	protected $table = 'worldtv';
    protected $fillable = ['name','logo','play_back_url','playback_url_amin','description','token','valid_time','broadcaster_id','admin_url_enabled'];

    protected $casts = ['admin_url_enabled'=>'boolean'];
}
