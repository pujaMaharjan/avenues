<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
	
    protected $fillable = ['host_name','api_key'];

}
