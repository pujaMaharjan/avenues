<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Broadcasters\App\Models\App;

class Menu extends Model{

	protected $primaryKey = 'menu_id';
	protected $table = 'template_menus';

	protected $fillable = ['menu_title','app_id','status','menu_order','logo'];

	protected $casts = [
		'status'=>'boolean'
	];

	public $timestamps = false;
}