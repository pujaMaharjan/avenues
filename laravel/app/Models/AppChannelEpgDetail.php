<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdSetting extends Model{

	protected $fillable = ['app_id','country_id','admob','nitvad'];

	protected $casts = [
		'admob'=>'boolean',
		'nitvad'=>'boolean',
	];
}