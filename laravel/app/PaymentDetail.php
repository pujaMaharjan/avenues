<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $table = 'payment_details';
    protected $primaryKey = 'payment_detail_id';

    protected $fillable = ['user_id','app_id','payment_status','paid_date','payment_information','payment_gateway','paid_for','payment_description'];

    public function app(){
        return $this->belongsTo(App::class);
    }
}
