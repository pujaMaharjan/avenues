<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentRegisterDetail extends Model
{
    protected $table = 'payment_register_details';



    protected $fillable = ['user_id','payment_status','paid_date','payment_gateway','paid_for','total_charge','payment_meta_data'];

    public function scopeUnPaid($query){
    	return $query->where('payment_status',0);
    }
 protected $casts = [
 'payment_meta_data'=>'json'
 ];


}
