<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RegistrationPaymentProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'apps.partials._cart', 'App\Composers\CartComposer@cart'
            );
         view()->composer(
            'apps.cart', 'App\Composers\CartComposer@cart'
            );
          view()->composer(
            'apps.payment', 'App\Composers\CartComposer@cart'
            );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
