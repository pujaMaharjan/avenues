<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppMenuProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'apps.partials._horizontal-menu', 'App\Http\AppMenuComposer@compose'
            );
        view()->composer(
            'apps.partials._sidebar', 'App\Http\AppMenuComposer@sideMenu'
            );

        view()->composer(
            'apps.partials._top-menu', 'App\Http\AppMenuComposer@appTopMenu'
            );
        view()->composer(
            'apps.partials._top-menu-left', 'App\Http\AppMenuComposer@appTopMenuLeft'
            );

        view()->composer(
            'broadcaster.partials._top-menu', 'App\Http\AppMenuComposer@broadcasterTopMenu'
            );
        view()->composer(
            'broadcaster.partials._sidebar', 'App\Http\AppMenuComposer@broadcasterTopMenu'
            );

        view()->composer(
            'broadcaster.master', 'App\Http\AppMenuComposer@broadcasterTopMenu'
            );

        view()->composer(
            'templates.create', 'App\Http\AppMenuComposer@templatePassAppId'
            );
        



        

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
