<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\App' => 'App\Policies\AppPolicy',
        'Broadcasters\Vod\Models\AppVod' => 'App\Policies\VodPolicy',
        'Broadcasters\NewsApi\Models\AppExternalApi' => 'App\Policies\NewsApiPolicy',
        'Broadcasters\Channel\Models\AppChannel' => 'App\Policies\ChannelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
        
    }
}
