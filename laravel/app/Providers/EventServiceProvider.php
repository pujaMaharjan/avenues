<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\BroadcasterServicePaid' => [
            'App\Listeners\EmailServicePurchaseConfirmation'
        ],
        'App\Events\StatusUpdate'=>[
            'App\Listeners\UpdateStatusMessage'
        ],
        'App\Events\WhileUserLoggedInLoggedOut'=>[
            'App\Listeners\UpdateLoginInformation'
        ] ,
        'App\Events\WhileUserLoggedOut'=>[
            'App\Listeners\UpdateLogoutInformation'
        ],
        'App\Events\WhileBroadcasterRegister'=>[
            'App\Listeners\SendCreateMessage'
        ],
        'App\Events\AppServicePaid'=>[
            'App\Listeners\EmailAppApproved'
        ],
        'App\Events\ExpireDate'=>[
            'App\Listeners\EmailExpireDate'
        ]

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
    }
}
