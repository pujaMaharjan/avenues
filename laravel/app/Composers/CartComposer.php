<?php
namespace App\Composers;

use Illuminate\Contracts\View\View;
use Auth;
use App\PaymentRegisterDetail;

class CartComposer
{

	protected $auth;
	protected $paymentRegistrationDetail;

	public function __construct(Auth $auth,PaymentRegisterDetail $paymentRegisterDetail){
		$this->auth = $auth;
		$this->paymentRegisterDetail = $paymentRegisterDetail;
	}


	public function cart(View $view){
			$view->with('payment_details',$this->paymentRegisterDetail->where('user_id',Auth::id())->UnPaid()->first());
	}


}
