<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use App\Api\Transformers\DvrTransformer;
use Broadcasters\Vod\Providers\VodServiceProvider as VodService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use App\Api\Transformers\VodTransformer;
use App\Api\Transformers\AppTransformer;
use App\Api\Transformers\AdUnitTransformer;
use Broadcasters\AddUnitSet\Providers\AddUnitSetServiceProvider as AdUnitService;
use Broadcasters\Platform\Providers\PlatformServiceProvider as PlatformService;
use Broadcasters\Channel\Providers\ChannelServiceProvider as ChannelService;
use App\Api\Transformers\ChannelTransformer;
use Broadcasters\NewsApi\Providers\NewsApiServiceProvider as NewsApiService;
use App\Api\Transformers\NewsApiTransformer;
use App\Api\Transformers\MenuTransformer;
use App\Api\Transformers\NotificationChannelsTransformer;

use Broadcasters\Movie\MovieServiceProvider as MovieService;
use App\Api\Transformers\MoviesTransformer;
use Broadcasters\Templates\Providers\TemplateProvider;
use Broadcasters\Radios\RadioServiceProvider as RadioService;
use App\Api\Transformers\RadioTransformer;
use App\Api\Transformers\AdSetTransformer;
use App\Dvr;

class VodController extends ApiController
{
    protected $vodSerivce;
    protected $transformer;
    protected $appService;
    protected $platformService;
    protected $appTransformer;
    protected $adUnitTransformer;
    protected $adUnitService;
    protected $channelService;
    protected $channelTransformer;
    protected $newsApiService;
    protected $newsApiTransformer;
    protected $notificationChannelsTransformer;
    protected $movieService, $moviesTransformer;
    protected $radioService, $radioTransformer;
    protected $menuTransformer, $adSetTransformer;
    protected $DvrTransformer;

    function __construct(VodService $vodSerivce,
                         VodTransformer $transformer,
                         AppService $appService,
                         AppTransformer $appTransformer,
                         AdUnitTransformer $adUnitTransformer,
                         PlatformService $platformService,
                         AdUnitService $adUnitService,
                         ChannelService $channelService,
                         ChannelTransformer $channelTransformer,
                         NewsApiService $newsApiService,
                         NewsApiTransformer $newsApiTransformer,
                         NotificationChannelsTransformer $notificationChannelsTransformer,
                         MovieService $movieService,
                         MoviesTransformer $moviesTransformer,
                         RadioService $radioService,
                         RadioTransformer $radioTransformer,
                         TemplateProvider $templateProvider,
                         MenuTransformer $menuTransformer,
                         AdSetTransformer $adSetTransformer,
                         DvrTransformer $dvrTransformer
    )
    {
        $this->vodSerivce = $vodSerivce;
        $this->transformer = $transformer;
        $this->appService = $appService;
        $this->appTransformer = $appTransformer;
        $this->adUnitTransformer = $adUnitTransformer;
        $this->platformService = $platformService;
        $this->adUnitService = $adUnitService;
        $this->channelService = $channelService;
        $this->channelTransformer = $channelTransformer;
        $this->newsApiService = $newsApiService;
        $this->newsApiTransformer = $newsApiTransformer;
        $this->notificationChannelsTransformer = $notificationChannelsTransformer;
        $this->movieService = $movieService;
        $this->moviesTransformer = $moviesTransformer;
        $this->radioService = $radioService;
        $this->radioTransformer = $radioTransformer;
        $this->templateProvider = $templateProvider;
        $this->menuTransformer = $menuTransformer;
        $this->adSetTransformer = $adSetTransformer;
        $this->dvrTransformer = $dvrTransformer;
    }

    public function getVod($appId)
    {
        $vod = $this->vodSerivce->getByAppId($appId);
        // dd($vod);
        if (!$vod) {
            return $this->respondNotFound('Vod not found.');
        }
        return $this->respond([
            'data' => $this->transformer->transform($vod)
        ]);

    }

    public function getAllAPi($appId)
    {
        $platform = \Request::get('platform');
        $currentAppVersion = \Request::get('currentAppVersion');

        if ($platform != "ios" && $platform != "android") {
            $configResponse = $this->notFoundMessage('No config for this platform.');
        }
        $app = $this->appService->getWithUnits($appId);
        // dd($app);
        // dd(count($app->adUnit));
        if (!$app) {

            $infoResponse = $this->notFoundMessage('No App available.');
            $configResponse = null;
        } else {
            $infoResponse = $this->appTransformer->transform($app);
            $platformId = $this->platformService->setPlatform($platform);
            $configResponse = null;
            if (count($app->adUnit) > 0) {
                $adUnit = $this->adUnitService->getUnitByAppId($appId, $platformId);
                $configResponse = $this->adUnitTransformer->setAppVersion($currentAppVersion)->transform($adUnit);
            }

            //dd($adUnit);

        }

        //channel
        //$channel = $this->channelService->getChannel($appId);
        $channel = $this->channelService->getChannelByAppId($appId);
        // return $channel;
        if (!$channel) {
            $channelResponse = $this->notFoundMessage('No Channels available.');
        } else {
            $channelResponse = $this->channelTransformer->transform($channel);
        }


        //vod
        $vod = $this->vodSerivce->getByAppId($appId);
        if (!$vod) {
            $vodResponse = $this->notFoundMessage('No Vod available.');

        } else {
            //$vodTransformer = new \App\Transformers\VodTransformer();
            $vodResponse = $this->transformer->transform($vod);
        }
//external news api 

        $newsAppApiSources = $this->newsApiService->getNews($appId);
        // dd($newsAppApiSources);
        if (!$newsAppApiSources) {
            $newsAppApiResponse = $this->notFoundMessage('No Api available.');
        } else {
            //$newsAppTransformer = new \App\Transformers\NewsAppTransformer();
            $newsAppApiResponse = $this->newsApiTransformer->transform($newsAppApiSources);

        }

        $subscribeNotificationChannels = $this->appService->getSubcribeChannels($appId);
        //dd($subscribeNotificationChannels);
        if (count($subscribeNotificationChannels) == 0) {
            $subscribeNotificationChannelsResponse = $this->notFoundMessage('No data available.');
        } else {
            $subscribeNotificationChannelsResponse = $this->notificationChannelsTransformer->transform($subscribeNotificationChannels);
        }


        //dvr
        $dvr = Dvr::where('app_id', $appId)->get();
        if ($dvr == '[]') {
            $dvrResponse = $this->notFoundMessage('No Dvr available.');
        } else {
            $dvrResponse = $this->dvrTransformer->transform($dvr);
        }

        //movies
        $movies = $this->movieService->all($appId);
        if (!$movies) {
            $moviesResponse = $this->notFoundMessage('No Movie available.');
        } else {
            $moviesResponse = $this->moviesTransformer->transform($movies);
        }


//radio
        $radio = $this->radioService->getRadioByAppId($appId);


        if (!$radio) {
            $radioResponse = $this->notFoundMessage('No Radio available.');
        } else {
            $radioResponse = $this->radioTransformer->transform($radio);
        }
        $adResponse = $this->adUnitService->getAdSetting($appId, $this->platformService->setPlatform($platform));
        $adResponse = $this->adSetTransformer->transform($adResponse);

        if (apiEncrypt() == true) {
            $response = [
                "menus" => \Config::get('cms.menus'),
                "general" => ["CurrentTimestampUrl" => route('CurrentTimestamp')],
                'info' => $infoResponse,
                'config' => $configResponse,
                "channels" => $channelResponse,
                "vod" => $vodResponse,
                "movies" => $moviesResponse,
                "dvr" => $dvrResponse,
                "radio" => $radioResponse,
                "news" => $newsAppApiResponse,
                "SubscribeNotificationChannels" => $subscribeNotificationChannelsResponse,
                "adset" => $adResponse,

            ];
            return $this->respond($response);
        }
        $response = $this->notFoundMessage('No data available.');
        return $this->respond($response);


    }


    public function returnAllAPi($app_code)
    {


        $appId = $this->appService->getIdByAppCode($app_code);
        $platform = \Request::get('platform');
        $currentAppVersion = \Request::get('currentAppVersion');

        if ($platform != "ios" && $platform != "android") {
            $configResponse = $this->notFoundMessage('No config for this platform.');
        }
        $app = $this->appService->getWithUnits($appId);
        // dd($app);
        // dd(count($app->adUnit));
        if (!$app) {

            $infoResponse = $this->notFoundMessage('No App available.');
            $configResponse = null;
        } else {
            $infoResponse = $this->appTransformer->transform($app);
            $platformId = $this->platformService->setPlatform($platform);
            $configResponse = null;
            if (count($app->adUnit) > 0) {
                $adUnit = $this->adUnitService->getUnitByAppId($appId, $platformId);
                $configResponse = $this->adUnitTransformer->setAppVersion($currentAppVersion)->transform($adUnit);
            }

            //dd($adUnit);

        }

        //channel
        //$channel = $this->channelService->getChannel($appId);
        $channel = $this->channelService->getChannelByAppId($appId);
        // dd($channel);
        // return $channel;
        if (!$channel) {
            $channelResponse = [];
        } else {
            $channelResponse = $this->channelTransformer->transform($channel);
        }


        //vod
        $vod = $this->vodSerivce->getByAppId($appId);
        if (!$vod) {
            $vodResponse = $this->notFoundMessage('No Vod available.');

        } else {
            //$vodTransformer = new \App\Transformers\VodTransformer();
            $vodResponse = $this->transformer->transform($vod);
        }
//external news api 

        $newsAppApiSources = $this->newsApiService->getNews($appId);
        // dd($newsAppApiSources->count());
        if (!$newsAppApiSources->count()) {
            $newsAppApiResponse = [];
        } else {
            //$newsAppTransformer = new \App\Transformers\NewsAppTransformer();
            $newsAppApiResponse = $this->newsApiTransformer->transform($newsAppApiSources);

        }
        // dd($newsAppApiResponse);

        $subscribeNotificationChannels = $this->appService->getSubcribeChannels($appId);
        //dd($subscribeNotificationChannels);
        if (count($subscribeNotificationChannels) == 0) {
            $subscribeNotificationChannelsResponse = $this->notFoundMessage('No data available.');
        } else {
            $subscribeNotificationChannelsResponse = $this->notificationChannelsTransformer->transform($subscribeNotificationChannels);
        }

        $templates = $this->templateProvider->getwithTemplate($appId);

        $movies = $this->movieService->all($appId);
        if (!$movies) {
            $moviesResponse = $this->notFoundMessage('No Movie available.');
        } else {
            $moviesResponse = $this->moviesTransformer->transform($movies);
        }

        $radio = $this->radioService->getRadioByAppId($appId);


        if (!$radio) {
            $radioResponse = $this->notFoundMessage('No Radio available.');
        } else {
            $radioResponse = $this->radioTransformer->transform($radio);
        }

        $menus = $this->appService->getById($appId)->menus()->orderBy('menu_order')->get();


        if (!$menus->count() > 0) {
            $menuResponse = ['error' => 'not aviliable'];
        } else {
            $menuResponse = $this->menuTransformer->transform($menus);
        }


        $adResponse = $this->adUnitService->getAdSetting($appId, $this->platformService->setPlatform($platform));

        // dd($adResponse);


        $adResponse = $this->adSetTransformer->transform($adResponse);


        if (apiEncrypt() == true) {
            $response = [
                "menus" => $menuResponse,
                "general" => ["CurrentTimestampUrl" => route('CurrentTimestamp')],
                'info' => $infoResponse,
                'config' => $configResponse,
                "channels" => $channelResponse,
                "vod" => $vodResponse,
                "movies" => $moviesResponse,
                "radio" => $radioResponse,
                "news" => $newsAppApiResponse,
                "SubscribeNotificationChannels" => $subscribeNotificationChannelsResponse,
                "templates" => $templates,
                "adset" => $adResponse,
            ];
            return $this->respond($response);
        }
        $response = $this->notFoundMessage('No data available.');
        return $this->respond($response);


    }

    public function getAbout()
    {
        return ['content' => \Config::get('cms.content')];
    }
}