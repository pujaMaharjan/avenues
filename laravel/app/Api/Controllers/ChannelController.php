<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use Broadcasters\Channel\Providers\ChannelServiceProvider as ChannelService;

class ChannelController extends ApiController
{
    protected $channel;
    protected $transformer;

    function __construct(\App\Api\Transformers\ChannelTransformer $transformer, ChannelService $channel)
    {
        $this->transformer = $transformer;
        $this->channel = $channel;
    }

    public function get($id)
    {
        $channel = $this->channel->getChannelByid($id);
        if (!$channel) {
            return $this->respondNotFound('Channel not found.');
        }

        return $channel;
    }

}
