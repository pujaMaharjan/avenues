<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use Broadcasters\App\Models\App;
use Broadcasters\Templates\Providers\TemplateProvider;


class AppController extends ApiController
{

	public function __construct(App $app,TemplateProvider $templateProvider)
	{
		$this->app = $app;
		$this->templateProvider = $templateProvider;
	}
	public function apiStatus($app_id)
	{
		$app = $this->app->where('app_code',$app_id)->first();
		if(is_null($app)){
			return ['message'=>'Unaviliable','status'=>0];
		}
		$app_id = $app->app_id;
		$temp = $this->templateProvider->getwithTemplate($app_id);
		// foreach($app as $value){
			// dd($value);
			$api = '';
			if($app['expire_at'] > date('Y-m-d H-i-s')){
				$api = '192.168.1.1';
			}
			$data = [
				'app_name'=>$app['app_name'],
				'description'=>$app['description'],
				'status'=>$app['status'],
				'expire_at'=>$app['expire_at'],
				'publishable'=>($app['expire_at'] > date('Y-m-d H-i-s'))?:false,
				'api'=>$api,
				'message'=>($app['expire_at'] > date('Y-m-d H-i-s'))?'Not Expired':'Expired',
				'temp_detail'=>$temp,
				'splashimage'=>asset('uploads/'.$app->splashimage),
				'terms_condition'=>$app['terms_condition']

			];
		// }
		return $data;
	}

	

}