<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use Broadcasters\Templates\Providers\TemplateProvider;
use Broadcasters\App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
class TemplateController extends ApiController {

	protected $templateProvider;

	public function __construct(TemplateProvider $templateProvider,AppServiceProvider $appService)
	{
		$this->templateProvider = $templateProvider;
		$this->appService = $appService;
	}

	public function getByAppId($app_id){
		$data = $this->templateProvider->getwithTemplate($app_id);
		return $data;
	}

	public function getLayouts()
	{
		return $this->templateProvider->getLayouts();
	}

	public function getServicesById($app_id)
	{
		$services =  $this->appService->getService($app_id);
		$data = [];
		foreach($services as $i=>$service){
			$data[$i] = [
				'name'=>$service->service_name
			];
		}
		return $data;
	}

	public function dumpRecords(Request $request)
	{
		$data =  $request->all();
		\App\Dump::create(['data'=>$request->all()]);
		return ['sucess'=>1,'message'=>'saved'];
	}

}