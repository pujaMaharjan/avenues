<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use Broadcasters\AddUnitSet\Providers\AddUnitSetServiceProvider as AdUnitService;
use App\Api\Transformers\AdUnitTransformer;
class AdUnitController extends ApiController
{
	protected $adUnitService;
	protected $transformer;
	function __construct(AdUnitService $adUnitService,AdUnitTransformer $transformer) {
		$this->adUnitService = $adUnitService;
		$this->transformer = $transformer;
	}

	public function getAdUnit($AppId)
	{
		$adUnit = $this->adUnitService->getUnitByAppId($AppId);
		//dd($adUnit);
		if(!$adUnit){
			return $this->respondNotFound('Epg not found.');
		}
		return $this->respond([
			'data'=>$this->transformer->transform($adUnit)
			]);

	}

}
