<?php
namespace App\Api\Controllers;

use App\Api\ApiController;

use Broadcasters\Radios\RadioServiceProvider as RadioService;
//use Broadcasters\Api\Transformers\AdUnitTransformer;
class RadioController extends ApiController
{
    protected $RadioService;
    //protected $transformer;
    function __construct(RadioService $radioService) {
        $this->radioService = $radioService;
      //  $this->transformer = $transformer;
    }

    public function getstream($id)
    {

       $secretKey = \Config::get('site.appHashSecretKey');
       $utc = (int)trim(\Request::get('utc'));
       $apphash = trim(\Request::get('hash'));
       $hash = md5($secretKey.$utc);
        //$data = ['utc'=>$utc,'apphash'=>$apphash,'hash'=>$hash];

       if($apphash!==$hash){
        return $this->respondNotFound('Stream not found-hash error.');

    }
    if(getUtc()-$utc>30){
        return $this->respondNotFound('Stream not found - time error.');    
    }
    $type="wms";
    $url = $this->radioService->getStreamUrl($id,$type);

    if(!$url){
        return $this->respondNotFound('Stream not found radio-channel error.');
    }
    return $this->respond([
        'stream'=>$url,
        'type'=>$type
        ]);

}

}
