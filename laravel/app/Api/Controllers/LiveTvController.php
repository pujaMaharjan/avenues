<?php namespace App\Api\Controllers;

use Broadcasters\Channel\Providers\ChannelServiceProvider as ChannelService;
use App\Api\ApiController;

class LiveTvController extends ApiController
{


    protected $channel;

    function __construct(ChannelService $channel)
    {
        $this->channel = $channel;

    }


    public function getstream($id)
    {
        $secretKey = \Config::get('site.appHashSecretKey');
        $utc = (int)trim(\Request::get('utc'));
        $apphash = trim(\Request::get('hash'));


        $hash = md5($secretKey . $utc);
        //$data = ['utc'=>$utc,'apphash'=>$apphash,'hash'=>$hash];
        //dd($hash);
        if ($apphash !== $hash) {
            return $this->respondNotFound('Stream not found-hash error.');

        }
        if (getUtc() - $utc > 300) {
            return $this->respondNotFound('Stream not found - time error.');
        }
        $type = "wms";
        $url = $this->channel->getStreamUrl($id, $type);

        if (!$url) {
            return $this->respondNotFound('Stream not found -channel error.');
        }
        return $this->respond([
            'stream' => $url,
            'type' => $type
        ]);

    }

    public function liveStream($id)
    {


        $type = "wms";
        $url = $this->channel->getStreamUrl($id, $type);
        if (!$url) {
            return $this->respondNotFound('Stream not found -channel error.');
        }
        return view('channel.stream')->with(compact(['url']));

    }

}
