<?php
namespace App\Api\V1\Controllers;

use App\Api\ApiController;
use Broadcasters\Movie\MovieServiceProvider as MovieService;
//use Broadcasters\Api\Transformers\AdUnitTransformer;
class MovieController extends ApiController
{
    protected $MovieService;
    protected $transformer;
    function __construct(MovieService $movieService) {
        $this->movieService = $movieService;
      //  $this->transformer = $transformer;
    }

    public function getstream($id)
    {
     $type="wms";
     $url = $this->movieService->getStreamUrl($id,$type);
     $secretKey = \Config::get('site.appHashSecretKey');
     $utc = (int)trim(\Request::get('utc'));
     $apphash = trim(\Request::get('hash'));
     $hash = md5($secretKey.$utc);
        //$data = ['utc'=>$utc,'apphash'=>$apphash,'hash'=>$hash];

     if($apphash!==$hash){
        return $this->respondNotFound('Stream not found-hash error.');

    }
    if(getUtc()-$utc>30){
        return $this->respondNotFound('Stream not found - time error.');    
    }
    $type="wms";
    $url = $this->movieService->getStreamUrl($id,$type);

    if(!$url){
        return $this->respondNotFound('Stream not found radio-channel error.');
    }
    
    return $this->respond([
        'data'=>['stream'=>$url],
        'type'=>$type
        ]);

}

}
