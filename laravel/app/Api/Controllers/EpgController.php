<?php
namespace App\Api\Controllers;

use App\Api\ApiController;
use Broadcasters\Epg\Providers\EpgServiceProvider as EpgService;
use App\Api\Transformers\EpgTransformer;
class EpgController extends ApiController
{
	protected $epgSerivce;
	protected $transformer;
	function __construct(EpgService $epgSerivce,EpgTransformer $transformer) {
		$this->epgSerivce = $epgSerivce;
		$this->transformer = $transformer;
	}

	public function getEpg($channelId)
	{
		$epg = $this->epgSerivce->getByChannel($channelId);
		// dd($epg);
		if(!$epg){
			return $this->respondNotFound('Epg not found.');
		}
		return $this->respond([
			'data'=>$this->transformer->transform($epg)
			]);

	}

}
