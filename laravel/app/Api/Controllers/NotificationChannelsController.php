<?php
namespace Broadcasters\Api\V1\Controllers;

use Broadcasters\Api\ApiController;
use Broadcasters\App\Providers\AppServiceProvider as AppService;

use Broadcasters\Api\Transformers\NotificationChannelsTransformer;
class NotificationChannelsController extends ApiController
{
	protected $appService;
	protected $transformer;
	function __construct(AppService $appService ,NotificationChannelsTransformer $transformer) {
		$this->appService = $appService;
		$this->transformer = $transformer;
	}

	public function getNotificationChannels($appId)
	{
		$subscribeNotificationChannels = $this->appService->getSubcribeChannels($appId);
		//dd(count($subscribeNotificationChannels));
		if(count($subscribeNotificationChannels) == 0){
			return $this->respondNotFound('Subscribe Notification Channels not found.');
		}
		//dd($subscribeChannels);
		//return $subscribeNotificationChannels;
		return $this->respond([
			'data'=>$this->transformer->transform($subscribeNotificationChannels)
			]);

	}

}
