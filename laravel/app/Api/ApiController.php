<?php namespace App\Api;

use App\Http\Controllers\BaseController;

class ApiController extends BaseController
{

    protected $statusCode = 200;

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function respondNotFound($msg = "Not Found")
    {
        return $this->setStatusCode('400')->respondWithError($msg);
    }


    public function respond($data, $headers = [])
    {
        $headers = ['Cache-Control' => 'max-age=2592000', 'Access-Control-Allow-Origin' => '*'];
        return \Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($msg)
    {
        return $this->respond([
            'error' => [
                'message' => $msg,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

    public function notFoundMessage($msg = "Not Found")
    {
        return [
            'error' => [
                'message' => $msg,
                'status_code' => '400'
            ]
        ];
    }
}