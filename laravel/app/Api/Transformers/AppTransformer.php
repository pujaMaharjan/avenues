<?php
namespace App\Api\Transformers;

use App\Api\Transformers\Transformer;
class AppTransformer extends Transformer
{

	public  function transform($model){

		return [
		'app name'=>$model['app_name'],
		'app_logo'=>asset('uploads/'.$model['logo']),
		'description'=>$model['description'],
		'web_link'=>$model->web_link,
		'web_link_usable'=>($model->web_link=='' || $model->web_link==null)?false:true,
		];
	}

}