<?php
namespace App\Api\Transformers;
use App\Api\Transformers\Transformer;

class AdUnitTransformer extends Transformer
{
	protected $currentAppVersion;
	public  function transform($model){
		
		// dd($model);
		// dd($this->currentAppVersion);

		$update = false;

		if($this->currentAppVersion!=null){
			$currentno = filter_var($this->currentAppVersion, FILTER_SANITIZE_NUMBER_INT);
			
			$latestno = 0;
			// dd($model);
			if($model[0]->app_version)
			{
				$latestno = $model[0]->app_version;
			}
			$latestno = filter_var($latestno, FILTER_SANITIZE_NUMBER_INT);
			
			//$latestno = filter_var($latestno, FILTER_SANITIZE_NUMBER_INT);
			if($latestno>$currentno){
				$update = true;
			}
			//dd($update);
		}
		foreach ($model as $i=>$value) {
			$adUnits[$i] = [$value->type =>$value->unit_name];

		}
		$result= [];
		if(isset($adUnits))
			for($i=0; $i<count($adUnits); $i++)
			{
				$result = array_merge($adUnits[$i],$adUnits[++$i]);
			}
		//dd($result);
		//$result = array_merge($adUnits[0],$adUnits[1]);
		return [
		'info'=>'null',
		'addcode'=>$result,
		'appUpdate'=>$update

		];
		
	}
	
	public function FunctionName($appId)
	{
		return ['admob'=>'','abc'=>abc];
	}

	public function setAppVersion($version){
		$this->currentAppVersion = $version;
		return $this;
	}

}