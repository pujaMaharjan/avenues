<?php
namespace App\Api\Transformers;
use App\Api\Transformers\Transformer;

class VodTransformer extends Transformer
{

	public  function transform($model){
		//dd($model[''])
		//$cod = unserialize($model->videos_count);
		//dd($cod);
		
		return [
		'channel'=>$model->youtube_channel_id,
		'featured_playlist'=>$model->feature_playlist,
		'count'=>$model->videos_count
		];
	}

}