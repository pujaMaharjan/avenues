<?php
namespace App\Api\Transformers;

use App\Api\Transformers\EpgTransformer as EpgTransformer;
use App\Api\Transformers\Transformer;

// use Broadcasters\Api\Transformers\EpgTransformer;
class ChannelTransformer extends Transformer
{
    public function transform($model)
    {

        foreach ($model as $i => $model) {
            $wmsAuthSign = getUrlSignature($model['url_token_key'], $model['valid_time']);
            $offset = 5 * 60 * 60 + 45 * 60; //converting 5 hours to seconds.
            $dateFormat = "N";
            $today = gmdate($dateFormat, time() + $offset) % 7 + 1;
            $epgs = null;
            if ($model['appChannelEpg'])
                $epgs = $model['appChannelEpg']->app_channel_epg_value;
            if ($model->appEpgDetails->count()) {
                $epgs = $this->transformEpgPrograms($model['appEpgDetails']);
            }
            //dd($epgs);
            $data[$i] = [
                'id' => $model['app_channel_id'],
                'name' => $model['name'],
                'today' => $today,
                'source' => $model['sources_local_url'] . '?wmsAuthSign=' . $wmsAuthSign,
                'logo' => asset('uploads/' . $model['logo']),
                'stream_url' => route('ChannelStreamUrl', $model['app_channel_id']),
                'stream_type' => \Config::get('site.streamAuthType'),
                'epg' => $epgs ? $epgs : (object)['programs' => ['data' => []]]

            ];

        }
        return $data;
    }

    public function transformEpgPrograms($data)
    {
        $epgTransformer = new EpgTransformer();
        return $epgTransformer->transform($data);
    }

}
