<?php
namespace App\Api\Transformers;
use App\Api\Transformers\Transformer;

class RadioTransformer extends Transformer
{
	
	public  function transform($model){
		$data['name'] =  $model->name;
		$data['url'] =  route('RadioStreamUrl',$model['radio_id']);
		$data['logo'] =  asset('uploads/radios/'.$model->logo);
		return $data;
	}
}