<?php
namespace App\Api\Transformers;

use App\Api\Transformers\Transformer;

class EpgTransformer extends Transformer
{

    public function transform($model)
    {
        //dd($model);
        //$model = $model->appChannelEpgDetail;

        $programs = $this->transformPrograms($model);
        // dd($model);
        return [
            'programs' => $programs
        ];
    }

    public function gmtCalc()
    {
        $ip_address = \Request::ip();
        $ip_country = geoip_country_code_by_name($ip_address);
        $timezone = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $ip_country);
        $dtz = new \DateTimeZone($timezone[0]);
        $time_in_country = new \DateTime('now', $dtz);

        $gmtOffset = $dtz->getOffset($time_in_country) / 3600;
        $arrayGmtOffset = explode(".", $gmtOffset);
        if (sizeof($arrayGmtOffset) > 1) {
            if ($arrayGmtOffset[1] == "75") {
                $gmtOffset = $arrayGmtOffset[0] . "." . 45;
            }
            if ($arrayGmtOffset[1] == "5") {
                $gmtOffset = $arrayGmtOffset[0] . "." . 30;
            }
            if ($arrayGmtOffset[1] == "25") {
                $gmtOffset = $arrayGmtOffset[0] . "." . 15;
            }
        }

        if ($gmtOffset < 0) {
            $calGmt = -5.45 + $gmtOffset;
        } else if ($gmtOffset < 5.45 && $gmtOffset > 0) {
            $calGmt = ($gmtOffset) - 5.45;
        } else if ($gmtOffset > 5.45) {
            $gmtOffsetArr = explode('.', $gmtOffset);
            if (sizeof($gmtOffsetArr) == 1) {
                $d = $gmtOffset - 1 . '.' . 60;
                $calGmt = $d - 5.45;
            } else {
                $dh = $gmtOffsetArr[0] - 1 - 5;
                $dm = ($gmtOffsetArr[1] - 45);
                if ($dm < 0) {
                    $dm = 60 + $dm;
                } else {
                    $dm = 60 - $dm;
                }
                if ($dm < 0) {
                    $dm = $dm * (-1);
                }
                $calGmt = $dh . '.' . $dm;
            }
        } else if ($gmtOffset == 0) {
            $calGmt = -5.45;
        } else if ($gmtOffset == 5.45) {
            $calGmt = 0;
        }

        return $calGmt;
    }

    public function transformPrograms($schedule)
    {
        $gmtOffset = $this->gmtCalc();
        $data = [];
//        $offset = 5 * 60 * 60 + 45 * 60; //converting 5 hours to seconds.
//        $dateFormat = "N";
//        $today = gmdate($dateFormat, time() + $offset) % 7;
        foreach ($schedule as $s) {
            $time = explode(":", $s['start_time']);
            if ($gmtOffset != 0) {
                $gmt_time = explode(".", $gmtOffset);
                if (sizeof($time) > 1) {
                    if (sizeof($gmt_time) > 1) {
                        $time_min = $time[1] + $gmt_time[1];
                        $time_hr = $time[0] + $gmt_time[0];
                        $time_hr = ($time_min >= 60) ? $time_hr = $time_hr + 1 : $time_hr;
                    } else {
                        $time_min = $time[1] + 0;
                        $time_hr = $time[0] + $gmt_time[0];
                        $time_hr = ($time_min >= 60) ? $time_hr = $time_hr + 1 : $time_hr;
                    }
                } else {
                    $time_min = 0 + $gmt_time[1];
                    $time_hr = $time[0] + $gmt_time[0];
                    $time_hr = ($time_min >= 60) ? $time_hr = $time_hr + 1 : $time_hr;
                }
                if ($time_hr >= 24) {
                    $s['day'] = $s['day'] + 1;
                    if ($s['day'] > 7) {
                        $s['day'] = 1;
                    }
                } else if ($time_hr < 0) {
                    $s['day'] = $s['day'] - 1;
                    if ($s['day'] < 1) {
                        $s['day'] = 7;
                    }
                }
            }
        }
        foreach ($schedule->groupBy('day') as $i => $value) {
            $data['data'][] = [
                'day' => $i,
                'detail' => $this->transformDetails($value)
            ];
        }
        return $data;
    }

    public function transformDetails($schedule)
    {
        $gmtOffset = $this->gmtCalc();
        $data = [];

        foreach ($schedule as $i => $value) {
            if ($gmtOffset < 0) {
                $gmtOffset_arr = explode('.', $gmtOffset);
                $start_time = explode(":", $value["start_time"]);
                if (sizeof($start_time) > 1) {
                    if (sizeof($gmtOffset_arr) > 1) {

                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        if ($start_time_hr < 0) {
                            $start_time_hr = 24 + $start_time_hr;
                        }

                        $start_time_min = $start_time[1] + $gmtOffset_arr[1];
                        if ($start_time_min < 0) {
                            $start_time_min = 60 + $start_time_min;
                        }

                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    } else {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        if ($start_time_hr < 0) {
                            $start_time_hr = 24 + $start_time_hr;
                        }
                        $start_time_min = $start_time[1] + 0;
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    }

                } else {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = 0 + $gmtOffset_arr[1];
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    } else {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = 0 + 0;
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    }
                }
                if ($start_time_hr < 0) {
                    $start_time_hr = $start_time_hr * (-1);
                }
                if ($start_time_min < 0) {
                    $start_time_min = $start_time_min * (-1);
                }
                $start_time = $start_time_hr . '.' . $start_time_min;

                $end_time = explode(":", $value["end_time"]);


                if (sizeof($end_time) > 1) {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        if ($end_time_hr < 0) {
                            $end_time_hr = 24 + $end_time_hr;
                        }


                        $end_time_min = $end_time[1] + $gmtOffset_arr[1];
                        if ($end_time_min < 0) {
                            $end_time_min = 60 + $end_time_min;
                        }

                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    } else {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        if ($end_time_hr < 0) {
                            $end_time_hr = 24 + $end_time_hr;
                        }
                        $end_time_min = $end_time[1] + 0;
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }
                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    }

                } else {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = 0 + $gmtOffset_arr[1];
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    } else {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = 0 + 0;
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }

                    }
                }
                $end_time = $end_time_hr . '.' . $end_time_min;

            } elseif ($gmtOffset > 0) {

                $start_time = explode(":", $value["start_time"]);
                $gmtOffset_arr = explode('.', $gmtOffset);
                if (sizeof($start_time) > 1) {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = $start_time[1] + $gmtOffset_arr[1];
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }
                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    } else {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = $start_time[1] + 0;
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    }

                } else {
                    if (sizeof($gmtOffset) > 1) {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = 0 + $gmtOffset_arr[1];
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    } else {
                        $start_time_hr = $start_time[0] + $gmtOffset_arr[0];
                        $start_time_min = 0 + 0;
                        if ($start_time_hr > 23) {
                            $start_time_hr = $start_time_hr - 24;
                        }

                        if ($start_time_min >= 60) {
                            $start_time_hr = $start_time_hr + 1;
                            $start_time_min = $start_time_min - 60;
                        }
                    }
                }
                $start_time = $start_time_hr . '.' . $start_time_min;

                $end_time = explode(":", $value["end_time"]);


                if (sizeof($end_time) > 1) {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = $end_time[1] + $gmtOffset_arr[1];
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
//                }

                    } else {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = $end_time[1] + 0;
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    }

                } else {
                    if (sizeof($gmtOffset_arr) > 1) {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = 0 + $gmtOffset_arr[1];
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    } else {
                        $end_time_hr = $end_time[0] + $gmtOffset_arr[0];
                        $end_time_min = 0 + 0;
                        if ($end_time_hr > 23) {
                            $end_time_hr = $end_time_hr - 24;
                        }

                        if ($end_time_min >= 60) {
                            $end_time_hr = $end_time_hr + 1;
                            $end_time_min = $end_time_min - 60;
                        }
                    }
                }
                $end_time = $end_time_hr . '.' . $end_time_min;
                $data[] = [
                    'name' => $value['program_name'],
                    'start' => $start_time,
                    'end' => $end_time
                ];
            }
            return $data;
        }
    }
}