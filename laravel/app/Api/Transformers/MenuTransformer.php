<?php
namespace App\Api\Transformers;

use App\Api\Transformers\Transformer;
class MenuTransformer extends Transformer
{
	
	public  function transform($menus){
		
		foreach ($menus as $key => $value) {
			$data[$key] = [
			'title'=>$value->menu_title,
			'logo'=>asset('uploads/menus/'.$value->logo)
			];
		}
		//dd($data);

		return $data;

	}
}