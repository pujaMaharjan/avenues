<?php
namespace App\Api\Transformers;

use App\Api\Transformers\Transformer;

class DvrTransformer extends Transformer
{
    public function transform($model)
    {
        $data = [];
        try {
            if ($model) {
                foreach ($model as $key => $value) {
                    $data['data'][] = [
                        'name' => array_key_exists("name", $value) ? $value['name'] : '',
                        'url' => array_key_exists("app_id", $value) && array_key_exists("id", $value) ? url('api/' . $value['app_id'] . '/dvrurl/' . $value['id']) : '',
                        'created_by' => array_key_exists("created_by", $value) ? $value['created_by'] : '',
                        'option' => array_key_exists("option", $value) ? $value['option'] : ''
                    ];
                }
            }
            return $data;
        } catch (\Exception $e) {
            return ($data['data'][] = [$e->getMessage()]);
        }

    }
}