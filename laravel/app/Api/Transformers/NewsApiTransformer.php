<?php
namespace App\Api\Transformers;
use App\Api\Transformers\Transformer;

class NewsApiTransformer extends Transformer
{



	public function exterapiTransForm($model){
			// return $model->appExternalApi;
		$data = [];
			foreach($model->appExternalApi as $newsApi){
				$data[] = 
					array_combine($newsApi['app_external_news']['key'],$newsApi['app_external_news']['value']);
				
			}
			return $data;
		}


		public  function transform($model){
			$data = [];
			foreach($model as $value){
				$data[] = [
					'name'=>$value['name'],
					'web_link'=>$value->web_link,
					'logo'=>asset('uploads/'.$value->logo),
					'sources'=>$this->exterapiTransForm($value)
				];	
			}
			return $data;
			
		}



}