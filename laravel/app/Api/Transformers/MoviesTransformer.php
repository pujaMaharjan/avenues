<?php
namespace App\Api\Transformers;

use App\Api\Transformers\Transformer;

class MoviesTransformer extends Transformer
{

    public function transform($model)
    {

        $data = [];
        foreach ($model as $key => $value) {

            $wmsAuthSign = getUrlSignature($value['url_token_key'], $value['valid_time']);
            $data['data'][] = [
                'id' => $value['app_movie_id'],
                'movie_name' => $value['movie_name'],
                'youtube_id' => $value['youtube_id'],
                'stream_url' => route('MovieStreamUrl', $value['app_movie_id'])
            ];
        }
        return $data;
    }
}