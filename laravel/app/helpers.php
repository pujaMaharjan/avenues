<?php
function setActive($path, $active ='active')
{
	$prefix = "admin";
	//$prefix = \Auth::user()->role_id==1?"admin":"broadcaster";
	if($path=="/"){
		$path = "admin";
		return Request::is($path) ?$active:"";
	}
	$path = $prefix.$path;

	return Request::is($path.'*') ?$active:"";

}

function setBroadcasterActive($path, $active="active")
{
	$prefix = "broadcaster";
	$appId = Request::segment(3);

	if($path == '/app')
	{
		
		$path = $prefix.$path.'/'.$appId;

		return Request::is($path)?$active:"";
	}
	if($path == '/')
	{
		$path = $prefix;
		return Request::is($path)?$active:"";
	}
	$path = $prefix.$path;
	//dd($path);
	return Request::is($path.'*') ?$active:"";
	

	
}


function getUrlSignature($key,$validminutes = 10)
{
	//dd($validminutes);
	$validminutes = (int)$validminutes;
	$today = gmdate("n/j/Y g:i:s A");
	$ip = '202.166.205.145';
	$ip = $_SERVER['REMOTE_ADDR'];
	//dd($ip);
	$str2hash = $ip . $key . $today . $validminutes;
	$md5raw = md5($str2hash, true);
	$base64hash = base64_encode($md5raw);
	$urlsignature = "server_time=" . $today . "&hash_value=" . $base64hash . "&validminutes=$validminutes";
	return base64_encode($urlsignature);
}
function getPpvUrlSignature($key,$validminutes = 10){

	$today = gmdate("n/j/Y g:i:s A");
	$id = 1;
  //$key = "NETKEYWNnetTOK20QR15";
	$validminutes = 1;
	$str2hash = $id . $key . $today . $validminutes;
	$md5raw = md5($str2hash, true);
	$base64hash = base64_encode($md5raw);
	$urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=" . $validminutes . "&id=" . $id;
	return base64_encode($urlsignature);
}

function getUtc() {
	$date = new DateTime(gmdate("Y-m-d H:i:s"));
	return $date->getTimestamp();
}

function apiEncrypt() {

	// $secretKey = \Config::get('site.appHashSecretKey');
	// $utc = (int)trim(\Request::get('utc'));
	// $apphash = trim(\Request::get('hash'));

	
	// $hash = md5($secretKey.$utc);
	
	// if($apphash!==$hash){
	// 	return false;

	// }
	// if(getUtc()-$utc>30){
	// 	return false;	
	// }
	return true;

}
