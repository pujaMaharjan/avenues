<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $primaryKey = 'app_id';

    protected $fillable = ['app_name','description','expire_at','status','logo','splashimage','app_code'];

    protected $table = 'app';

    public function channels()
    {
        return $this->hasMany('Broadcasters\Channel\Models\AppChannels','app_id');
    }
    public function appNews()
    {
        return $this->hasmany('Broadcasters\Models\AppNews','app_id');
    }
    public function appVod()
    {
        return $this->hasmany('Broadcasters\Vod\Models\AppVod','app_id');
    }

    public function services()
    {
        return $this->belongsToMany(\Broadcasters\Models\Services::class,'app_services','app_id','service_id');
    }

    public function user()
    {
        return $this->hasMany(App\User::class);
    }

    public function templates()
    {
        return $this->hasMany(\Broadcasters\Templates\Models\AppTemplate::class);
    }

    public function paymentReachedStep(){
        return $this->hasOne(PaymentReachStep::class);
    }

     public function paymentDetail(){
        return $this->hasMany(PaymentDetail::class);
    }

    public function movie()
    {
        return $this->hasMany('Broadcasters\Movie\AppMovie');
    }

    public function radios()
    {
        return $this->hasMany('Broadcasters\Radios\Radio');
    }
    public function audios()
    {
        return $this->hasMany('App\Audio');
    }

   public function appChannelEpgs(){
        return $this->hasMany('Broadcasters\Epg\Models\AppChannelEpg');
   }
}
