<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collector extends Model
{
    protected $table = 'sessions';

    protected $fillable = ['data','user_id','services'];
    protected $cast = ['data'=>'array'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

 

    public function setdataAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }
     public function getdataAttribute($value)
    {
        return  json_decode($value);
    }

/*
   
     public function setservicesAttribute($value)
    {
        $this->attributes['services'] = json_encode($value);
    }
*/
}
