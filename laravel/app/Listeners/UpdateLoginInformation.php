<?php

namespace App\Listeners;

use App\Events\WhileUserLoggedInLoggedOut;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Broadcasters\LoginHistory\Models\LoginHistory;
use Broadcasters\User\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
class UpdateLoginInformation
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LoginHistory $loginHistory,Request $request)
    {
        $this->loginHistory = $loginHistory;
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  WhileUserLoggedInLoggedOut  $event
     * @return void
     */
    public function handle(WhileUserLoggedInLoggedOut $event)
    {
        //dd($event->user->remember_token);

        $history = $this->loginHistory->create([
            'user_id'=>$event->user->user_id,
            'login_time'=>Carbon::now(),
            'device_ip'=>$_SERVER['REMOTE_ADDR'],
            'user_agent'=>$_SERVER['HTTP_USER_AGENT']
            ]);
        //$value = $this->request->cookie('user');
        //   dd($value);

        // $response = new \Illuminate\Http\Response($history);
        // //return $response;


        
        // //$value = Cookie::get('name');
        // $respo = $response->withCookie(cookie('user',$history, 60));
        // return $respo;

        // dd("dsa");
        //dd($respo);
        //session_set_cookie_params(3600,"/");
        $forever = 2000000000;
        setcookie ("login_histroy", serialize($history),$forever);
        //\Session::put('login_histroy',$history);

    }
}
