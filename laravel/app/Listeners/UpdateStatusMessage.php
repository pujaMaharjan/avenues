<?php

namespace App\Listeners;

use App\Events\StatusUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateStatusMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatusUpdate  $event
     * @return void
     */
    public function handle(StatusUpdate $event)
    {
        //
    }
}
