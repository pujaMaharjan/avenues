<?php

namespace App\Listeners;

use App\Events\AppServicePaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
class EmailAppApproved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  AppServicePaid  $event
     * @return void
     */
    public function handle(AppServicePaid $event)
    {
        $app = $event->app;
        $user = $app->user;
        

        Mail::send('welcome', ['user' => $user,'app' => $app], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('Your app has been approved!');
        });
    }
}
