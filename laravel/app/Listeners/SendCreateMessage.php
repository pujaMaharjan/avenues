<?php

namespace App\Listeners;

use App\Events\WhileBroadcasterRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Broadcasters\Models\Broadcaster;
use Mail;

class SendCreateMessage
{
    public $user_id;
    public $broadcaster;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Broadcaster $broadcaster, Mail $mail)
    {
        $this->$broadcaster = $broadcaster;
        $this->mail = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  WhileBroadcasterRegister  $event
     * @return void
     */
    public function handle(WhileBroadcasterRegister $event)
    {
        $user = $event->user;
        

        Mail::send('emails.broadcaster_create', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });


    }
}
