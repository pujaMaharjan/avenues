<?php

namespace App\Listeners;

use App\Events\WhileUserLoggedOut;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Broadcasters\LoginHistory\Models\LoginHistory;
use Broadcasters\User\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UpdateLogoutInformation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LoginHistory $loginHistory,Request $request)
    {
     $this->loginHistory = $loginHistory;
     $this->request = $request;
 }

    /**
     * Handle the event.
     *
     * @param  WhileUserLoggedInLoggedOut  $event
     * @return void
     */
    public function handle(WhileUserLoggedOut $event)
    {   
        // $value = $this->request->cookie('user');
        // $value = \Cookie::get('user');
        // dd($value);

        $history = unserialize($_COOKIE["login_histroy"]);
        $this->loginHistory
        ->where(['user_id'=>$event->user->user_id,'login_history_id'=>$history->login_history_id])
        ->update([
            'logout_time'=>Carbon::now()
            ]);
    }
}
