<?php

namespace App\Listeners;

use App\Events\BroadcasterServicePaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

class EmailServicePurchaseConfirmation
{
    protected $message;
    protected $mail;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  BroadcasterServicePaid  $event
     * @return void
     */
    public function handle(BroadcasterServicePaid $event)
    {
        $app = $event->app;
        $user = $app->user;
        
        $data = ['user'=>'message'];
        $this->mail->send('welcome', $data, function ($message) use ($user) {
            $message->from('us@example.com', 'Laravel');
            $message->to($user->email, $user->name)->subject('This user had paid');

            
        });
    }
}
