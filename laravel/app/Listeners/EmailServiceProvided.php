<?php

namespace App\Listeners;

use App\Events\BroadcasterServicePaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

class EmailServiceProvided
{
    protected $mailer;
    protected $message = 'message';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
         //$this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  BroadcasterServicePaid  $event
     * @return void
     */
    public function handle(BroadcasterServicePaid $event)
    {
        //
    }
}
