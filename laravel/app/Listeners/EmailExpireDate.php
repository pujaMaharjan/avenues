<?php

namespace App\Listeners;

use App\Events\ExpireDate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class EmailExpireDate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  ExpireDate  $event
     * @return void
     */
    public function handle(ExpireDate $event)
    {
         $app = $event->app;
        $user = $app->user;
        

        Mail::send('emails.expire_date', ['user' => $user,'app' => $app], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('your app is going to expire');
        });
    }
}
