<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentRegisterStep extends Model
{
    protected $table = 'payment_register_step';
    protected $fillable = ['name','description','status'];

    
}
