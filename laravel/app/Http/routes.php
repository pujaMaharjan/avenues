<?php

use Ps\PdfBundle\Annotation\Pdf;
Route::get('md5',function(){
	// return bcrypt('erik@123');
     $secretKey = \Config::get('site.appHashSecretKey');
        $utc = getUtc();
        $hash = md5($secretKey.$utc);
        echo $utc."<br>";
return $hash;
});

Route::get('video',function (){
	return view('admin.video');
});

Route::post('check-user',['uses'=>'\Broadcasters\User\Controllers\UserController@checkUserAvailable','as'=>'checkUserAvailable']);
Route::post('check-app-payment',['uses'=>'AppController@checkAppPayment','as'=>'checkAppPayment']);



$service = new \Broadcasters\Admin\Providers\RouteServiceProvider(new \Broadcasters\Admin\Models\Route);


$routes = $service->getAllSingleRoutes();
foreach ($routes as $route) {
	$params=[];
	$params['uses'] = $route->route_uses;
	if($route->route_as){
		$params['as'] = $route->route_as;
	}
	if ($route->middleware) {
		$params['middleware'] = $route->middleware;
	}
	switch ($route->route_verb) {

		case 'get':
		Route::get($route->route_name,$params);
		//dd($params);
		break;
		case 'post':
		Route::post($route->route_name,$params);
		break;		
		default:
			# code...
		break;
	}
}

$groupService = new \Broadcasters\Admin\Providers\RouteGroupServiceProvider(new \Broadcasters\Admin\Models\RouteGroup);

$groups = $groupService->getGroupWithRoute();
//dd($groups);

foreach ($groups as $group) {

	//dd($group->routes->route_as);
	$params = [];
	$params['prefix'] = $group->prefix;

	if($group->middleware) {
		$params['middleware'] = $group->middleware;
	}
	$routes = $group->routes;
	$children = $group->children;
	//dd($children->prefix);
	//dd($routes);
	Route::group($params, function() use ($routes,$children)
	{
		if($children) {
			foreach ($children as $child) {
				$childParams = [];
				$childParams['prefix'] = $child->prefix;
				$childRoutes = $child->routes;
				// dd($childRoutes);
				Route::group($childParams,function() use($childRoutes){
					foreach ($childRoutes as $route) {

						$routeParams=[];
						$routeParams['uses'] = $route->route_uses;
						if($route->route_as){
							$routeParams['as'] = $route->route_as;
						}
						if ($route->middleware) {
							$routeParams['middleware'] = $route->middleware;
						}
						switch ($route->route_verb) {

							case 'get':
							Route::get($route->route_name,$routeParams);
		//dd($params);
							break;
							case 'post':
							Route::post($route->route_name,$routeParams);
							break;		
							default:
			# code...
							break;
						}
					}
				});
			}
		}
		foreach ($routes as $route) {

			$routeParams=[];
			$routeParams['uses'] = $route->route_uses;
			if($route->route_as){
				$routeParams['as'] = $route->route_as;
			}
			if ($route->middleware) {
				$routeParams['middleware'] = $route->middleware;
			}
			switch ($route->route_verb) {

				case 'get':
				Route::get($route->route_name,$routeParams);

				break;
				case 'post':
				Route::post($route->route_name,$routeParams);
				break;		
				default:
			# code...
				break;
			}
		}

	});


}
Route::get('broadcaster/{broadcaster_id}/unit/admob',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@indexBroadcaster','as'=>'broadcasterAdmob']);



Route::get('admin/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarning','as'=>'unitEarning']);
Route::post('admin/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarning','as'=>'unitEarning']);

Route::get('admin/unit/{unit_id}/earning/details',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningDetails','as'=>'unitEaringDetails']);
//Route::post('admin/unit/{unit_id}/earning/details',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningDetails','as'=>'postunitEaringDetails']);
//Route::get('admin/unit/{unit_id}/earning/filter',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningFilter','as'=>'unitEarningFilter']);

Route::get('admin/unit/{unit_id}/update/version',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@version','as'=>'versionUpdate']);
Route::post('admin/unit/{unit_id}/update/version',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@updateVersion','as'=>'versionUpdate']);
Route::get('admin/broadcaster/{broadcaster_id}/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningBroadcaster','as'=>'unitEarningBroadcaster']);
Route::post('admin/broadcaster/{broadcaster_id}/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningBroadcaster','as'=>'unitEarningBroadcaster']);




Route::resource('broadcaster/channels','WorldTvController');

get('broadcaster/app/{id}/template','\Broadcasters\Templates\Controllers\TemplateController@index');
get('broadcaster/app/{id}/template/{template_id}/edit','\Broadcasters\Templates\Controllers\TemplateController@edit');

post('broadcaster/app/{id}/template/{template_id}/update','\Broadcasters\Templates\Controllers\TemplateController@update');
post('broadcaster/app/{id}/template/save','\Broadcasters\Templates\Controllers\TemplateController@save');
post('broadcaster/app/{id}/template/preview','\Broadcasters\Templates\Controllers\TemplateController@preview');
get('broadcaster/app/{id}/template/save','\Broadcasters\Templates\Controllers\TemplateController@save');
post('layout/{id}','\Broadcasters\Templates\Controllers\LayoutController@getLayout');


//Route::resource('services','serviceController');
Route::get('admin/services/list',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@index',
	'as'=>'serviceIndex']);
Route::get('admin/services/create',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@create',
	'as'=>'serviceCreate']);
Route::get('admin/services/{service_id}/edit',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@edit',
	'as'=>'serviceEdit']);
Route::post('admin/services/store',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@store',
	'as'=>'serviceStore']);
//Route::get('services','\Broadcasters\Service\Controllers\ServiceController@index');
//Route::get('services/create','serviceController@create');
//Route::get('services/{service_id}/edit','serviceController@edit');
Route::post('admin/services/{service_id}/update',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@update',
	'as'=>'serviceUpdate']);
Route::get('admin/services/{service_id}/delete',
	['uses'=>'\Broadcasters\Service\Controllers\ServiceController@delete',
	'as'=>'serviceDelete']);

get('broadcaster/app/{app_id}/channels/restore/{channel_id}',['uses'=>'\Broadcasters\Channel\Controllers\ChannelController@restore']);
post('broadcaster/broadcaster/app/{app_id}/channel/{app_channel_id}/epg/edit-row',['uses'=>'\Broadcasters\Epg\Controllers\EpgController@editRow']);

get('apps/service-detail/{id}',['uses'=>'AppController@getServiceDetail']);
Route::any('apps/create',['uses'=>'AppController@create']);
Route::post('apps/postCreate',['uses'=>'AppController@postCreate']);
Route::any('apps/edit/{app_id}',['uses'=>'AppController@edit','as'=>'app.update']);
Route::any('apps/cart/{app_id}',['uses'=>'AppController@cart']);
Route::any('apps/analysis-step/{app_id}',['uses'=>'AppController@analysisStep']);
Route::any('apps/postCart/{app_id}',['uses'=>'AppController@postCart']);
Route::any('apps/postRenew/{app_id}',['uses'=>'AppController@postRenew']);

Route::any('apps/payment/{app_id}',['uses'=>'AppController@payment']);
Route::any('apps/renew-payment/{app_id}',['uses'=>'AppController@renewPayment']);
Route::any('apps/update-cart/{app_id}/{id}',['uses'=>'AppController@updateCart']);
Route::any('apps/update-exist-cart/{app_id}/{id}',['uses'=>'AppController@updateExistCart']);
Route::any('payment/bank/{app_id}',['uses'=>'AppController@paymentBank']);
Route::any('payment/stripe/{app_id}',['uses'=>'AppController@paymentStripe']);
Route::any('payment/paypal',['uses'=>'AppController@paymentPaypal']);
Route::any('apps/pending/{app_id}',['uses'=>'AppController@pending']);
Route::any('apps/renew-pending/{app_id}',['uses'=>'AppController@pending']);

Route::any('apps/{app_id}/payment_confirmed/{payment_detail_id}',['uses'=>'AppController@confirm']);
Route::get('apps/{app_id}/payment_pdf/{payment_detail_id}',['uses'=>'AppController@paymentDetailPdf', 'as'=>'paymentPdf']);
Route::get('apps/{app_id}/payment_pdf_post/{payment_detail_id}',['uses'=>'AppController@paymentDetailPdfPost', 'as'=>'paymentPdfPost']);



//broacaster
Route::get('broadcaster/cart',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@cart']);
Route::post('broadcaster/postcart',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@postCart']);

Route::get('broadcaster/{broadcaster_id}/payment-register',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@paymentRegister']);

Route::get('broadcaster/gateway',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@gateway']);
Route::post('broadcaster/gateway',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@postGateway']);
Route::any('broadcaster/paypal',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@postGateway']);
Route::get('broadcaster/pending-approved',['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@pendingForApproved']);



// get('/',function(){
// 	Event::fire(new \App\Events\BroadcasterServicePaid());
// });

//route for app config

Route::group(['prefix'=>'broadcaster'],function(){
	Route::resource('app/{app_id}/services/audios','AudioController');
	Route::resource('app/{app_id}/menus','\Broadcasters\Menu\MenuController');
	Route::resource('app/{app_id}/services/news','NewsController');
	Route::resource('app/{app_id}/services/news/{news_id}/newsapi','\Broadcasters\NewsApi\Controllers\NewsApiController');

	Route::get('{app_id}/paymentdetails',
		['uses'=>'\Broadcasters\PaymentDetails\Controllers\PaymentDetailsController@index',
		'as'=>'paymentIndex']);
	Route::get('{app_id}/status',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@updateStatus',
		'as'=>'appStatus']);
	Route::get('{app_id}/renew',
		['uses'=>'AppController@renewApp',
		'as'=>'appRenew']);
	Route::get('{app_id}/status/notes',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@appNotes',
		'as'=>'statusNotes']);
	Route::post('{app_id}/status/notes',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@appNotePost',
		'as'=>'appNotePost']);
	Route::get('{app_id}/notes/view',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@viewNotes',
		'as'=>'viewNotes']);
	Route::get('{app_id}/notes/pdf',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@pdfNotes',
		'as'=>'pdfNotes']);
	Route::get('{app_id}/notes/pdf-post',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@pdfPost',
		'as'=>'pdfPost']);


	

	//app group 
	Route::group(['prefix'=>'app'],function(){
		Route::get('{app_id}/appconfig/key',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@create',
			'as'=>'appConfigCreate']);
		Route::post('{app_id}/appconfig/key/store',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@store',
			'as'=>'appConfigStore']);
		Route::get('{app_id}/appconfig/notificationchannels',
			['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@index',
			'as'=>'parseChannels']);
		Route::post('{app_id}/appconfig/notificationchannels/subcribe',
			['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@subcribe',
			'as'=>'parseChannelsSubcribe']);
		Route::get('{app_id}/push/general',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@generalPushView',
			'as'=>'generalPushView']);
		Route::post('{app_id}/push/general',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@generalPush',
			'as'=>'generalPush']);
		Route::get('{app_id}/appconfig/epgsetting',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@getEpgView',
			'as'=>'epgNotification']);
		Route::post('{app_id}/appconfig/epgsetting',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@epgNotification',
			'as'=>'epgNotification']);
		Route::get('{app_id}/push/epgs',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@pushEpgNotification',
			'as'=>'pushEpgNotification']);

		Route::get('{app_id}/epgs/post-push',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@postUushNotification',
			'as'=>'post.pushEpgNotification']);
		Route::post('{app_id}/epgs/updateStatus',
			['uses'=>'\Broadcasters\AppConfig\Controllers\AppConfigController@updateNotificationStatus',
			'as'=>'updateNotificationStatus']);

	});


});
Route::group(['prefix'=>'admin'],function(){
	Route::resource('layouts','\Broadcasters\Templates\Controllers\LayoutController');
	Route::get('layouts/{id}/delete','\Broadcasters\Templates\Controllers\LayoutController@delete');
	Route::get('notificaiton/create',
		['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@create',
		'as'=>'parseChannelsCreate']);
	Route::post('notificaiton/save',
		['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@store',
		'as'=>'parseChannelsSave']);
	Route::post('notificaiton/list',
		['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@index',
		'as'=>'parseChannelsIndex']);
	Route::get('notificaiton/{id}/delete',
		['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@delete',
		'as'=>'parseChannelsDelete']);
	Route::get('notificaiton/{id}/status',
		['uses'=>'\Broadcasters\ParseChannels\Controllers\ParseChannelsController@status',
		'as'=>'parseChannelsStatus']);


	Route::get('notificaiton/push',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@adminPushView',
		'as'=>'adminPushView']);
	Route::post('notificaiton/push',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminBroadcasterController@adminPushPost',
		'as'=>'adminPushPost']);

	Route::get('approveBroadcaster/{broadcaster_id}',
		['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterController@approveBroadcaster',
		'as'=>'approveBroadcaster']);
	Route::get('view-all',
		['uses'=>'\Broadcasters\Admin\Controllers\AdminController@viewAllBroadcaster',
		'as'=>'viewAllBroadcaster']);
});


Route::get('test-event',function(){
	\Event::fire(new \App\Events\WhileBroadcasterRegister(\Broadcasters\User\Models\User::find(\Auth::Id())));
});

//test
Route::get('test/{app_id}',
	['uses'=>'\Broadcasters\App\Controllers\AppController@appService',
	'as'=>'test']
	);


// use Monolog\Logger;
// use Monolog\Handler\StreamHandler;
// Route::get('log',function(){
// 	$view_log = new Logger('View Logs');
// 	$view_log->pushHandler(new StreamHandler('paypal.log', Logger::INFO));
// 	$data = ['name'=>'nirajan'];
// 	$view_log->addInfo(json_encode($data));

// });

Route::get('test',function (){
	$mytime = Carbon\Carbon::now();
		// echo $mytime->todateTimeString();
		// $today = $mytime->toDateString();
	$app = \Broadcasters\App\Models\App::find(10);
dd($mytime->todateTimeString());
	
 \Event::fire(new \App\Events\ExpireDate(\Broadcasters\App\Models\App::find(10)));
	
	
});

Route::any('app/payment',function(){
	$request = new \Illuminate\Http\Request;
	$paymentDetail = new \App\PaymentDetail;
	$view_log = new Logger('View Logs');
	$view_log->pushHandler(new StreamHandler('paypal.log', Logger::INFO));

	$view_log->addInfo(json_encode($request->all()));
	$payment->create(['payment_gateway'=>'paypal','payment_description'=>json_encode($request->all())]);


});

Route::get('checkEvent',function(){
	$app = \App\App::find(62);
	return \Event::fire(new \App\Events\BroadcasterServicePaid($app));
});
Route::get('broadcaster/app/{app_id}/services/movies',['uses'=>'\Broadcasters\Movie\MovieController@index']);
Route::get('broadcaster/app/{app_id}/services/movies/edit/{id}',['uses'=>'\Broadcasters\Movie\MovieController@edit']);
Route::get('broadcaster/app/{app_id}/services/movies/create',['uses'=>'\Broadcasters\Movie\MovieController@create']);
Route::post('broadcaster/app/{app_id}/services/movies/update/{id}',['uses'=>'\Broadcasters\Movie\MovieController@update','as'=>'broadcaster.app.movie.update']);
Route::post('broadcaster/app/{app_id}/services/movies/store',['uses'=>'\Broadcasters\Movie\MovieController@store','as'=>'broadcaster.app.movie.store']);
Route::get('broadcaster/app/{app_id}/services/movies/delete/{id}',['uses'=>'\Broadcasters\Movie\MovieController@delete']);
Route::get('broadcaster/app/{app_id}/services/movies/show/{app_movie_id}',['uses'=>'\Broadcasters\Movie\MovieController@show']);



Route::get('broadcaster/app/{app_id}/services/radios',['uses'=>'\Broadcasters\Radios\RadioController@index']);
Route::get('broadcaster/app/{app_id}/services/radios/edit/{id}',['uses'=>'\Broadcasters\Radios\RadioController@edit']);
Route::get('broadcaster/app/{app_id}/services/radios/create',['uses'=>'\Broadcasters\Radios\RadioController@create']);
Route::post('broadcaster/app/{app_id}/services/radios/update/{id}',['uses'=>'\Broadcasters\Radios\RadioController@update','as'=>'broadcaster.app.radio.update']);
Route::post('broadcaster/app/{app_id}/services/radios/store',['uses'=>'\Broadcasters\Radios\RadioController@store','as'=>'broadcaster.app.radio.store']);
Route::get('broadcaster/app/{app_id}/services/radios/delete/{id}',['uses'=>'\Broadcasters\Radios\RadioController@delete']);
Route::get('admin/broadcaster/{broadcaster_id}/apps/create','AppController@create');

Route::get('broadcaster/app/{app_id}/services/radios/show/{app_radio_id}',['uses'=>'\Broadcasters\Radios\RadioController@show']);
Route::get('admin/broadcaster/{broadcaster_id}/apps/create','AppController@create');



//switch user 
Route::get('admin/user/switch/stop','\Broadcasters\Admin\Controllers\AdminBroadcasterController@user_switch_stop');

// vod show details 

Route::get('broadcaster/app/{app_id}/services/vod/show/{app_vod_id}',['uses'=>'\Broadcasters\Vod\Controllers\VodController@show']);


Route::get('template/abc',function(){
	return view('templates.abc');

});

Route::get('template/himalaya',function(){
	return view('templates.himalaya');

});
Route::get('template/filmy',function(){
	return view('templates.filmy');

});



//admin route

Route::get('admin/ad-manage','\Broadcasters\Admanage\AdManageController@index');
Route::get('admin/ad-manage/create','\Broadcasters\Admanage\AdManageController@create');

Route::post('admin/ad-manage/update-setting','\Broadcasters\Admanage\AdManageController@updateSetting');


//
$message = array("message" => " Hello !!!");

Route::get('token/generate',function(\Illuminate\Http\Request $request){
	Log::info('Showing user profile for user: '.$request->get('Token'));
	return ['status'=>1,'message'=>'success'];
});



//if it wants to handle this url

Route::get('handle/new/api',function(\Illuminate\Http\Request $request){



	$url = url('/register/notification');
	$headers = array(
			'key = AIzaSyCR9V2CBvH7TXfZ5KSmSnlxxVkeLc7xyC4',
			'Content-Type: application/json',
			'getTrustedHeaderName: localhost'
			);
	$fields = ['to_website'=>'nrn','news_url'=>'http://newsnrn.com/api/public/v1/news/54089','title'=>'काठमाण्डौंमा जापान फेस्टिबल हुने (भिड'];

	  $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);  
        if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
});

Route::post('/register/notification',function(\Illuminate\Http\Request $request){

	//dd($request->header('Authorization'));
	$data = $request->header('getTrustedHeaderName');
	// dd($data);
	$data = ['to_website'=>'nrn','news_url'=>'http://newsnrn.com/api/public/v1/news/54089','title'=>'काठमाण्डौंमा जापान फेस्टिबल हुने (भिड'];

	// dd($_SERVER);
	$registration_ids = ['dm2koaTZk_c:APA91bE6usj421BZEKUSpCzqgn27ldTT2Efgrx3MHQcYyUlQnMDKuIOPVPfkWurJwv0putUJPkkWY3AOHS1OjtNWBvSHbqOXsjqo3qicscjYCBNLNhu7eYaXo_bNgOrsrj_B5hiHgAMT'];
	$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $registration_ids,
			 'data' => $data
			);
		$headers = array(
			'Authorization:key = AIzaSyCR9V2CBvH7TXfZ5KSmSnlxxVkeLc7xyC4',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
});

