<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddUnitSetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request->get('imgBanner'));

        $rules = [
        //'unit_name' => 'required|unique:add_unit_set|max:255',
        //'imgBanner' => 'required|unique:add_units,unit_name',
        //'imgInterstitial' => 'required|max:255',
        'name' => 'required|unique:add_unit_set',
        'app_id'=>'required|unique:add_unit_set',
        //'imgBanner' => 'unique:add_unit,unit_name',
        ];

        foreach($this->request->get('imgBanner') as $key => $val)
        {
            //dd('imgBanner'.$val);
            $rules['imgBanner.0'] = 'different:imgBanner.1';
            $rules['imgBanner.'.$key] = 'required|different:imgInterstitial.'.$key.'|unique:add_unit,unit_name';
        }

        foreach($this->request->get('imgInterstitial') as $key => $val)
        {
            $rules['imgInterstitial.0'] = 'different:imgInterstitial.1';
            $rules['imgInterstitial.'.$key] = 'required|different:imgBanner.'.$key.'|unique:add_unit,unit_name';
        }

        return $rules;

    }
    public function messages()
    {
       $messages = [
       'name.required' => 'A name for admov is required',
       'app_id.required'  => 'A app is required', 
       'app_id.unique' => 'A app already taken',
       //'unit_name.unique' => 'Banner and Interstitial should be unique',
       ];
       foreach($this->request->get('imgBanner') as $key => $val)
       {
        $messages['imgBanner.'.$key.'.required'] = 'Banner image links are required';
        $messages['imgBanner.'.$key.'.unique'] = 'Banner image links should be unique';
        $messages['imgBanner.'.$key.'.different'] = 'Interstitial and Banner name should be different';

    }
    foreach($this->request->get('imgInterstitial') as $key => $val)
    {
        $messages['imgInterstitial.'.$key.'.required'] = 'Interstitial image links are required';
        $messages['imgInterstitial.'.$key.'.unique'] = 'Interstitial image links should be unique';
        $messages['imgInterstitial.'.$key.'.different'] = 'Interstitial and Banner name should be different';

    }

    return $messages;

}
}
