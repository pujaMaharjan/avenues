<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BroadcasterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'username' => 'required|unique:users',
        'email'=>'required|email|unique:users',
        'password'=>'required|confirmed|min:6',
        'company_name'=>'required|unique:broadcasters',
        'display_name'=>'required|min:6',
        'country_code'=>'required',
        'phone'=>'required|min:7',

        ];
    }

    public function messages()
    {
        return [
        'email.required' => 'A email is required',
        'password.required'  => 'A password is required',
        'company_name.required'  => 'A company name is required',
        'display_name.required'  => 'A display name is required',
        'country_id.required'  => 'A country is required',
        'phone.required'  => 'A phone is required',


        ];
    }
}
