<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NotificationChannelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'parseChannels' => 'required|unique:notification_channels,channels',
        

        ];
    }

    public function messages()
    {
        return [
        'parseChannels.required' => 'A parse Channels is required',
        'parseChannels.unique'  => 'A parse Channels is unique',


        ];
    }
}
