<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateAddUnitSetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       // dd($this->route('broadcaster_id'));
        $rules = [
        'img' => 'required|max:255',
        'name' => 'required|unique:add_unit_set,name,'.$this->route('id').',add_unit_set_id',
        //'name' => 'required|unique:add_unit_set,name,'.$this->segment(3).',add_unit_set_id',

        ];

        foreach($this->request->get('img') as $key => $val)
        {
            $rules['img.'.$key] = 'required|max:255';
        }

        return $rules;

    }
    public function messages()
    {
       $messages = [
     //'name.required' => 'A name for admov is required',
       ];
       foreach($this->request->get('img') as $key => $val)
       {
        $messages['img.'.$key.'.required'] = 'Ad Id is required';
    }
    return $messages;

}
}
