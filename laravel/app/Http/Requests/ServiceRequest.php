<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(\Illuminate\Http\Request $request)
    {
        //dd($request->segment(3));

        return [
        'service_name' => 'required|unique:services,service_name,' .$request->segment(3).',service_id'
        

        ];
    }

    public function messages()
    {
        return [
        'service_name.required' => 'A service name is required',
        'service_name.unique'  => 'A service name is unique',


        ];
    }
}
