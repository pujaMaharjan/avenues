<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AppRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        //dd($this->request->all());
        return [

         //'app_name' => 'required|unique:notification_channels,channels',
        'app_name'=>'required|unique:app,app_name',
        'services' =>'required'

        ];
    }

    public function messages()
    {

        return [
        'app_name.required' => 'App name is required',
        'app_name.unique'  => 'App name is already taken',
        'services.required' => 'Atleast one service is required'



        ];
    }
}
