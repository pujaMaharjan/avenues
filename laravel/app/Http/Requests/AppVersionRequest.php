<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AppVersionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
     foreach($this->request->get('appVersion') as $key => $val)
     {
        $rules['appVersion.'.$key] = 'required';
    }
    return $rules;
}
public function messages($value='')
{
   foreach($this->request->get('appVersion') as $key => $val)
   {

    $messages['appVersion.'.$key.'.required'] = 'version are required';
}
return $messages;
}
}
