<?php

use Ps\PdfBundle\Annotation\Pdf;


Route::get('abc/search', function (\Illuminate\Http\Request $request) {
    return $request->all();
});


Route::get('md5', function () {
    // return bcrypt('erik@123');
    $secretKey = \Config::get('site.appHashSecretKey');
    $utc = getUtc();
    $hash = md5($secretKey . $utc);
    echo $utc . "<br>";
    return $hash;
});

Route::get('video', function () {
    return view('admin.video');
});


Route::get('video', function () {
    return view('hello');
});

Route::post('check-user', ['uses' => '\Broadcasters\User\Controllers\UserController@checkUserAvailable', 'as' => 'checkUserAvailable']);
Route::post('check-app-payment', ['uses' => 'AppController@checkAppPayment', 'as' => 'checkAppPayment']);

Route::get('/', 'SiteController@home');
Route::get('/login', 'Auth\AuthController@getLogin');
Route::get('/terms', 'Auth\AuthController@terms');

Route::group(['prefix' => 'admin'], function () {
    Route::get('unit/{unit_id}/update/version', ['uses' => 'Admin\AddUnitSetController@version', 'as' => 'versionUpdate']);
    Route::post('unit/{unit_id}/update/version', ['uses' => 'Admin\AddUnitSetController@updateVersion', 'as' => 'versionUpdate']);
    Route::get('broadcaster/{broadcaster_id}/unit/{unit_id}/earning', ['uses' => 'Admin\AddUnitSetController@showEarningBroadcaster', 'as' => 'unitEarningBroadcaster']);
    Route::post('admin/broadcaster/{broadcaster_id}/unit/{unit_id}/earning', ['uses' => 'Admi\AddUnitSetController@showEarningBroadcaster', 'as' => 'unitEarningBroadcaster']);
    Route::get('/', ['uses' => 'Admin\AdminController@index', 'as' => 'broadcasterIndex']);
    Route::get('unit/create', ['uses' => 'Admin\AddUnitSetController@create', 'as' => 'unitCreate']);
    Route::post('unit/store', ['uses' => 'Admin\AddUnitSetController@store', 'as' => 'unitStore']);
    Route::get('unit/list', ['uses' => 'Admin\AddUnitSetController@index', 'as' => 'unitIndex']);
    Route::resource('nitv/list', 'NitvAdController');
    Route::get('services/list',
        ['uses' => '\Broadcasters\Service\Controllers\ServiceController@index',
            'as' => 'serviceIndex']);
    Route::get('services/create',
        ['uses' => '\Broadcasters\Service\Controllers\ServiceController@create',
            'as' => 'serviceCreate']);
    Route::get('services/{service_id}/edit',
        ['uses' => '\Broadcasters\Service\Controllers\ServiceController@edit',
            'as' => 'serviceEdit']);
    Route::post('services/store',
        ['uses' => '\Broadcasters\Service\Controllers\ServiceController@store',
            'as' => 'serviceStore']);
    Route::resource('layouts', '\Broadcasters\Templates\Controllers\LayoutController');
    Route::get('layouts/{id}/delete', '\Broadcasters\Templates\Controllers\LayoutController@delete');
    Route::get('notificaiton/create',
        ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@create',
            'as' => 'parseChannelsCreate']);
    Route::post('notificaiton/save',
        ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@store',
            'as' => 'parseChannelsSave']);
    Route::post('notificaiton/list',
        ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@index',
            'as' => 'parseChannelsIndex']);
    Route::get('notificaiton/{id}/delete',
        ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@delete',
            'as' => 'parseChannelsDelete']);
    Route::get('notificaiton/{id}/status',
        ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@status',
            'as' => 'parseChannelsStatus']);


    Route::get('notificaiton/push',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@adminPushView',
            'as' => 'adminPushView']);
    Route::post('notificaiton/push',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@adminPushPost',
            'as' => 'adminPushPost']);

    Route::get('approveBroadcaster/{broadcaster_id}',
        ['uses' => 'Broadcasters\BroadcasterController@approveBroadcaster',
            'as' => 'approveBroadcaster']);
    Route::get('view-all',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminController@viewAllBroadcaster',
            'as' => 'viewAllBroadcaster']);
    Route::get('unit/{id}/edit', ['uses' => 'Admin\AddUnitSetController@edit', 'as' => 'unitEdit']);
    Route::post('unit/{id}/edit', ['uses' => 'Admin\AddUnitSetController@update', 'as' => 'unitUpdate']);
    Route::get('unit/{id}/delete', ['uses' => 'Admin\AddUnitSetController@delete', 'as' => 'unitDelete']);
    Route::get('unit/{id}/show', ['uses' => 'Admin\AddUnitSetController@show', 'as' => 'unitShow']);
    Route::get('broadcaster/{broadcaster_id}/unit/{unit_id}/edit', ['uses' => 'Admin\AddUnitSetController@editBroadcaster', 'as' => 'unitBroadcasterEdit']);
    Route::get('broadcaster/{broadcaster_id}/unit/{unit_id}/edit', ['uses' => 'Admin\AddUnitSetController@editBroadcaster', 'as' => 'unitBroadcasterEdit']);
    Route::get('unit/{unit_id}/earning', ['uses' => 'Admin\AddUnitSetController@showEarning', 'as' => 'unitEarning']);
    Route::post('unit/{unit_id}/earning', ['uses' => 'Admin\AddUnitSetController@showEarning', 'as' => 'unitEarning']);

    Route::get('unit/{unit_id}/earning/details', ['uses' => 'Admin\AddUnitSetController@showEarningDetails', 'as' => 'unitEaringDetails']);
    Route::get('ad-manage', 'AdManageController@index');
    Route::get('ad-manage/create', 'AdManageController@create');

    Route::post('ad-manage/update-setting', 'AdManageController@updateSetting');

    Route::get('broadcaster/{broadcaster_id}', ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@broadcaster', 'as' => 'broadcaster']);
    Route::get('route/order', ['uses' => '\Broadcasters\Admin\Controllers\RouteController@order', 'as' => 'routeOrder']);

    Route::post('broadcaster/unit/{id}/edit', ['uses' => '\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@updateBroadcaster', 'as' => 'unitBroadcasterUpdate']);
    Route::get('user/switch/stop', '\Broadcasters\Admin\Controllers\AdminBroadcasterController@user_switch_stop');
    Route::get('notificaiton/firebase', '\Broadcasters\Admin\Controllers\AdminBroadcasterController@showNotification');
    Route::post('notificaiton/firebase', '\Broadcasters\Admin\Controllers\AdminBroadcasterController@showNotification');
    Route::get('broadcaster/{broadcaster_id}/apps/create', 'AppController@create');
    Route::get('broadcaster/{broadcaster_id}/apps/create', 'AppController@create');
});
Route::get('broadcaster/app/{app_id}', ['uses' => '\Broadcasters\Dashboard\Controllers\DashboardController@dashboard']);
// Route::get('admin',['uses'=>'\Broadcasters\Admin\Controllers\AdminController@index','as'=>'broadcasterIndex']);
// Route::get('admin/unit/create',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@create','as'=>'unitCreate']);
// Route::post('admin/unit/store',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@store','as'=>'unitStore']);
// Route::get('admin/unit/list',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@index','as'=>'unitIndex']);
// Route::get('admin/route/create',['uses'=>'\Broadcasters\Admin\Controllers\RouteController@create']);
// Route::get('admin/route/group/create',['uses'=>'\Broadcasters\Admin\Controllers\RouteGroupController@create','as'=>'routeGroupCreate']);
// Route::get('admin/route/list',['uses'=>'\Broadcasters\Admin\Controllers\RouteController@listRoute']);
// Route::post('admin/route/update-order',['uses'=>'\Broadcasters\Admin\Controllers\RouteController@updateOrder','as'=>'updateOrder']);
// Route::post('admin/route/group',['uses'=>'\Broadcasters\Admin\Controllers\RouteGroupController@listRouteGroup']);

// Route::get('admin/route/group/{id}/edit',['uses'=>'\Broadcasters\Admin\Controllers\RouteGroupController@edit','as'=>'routeGroupEdit']);
// Route::get('admin/route/group/{id}/edit',['uses'=>'\Broadcasters\Admin\Controllers\RouteGroupController@update','as'=>'routeGroupUpdate']);
// Route::get('admin/route/group/{id}/delete',['uses'=>'\Broadcasters\Admin\Controllers\RouteGroupController@delete','as'=>'routeGroupDelete']);
// Route::get('admin/route/{id}/edit',['uses'=>'\Broadcasters\Admin\Controllers\RouteController@edit','as'=>'routeEdit']);
// Route::post('admin/route/{id}/edit',['uses'=>'\Broadcasters\Admin\Controllers\RouteController@update','as'=>'routeUpdate']);
Route::get('user/register', ['uses' => 'Auth\AuthController@getRegister']);
Route::post('user/register', ['uses' => 'Auth\AuthController@postRegister']);
Route::post('user/login', ['uses' => 'Auth\AuthController@postLogin']);
Route::get('user/login', ['uses' => 'Auth\AuthController@getLogin']);
Route::get('user/logout', ['uses' => 'Auth\AuthController@getLogout', 'as' => 'logout']);
Route::get('profile/changepassword', ['uses' => 'Auth\AuthController@getLogin']);
Route::get('profile/changepassword', ['uses' => '\Broadcasters\Broadcaster\Controllers\BroadcasterController@changePassword']);
Route::get('broadcaster/create', ['uses' => '\Broadcasters\Broadcaster\Controllers\BroadcasterController@register', 'as' => 'broadcaster.create']);
Route::post('broadcaster/store', ['uses' => '\Broadcasters\Broadcaster\Controllers\BroadcasterController@store', 'as' => 'broadcaster.store']);
Route::get('apps/create', ['uses' => 'AppController@create']);
Route::get('apps/store', ['uses' => 'AppController@store']);
Route::resource('services', 'ServiceController');

Route::get('broadcaster/{broadcaster_id}/unit/admob', ['uses' => '\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@indexBroadcaster', 'as' => 'broadcasterAdmob']);

Route::post('broadcaster/app/{app_id}/services/newsapi/update/{app_external_api_id}', ['uses' => '\Broadcasters\NewsApi\Controllers\NewsApiController@update', 'as' => 'newsApiUpdate']);
Route::get('broadcaster/app/{app_id}/services/newsapi/delete/{app_external_api_id}', ['uses' => '\Broadcasters\NewsApi\Controllers\NewsApiController@delete', 'as' => 'newsApiDelete']);
Route::get('admin/broadcaster/{broadcaster_id}/unit', ['uses' => '\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@indexBroadcaster', 'as' => 'unitBroadcasterIndex']);
Route::get('password/email', ['uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['uses' => 'Auth\PasswordController@postEmail']);
Route::get('password/reset/{token}', ['uses' => 'Auth\PasswordController@postReset']);
Route::post('password/reset', ['uses' => 'Auth\PasswordController@postReset']);


//Route::post('admin/unit/{unit_id}/earning/details',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningDetails','as'=>'postunitEaringDetails']);
//Route::get('admin/unit/{unit_id}/earning/filter',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningFilter','as'=>'unitEarningFilter']);

// Route::get('admin/unit/{unit_id}/update/version',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@version','as'=>'versionUpdate']);
// Route::post('admin/unit/{unit_id}/update/version',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@updateVersion','as'=>'versionUpdate']);
// Route::get('admin/broadcaster/{broadcaster_id}/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningBroadcaster','as'=>'unitEarningBroadcaster']);
// Route::post('admin/broadcaster/{broadcaster_id}/unit/{unit_id}/earning',['uses'=>'\Broadcasters\AddUnitSet\Controllers\AddUnitSetController@showEarningBroadcaster','as'=>'unitEarningBroadcaster']);

Route::post('broadcaster/app/{app_id}/services/channels/update/{channel_id}', ['uses' => '\Broadcasters\Channel\Controllers\ChannelController@update', 'as' => 'app.channels.update']);
/*Route::get('broadcaster/update-column',['uses'=>'\Broadcaster\BroadCasterController@updateColumn','as'=>'unitBroadcasterUpdate']);*/


Route::resource('broadcaster/channels', 'WorldTvController');

get('broadcaster/app/{id}/template', '\Broadcasters\Templates\Controllers\TemplateController@index');
get('broadcaster/app/{id}/template/{template_id}/edit', '\Broadcasters\Templates\Controllers\TemplateController@edit');

post('broadcaster/app/{id}/template/{template_id}/update', '\Broadcasters\Templates\Controllers\TemplateController@update');
post('broadcaster/app/{id}/template/save', '\Broadcasters\Templates\Controllers\TemplateController@save');
post('broadcaster/app/{id}/template/preview', '\Broadcasters\Templates\Controllers\TemplateController@preview');
get('broadcaster/app/{id}/template/save', '\Broadcasters\Templates\Controllers\TemplateController@save');
post('layout/{id}', '\Broadcasters\Templates\Controllers\LayoutController@getLayout');


//Route::resource('services','serviceController');

//Route::get('services','\Broadcasters\Service\Controllers\ServiceController@index');
//Route::get('services/create','serviceController@create');
//Route::get('services/{service_id}/edit','serviceController@edit');
Route::post('admin/services/{service_id}/update',
    ['uses' => '\Broadcasters\Service\Controllers\ServiceController@update',
        'as' => 'serviceUpdate']);
Route::get('admin/services/{service_id}/delete',
    ['uses' => '\Broadcasters\Service\Controllers\ServiceController@delete',
        'as' => 'serviceDelete']);


get('apps/service-detail/{id}', ['uses' => 'AppController@getServiceDetail']);
Route::any('apps/create', ['uses' => 'AppController@create']);
Route::post('apps/postCreate', ['uses' => 'AppController@postCreate']);
Route::any('apps/edit/{app_id}', ['uses' => 'AppController@edit', 'as' => 'app.update']);
Route::any('apps/cart/{app_id}', ['uses' => 'AppController@cart']);
Route::any('apps/analysis-step/{app_id}', ['uses' => 'AppController@analysisStep']);
Route::any('apps/postCart/{app_id}', ['uses' => 'AppController@postCart']);
Route::any('apps/postRenew/{app_id}', ['uses' => 'AppController@postRenew']);

Route::any('apps/payment/{app_id}', ['uses' => 'AppController@payment']);
Route::any('apps/renew-payment/{app_id}', ['uses' => 'AppController@renewPayment']);
Route::any('apps/update-cart/{app_id}/{id}', ['uses' => 'AppController@updateCart']);
Route::any('apps/update-exist-cart/{app_id}/{id}', ['uses' => 'AppController@updateExistCart']);
Route::any('payment/bank/{app_id}', ['uses' => 'AppController@paymentBank']);
Route::any('payment/stripe/{app_id}', ['uses' => 'AppController@paymentStripe']);
Route::any('payment/paypal', ['uses' => 'AppController@paymentPaypal']);
Route::any('apps/pending/{app_id}', ['uses' => 'AppController@pending']);
Route::any('apps/renew-pending/{app_id}', ['uses' => 'AppController@pending']);

Route::any('apps/{app_id}/payment_confirmed/{payment_detail_id}', ['uses' => 'AppController@confirm']);
Route::get('apps/{app_id}/payment_pdf/{payment_detail_id}', ['uses' => 'AppController@paymentDetailPdf', 'as' => 'paymentPdf']);
Route::get('apps/{app_id}/payment_pdf_post/{payment_detail_id}', ['uses' => 'AppController@paymentDetailPdfPost', 'as' => 'paymentPdfPost']);


//broacaster


// get('/',function(){
// 	Event::fire(new \App\Events\BroadcasterServicePaid());
// });

//route for app config


Route::get('test-event', function () {
    \Event::fire(new \App\Events\WhileBroadcasterRegister(\Broadcasters\User\Models\User::find(\Auth::Id())));
});

//test
Route::get('test/{app_id}',
    ['uses' => '\Broadcasters\App\Controllers\AppController@appService',
        'as' => 'test']
);


// use Monolog\Logger;
// use Monolog\Handler\StreamHandler;
// Route::get('log',function(){
// 	$view_log = new Logger('View Logs');
// 	$view_log->pushHandler(new StreamHandler('paypal.log', Logger::INFO));
// 	$data = ['name'=>'nirajan'];
// 	$view_log->addInfo(json_encode($data));

// });

Route::get('test', function () {
    $mytime = Carbon\Carbon::now();
    // echo $mytime->todateTimeString();
    // $today = $mytime->toDateString();
    $app = \Broadcasters\App\Models\App::find(10);
    dd($mytime->todateTimeString());

    \Event::fire(new \App\Events\ExpireDate(\Broadcasters\App\Models\App::find(10)));


});

Route::any('app/payment', function () {
    $request = new \Illuminate\Http\Request;
    $paymentDetail = new \App\PaymentDetail;
    $view_log = new Logger('View Logs');
    $view_log->pushHandler(new StreamHandler('paypal.log', Logger::INFO));

    $view_log->addInfo(json_encode($request->all()));
    $payment->create(['payment_gateway' => 'paypal', 'payment_description' => json_encode($request->all())]);


});

Route::get('checkEvent', function () {
    $app = \App\App::find(62);
    return \Event::fire(new \App\Events\BroadcasterServicePaid($app));
});


Route::get('broadcaster/app/{app_id}/services/radios/show/{app_radio_id}', ['uses' => '\Broadcasters\Radios\RadioController@show']);


//switch user


// vod show details

Route::get('broadcaster/app/{app_id}/services/vod/show/{app_vod_id}', ['uses' => '\Broadcasters\Vod\Controllers\VodController@show']);


Route::get('template/abc', function () {
    return view('templates.abc');

});

Route::get('template/himalaya', function () {
    return view('templates.himalaya');

});
Route::get('template/nitvmedia', function () {
    return view('templates.nitvmedia');

});


//admin route


//
$message = array("message" => " Hello !!!");

Route::get('token/generate', 'FirebaseController@generate');


Route::get('send/message', 'FirebaseController@handleException');
Route::post('handle/new/api', 'FirebaseController@pushNotification');
Route::post('handle/test/api', 'FirebaseController@testpushNotification');
Route::get('handle/renew/api', 'FirebaseController@renotifyFirebase');
// Route::get('/','FirebaseController@handleException');

Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);

Route::post(
    'stripe/success-payment',
    'WebhookController@handleChargePaymentSucceeded'
);


Route::get('abc/abc', function () {
    $dtz = 'Asia/Kathmandu';

    $time_in_sofia = new DateTime('now', timezone_open($dtz));
    $offset = date_offset_get($time_in_sofia);
    $hr = explode('.', $offset / 3600);
    $hre = $hr[0];
    if (strpos($hre, '-') !== false) {
        $hre = str_replace('-', '-0', $hre);
    } else {
        if (strlen($hre) < 2)
            $hre = '+0' . $hre;
        else
            $hre = '+' . $hre;
    }
    $min = '00';
    if (isset($hr[1])) {
        $min = $hr[1];

        $min = $min * 60 / 100;
    }
    $offsettime = $hre . ":" . $min;
    $offsettime = $offsettime;
    $dir = substr($offsettime, 0, 1) == "+" ? 1 : -1;
    $sQuery = "select `program_name`,`start_time`,date_format(CONVERT_TZ(Concat(CURDATE(),' ',start_time),'+00:00',$offsettime),'%H:%i') as start_time_1,`end_time`,date_format(CONVERT_TZ(Concat(CURDATE(),' ',end_time),'+00:00',$offsettime),'%H:%i') as end_time_1,`day_int` from tbl_program where channel_id = '$id' and day_int between 1 and 7 order by day_int asc,start_time asc";

    $oResource = $conObj->execute($sQuery);
    $merge1 = $merge2 = [];
    while ($asData = $conObj->fetch_assoc($oResource)) {
        $notput = true;
        $m1 = (int)date('H', strtotime($asData['start_time']));
        $m2 = (int)date('H', strtotime($asData['start_time_1']));
        $changed = (($m2 - $m1) * $dir) < 0;

        if ($changed) {
            $asData['day_int'] = ($asData['day_int'] + $dir) % 7;
            if ($asData['day_int'] == 0) {
                $asData['day_int'] = 7;
            }
            if (($dir > 0 && $asData['day_int'] == 1) || ($dir < 0 && $asData['day_int'] == 7)) {
                $merge1[] = $asData;
                $notput = false;
            }
        }
        if ($notput)
            $merge2[] = $asData;
    }
    if ($dir == -1)
        $jsonData = array_merge($merge2, $merge1);
    if ($dir == 1)
        $jsonData = array_merge($merge1, $merge2);
    $output = [];
    $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    foreach ($jsonData as $data) {
        $output[$days[$data['day_int'] - 1]][] = [
            'program_name' => $data['program_name'],
            'start_time' => $data['start_time_1'],
            'end_time' => $data['end_time_1'],
            'day_int' => $data['day_int']
        ];
    }
    echo json_encode($output);
});
Route::get('abc/abc/{id}', 'Broadcasters\EpgController@getChannelNotification');

