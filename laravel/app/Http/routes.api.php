<?php
Route::group(['prefix'=>'api'],function(){
		Route::get('hello',function(){
			return "hello";
		});
		Route::get('broadcasters/{id}/channels',['uses'=>'ChannelController@get','as'=>'getChannel']);
		Route::get('broadcasters/{id}/apilist',['uses'=>'VodController@getAllAPi','as'=>'getAll']);
		Route::get('broadcasters/{id}/newapilist',['uses'=>'VodController@returnAllAPi','as'=>'returnAllAPi']);
		Route::get('broadcasters/{id}/about',['uses'=>'VodController@getAbout']);
		Route::get('epg/{channel_id}',['uses'=>'EpgController@getEpg']);
		Route::get('broadcasters/{id}/vods','VodController@getVod');
		Route::get('broadcasters/{id}/adunits',['uses'=>'AdUnitController@getAdUnit']);
		Route::get('channels/{id}/stream',['as'=>"ChannelStreamUrl",'uses'=>'LiveTvController@getstream']);
		Route::get('channels/{id}/live',['as'=>"LiveStream",'uses'=>'LiveTvController@liveStream']);
		Route::get('radio/{id}/stream',['as'=>"RadioStreamUrl",'uses'=>'RadioController@getstream']);
		Route::get('movies/{id}/stream',['as'=>"MovieStreamUrl",'uses'=>'MovieController@getstream']);
		Route::get('broadcasters/{id}/data','VodController@test');
		Route::get('getCurrentTimeStamp', ['as' => 'CurrentTimestamp', function () {
			return \Response::json(['timestamp'=>getUtc()]);
		}]);
        Route::get('token/generate','FirebaseController@generate');

		//notification channels
		Route::get('broadcasters/app/{id}/notification/channel',['uses'=>'NotificationChannelsController@getNotificationChannels','as'=>'getNotificationChannels']);


		Route::get('template/{id}',['uses'=>'TemplateController@getByAppId']);
		Route::get('menus/{id}',['uses'=>'TemplateController@getServicesById']);

		Route::get('layouts',['uses'=>'TemplateController@getLayouts']);
		//check weather api is valid or not

		Route::get('ad-api/{id}',['uses'=>'\Broadcasters\Admanage\AdManageController@getAdSetting']);

		Route::get('api-status/{id}',['uses'=>'\App\Api\Controllers\AppController@apiStatus']);


		//dumping the record in database


		Route::post('dump-records',['uses'=>'\App\Api\Controllers\TemplateController@dumpRecords']);

});