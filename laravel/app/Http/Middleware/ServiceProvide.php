<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\App;
use Broadcasters\Models\Services;
class ServiceProvide
{
  protected $auth;


  public function __construct(Guard $auth){
    $this->auth = $auth;
  }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if($this->appServiceList($request)){
        return $next($request);
      } 

      return redirect('broadcaster');
    }

    //
    public function servicesList(){
      // return Services::lists('service_id')->toArray();
    }

    //
    public function appServiceList($request){

        $app_id = $request->segment(3);

        $service = $request->segment(5);
        //dd($service);
        $service_id = Services::where('service_name',$service)->first()->service_id;
        $all_service_id = App::find($app_id)->services()->get()->lists('service_id')->toArray();
        if(in_array($service_id,$all_service_id)){
            return true;
        }
        return false;


    }
  }
