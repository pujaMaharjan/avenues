<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\App;
use App\Http\Repository\Broadcaster as Repository;
use Broadcasters\Models\User;

class Subscriber
{
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
    if ($this->isPaid()) {
          return $next($request);
      }
    
    
    return redirect('broadcaster');
     
    }


    public function isPaid(){
        
        if($this->auth->user()->type=='broadcaster'){
             return (new Repository)->isPaid($this->auth->id());
        } 


    }


}
