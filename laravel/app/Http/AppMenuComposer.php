<?php
namespace App\Http;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use Broadcasters\Broadcaster\Providers\BroadcasterServiceProvider as BroadcasterService;
use Broadcasters\Channel\Providers\ChannelServiceProvider as ChannelService;
use Illuminate\Contracts\Auth\Guard;

class AppMenuComposer
{
    
    protected $request;
    protected $auth;
    protected $appService;
    protected $broadcasterService;
    protected $channel;

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function __construct(Request $request,AppService $appService, 
        Guard $auth, BroadcasterService $broadcasterService,
        ChannelService $channel){
        $this->request = $request;
        $this->appService = $appService;
        $this->auth = $auth;
        $this->broadcasterService = $broadcasterService;
        $this->channel = $channel;
       // $this->channelServiceProvider = $channelServiceProvider;
    }

    public function getId(){
        return $this->request->segment(3);
    }

    public function templatePassAppId(View $view){
       $app_id = (int)$this->getId();
       $view->with('channels',\App\App::find($app_id)->channels);
   }

   public function compose(View $view)
   {
    $view->with('menus',$this->appService->getService($this->getId()));
}
public function sideMenu(View $view)
{
    $view->with('appId',$this->getId());
}

public function appTopMenu(View $view)
{
    $appId = $this->getId();
    $app = $this->appService->getById($appId);
    $userId = $app->user_id;
    $broadcaster = $this->broadcasterService->getByUser($userId);
    $view->with('broadcaster',$broadcaster);
}

public function appTopMenuLeft(View $view)
{
    $user = $this->auth->user();


    $apps = $this->appService->approveAppByUserId($user->user_id);

    $appId = $this->getId();

    $selectApp = $this->appService->getById($appId); 
    if($user->type == 'admin')
    {
        $apps = $this->appService->approveAppByUserId($selectApp->user_id);
    }
    $view->with(['apps'=>$apps,
        'selectApp'=>$selectApp]);
}

public function passStreamUrl(View $view){

    $secretKey = \Config::get('site.appHashSecretKey');
    $utc = (int)trim(\Request::get('utc'));
    $apphash = trim(\Request::get('hash'));
    $type="wms";
    $url = $this->channel->getStreamUrl(2,'wms');
            //dd($this->channel->getStreamUrl(105));
    if(!$url){
        $url= 'jjj';
    }
    $view->with('url',$url);
}


//template confirmation section
public function recentNews(View $view){
    $recent_news = \Config::get('sample_api.new_api.recent_news');

    $recent_news = json_decode(file_get_contents($recent_news));
    $view->with('recent_news',$recent_news);
}

public function broadcasterTopMenu(View $view)
{
    //$user = $this->auth->user();
    //dd(\App\User::find($this->auth->user()->user_id)->type);
    $user = \App\User::find($this->auth->user()->user_id);

    
    $broadcaster = ($user->type=='admin')?
    $this->broadcasterService->getById($this->getId()):
    $this->broadcasterService->getByUser($user->user_id);;
   // dd($this->broadcasterService->getByUser($user->user_id));

    // dd($broadcaster);
       // $broadcaster = $this->broadcasterService->getByUser($user->user_id);

    $view->with('broadcaster',$broadcaster);
    
}

}