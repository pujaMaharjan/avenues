<?php

namespace App\Http\Controllers\NewsApi;

//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Models\AppExternalApi as Api;
use Broadcasters\Models\App;
use App\Http\Requests\NewsApiRequest as Request;


class NewsApiController extends Controller
{
    protected $api;

    public function __construct(Api $api){

        $this->api = $api;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $this->api = App::find($id);
        //dd($this->api = App::find($id));
        $newsApis =  $this->api->appExternalApi;
        return view("newsapi.index",compact('newsApis','id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('newsapi.new',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $app = App::find($id);
        $input =$request->all();

        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getExtension();
            $fileName = rand(1000,9999).".".$extension;

            $request->file('logo')->move('uploads',$fileName);

            $input = array_add($input,'logo',$fileName);
        }

        $app->channels()->create($input);
        \Session::flash('message', 'Channel has been added successfully');
        return redirect("broadcaster/app/$id/channels");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$app_channel_id)
    {
        $channel = Channel::find($app_channel_id);
        return view('channel.show',compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$channel_id)
    {
        $channel = Channel::find($channel_id);
        return view('channel.edit',compact('id','channel_id','channel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$channel_id)
    {
        $channel = Channel::find($channel_id);
        $channel->update($request->all());
        return redirect("broadcaster/app/$id/channels");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id,$channel_id)
    {
        Channel::find($channel_id)->delete();
        \Session::flash('message', 'Channel has been deleted successfully');
        return redirect(url('broadcaster/app/'.$id.'/channels'));

    }

}
