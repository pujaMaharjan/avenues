<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request as Simple;
use App\Models\AdSetting;
use Broadcasters\BaseController;
use Broadcasters\App\Models\App;

class AdManageController extends BaseController{

	public function __construct(AdSetting $setting,App $app)
	{
		$this->setting = $setting;
		$this->app = $app;
	}

	public function index(Simple $request)
	{
		$settings = $this->setting->all();
		return view('admin.ad-manage.index');
	}

	public function create(Simple $request)
	{
		$apps = $this->app->all();
		return view('admin.ad-manage.new',compact('apps'));
	}

	public function updateSetting(Simple $request)
	{
		$request->all();
		//let check app_id + cid + admob exists or not
		$app_id = $request->get('app_id');
		$adtype = $request->get('adtype');
		$country_id = $request->get('country_id');
		$admob_type_value = $request->get('admob_type_value');

		$setting = $this->setting
			->where(['app_id'=>$app_id,$adtype=>$admob_type_value,'country_id'=>$country_id])
			->orWhere([$adtype=>!$admob_type_value])
			->get();
		// return $setting->count();


		if($setting->count() > 0){
			 $this->setting->where(['app_id'=>$app_id,$adtype=>!$admob_type_value,'country_id'=>$country_id])->update([$adtype=>$admob_type_value]);
			 return ['success'=>1,'message'=>'Updated Success Fully'];
		}else{
			return $this->app->find($app_id)->adSetings()->create(['app_id'=>$app_id,$adtype=>$admob_type_value,'country_id'=>$country_id]);
		}


	}

	public function getAdSetting($app_id)
	{
		$countries = ['nepal','japan','chaina'];
		$ip = "202.166.205.145";
		$data = simplexml_load_file('http://www.geoplugin.net/xml.gp?ip=202.166.205.145');
		//$informations = new \SimpleXMLElement($data);
		// dd($data->geoplugin_countryName);
		$country =  $data->geoplugin_countryName;
		$cid = array_search($country,$countries);

		 return $this->app->find($app_id)->adSetings()->where('country_id',$cid)->first();
	}


}