<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Admanage\NitvAdKey;
use App\App;
class NitvAdController extends Controller
{

  private $nitvAdKey;
  private $app;

  public function __construct(NitvAdKey $nitvAdKey,App $app)
  {
      $this->nitvAdKey = $nitvAdKey;
      $this->app = $app;
  }
    /**
       * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adKeys = $this->nitvAdKey->all();

        return view('admin.nitv-ad.index',compact('adKeys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $apps =$this->app->lists('app_name','app_id');
        return view('admin.nitv-ad.add',compact('apps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'app_id'=>'required|unique:nitv_ad_keys',
          'ios_banneradunitid'=>'required',
          'ios_interestialadunitid'=>'required',
          'android_banneradunitid'=>'required',
          'android_interestialadunitid'=>'required'
        ]);
        $this->nitvAdKey->create($request->all());
        return redirect('admin/nitv/list');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $adKey =   $this->nitvAdKey->find($id);
           $apps =$this->app->lists('app_name','app_id');
        return view('admin.nitv-ad.edit',compact('adKey','apps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->nitvAdKey->find($id)->update($request->all());
        return redirect('admin/nitv/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
