<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\App;
use Auth;
use App\Http\Requests\AppRequest;
use Broadcasters\Models\Services;
use Session;
use App\Collector;
use App\PaymentDetail;
use App\PaymentReachStep;
use App\jobs\AppCreateJob;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Auth\Guard;

class AppController extends Controller
{
    protected $app;
    private $auth;

    public function __construct(App $app,Guard $auth,PaymentReachStep $paymentStep,PaymentDetail $paymentDetail){
        $this->app = $app;
        $this->auth = $auth;
        $this->paymentStep = $paymentStep;
        $this->paymentDetail = $paymentDetail;
        //disable payment
      //  $this->middleware('subscribe',['only'=>['create']]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $request = new AppRequest;
        $services = Services::lists('service_name','service_id');
        $services = Services::all();
        return view('apps.new',compact('services'));
    }

    public function postCreate(AppRequest $request){
        $data = $request->all();
        $data['app_code'] = mt_rand(1000, 9999);
        $data['status'] = 1;
        $app = $request->user()->apps()->create($data);
        $this->app->find($app->app_id)->services()->sync($request->get('services'));
        // $this->app->find($app->app_id)->paymentReachedStep()->create(['name'=>'cart','description'=>'..']);
        // return redirect('apps/cart/'.$app->app_id);
        return redirect('broadcaster');
        
    }

    public function cart(Request $request,$app_id){
     $this->authorize('update', $this->app->find($app_id));

     $services =  $this->app->find($app_id)->services;
     return view('apps.cart',compact('services','app_id'));

 }


 //for updating cards


 public function postCart(Request $request,$app_id){
    $this->authorize('update', $this->app->find($app_id)); 
    if($request->isMethod('post')){ 
        session::put('services',$request->get('service'));
        $this->app->find($app_id)->services()->sync($request->get('service'));
        $this->app->find($app_id)->paymentReachedStep()->update(['name'=>'payment','description'=>'payment Step']);
        return redirect('apps/payment/'.$app_id);
    }
}
public function postRenew(Request $request,$app_id){
    $this->authorize('update', $this->app->find($app_id)); 
    if($request->isMethod('post')){ 
        session::put('services',$request->get('service'));
        $this->app->find($app_id)->services()->sync($request->get('service'));
        $this->app->find($app_id)->paymentReachedStep()->update(['name'=>'payment','description'=>'payment Step']);
        return redirect('apps/renew-payment/'.$app_id);
    }
}




public function save(Request $request){
    $this->app->app_name = $request->input('app_name');
    $this->app->description = $request->input('description');
    $request->user()->app()->save();
    return $this->app->app_id;
}

public function payment(Request $request,$app_id){
    $this->authorize('update', $this->app->find($app_id));
    $services =  $this->app->find($app_id)->services;
    return view('apps.payment',compact('services','app_id'));
}

//Renew renewPayment

public function renewPayment(Request $request,$app_id){
    $this->authorize('update', $this->app->find($app_id));
    $services =  $this->app->find($app_id)->services;
    return view('apps.renew-payment',compact('services','app_id'));
}


public function updateCart(Request $request,$app_id,$id){
   $this->app->find($app_id)->services()->detach([$id]);
   return redirect('apps/cart/'.$app_id);

}


public function getServiceDetail($id){
    return Services::find($id);
}

public function paymentPaypal(Request $request){
    $app_id = $request->get('custom');

    $this->app->find($app_id)->update(['expire_at'=>date('Y-m-d', strtotime("+1 years")),'status'=>1]);


    if($request->has('txn_id')){
        \Log::info($request->all());
        $this->app->find($app_id)->paymentDetail()->create([

            'payment_gateway'=>'paypal',
            'total'=>$request->get('mc_gross'),
            'payment_description'=>json_encode($request->all()),
            'payment_status'=>1,
            'paid_for'=>\Carbon\Carbon::now()->addMonth()
            ]);
    }
// return $this->app->find($app_id);
    
   // sreturn redirect('/');

}


public function paymentStripe(Request $request,$app_id,\Auth $auth){
    // return $request->all();
    $auth::user()->newSubscription('main', 'monthy')->create($request->get('stripeToken'));
    $this->app->find($app_id)->update(['expire_at'=>date('Y-m-d', strtotime("+1 years")),'status'=>1]);
    $this->app->find($app_id)->paymentDetail()->create([
        'payment_gateway'=>'stripe',
        'total'=>$request->get('amount'),
        'payment_description'=>json_encode($request->all()),
        'payment_status'=>1,
        'paid_for'=>\Carbon\Carbon::now()->addMonth()
        ]);
    return redirect('broadcaster');
}

public function paymentBank(Request $request,$app_id,\Auth $auth){

    if($request->hasFile('bank_credential')){

        $filename = $this->auth->Id()."-".date('Y-m-d').".".$request->file('bank_credential')->getClientOriginalExtension();
        $request->file('bank_credential')->move("uploads/payments/",$filename);
    }
    $this->app->find($app_id)->update(['expire_at'=>date('Y-m-d', strtotime("+1 years")),'status'=>1]);
    $this->app->find($app_id)->paymentDetail()->create([
        'payment_gateway'=>'bank',
        'total'=>$request->get('total'),
        'payment_information'=>$filename,
        'paid_date'=>date('Y-m-d H:i:s'),
        'payment_status'=>1,
        'paid_for'=>\Carbon\Carbon::now()->addMonth()
        ]);

    $this->app->find($app_id)->paymentReachedStep()->update(['name'=>'pending','description'=>'Completing']);

 // \Event::fire(new \App\Events\BroadcasterServicePaid(\Broadcasters\App\Models\App::find($app_id)));

    //\Event::fire(new \App\Events\BroadcasterServicePaid($this->app->find($app_id)));
    // return redirect('apps/pending/'.$app_id);
    return redirect('broadcaster');

}


public function edit(Request $request,$app_id){
    $services = Services::lists('service_name','service_id');
    $services = Services::all();
    $app = $this->app->find($app_id);
    return view("apps.edit",compact("services","app"));
}



public function analysisStep(Request $request,$app_id){

    $step =  $this->paymentStep->where('app_id',$app_id)->first();
    
    
    switch($step->name){

        case 'cart':
        return redirect('apps/cart/'.$app_id);
        break;
        case 'payment':
        return redirect('apps/payment/'.$app_id);
        break;
        case 'pending':
        return redirect('apps/pending/'.$app_id);
        break;
    }
}

public function pending($app_id){

    $this->authorize('update', $this->app->find($app_id));
    $payment_details =  $this->app->find($app_id)->paymentDetail()->first();

    return view("apps.pending",compact("services","payment_details"));

}

public function confirm($app_id,$payment_detail_id,PaymentDetail $paymentDetail){
   $paymentDetail->find($payment_detail_id)->update(['payment_status'=>1]);
   return json_encode(['message'=>'success']);
}

public function paymentDetailPdf($app_id,$payment_detail_id){

  $payment = $this->paymentDetail->where('payment_detail_id',$payment_detail_id)->with('app')->first();
  
  return view('apps.paymentDetailPdf',compact('payment'));
}

public function paymentDetailPdfPost($app_id,$payment_detail_id)
{
 $content = file_get_contents(route('paymentPdf',[$app_id,$payment_detail_id]));


 $html2pdf = new \HTML2PDF('P','A4','fr');
 $html2pdf->WriteHTML($content);
 $html2pdf->Output('exemple.pdf');
}


public function renewApp($app_id){
    $services =  $this->app->find($app_id)->services;
    return view('apps.renew',compact('services','app_id'));
}


public function analysisRenewStep(Request $request,$app_id){

    $step =  $this->paymentStep->where('app_id',$app_id)->first();
    switch($step->name){

        case 'cart':
        return redirect('apps/renew/'.$app_id);
        break;
        case 'payment':
        return redirect('apps/renew-payment/'.$app_id);
        break;
        case 'pending':
        return redirect('apps/renew-pending/'.$app_id);
        break;
    }
}
public function checkAppPayment(Request $request)
{


    $paymentDetail =  $this->app->find($request->get('id'))->paymentDetail()->get()->last();
    
    
    $app = $this->app->find($request->get('id'));
    $today = date('Y-m-d h-m-s');


    if($paymentDetail->payment_status == 0 || $app->status == 0 || $app->expire_at < $today )
    {
        return ['message'=>'Please confirm app payment first/renew','flag'=>false];
    }
    else
    {
        return ['message'=>'Add note while conform','flag'=>true];
    }
    
}
}
