<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Audio;
use App\App;
class AudioController extends Controller
{

    private $audio;
    public function __construct(Audio $audio)
    {
        $this->audio = $audio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($app_id)
    {
        $audios =  App::find($app_id)->audios()->get();
        return view('audios.index',compact('audios','app_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($app_id)
    {
        return view('audios.form',compact('app_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($app_id,Request $request)
    {

        $this->validate($request,[
            'title'=>'required',
            'description'=>'required'
        ]);
        $data = $request->all();
        if ($request->hasFile('image')) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = date('Y-m-d-H-i-d').".".$extension;

            $request->file('image')->move('uploads',$fileName);

            $data = array_add($data,'logo',$fileName);
        }
        $this->audio->create($data);
        return redirect('broadcaster/app/'.$app_id.'/services/audios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($app_id,$id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($app_id,$id)
    {
        $audio = $this->audio->find($id);
       return view('audios.form',compact('app_id','id','audio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($app_id,$id,Request $request)
    {

        $this->validate($request,[
            'title'=>'required',
            'description'=>'required'
        ]);
        $data = $request->all();
        if ($request->hasFile('image')) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = date('Y-m-d-H-i-d').".".$extension;

            $request->file('image')->move('uploads',$fileName);

            $data = array_add($data,'logo',$fileName);
        }
        $this->audio->find($id)->create($data);
        return redirect('broadcaster/app/'.$app_id.'/services/audios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($app_id,$id)
    {
         $this->audio->find($id)->delete();
         return redirect('broadcaster/app/'.$app_id.'/services/audios');
    }
}
