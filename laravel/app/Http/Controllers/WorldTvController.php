<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\WorldTv;
use App\Http\Requests\WorldTvRequest;
use  Broadcasters\Models\Broadcaster;
class WorldTvController extends Controller
{
    public $worldTv;
    public $broadcaster;
    public function __construct(WorldTv $worldTv,Broadcaster $broadcaster)
    {
        $this->worldTv = $worldTv;
        $this->broadcaster = $broadcaster;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = auth()->user()->broadcaster->worldTv()->get();
        return view('world-tv.index',compact('channels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('world-tv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorldTvRequest $request)
    {
        // dd($request->all());
        auth()->user()->broadcaster->worldTv()->create($request->all());
        return redirect('broadcaster/channels');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $channel =  $this->worldTv->find($id);
        return view('world-tv.edit',compact('channel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorldTvRequest $request, $id)
    {
        // return $request->all();
        $this->worldTv->find($id)->update($request->all());
        return redirect('broadcaster/channels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
