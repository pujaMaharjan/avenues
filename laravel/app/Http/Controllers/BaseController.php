<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseController extends Controller
{
	protected $common;

	/**
	 * overwrite parent view function
	 */
	public function view($path, $data)
	{
		return Parent::view($path, $data);
	}
}