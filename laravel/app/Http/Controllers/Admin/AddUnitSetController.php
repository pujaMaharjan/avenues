<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request as Simple;

use Broadcasters\BaseController;
use App\Http\Requests\AddUnitSetRequest as Request;
use App\Http\Requests\AppVersionRequest as VersionRequest;
use App\Http\Requests\UpdateAddUnitSetRequest as SimpleRequest;
use Broadcasters\AddUnitSet\Providers\AddUnitSetServiceProvider as AddUnitSetService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use Broadcasters\Broadcaster\Providers\BroadcasterServiceProvider as BroadcasterService;

use Broadcasters\Platform\Providers\PlatformServiceProvider as PlatformService;
use Broadcasters\AddUnit\Providers\AddUnitServiceProvider as AddUnitService;
use Broadcasters\UnitEarning\Providers\EarningsServiceProvider as EarningService;


class AddUnitSetController extends BaseController
{
  protected $service;
  protected $AppService;
  protected $platformService;
  protected $addUnitService;
  protected $broadcasterService;
  function __construct(AddUnitSetService $service,AppService $appService,PlatformService $platformService,AddUnitService $addUnitService,EarningService $earningService,BroadcasterService $broadcasterService)
  {
    parent::__construct();
    $this->service = $service; // addUnitSet service 
    $this->appService = $appService; // serivce for broadcaster
    $this->platformService = $platformService; // service for platfrom 
    $this->addUnitService = $addUnitService; // service for add units
    $this->earningService = $earningService;
    $this->broadcasterService = $broadcasterService;
    $this->middleware('auth');
    $this->middleware('admin',['except' => ['indexBroadcaster','showEarningBroadcaster']]);
  }

/**
 * return list view with unit and broadcaster
 */
public function index()
{
  $units = $this->service->getAddUnit();
  $app = $this->service->getApp();

  return $this->view("admin.addunits.index",compact('units','app'));
  
}

/**
 * create unit and its sets 
 return create view 
 */
 public function create()
 {
  $apps = $this->service->test();
  //dd($app);
  //$categories = Categories::all();
  $selectApp = [];

  foreach($apps as $app) {
    $selectApp[$app->app_id] = $app->app_name;
  }
  

  $platformList = $this->platformService->getLists();
  //$broadcasterList = $this->broadcasterService->getLists('display_name','broadcaster_id')->all();

  return view('admin.addunits.create')->with(compact(['platformList','selectApp']));
}
/**
 * store all values into add unit and add units set 
  return list view with success message 
 */
  public function store(Request $request)
  {
  
    $id = $this->service->create($request);
    // dd($id);
    $add_unit_set_id = $id->add_unit_set_id;
    for($i=0; $i<count($request->get('platform_id')); $i++){
      $img = $request->get('imgBanner');
      $type = $request->get('typeBanner');
      $input = [
      'add_unit_set_id' => $add_unit_set_id,
      'unit_name' => $img[$i],
      'type' => $type[$i],
      'platform_id' => $request->get('platform_id')[$i]
      ];
      //dd($input);
      $this->addUnitService->saveAddUnit($input);
    }
    for($i=0; $i<count($request->get('platform_id')); $i++){
      $img = $request->get('imgInterstitial');
      $type = $request->get('typeInterstitial');
      $input = [
      'add_unit_set_id' => $add_unit_set_id,
      'unit_name' => $img[$i],
      'type' => $type[$i],
      'platform_id' => $request->get('platform_id')[$i]
      ];
      //dd($input);
      $this->addUnitService->saveAddUnit($input);
    }


    return redirect('admin/unit/list')->with("success","New units has been added!");
  } 
  //return edit view
  public function edit($id)
  {

    $units = $this->service->getById($id);
    $platformList = $this->platformService->getLists();
    $apps = $this->appService->getAppList('app_name','app_id')->all();


    return view('admin.addunits.edit')->with(compact(['units','apps','platformList']));
  }
/**
 * update unit set and its units and return to list view 
 */
public function update(SimpleRequest $request,$id)
{

  $this->service->update($request,$id);
  $ids = $request->get('id');

  //$addUnitService = new \Broadcasters\AddUnit\Models\AddUnit;
  for($i=0; $i<count($request->get('img')); $i++){
    //$addUnitService->find($ids[$i]);
    // dd(isset($request->get('status')[$i]));
    $add_unit_id = $ids[$i];
    $input = [
    'add_unit_set_id' => $id,
    'unit_name' => $request->get('img')[$i],
    'type' => $request->get('type')[$i],
    'platform_id' => $request->get('platform_id')[$i],
    'status'=>isset($request->get('status')[$i])?true:false
    ];

    $this->addUnitService->update($input,$add_unit_id);
  }
  return redirect('admin/unit/list')->with("success","units has been updated!");
}

/**
 * delete unit set and return list view 
 */
public function delete($id)
{
  $this->service->delete($id);
  return redirect('admin/unit/list')->with("success","Deleted");
}

  /**
   * return view to show details 
   */
  public function show($id)
  {

    $units = $this->service->getById($id);
    $platformList = $this->platformService->getLists();
    $apps = $this->service->getApp();

    return view('admin.addunits.show')->with(compact(['units','apps','platformList']));
  }

//broadcaster part

  public function indexBroadcaster($broadcasterId)
  {
    $adminAsBroadcaster = $this->broadcasterService->getById($broadcasterId);
    $userId = $adminAsBroadcaster->user_id;
    $apps = $this->appService->getAppByUserId($userId);

   // dd($appId);

    $units = $this->service->getUnitByApp($apps);
    //echo count($units);
    // dd($units->addUnit);
    $platformList = $this->platformService->getLists();
    //$adminAsBroadcaster = $this->broadcasterService->getById($broadcasterId);
    return $this->view("broadcaster.units.index",compact('units','adminAsBroadcaster','platformList'));
  }
  public function editBroadcaster($broadcasterId,$id)
  {
   // dd($id);
    $units = $this->service->getById($id);
    $platformList = $this->platformService->getLists();
    $apps = $this->appService->getAppList('app_name','app_id')->all();
    //$broadcasterList = $this->broadcasterService->getLists('display_name','broadcaster_id')->all();
    $adminAsBroadcaster = $this->broadcasterService->getById($broadcasterId);
    //dd($adminAsBroadcaster);
    return $this->view('broadcaster.units.edit',compact('units','apps','adminAsBroadcaster','platformList'));
  }


  public function updateBroadcaster(SimpleRequest $request,$id)
  {
 // dd($request->all());
    $this->service->update($request,$id);

    $ids = $request->get('id');
    $broadcaster_id = $request->get('broadcaster_id');

  //$addUnitService = new \Broadcasters\AddUnit\Models\AddUnit;
    for($i=0; $i<count($request->get('img')); $i++){
    //$addUnitService->find($ids[$i]);
      $add_unit_id = $ids[$i];
      $input = [
      'add_unit_set_id' => $id,
      'unit_name' => $request->get('img')[$i],
      'type' => $request->get('type')[$i],
      'platform_id' => $request->get('platform_id')[$i]
      ];

      $this->addUnitService->update($input,$add_unit_id);
    }

    return redirect("admin/broadcaster/$broadcaster_id/unit")->with("success","units has been updated!");
  }
  public function showEarning(Simple $request,$id)
  {
    //dd($request->all());
    $total = 0;
    $units = $this->service->getById($id);
  //dd($units->addUnit->first()->unit_name);
    $earning = $this->earningService->getEarningByUnits($units);
    $platformList = $this->platformService->getLists();
    $app = $this->service->getApp();
    //$broadcasters = $this->service->getBroadcaster();
    if($request->isMethod('post')){
    //return $request->all();
      $earning = $this->earningService->getFilterEarning($request,$units);
      foreach ($earning as $value) {

        $total = $total + $value;
      }
      return [
      'earning'=>$earning,
      'units'=>$units->addUnit,
      'platformList'=>$platformList,
      'app'=>$app,
      'total'=>$total
      ];
    }
    foreach ($earning as $value) {

      $total = $total + $value;
    }



    return $this->view('admin.addunits.earning',compact('units','app','earning','platformList','total','id'));

  }
  public function showEarningDetails($id)
  {
    $units = $this->service->getById($id);
    $earningDetails = $this->earningService->getEarningDetailsByUnits($units);
    return $this->view('admin.addunits.earningDetails',compact('earningDetails'));;
  }
  public function showEarningBroadcaster(Simple $request,$broadcasterId,$id)
  {
    $total = 0;
    $units = $this->service->getById($id);
    $earning = $this->earningService->getEarningByUnits($units);
    $platformList = $this->platformService->getLists();
    $app = $this->service->getApp();
    $adminAsBroadcaster = $this->broadcasterService->getById($broadcasterId);
    
    if($request->isMethod('post')){

      $earning = $this->earningService->getFilterEarning($request,$units);

      foreach ($earning as $value) {
       $total = $total + $value;
     }

     return [
     'earning'=>$earning,
     'units'=>$units->addUnit,
     'platformList'=>$platformList,
     'app'=>$app,
     'total'=>$total
     ];
   }
   
   foreach ($earning as $value) {

    $total = $total + $value;
  }



  return $this->view('broadcaster.units.earning',compact('units','app','earning','platformList','total','id','adminAsBroadcaster'));
}

public function version($id)
{
  //dd($id);
  $units = $this->service->getById($id);
  $app = $this->appService->getApp($units->app_id);
  //dd($app);
  $platformList = $this->platformService->getLists();
  return view('admin.addunits.updateVersion')->with(compact(['platformList','units','id','app']));
}

public function updateVersion(VersionRequest $request,$id)
{
  //dd($id);
  $result = $this->addUnitService->updateVersion($request);
  //dd($result);
  if($result > 0)
  {
    return redirect('admin/unit/list')->with("success","App Version Updated");
    //return 'update';
  }
  else
  {
    return redirect('admin/unit/'.$id.'/update/version')->with("failure","check your app version once");
  }

}

}