<?php
namespace App\Http\Controllers\Admin;

use Broadcasters\BaseController;
use Broadcasters\Broadcaster\Providers\BroadcasterServiceProvider as BroadcasterService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use Broadcasters\UnitEarning\Providers\EarningsServiceProvider as EarningService;
use Broadcasters\PaymentDetails\Providers\PaymentDetailsServiceProvider as PaymentDetailService;

use App\Http\Requests\Request;

class AdminController extends BaseController
{
	function __construct(BroadcasterService $broadcasterService,AppService $appService,
		EarningService $earningService,PaymentDetailService $paymentDetails)
	{
		parent::__construct();
		$this->broadcasterService = $broadcasterService;
		$this->appService = $appService;
		$this->earningService = $earningService;
		$this->paymentDetails = $paymentDetails;
		$this->middleware('auth',['only' => ['index']]);
		$this->middleware('admin',['only' => ['index']]);
		
	}

	public function index()
	{
		
		$broadcasters = $this->broadcasterService->getAll();
		$count = $this->broadcasterService->count();
		$countApp = $this->appService->count();
		$totalEarning = $this->earningService->totalEarning();
		$totalAppEarning = $this->paymentDetails->totalEarning();
		
        //$title = $this->title." - Profile";
		return $this->view("admin.dashboard",compact('broadcasters','count','countApp','totalEarning','totalAppEarning'));

	}

	public function viewAllBroadcaster()
	{
		$broadcasters = $this->broadcasterService->getAll();
		return $this->view("admin.viewAllBroadcaster",compact('broadcasters'));

	}

	
}
