<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Models\Services;

class SiteController extends Controller
{

    public function home()
    {
       return view('welcome');
    }
}
