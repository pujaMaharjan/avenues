<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Laravel\Cashier\Http\Controllers\WebhookController as Controller

class WebhookController extends Controller
{
   public function handleChargePaymentSucceeded(Request $request)
    {
        return $request->all();
    }   
}
