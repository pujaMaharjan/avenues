<?php

namespace App\Http\Controllers\Broadcasters;

use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\App;
use App\Dvr;
use Broadcasters\BaseController;
use App\Api\ApiController;
use Carbon\Carbon;


class DvrController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index($id)
//    {
//        $app = App::find($id);
//        $news = $app->appNews;
//        return view("news.index", compact('news', 'id'));
//    }


    function __construct()
    {
        parent::__construct();
        $this->middleware('auth', ['only' => ['index']]);
        $this->middleware('service', ['only' => ['index', 'create', 'edit', 'update', 'delete']]);

        //$this->middleware('admin',['only' => ['index']]);
    }

    public function create($app_id)
    {
        return view('dvr.new', compact('app_id'));
    }

    public function index($app_id)
    {
        $dvrs = Dvr::where('app_id', $app_id)->get();
        return view("dvr.index", compact('app_id', 'dvrs'));
    }

    public function store(Request $request)
    {
        Dvr::firstOrCreate([
            "name" => $request->name,
            "url" => $request->url,
            "app_id" => $request->app_id,
            "created_by" => $request->created_by,
            "option" => $request->option
        ]);
        \Session::flash('message', 'DVR has been added successfully');
        return redirect("broadcaster/app/$request->app_id/services/Dvr");
    }

    public function edit($app_id, $id)
    {
        $dvr = Dvr::where('id', $id)->first();
        return view('dvr.edit', compact('app_id', 'dvr'));
    }

    public function update($app_id, $id, Request $request)
    {
        Dvr::where('id', $id)->update([
            "name" => $request->name,
            "url" => $request->url,
            "app_id" => $request->app_id,
            "created_by" => $request->created_by,
            "option" => $request->option
        ]);
        \Session::flash('message', 'DVR has been updated successfully');
        return redirect("broadcaster/app/$app_id/services/Dvr");
    }

    public function delete($app_id, $id)
    {
        Dvr::where('id', $id)->delete();
        \Session::flash('message', 'DVR has been deleted successfully');
        return redirect("broadcaster/app/$app_id/services/Dvr");
    }

    public function getStreamUrl($app_id, $dvr, Request $request)
    {
        $secretKey = \Config::get('site.appHashSecretKey');
        $utc = (int)trim(\Request::get('utc'));
        $apphash = trim(\Request::get('hash'));
        $hash = md5($secretKey . $utc);
        $apiResponse = new ApiController();
        if ($apphash != $hash) {
            return $apiResponse->respondNotFound('Stream not found-hash error.');
        }

        if (getUtc() - $utc > 300) {
            return $apiResponse->respondNotFound('Stream not found - time error.');
        }
        return $this->makeStreamUrl(Dvr::find($dvr));
    }


    public function getUrlSignature($key, $validminutes)
    {
        $today = gmdate("n/j/Y g:i:s A");
        $ip = $_SERVER['REMOTE_ADDR'];
        $str2hash = $ip . $key . $today . $validminutes;
        $md5raw = md5($str2hash, true);
        $base64hash = base64_encode($md5raw);
        $urlsignature = "server_time=" . $today . "&hash_value=" . $base64hash . "&validminutes=" . $validminutes;
        return base64_encode($urlsignature);
    }

    public function makeStreamUrl($dvr)
    {
        $wmskey = $this->getUrlSignature('##TVTVNKEY#W2016Q2', 2);
        $apiResponse = new ApiController();
        return $apiResponse->respond([
            'stream' => $dvr->url . '?wmsAuthSign=' . $wmskey,
            'type' => "wms"
        ]);

    }

    public function getTimestamp()
    {
        return response()->json(["status" => 200, "timestampUrl" => url('api/redirectTimestamp')]);
    }

    public function redirectTimestamp()
    {
        $date = new \DateTime(gmdate("Y-m-d H:i:s"));
        $timestamp = $date->getTimestamp();
        return response()->json(["status" => 200, "timestamp" => $timestamp]);
    }

    public function test()
    {
//
//        $timezone = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, "NP");
//        dd($timezone);
//
//        $dateTimeZoneNepal = new \DateTimeZone("Asia/Kathmandu");
//        $dateTimeNepal = new \DateTime("now", $dateTimeZoneNepal);
//        dd($dateTimeNepal);

        $ip_address = "202.166.207.237";
        $ip_country = geoip_country_code_by_name($ip_address);
        $timezone = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $ip_country);

        $dtz = new \DateTimeZone($timezone[0]);
        $time_in_sofia = new \DateTime('now', $dtz);

        $offset = $dtz->getOffset($time_in_sofia) / 3600;
        $d_offset = explode(".", $offset);
        if (sizeof($d_offset) > 1) {
            if ($d_offset[1] == "75") {
                $offset = $d_offset[0] . "." . 45;
            }
            if ($d_offset[1] == "5") {
                $offset = $d_offset[0] . "." . 30;
            }
            if ($d_offset[1] == "25") {
                $offset = $d_offset[0] . "." . 15;
            }
        }

//        $gmt = $offset < 0 ? $offset : $offset;


        $start_time = "6:00" + $offset;
        $end_time = "7:00" + $offset;
        $data =
            [
                "1" => [
                    "name" => "abc",
                    "start" => $start_time,
                    "end" => $end_time
                ],
                "2" => [
                    "name" => "efg",
                    "start" => $start_time,
                    "end" => $end_time
                ]
            ];
        dd($data);

//        dd($dateTimeNepal = new \DateTime("now", GMT));
//
//
//        $date = new \DateTime();
//        $x = $date->setTimezone("9:00",\DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, "NP"));
//        dd($x);
//        $ip_address = "202.166.207.237";
//        echo "$ip_address, ";
//        $ip_country = geoip_country_code_by_name($ip_address);
//        echo "$ip_country <br/>";
//        $date = \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $ip_country);
//        $d = new \DateTime("2017-08-30 20:14:03", new \DateTimeZone($date[0]));
//        dd($d);
//        echo $d->format(\DateTime::W3C); //2010-08-14T15:22:22+00:00

//
//        if (env("APP_ENV") == 'local') {
//            return 'local';
//        } else {
//            $country = geoip_country_code_by_name(\Request::ip());
//            if ($country == 'np') {
//                return 'nepal';
//            } else {
//                return $country;
//            }
//        }


//        $GMT = new \DateTimeZone("GMT");
//        $data = [
//            "name" => "sdsd",
//            "start" => new \DateTime("16:00:00", $GMT),
//            "end" => "7:00"
//        ];
//
//        return $data;
    }
}