<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Channel\Models\AppChannels as Channel;
use Broadcasters\Epg\Models\AppChannelEpg as Epg;
use App\Supports\UploadHelper;
use App\AppChannelEpgDetail as EpgDetail;
use Broadcasters\Epg\Providers\EpgServiceProvider;

class EpgController extends Controller
{
    use UploadHelper;

    protected $epg;
    protected $epgServiceProvider;

    
    public function __construct(Epg $epg, EpgServiceProvider $epgServiceProvider){
        $this->epg = $epg;
        $this->epgServiceProvider = $epgServiceProvider;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($app_id,$channel_id)
    {
        $channel = Channel::find($channel_id);
        $this->epg = Epg::first();
        $epgs = [];
        if($this->epg) {
            $epgs = $this->epg->appChannelEpgDetail;
        }
        return view('epg.index',compact('epgs','app_id','channel_id'));
    }
    /**
     * Store a newly created resource in storage.
     *$channel = Channel::find($channel_id);
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$app_id,$channel_id)
    {
        if ($request->hasFile('schedule')) {

            $this->epgServiceProvider->save($app_id,$channel_id,'file',$request);
        }else{
            $this->epgServiceProvider->save($app_id,$channel_id,'url',$request);

        }
       return redirect("broadcaster/app/$app_id/services/channel/$channel_id/epg");
    }

   
}
