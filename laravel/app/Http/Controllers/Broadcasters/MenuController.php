<?php

namespace App\Http\Controllers\Broadcasters;

use App\Http\Requests;
use Illuminate\Http\Request;
use Broadcasters\Admanage\AdSetting;
use Broadcasters\BaseController;
use Broadcasters\App\Models\App;
use App\Models\Menu;

class MenuController extends BaseController{

	public function __construct(App $app,Menu $menu)
	{
		$this->app = $app;
		$this->menu = $menu;
	}
	public function index($appId)
	{
		$menus =  $this->app->find($appId)->menus;
		return view('menus.index',compact('menus','appId'));
	}

	public function create($appId)
	{
		return view('menus.form',compact('appId'));
	}

	public function store($appId,Request $request)
	{

		$input = $request->all();

		if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();

            $fileName = rand(1000,9999).".".$extension;

            $request->file('logo')->move('uploads/menus/',$fileName);

            $input['logo'] = $fileName;
        }


		$this->app->find($appId)->menus()->create($input);
		return redirect('broadcaster/app/'.$appId.'/menus');
	}

	public function edit($appId,$menuId)
	{
		$menu = $this->menu->find($menuId);
		return view('menus.form',compact('appId','menu'));
	}

	public function update($appId,$menuId,Request $request)
	{
		$input = $request->all();

		if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
             // dd($extension);
            $fileName = rand(1000,9999).".".$extension;
             // dd($fileName);
            $request->file('logo')->move('uploads/menus/',$fileName);

            $input['logo'] = $fileName;
        }

		$this->menu->find($menuId)->update($input);

		return redirect('broadcaster/app/'.$appId.'/menus');
	}
}