<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Radios\RadioServiceProvider;

class RadioController extends Controller
{
    private $provider;

    public function __construct(RadioServiceProvider $provider){
    	$this->provider = $provider;
    }



    public function index($app_id){
    	$radios= $this->provider->all($app_id);
    	return view('radios.index',compact('app_id','radios'));
    }

    public function create($app_id){

      $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
      return view('radios.form',compact('app_id','countries'));
  }

  public function store($app_id,Request $request){
     $this->validate($request,[
        'name'=>'required',
        'bank_credential'=>'mimes:jpeg,bmp,png,gif,jpg'
    ]);
    $input = $request->all();

    if($request->hasFile('logo')){
        $input['logo'] =  $request
        ->file('logo')
        ->getClientOriginalName();
        $request->file('logo')->move('uploads/radios',$input['logo']);
    }
    $this->provider->save($input,$app_id);
    return redirect("broadcaster/app/$app_id/services/radios");
}

public function edit($app_id,$id){
    $radio = $this->provider->find($id);

    $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
    return view('radios.form',compact('radio','app_id','countries','id'));
}
public function update($app_id,$id,Request $request){
    $input = $request->all();
   $this->validate($request,[
        'name'=>'required',
        'logo'=>'mimes:jpeg,bmp,png,gif,jpg'
    ]);
   
    if($request->hasFile('logo')){
        $input['logo'] =  $request
        ->file('logo')
        ->getClientOriginalName();
        $request->file('logo')->move('uploads/radios',$input['logo']);
    }


    $this->provider->save($input,$app_id,$id);
    return redirect("broadcaster/app/$app_id/services/radios");
}

public function delete($app_id,$id)
{
    $this->provider->delete($id);
    return redirect("broadcaster/app/$app_id/services/radios");
    

}
public function show($app_id,$id)
{
    $radio = $this->provider->find($id);
    return view('radios.show',compact('radio'));


    

}


}
