<?php

namespace App\Http\Controllers\Broadcasters;

//use Illuminate\Http\Request;
use App\Http\Requests;
use Broadcasters\BaseController;
use Broadcasters\Channel\Models\AppChannels as Channel;
use App\App;
use Broadcasters\Channel\Requests\ChannelRequest as Request;

class ChannelController extends BaseController
{
    function __construct() {
        parent::__construct();
        $this->middleware('auth',['only' => ['index']]);
        $this->middleware('service',['only' => ['index','create','edit']]);

        //$this->middleware('admin',['only' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id,App $app)
    {

        $app = $app->find($id);
        $channels =  $app->channels;
        return $this->view("channel.index",compact('channels','id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
        return view('channel.new',compact('id','countries'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $app = App::find($id);
        $input =$request->all();

        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileName = rand(1000,9999).".".$extension;

            $request->file('logo')->move('uploads',$fileName);

            $input = array_add($input,'logo',$fileName);
        }

        $app->channels()->create($input);
        \Session::flash('message', 'Channel has been added successfully');
        return redirect("broadcaster/app/$id/services/channels");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$app_channel_id)
    {
        $channel = Channel::find($app_channel_id);
        return view('channel.show',compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$channel_id)
    {
        if(\Gate::denies('update',App::find($id))){
            abort(403,"You Cannot edit this");
        }
        $channel = Channel::find($channel_id);
        $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
        return view('channel.edit',compact('id','channel_id','channel','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(Request $request, $id,$channel_id)
    {
        $channel = Channel::find($channel_id);


        $input = $request->all();
        //dd($request->hasFile('logo'));
        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileName = rand(1000,9999).".".$extension;
            // dd($fileName);

            $request->file('logo')->move('uploads',$fileName);

            $input['logo'] = $fileName;
        }

        // dd($input);

        $channel->update($input);
        return redirect("broadcaster/app/$id/services/channels");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id,$channel_id)
    {
        Channel::find($channel_id)->delete();
        \Session::flash('message', 'Channel has been deleted successfully');
        return redirect(url('broadcaster/app/'.$id.'/services/channels'));

    }
}