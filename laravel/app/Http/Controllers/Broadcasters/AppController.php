<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request;
use App\Audio;
use App\App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\AppRequest;
use Broadcasters\Models\Services;
use Session;
use App\Collector;
use App\PaymentDetail;
use App\PaymentReachStep;
use App\jobs\AppCreateJob;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Auth\Guard;
use App\Models\FirebaseToken;

class AppController extends Controller
{


    public function __construct(App $app, Guard $auth, PaymentReachStep $paymentStep, PaymentDetail $paymentDetail)
    {
        $this->app = $app;
        $this->auth = $auth;
        $this->paymentStep = $paymentStep;
        $this->paymentDetail = $paymentDetail;
        $this->middleware('subscribe', ['only' => ['create']]);
    }


    public function changeLogo($app_id, Request $request)
    {

        return view('apps.change-logo', compact('app_id'));
    }

    public function splashImage($app_id, Request $request)
    {

        return view('apps.splash-image', compact('app_id'));
    }


    public function postChangeLogo($app_id, Request $request)
    {
        $this->validate($request, [
            'logo' => 'mimes:jpeg,bmp,png,gif,jpg'
        ]);
        $data = ['web_link' => $request->get('web_link')];
        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileName = date('Y-m-d-H-i-s') . "." . $extension;

            $request->file('logo')->move('uploads', $fileName);

            $data['logo'] = $fileName;
        }
        $this->app->find($app_id)->update($data);
        return redirect('broadcaster');
    }

    public function postsplashImage($app_id, Request $request)
    {
        if ($request->hasFile('splashimage')) {
            $extension = $request->file('splashimage')->getClientOriginalExtension();
            $fileName = date('Y-m-d-H-i-s') . "." . $extension;

            $request->file('splashimage')->move('uploads', $fileName);
            $this->app->find($app_id)->update(['splashimage' => $fileName]);
            return redirect('broadcaster');
        }
    }

    public function renewPayment(Request $request, $app_id)
    {
        $this->authorize('update', $this->app->find($app_id));
        $services = $this->app->find($app_id)->services;
        return view('apps.renew-payment', compact('services', 'app_id'));
    }

    public function sendMessage($app_id)
    {
        return view('broadcaster.notification.firebase', compact('app_id'));
    }

    public function postEendMessage($app_id, Request $request)
    {
        $registration_ids = [];
        $results = FirebaseToken::where('app_id', $app_id)->select('token')->get();
        foreach ($results as $res) {
            $registration_ids[] = $res->token;
        }
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $registration_ids,
            'notification' => ['text' => $request->get('message'), 'title' => $request->get('title'), 'app_id' => $app_id],
            'priority' => 'high',
//            'data' => ['message' => $request->get('message'), 'redirect' => $request->get('title'), 'app_id' => $app_id]
        );
//        return $this->app->where('app_id',$app_id)->select('app_key')->first();

        $headers = array(
            'Authorization:key = AIzaSyA5FhDcEqWRLeMST9MAX9F86Ss_t-lbiic',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return redirect('broadcaster/app/' . $app_id . '/notification');
    }

    public function serverKey(Request $request)
    {
        $this->app->where('app_id', $request->appId)->update(["app_key" => $request->appKey]);
        return redirect('broadcaster');
    }
}