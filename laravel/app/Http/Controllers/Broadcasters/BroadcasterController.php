<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request as SimpleRequest;
use App\Http\Requests;
use Broadcasters\BaseController;
//use Broadcasters\Broadcaster\Models\Broadcaster as Broadcaster;
use App\Http\Requests\BroadcasterRequest as Request;
use App\Http\Requests\ChangePasswordRequest as ChangePasswordRequest;

use Broadcasters\Broadcaster\Providers\BroadcasterServiceProvider as BroadcasterService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use Broadcasters\Country\Providers\CountryServiceProvider as CountryService;
use Broadcasters\User\Providers\UserServiceProvider as UserService;
use Broadcasters\PaymentRegisterDetail\Providers\PaymentRegisterDetailServiceProvider as PaymentRegisterDetailService;
use Broadcasters\User\Models\User;


class BroadcasterController extends BaseController
{
    protected $service;

    function __construct(BroadcasterService $service, AppService $appService, CountryService $countryService, UserService $userService,
                         PaymentRegisterDetailService $paymentRegisterDetailService)
    {
        parent::__construct();
        $this->service = $service;
        $this->appService = $appService;
        $this->countryService = $countryService;
        $this->userService = $userService;
        $this->paymentRegisterDetailService = $paymentRegisterDetailService;

        //$this->middleware('auth',['except' => ['register','store','cart','postCart']]);
        //$this->middleware('admin',['only' => ['index']]);
        //$this->middleware('broadcaster',['only' => ['index','profile','postCart','cart']]);
        $this->middleware('subscribe', ['only' => ['index']]);

    }


    public function index()
    {

        $broadcaster = $this->service->getBroadcaster();

        // dd($broadcaster);
        $id = $broadcaster->broadcaster_id;
        $userId = $broadcaster->user_id;

        $apps = $this->appService->getAppByUserId($userId);
        // if($apps->count() ==1){
        //           //return redirect(url('broadcaster/app/'.$apps[0]->app_id));
        // }
// return \Auth::user();
        return $this->view('broadcaster.dashboard', compact('apps', 'id'));
    }

    public function register()
    {
        $country = $this->countryService->getLists('country_name', 'country_code')->all();

        return view('broadcaster.new')->with(compact(['country']));
    }

    public function store(Request $request)
    {

        $user = $this->userService->create($request);

        return redirect('broadcaster');
    }

    public function cart()
    {
        $user = \Auth::user();

        $payment_details = $user->paymentRegisterDetail;

        return view('broadcaster.cart', compact('payment_details'));
    }

    public function postCart(SimpleRequest $request)
    {

        (new user)->find(\Auth::user()->user_id)->paymentRegisterStep()->update(['name' => 'gateway', 'description' => 'completing..']);
        return redirect('broadcaster/gateway');
    }


    public function gateway()
    {
        $user = \Auth::user();
        $payment_details = $user->paymentRegisterDetail;
        return view('broadcaster.payment', compact('payment_details'));
    }


    public function pendingForApproved()
    {
        $user = \Auth::user();

        if ($user->broadcaster->approved) {
            return redirect("broadcaster");
        }
        $payment_details = $user->paymentRegisterDetail;

        return view('broadcaster.pending-approved', compact('payment_details'));
    }

    public function postGateway(SimpleRequest $request)
    {

        $this->validate($request, [
            'gateway' => 'required',
            'bank_credential' => 'mimes:jpeg,bmp,png,gif,jpg'
        ]);
        $data = [];

        $user = \Auth::user();
        (new user)->find($user->user_id)->paymentRegisterStep()->update(['name' => 'pending-approved', 'description' => 'completing..']);
        $payment_details = $user->paymentRegisterDetail();
        if ($request->input('gateway') == 'bank') {

            $data = ['payment_gateway' => 'bank',
                'payment_meta_data' =>
                    json_encode(['information_submit_date' => date('Y-m-d H:i:s'),
                        'bank_name' => $request->get('bank_name')])];
            if ($request->hasFile('bank_credential')) {

                $filename = \Auth::Id() . "-" . date('Y-m-d') . "." . $request->file('bank_credential')->getClientOriginalExtension();
                $request->file('bank_credential')->move("uploads/payments/", $filename);
                $data['paid_information'] = $filename;
            }

        } elseif ($request->input('gateway') == 'stripe') {

            $this->stripePayment(\Auth::user(), $request);
            $data = ['payment_gateway' => 'stripe',
                'payment_status' => 1,
                'payment_meta_data' =>
                    json_encode(['information_submit_date' => date('Y-m-d H:i:s'),
                        'amount' => $request->get('amount')])];

        } else {
            $data = $request->all();


            $status = ($request->get('st') == 'Completed') ? 1 : 0;
            (new user)->find($user->user_id)->broadcaster()->update(['approved' => 1]);
            $data = ['payment_gateway' => 'paypal',
                'payment_status' => $status,
                'payment_meta_data' =>
                    json_encode(['information_submit_date' => date('Y-m-d H:i:s'),
                        'txn_id' => $request->get('tx')])];
        }

        $payment_details->update($data);
        return redirect('broadcaster/pending-approved');

    }


    public function stripePayment($user, $request)
    {
        $user->charge($request->get('amount'), [
            'source' => $request->get('stripeToken'),
            'receipt_email' => $user->email,
        ]);
        (new user)->find($user->user_id)->broadcaster()->update(['approved' => 1]);
    }

    public function chanageImage(SimpleRequest $request)
    {
        if ($request->hasFile('profile_image')) {
            $img = $this->service->save($request);
            return asset($img);
        }
    }

    public function profile()
    {

        $broadcaster = $this->service->getBroadcaster();

        $id = $broadcaster->broadcaster_id;
        $broadcasterDetails = $this->service->getBroadcasterDetails($broadcaster->broadcaster_id);
        $companyInfo = $this->service->getCompanyInfo($id);
        $userId = $broadcaster->user_id;

        $apps = $this->appService->getAppByUserId($userId);

        //dd($broadcasterDetails[0]->BroadcasterDetails->first_name);

        return $this->view("broadcaster.profile", compact('broadcaster', 'broadcasterDetails', 'id', 'companyInfo', 'apps'));
        //return view('broadcaster.profile')->with(compact(['broadcaster','broadcasterDetails','id']));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        //$currentPassword = $request->get('oldPassword');
        //dd('here');
        $newPassword = $this->userService->updatePassword($request);

        if ($newPassword) {
            return redirect('broadcaster/profile')->with("success", "password has been updated!");
        } else {
            return redirect('broadcaster/profile')->with("failure", "current password is not matched!");
        }

    }

    public function approveBroadcaster($broadcasterId)
    {
        $userId = $this->service->getUserId($broadcasterId);
        //dd($userId);
        $this->paymentRegisterDetailService->paymentStatus($userId);
        $this->service->approve($broadcasterId);
        \Event::fire(new \App\Events\WhileBroadcasterRegister(\Broadcasters\User\Models\User::find($userId)));

        return back();
    }


    public function paymentRegister($broadcasterId)
    {
        $userId = $this->service->getUserId($broadcasterId);
        $registerStep = $this->userService->getRegisterStepByUserId($userId);

        //dd($userId);
        $payment = $this->paymentRegisterDetailService->getById($userId);


        return view('broadcaster.paymentregister', compact('payment', 'registerStep', 'broadcasterId'));
    }
}