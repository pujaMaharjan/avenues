<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request;
use Broadcasters\Vod\Requests\VodRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\Vod\Models\AppVod as Vod;
use App\App;
use Session;
class VodController extends Controller
{
    protected $app;
    protected $vod;
    public function __construct(App $app, Vod $vod){
        $this->app = $app;
        $this->vod = $vod;
        $this->middleware('auth',['only' => ['index']]);
        $this->middleware('service',['only' => ['index','create']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $vods = $this->app->find($id)->appVod;
        
        return view("vods.index",compact('vods','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('vods.new',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VodRequest $request,$id)
    {
        $this->app->find($id)->appVod()->create($request->all());
        Session::flash('message', 'Vod has been added successfully');
        return redirect("broadcaster/app/$id/services/vod");
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$vod_id)
    {
        $vod = $this->vod->find($vod_id);
        return view('vods.edit',compact('id','vod_id','vod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$vod_id)
    {
        $news = $this->vod->find($vod_id);
        $news->update($request->all());
        Session::flash('message','Vod has been updated successfully');
        return redirect("broadcaster/app/$id/services/vod");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id,$app_vod_id)
    {
        $this->vod->find($app_vod_id)->delete();
        Session::flash('message', 'Vod has been deleted successfully');
        return redirect(url('broadcaster/app/'.$id.'/services/vod'));

    }

    public function show($id,$app_vod_id)
    {
        $vod = $this->vod->find($app_vod_id);
        return view('vods.show',compact('vod'));
    }
}
