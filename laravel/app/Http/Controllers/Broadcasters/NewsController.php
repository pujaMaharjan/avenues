<?php

namespace App\Http\Controllers\Broadcasters;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\App;
use Broadcasters\Models\AppNews as News;
use Session;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $app = App::find($id);
        $news =  $app->appNews;
        return view("news.index",compact('news','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('news.new',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $app = App::find($id);
        $input =$request->all();

        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileName = rand(1000,9999).".".$extension;

            $request->file('logo')->move('uploads',$fileName);

            $input = array_add($input,'logo',$fileName);
        }

        $app->appNews()->create($input);
        \Session::flash('message', 'Channel has been added successfully');
        return redirect("broadcaster/app/$id/services/news");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$news_id)
    {
        $news = News::find($news_id);
        return view('news.edit',compact('id','news_id','news'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$news_id)
    {

        $news = News::find($news_id);
        $input = $request->all();
         if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileName = rand(10000,99990).".".$extension;
            // dd($fileName);

            $request->file('logo')->move('uploads',$fileName);

            $input['logo'] = $fileName;
        }
        $news->update($input);
        Session::flash('message','Message has been updated successfully');
        return redirect("broadcaster/app/$id/services/news");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id,$news_id)
    {
        News::find($news_id)->delete();
        \Session::flash('message', 'Channel has been deleted successfully');
        return redirect(url('broadcaster/app/'.$id.'/services/news'));

    }
}
