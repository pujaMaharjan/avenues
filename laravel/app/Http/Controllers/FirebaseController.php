<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\FirebaseToken;
use App\Models\ApiKey;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class FirebaseController extends Controller
{
    public function generate(Request $request)
    {

    	if($request->has('app_id')){
    		$row =FirebaseToken::where(['app_id'=>$request->get('app_id'),'token'=>$request->get('token')])->first();
	    	if($row){
	    		return ['success'=>1,'message'=>"Already Exists"];
	    	}

    	}
    	
    	$f = FirebaseToken::create($request->all());

    	return ['success'=>1,'message'=>'token generate successfully'];
    }

    public function pushNotification(\Illuminate\Http\Request $request){
	//return $request->all();	
 	$getApiKey = $request->header('x-broadcaster-api-key');
	$http_host = $request->ip();	
	$apiKey = ApiKey::where('host_name',$http_host)->first();
		if(is_null($apiKey)){
			return "undefined host";
		}
		$key = $apiKey->api_key;
		if($getApiKey != $key){
			return 'unauthorized key';
		} 
		$id = $request->get('id');
		 $title = $request->get('title');
		

		$fields = ["priority" => "high",'content_available'=>true,'to_website'=>'nrn','news_url'=>'http://nepaljapan.com/api/public/v1/newshtml'.'/'.$id,'title'=>$title];
		return $this->notifyFirebase($fields);
	 
	}


	function notifyFirebase($fields){
		$registration_ids =[];
		$results = FirebaseToken::where('app_id',0)->select('token')->get();
			foreach($results as $res){
				$registration_ids[] = $res->token;
			}
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => $registration_ids,
			'data' => $fields,
			'priority'=>'high',
			// 'notification'=>['title'=>$fields['title'],'text'=>$fields['title']]
			);
		$headers = array(
			'Authorization:key = AIzaSyCR9V2CBvH7TXfZ5KSmSnlxxVkeLc7xyC4',
			'Content-Type: application/json'
			);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);           
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;
	}


	public function renotifyFirebase(){

		$fields = ['title'=>'high','text'=>'rrr'];
		// print_r(json_encode($fields));
		// die;
		$registration_ids =[];
		$results = FirebaseToken::where('app_id',$app_id)->select('token')->get();
			foreach($results as $res){
				$registration_ids[] = $res->token;
			}
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids'=>$registration_ids,
			// 'notification'=>['title'=>'कलाकार आरपी भट्टराई ‘लाजालाम’को निधन','text'=>'कलाकार आरपी भट्टराई ‘लाजालाम’को निधन'],
			'data' => ['to_website'=>'nepaljapan','news_url'=>'http://nepaljapan.com/api/public/v1/newshtml'.'/'.'305789','title'=>'कलाकार आरपी भट्टराई ‘लाजालाम’को निधन']
			);
		$headers = array(
			'Authorization:key = AIzaSyCR9V2CBvH7TXfZ5KSmSnlxxVkeLc7xyC4',
			'Content-Type: application/json'
			);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);           
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;
	}

	public function handleException(){
		$fields = ['title'=>'high','text'=>'rrr'];
		// print_r(json_encode($fields));
		// die;
		$registration_ids =[];
		$results = FirebaseToken::where('app_id',2)->select('token')->get();
			foreach($results as $res){
				$registration_ids[] = $res->token;
			}
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids'=>$registration_ids,
			// 'notification'=>['title'=>'कलाकार आरपी भट्टराई ‘लाजालाम’को निधन','text'=>'कलाकार आरपी भट्टराई ‘लाजालाम’को निधन'],
			'data' => ['message'=>'channel alert','redirect'=>'message','app_id'=>2]
			);
		$headers = array(
			'Authorization:key = AIzaSyCR9V2CBvH7TXfZ5KSmSnlxxVkeLc7xyC4',
			'Content-Type: application/json'
			);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);           
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		return $result;

	}

	
}
