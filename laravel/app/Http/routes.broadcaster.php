<?php
use Ps\PdfBundle\Annotation\Pdf;

Route::group(['prefix' => 'broadcaster', 'namespace' => 'Broadcasters'], function () {
    Route::post('serverkey', 'AppController@serverKey');
    Route::get('/', ['uses' => 'BroadcasterController@index', 'as' => 'broadcasterHome']);
    Route::resource('app/{app_id}/services/audios', 'AudioController');
    Route::resource('app/{app_id}/menus', 'MenuController');
    Route::resource('app/{app_id}/services/news', 'NewsController');
    Route::get('app/{app_id}/services/news/{news_id}/delete', 'NewsController@delete');
    Route::resource('app/{app_id}/services/vod', 'VodController');
    Route::get('app/{app_id}/services/vod/delete/{app_vod_id}', ['uses' => 'VodController@delete']);
    Route::resource('app/{app_id}/services/radios', 'RadioController');
    Route::get('app/{app_id}/services/radios/delete/{id}', ['uses' => 'RadioController@delete']);
    Route::resource('app/{app_id}/services/channels', 'ChannelController');
    Route::get('app/{app_id}/services/channels/delete/{channel_id}', ['uses' => 'ChannelController@delete']);
    Route::get('app/{app_id}/services/channel/{channel_id}/epg', 'EpgController@index');
    Route::post('app/{app_id}/services/channel/{channel_id}/epg/store', 'EpgController@store');
    // Route::get('broadcaster/app/{app_id}/services/channel/{channel_id}/epg/store','EpgController@store');
    Route::get('pending-approved', ['uses' => 'BroadcasterController@pendingForApproved']);
    Route::resource('app/{app_id}/services/news/{news_id}/newsapi', '\Broadcasters\NewsApi\Controllers\NewsApiController');

    Route::get('{app_id}/paymentdetails',
        ['uses' => '\Broadcasters\PaymentDetails\Controllers\PaymentDetailsController@index',
            'as' => 'paymentIndex']);
    Route::get('{app_id}/status',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@updateStatus',
            'as' => 'appStatus']);

    Route::get('{app_id}/renew',
        ['uses' => 'AppController@renewApp',
            'as' => 'appRenew']);

    Route::get('{app_id}/status/notes',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@appNotes',
            'as' => 'statusNotes']);
    Route::post('{app_id}/status/notes',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@appNotePost',
            'as' => 'appNotePost']);
    Route::get('{app_id}/notes/view',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@viewNotes',
            'as' => 'viewNotes']);
    Route::get('{app_id}/notes/pdf',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@pdfNotes',
            'as' => 'pdfNotes']);
    Route::get('{app_id}/notes/pdf-post',
        ['uses' => '\Broadcasters\Admin\Controllers\AdminBroadcasterController@pdfPost',
            'as' => 'pdfPost']);


    /*Route::post('profile/store/company',
        ['uses'=>'\Broadcasters\Broadcaster\Controllers\BroadcasterDetailsController@storeCompanyInfo',
        'as'=>'storeCompanyInfo']);
*/
    Route::get('create', ['uses' => 'BroadcasterController@register', 'as' => 'broadcaster.create']);
    Route::post('store', ['uses' => 'BroadcasterController@store', 'as' => 'broadcaster.store']);
    //app group
    Route::group(['prefix' => 'app'], function () {
        Route::get('{app_id}/appconfig/key',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@create',
                'as' => 'appConfigCreate']);
        Route::post('{app_id}/appconfig/key/store',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@store',
                'as' => 'appConfigStore']);
        Route::get('{app_id}/appconfig/notificationchannels',
            ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@index',
                'as' => 'parseChannels']);
        Route::post('{app_id}/appconfig/notificationchannels/subcribe',
            ['uses' => '\Broadcasters\ParseChannels\Controllers\ParseChannelsController@subcribe',
                'as' => 'parseChannelsSubcribe']);
        Route::get('{app_id}/push/general',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@generalPushView',
                'as' => 'generalPushView']);
        Route::post('{app_id}/push/general',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@generalPush',
                'as' => 'generalPush']);
        Route::get('{app_id}/appconfig/epgsetting',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@getEpgView',
                'as' => 'epgNotification']);
        Route::post('{app_id}/appconfig/epgsetting',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@epgNotification',
                'as' => 'epgNotification']);
        Route::get('{app_id}/push/epgs',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@pushEpgNotification',
                'as' => 'pushEpgNotification']);

        Route::get('{app_id}/epgs/post-push',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@postUushNotification',
                'as' => 'post.pushEpgNotification']);
        Route::post('{app_id}/epgs/updateStatus',
            ['uses' => '\Broadcasters\AppConfig\Controllers\AppConfigController@updateNotificationStatus',
                'as' => 'updateNotificationStatus']);

    });

    Route::get('cart', ['uses' => 'BroadcasterController@cart']);
    Route::post('postcart', ['uses' => 'BroadcasterController@postCart']);

    Route::get('{broadcaster_id}/payment-register', ['uses' => 'BroadcasterController@paymentRegister']);

    Route::get('gateway', ['uses' => 'BroadcasterController@gateway']);
    Route::post('gateway', ['uses' => 'BroadcasterController@postGateway']);
    Route::any('paypal', ['uses' => 'BroadcasterController@postGateway']);


    Route::get('profile', ['uses' => 'BroadcasterController@profile', 'as' => 'broadcasterProfile']);
    Route::get('profile/store', ['uses' => 'BroadcasterDetailsController@store', 'as' => 'profileStore']);
    Route::post('profile/chanageimage', ['uses' => 'BroadcasterController@chanageImage', 'as' => 'chanageImage']);


    Route::get('app/{app_id}/services/movies', ['uses' => '\Broadcasters\Movie\MovieController@index']);
    Route::get('app/{app_id}/services/movies/edit/{id}', ['uses' => '\Broadcasters\Movie\MovieController@edit']);
    Route::get('app/{app_id}/services/movies/create', ['uses' => '\Broadcasters\Movie\MovieController@create']);
    Route::post('app/{app_id}/services/movies/update/{id}', ['uses' => '\Broadcasters\Movie\MovieController@update', 'as' => 'broadcaster.app.movie.update']);
    Route::post('app/{app_id}/services/movies/store', ['uses' => '\Broadcasters\Movie\MovieController@store', 'as' => 'broadcaster.app.movie.store']);
    Route::get('app/{app_id}/services/movies/delete/{id}', ['uses' => '\Broadcasters\Movie\MovieController@delete']);
    Route::get('app/{app_id}/services/movies/show/{app_movie_id}', ['uses' => '\Broadcasters\Movie\MovieController@show']);
    Route::get('app/{app_id}/services/Dvr', 'DvrController@index');
    Route::get('app/{app_id}/services/dvr/create', 'DvrController@create');
    Route::post('app/{app_id}/services/dvr', 'DvrController@store');
    Route::get('app/{app_id}/services/dvr/{id}/edit', 'DvrController@edit');
    Route::post('app/{app_id}/services/dvr/{id}', 'DvrController@update');
    Route::get('app/{app_id}/services/dvr/{id}', 'DvrController@delete');


    Route::get('app/{app_id}/change-logo', 'AppController@changeLogo');
    Route::post('app/{app_id}/change-logo', 'AppController@postChangeLogo');

    Route::get('app/{app_id}/splashimage', 'AppController@splashImage');
    Route::post('app/{app_id}/splashimage', 'AppController@postsplashImage');
    Route::get('app/{app_id}/notification', 'AppController@sendMessage');
    Route::post('app/{app_id}/send-notification', 'AppController@postEendMessage');

    Route::get('/mail', function () {
        $data = ['user' => \Auth::user()];
        Mail::send('emails.broadcaster_create', $data, function ($message) {
            $message->from('tech@castnow.tv', 'Cast Now');

            $message->to(\Auth::user()->email);
        });
    });
});

Route::get('api/{app_id}/dvrurl/{id}', ['uses' => 'Broadcasters\DvrController@getStreamUrl', 'as' => 'getStreamUrl']);
Route::get('api/getTimestamp', ['uses' => 'Broadcasters\DvrController@getTimestamp', 'as' => 'getTimestamp']);
Route::get('api/redirectTimestamp', ['uses' => 'Broadcasters\DvrController@redirectTimestamp', 'as' => 'redirectTimestamp']);
Route::get('test', ['uses' => 'Broadcasters\DvrController@test', 'as' => 'test']);