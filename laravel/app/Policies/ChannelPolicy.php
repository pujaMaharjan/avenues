<?php

namespace App\Policies;

class ChannelPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

  
    public function update($user,$app){
       return $user->user_id === $app->user_id || $user->type=='admin';
    }
}
