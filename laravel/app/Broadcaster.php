<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadcaster extends Model
{
	protected $primaryKey = 'broadcaster_id';
	protected $fillable = ['company_name','display_name','user_id','country_id','is_oem','is_worldtv'];
}
