<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Dump extends Model{

	protected $table = 'dumps_data';

	protected $fillable = ['data'];

	protected $casts = [
		'data'=>'json'
	];
}