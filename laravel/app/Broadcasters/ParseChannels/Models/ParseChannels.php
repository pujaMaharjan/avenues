<?php

namespace Broadcasters\ParseChannels\Models;

use Illuminate\Database\Eloquent\Model;

class ParseChannels extends Model
{
	protected $primaryKey = 'notification_channels_id';
	protected $fillable = ['channels','status'];
	protected $table = 'notification_channels';

    public function app()
    {
        return $this->belongsToMany('Broadcasters\App\Models\App','app_notification_channels','app_id','notification_channels_id'); 
    }

}