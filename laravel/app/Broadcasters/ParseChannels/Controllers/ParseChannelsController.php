<?php

namespace Broadcasters\ParseChannels\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request as Simple;
use App\Http\Requests\NotificationChannelRequest as Request;

use Broadcasters\BaseController;

use Broadcasters\ParseChannels\Providers\ParseChannelsServiceProvider as ParseChannelsSetService;

use Broadcasters\AppNotificationChannels\Providers\AppNotificationChannelsServiceProvider as AppNotificationChannelsService;

use Broadcasters\App\Providers\AppServiceProvider as AppService;
class ParseChannelsController extends BaseController
{
    protected $service;
    protected $appNotificationchannelsService;
    protected $appService;
    function __construct(ParseChannelsSetService $service,AppNotificationChannelsService $appNotificationchannelsService,AppService $appService) {
        $this->service = $service;
        $this->appNotificationchannelsService = $appNotificationchannelsService;
        $this->appService = $appService;
    }

    public function create()
    {
        $parseChannels = $this->service->getPraseChannels();
       // dd($parseChannels);
        return view('apps.appConfig.parseChannels')->with(compact(['parseChannels']));

    }
    public function store(Request $request)
    {

        $this->service->create($request);
        return back();
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return back();
    }
    public function index($appId)
    {


        $parseChannels = $this->service->getPraseChannels();

        $subscribeChannels = $this->appNotificationchannelsService->getSubscribeChannels($appId)->toArray();

        //$subscribeChannels = \Broadcasters\App\Models\App::find($appId)->notificationChannel()->get();

      //  return \Broadcasters\ParseChannels\Models\ParseChannels::find(1)->app()->get();



        // dd($subscribeChannels);
        return view('apps.appConfig.appParseChannels')->with(compact(['parseChannels','appId','subscribeChannels']));

    }

    public function subcribe(simple $request,$appId)
    {
        // $this->appNotificationchannelsService->create($request,$appId);

       $result = $this->appService->sync($request,$appId);

       return back();
        //dd($request->all());
   }

   public function status($id)
   {
    $this->service->updateStatus($id);
    return back();
}

}
