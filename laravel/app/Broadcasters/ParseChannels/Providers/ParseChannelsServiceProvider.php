<?php namespace Broadcasters\ParseChannels\Providers;

use Broadcasters\ParseChannels\Models\ParseChannels as Model;




class ParseChannelsServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	public function create($request)
	{
		try {
			//dd($request->all());
			for($i=0;$i<count($request->get('parseChannels')); $i++)
			{
				$input = [
				'channels' => $request->get('parseChannels')[$i],
				'status' => $request->get('statusParseChannels')[$i]
				];
				$this->model->create($input);
			}
			
		} catch (Exception $e) {
			
		}
		
	}
	public function getPraseChannels()
	{
		try {

			return $this->model->orderBy('notification_channels_id', 'desc')->get();
		} catch (Exception $e) {
			
		}
	}
	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			
		}
	}
	public function updateStatus($id)
	{
		try {
			$status = $this->model->find($id)->status;
			return $this->model->find($id)->update(['status'=>!$status]);
		} catch (Exception $e) {
			
		}
	}
	
	// public function unSubscribeChannels()
	// {
	// 	return 	\DB::select("SELECT * FROM `nitv_notification_channels` where `nitv_notification_channels`.`notification_channels_id` NOT IN ( SELECT `nitv_app_notification_channels`.`notification_channels_id` FROM `nitv_app_notification_channels` )");
	// }

}
