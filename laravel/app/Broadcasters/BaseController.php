<?php
namespace Broadcasters;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//use Illuminate\Foundation\Http\FormRequest as Request;


class BaseController extends Controller
{
	protected $common;
	protected $_data;
	function __construct() {
		//parent::__construct();
		$user = null;
		$broadcaster = null;
		if(\Auth::check()){

			$user = \Auth::user();
			if($user->type=="broadcaster"){
				$broadcaster = $user->broadcaster;
				//dd($user->broadcaster_id);
			}
		}

		$this->_data = ['authUser'=>$user,'authBroadcaster'=>$broadcaster];
	}

	/**
	 * overwrite parent view function
	 */
	public function view($path, $data)
	{
		//dd($data);
		if(isset($data['adminAsBroadcaster'])){

			if($data['adminAsBroadcaster'] != '')
			{
				$this->_data['authBroadcaster'] = $data['adminAsBroadcaster'];
			}
		}
		
		$data = array_merge($data,$this->_data);
		return view($path, $data);
	}
}