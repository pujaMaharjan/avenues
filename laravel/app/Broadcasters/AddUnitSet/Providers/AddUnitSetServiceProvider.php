<?php namespace Broadcasters\AddUnitSet\Providers;

use Broadcasters\AddUnitSet\Models\AddUnitSet as Model;
use Broadcasters\App\Models\App;

use Broadcasters\AdManage\NitvAdKey;

class AddUnitSetServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model,App $app)
	{
		$this->model = $model;
		$this->app = $app;
	}
	

	/**
	 * 
	 */
	public function create($request)
	{
		try{
			//dd($request->all());
			return $this->model->create($request->all());
			
		}
		catch(\Exception $e){
			return $e->getMessage();
		}

	}

	/**
	 * 
	 */
	public function getAll()
	{
		try {
			return $this->model->all();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		try {
			return $this->model->with('addUnit')->find($id);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	public function getApp()
	{
		//dd($this->model->with('app')->get());
		return $this->model->with('app')->get();
	}

	public function getAddUnit()
	{
		return $this->model->with('addUnit')->get();
	}

	public function getUnitByApp($apps)
	{
		$a = null;
		if(count($apps)>0){
			try {
				foreach($apps as $app)
				{
					if(count($this->model->where('app_id',$app->app_id)->get())>0)
					{
						$a[] = $this->model->with('addUnit')->where('app_id',$app->app_id)->get();
					}
					
					//dd(count($a));
				}
				//dd($a);
				//dd(count($a));
				return $a;
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		
	}

	public function test()
	{
		return 	\DB::select("SELECT app_id, app_name from nitv_app WHERE (app_id NOT IN (SELECT app_id FROM nitv_add_unit_set))");
	}


	/**
	 * method for api 
	 */
	public function getUnitByAppId($appId,$platformId)
	{
		$units = [];
		try {
			$adUnits = $this->model->with('addUnit')->where('app_id',$appId)->first();
			// dd($adUnits);
			foreach ($adUnits->addUnit as $key => $value) {
				//dd($value);

				if($platformId == $value->platform_id)
				{
					//echo $value->platform_id;
					$units [] = $value;
					//echo $units;
				}
				
			}
			return $units;

		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getNitvAdd($app_id,$platformId)
	{
		return $this->app->find($app_id)->nitvAddKeys()->first();
	}

	public function getAdSetting($app_id,$platformId)
	{
		$countries = ['nepal','japan','chaina'];
		$ip = "202.166.205.145";
		$data = simplexml_load_file('http://www.geoplugin.net/xml.gp?ip=202.166.205.145');
		//$informations = new \SimpleXMLElement($data);
		// dd($data->geoplugin_countryName);
		$country =  $data->geoplugin_countryName;
		$cid = array_search($country,$countries);
		$data = [];
		$adset = $this->app->find($app_id) ? $this->app->find($app_id)->adSetings()->where('country_id',$cid)->count() : '';
		$result = $this->app->find($app_id) ? $this->app->find($app_id)->adSetings()->where('country_id',$cid)->first() : '';
		
		if($adset < 1){
			return response()
            	->json(['status' => 0, 'message' => 'No Ad Setting']);
		}
		$resu['nitv'] = $this->getNitvAdd($result->app_id,$platformId);
		

		if(!is_null($resu['nitv'])){
			$data['nitv'] = $this->getNitvAdd($result->app_id,$platformId);
			// dd($data['nitv']);
			$res['nitv']['priority'] = 7;
			$res['nitv']['android_banner'] = $data['nitv']->android_banneradunitid;
			$res['nitv']['android_banner_status'] = $data['nitv']->android_banneradunitid_status;
			$res['nitv']['android_interestial'] = $data['nitv']->android_interestialadunitid;
			$res['nitv']['android_interestial_status'] = $data['nitv']->android_interestialadunitid_status;
			$res['nitv']['ios_banner'] = $data['nitv']->ios_banneradunitid;
			$res['nitv']['ios_banner_status'] = $data['nitv']->ios_banneradunitid_status;
			$res['nitv']['ios_interestial'] = $data['nitv']->ios_interestialadunitid;
			$res['nitv']['ios_interestial_status'] = $data['nitv']->ios_interestialadunitid_status;
		}

		$data = $this->getUnitByAppId($result->app_id,$platformId);
		$res['admob']['priority'] = 9;
		foreach ($data as $key => $value) {
			// dd($value);
			$res['admob'][$value->type] =[
			'name'=>$value->unit_name,
			'status'=>$value->status,
			];
		}
		// $data['admob']['value'] = $result->admob;


		
		
		
		return $res;
	}

	
}
