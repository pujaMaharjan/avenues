<?php

namespace Broadcasters\AddUnitSet\Models;

use Illuminate\Database\Eloquent\Model;

class AddUnitSet extends Model
{
	protected $primaryKey = 'add_unit_set_id';
	protected $fillable = ['name','app_id'];

	protected $table = 'add_unit_set';
    /**
     * Get the user relationship
     */
    public function app()
    {
        return $this->belongsTo('Broadcasters\App\Models\app','app_id'); 
    }

    public function addUnit()
    {
        return $this->hasMany('Broadcasters\AddUnit\Models\AddUnit','add_unit_set_id'); 
    }
}