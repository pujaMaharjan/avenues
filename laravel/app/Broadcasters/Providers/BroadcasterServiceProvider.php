<?php namespace Broadcasters\Providers;

use Broadcasters\Models\Broadcaster as Model;

class RouteServiceProvider{


	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Create application route
	 */
	public function create($request)
	{
		try{
			if($request->input('route_group_id')==""){
				$request->merge(['route_group_id'=>null]);
			}
			$this->model->create($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}
	/**
	 * Update application route
	 */
	public function update($request,$id)
	{
		try{
			if($request->input('route_group_id')==""){
				$request->merge(['route_group_id'=>null]);
			}
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}

	/**
	 * Get all routes
	 */
	public function getWithGroup()
	{
		return $this->model->with('group')->get();
	}

	/**
	 * Get all routes
	 */
	public function all()
	{
		return $this->model->all();
	}
	/**
	 * Get route based on id
	 */
	public function getById($id)
	{
		return $this->model->with('group')->find($id);
	}
}