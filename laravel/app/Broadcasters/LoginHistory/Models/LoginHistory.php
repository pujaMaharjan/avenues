<?php

namespace Broadcasters\LoginHistory\Models;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
	protected $table = 'login_history';
	protected $primaryKey = 'login_history_id';
	protected $fillable = ['user_id','login_time','logout_time','device_ip','user_agent'];
	public $timestamps = false;

}