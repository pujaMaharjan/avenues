<?php

namespace Broadcasters\Broadcaster\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Http\Request as SimpleRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BroadcasterDetailsRequest as Request;
use App\Http\Requests\CompanyInfoRequest as CompanyInfoRequest;

use Broadcasters\Broadcaster\Models\BroadcasterDetails as BroadcasterDetails;
use Broadcasters\Broadcaster\Models\BroadcasterDetails as model;
use Broadcasters\Broadcaster\Providers\BroadcasterDetailsServiceProvider as BroadcasterDetailsService;

use Broadcasters\Broadcaster\Providers\CompanyInfoServiceProvider as CompanyInfoService;

class BroadcasterDetailsController extends Controller
{
	protected $service,$companyInfoService;
	function __construct(BroadcasterDetailsService $service, CompanyInfoService $companyInfoService) {
		$this->service = $service;
		$this->companyInfoService = $companyInfoService;
	}
	// public function profile() 
	// { 
	// 	return $this->service->
	// 	$id=15;
	// 	$broadcaster = $this->service->broadcaster($id);
	// 	//dd($broadcaster);
	// 	return view('broadcaster.profile')->with(['broadcaster' => $broadcaster]);
	// }

	public function store(Request $request)
	{
		$id = $request->get('Broadcaster_Details_id');
		if($id) 
		{
			$this->service->update($request,$id);
		}
		else
		{

			$this->service->create($request);
		}
		return redirect('broadcaster/profile')->with("success","Details has been updated!");
	} 

	public function storeCompanyInfo(CompanyInfoRequest $request)
	{
		//dd($request);
		$this->companyInfoService->create($request);
		return redirect('broadcaster/profile')->with("success","Details has been updated!");
	}

}