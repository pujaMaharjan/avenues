<?php

namespace Broadcasters\Broadcaster\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
	protected $primaryKey = 'company_info_id';
	protected $fillable = ['company_name','company_address','support_email','register_name','website','broadcaster_id'];

	protected $table = 'company_info';
    /**
     * 
     */
    public function broadcaster()
    {
        return $this->belongsTo('Broadcasters\Models\Broadcaster','broadcaster_id');
    }
}