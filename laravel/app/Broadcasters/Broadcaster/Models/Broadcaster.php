<?php

namespace Broadcasters\Broadcaster\Models;

use Illuminate\Database\Eloquent\Model;

class Broadcaster extends Model
{
	protected $primaryKey = 'broadcaster_id';
	protected $fillable = ['company_name','display_name','user_id','logo','country_code','approved'];

	protected $table = 'broadcasters';
    /**
     * Get the user relationship
     */
    public function user()
    {
    	return $this->hasMany('Broadcasters\Models\Broadcaster','user_id');
    }

    public function broadcasterDetails()
    {
        return $this->hasOne('Broadcasters\Broadcaster\Models\BroadcasterDetails','broadcaster_id');
    }

    public function companyInfo()
    {
        return $this->hasOne('Broadcasters\Broadcaster\Models\CompanyInfo','broadcaster_id');
    }
}