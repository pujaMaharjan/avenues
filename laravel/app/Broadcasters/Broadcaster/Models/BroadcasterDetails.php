<?php

namespace Broadcasters\Broadcaster\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcasterDetails extends Model
{
	protected $primaryKey = 'broadcaster_details_id';
	protected $fillable = ['first_name','last_name','mobile','address','broadcaster_id'];

	protected $table = 'broadcaster_details';
    /**
     * 
     */
    public function broadcaster()
    {
        return $this->belongsTo('Broadcasters\Models\Broadcaster','broadcaster_id');
    }
}