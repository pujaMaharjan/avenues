<?php
namespace Broadcasters\Broadcaster\Providers;

use Broadcasters\Broadcaster\Models\CompanyInfo as Model;
use Broadcasters\Models\User;
use Illuminate\Contracts\Auth\Guard as Auth;
use Broadcasters\Broadcaster\Models\Broadcaster as Broadcaster;



class CompanyInfoServiceProvider{

	protected $model,$auth,$broadcaster,$user;
	/**
	 * constructer
	 */
	public function __construct(Model $model,Auth $auth,Broadcaster $broadcaster,User $user)
	{
		$this->model = $model;
		$this->auth = $auth;
		$this->broadcaster = $broadcaster;
		$this->user = $user;
	}
	

	/**
	 * 
	 */
	public function create($request)
	{
		// dd($request->all());
		try{

			$broadcasterId = $this->user->find($this->auth->user()->user_id)->broadcaster->broadcaster_id;
			$value = $this->broadcaster->find($broadcasterId)->companyInfo;

			if($value)
			{
				
				$companyInfoId = $this->broadcaster->find($broadcasterId)->companyInfo->company_info_id;

				$this->model->find($companyInfoId)->update($request->all());
			}
			
			$this->broadcaster->find($broadcasterId)->companyInfo()->create($request->all());

		}
		catch(\Exception $e){
			dd($e->getMessage());
		}
	}

	/**
	 * 
	 */
	public function getAll()
	{
		try {
			return $this->model->all();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		try {
			return $this->model->find($id);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}
	

}
