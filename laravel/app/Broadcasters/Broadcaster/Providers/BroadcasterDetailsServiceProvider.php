<?php
namespace Broadcasters\Broadcaster\Providers;

use Broadcasters\Broadcaster\Models\BroadcasterDetails as Model;


class BroadcasterDetailsServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	

	/**
	 * 
	 */
	public function create($request)
	{
		try{
			$this->model->create($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * 
	 */
	public function getAll()
	{
		try {
			return $this->model->all();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		try {
			return $this->model->find($id);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	public function count($value='')
	{
		try {
			return $this->model->count();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function broadcaster($id)
	{
		return $this->model->with('broadcaster')->find($id);
		
	}

	

}
