<?php namespace Broadcasters\Broadcaster\Providers;

use Broadcasters\Broadcaster\Models\Broadcaster as Model;
use Illuminate\Contracts\Auth\Guard as Auth;
use App\User;
use App\PaymentRegisterDetail;

class BroadcasterServiceProvider{

	protected $model;
	protected $auth;
	protected $PaymentRegisterDetail;
	/**
	 * constructer
	 */
	public function __construct(Model $model,Auth $auth,PaymentRegisterDetail $PaymentRegisterDetail)
	{
		$this->model = $model;
		$this->auth = $auth;
		$this->PaymentRegisterDetail = $PaymentRegisterDetail;
	}
	

	/**
	 * 
	 */
	public function create($request,$userId)
	{
		try{
			//dd($request->get('country_id'));
			$this->model->create([
				'company_name' => $request->get('company_name'),
				'display_name' => $request->get('display_name'),
				'phone' => $request->get('phone'),
				'country_code' => $request->get('country_code'),
				'user_id' => $userId,
				]);
			

		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * 
	 */
	public function getAll()
	{
		try {
			return $this->model->all();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		try {
			return $this->model->find($id);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	public function count($value='')
	{
		try {
			return $this->model->count();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function save($request)
	{

		try {
			$id = $request->get('broadcaster_id');
			$file = $request->file('profile_image');
			$destinationPath = 'uploads';
			$fileName = strtolower($file->getClientOriginalExtension());
			$name = date('Y-m-s h-m-s');
			
			$name = 'uploads/'.$name.'.'.$fileName;
			$request->file('profile_image')->move($destinationPath, $name);
           // $this->service->save($request);
			$this->model->find($id)->update(['logo'=>$name]);
			return $name;
			
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getBroadcaster(){
		//\Auth::loginUsingId(1);
		$userId = $this->auth->id();
		
		//return $this->model->where('user_id',$user_id)->select('broadcaster_id')->get();
		return $this->model->where('user_id',$userId)->first();
	}

	public function getBroadcasterDetails($id)
	{
		
		return $this->model->with('broadcasterDetails')->where('broadcaster_id',$id)->get();

	}
	public function getCompanyInfo($id)
	{
		
		return $this->model->with('companyInfo')->where('broadcaster_id',$id)->first();

	}
	/**
 * get the list of brodcaster 
 */
	public function getLists($param1,$param2=null)
	{
		return $this->model->lists($param1, $param2);
	}

	function getUserId($broadcasterId)
	{
		$broadcaster = $this->model->where('broadcaster_id',$broadcasterId)->first();
		return $broadcaster->user_id;

	}


	public function insertIntoPayment($PaymentRegisterDetail){

		$this->PaymentRegisterDetail->create($PaymentRegisterDetail);


	}

	public function getByUser($userId)
	{
		return $this->model->where('user_id',$userId)->first();
	}

	public function approve($broadcasterId)
	{
		$approved = $this->model->find($broadcasterId)->approved;
		// dd($approved);
		return $this->model->find($broadcasterId)->update(['approved'=>!$approved]);

	}

	



}
