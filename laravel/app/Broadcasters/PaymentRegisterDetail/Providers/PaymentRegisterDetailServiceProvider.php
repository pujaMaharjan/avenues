<?php namespace Broadcasters\PaymentRegisterDetail\Providers;

use App\PaymentRegisterDetail as Model;


class PaymentRegisterDetailServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function paymentStatus($userId)
	{
		$paymentStatus = $this->model->where('user_id',$userId)->first();
		// dd($paymentStatus->payment_status);
		return $this->model->where('user_id',$userId)->update(['payment_status'=>!$paymentStatus->payment_status]);

	}

	public function getById($userId)
	{
		return $this->model->where('user_id',$userId)->first();
	}

}
