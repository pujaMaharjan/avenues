<?php

namespace Broadcasters\Epg\Models;

use Illuminate\Database\Eloquent\Model;

class AppChannelEpg extends Model
{
    protected $table = 'app_channel_epg';
	protected $primaryKey = 'app_channel_epg_id';
	protected $fillable = ['app_channel_epg_detail_id','app_channel_id','app_channel_epg_value','app_channel_epg_type'];
   

    protected  $cast = [
        'schedule'=>'array'
    ];
    /**
     * Get the user relationship
     */
    public function appChannel()
    {
    	return $this->belongsTo('Broadcasters\Models\appChannels','app_channel_id');
    }

    public function appChannelEpgDetail()
    {
        return $this->hasMany('App\appChannelEpgDetail');
    }

}