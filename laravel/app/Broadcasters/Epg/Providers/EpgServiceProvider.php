<?php namespace Broadcasters\Epg\Providers;

use Broadcasters\Epg\Models\AppChannelEpg as Model;
use App\Supports\UploadHelper;
use Broadcasters\Channel\Models\AppChannels;
use App\AppChannelEpgDetail;

class EpgServiceProvider{

    use UploadHelper;

	protected $model;
	protected $channel;
	protected $epgDetail;
	/**
	 * constructer
	 */
	public function __construct(Model $model, AppChannels $channel,AppChannelEpgDetail $epgDetail)
	{
		
		$this->model = $model;
		$this->channel = $channel;
		$this->epgDetail = $epgDetail;
	}
	/**
	 * Get Epg details with channel id 
	 */
	public function getByChannel($channelId)
	{
		try {
			return $this->model
			->with('appChannelEpgDetail')
			->where('app_channel_id',$channelId)
			->first();
		} catch (Exception $e) {
			
		}
	}

	public function save($app_id,$channel_id,$type,$request,$file=null){
		$column = $this->channel->find($channel_id)->appChannelEpg;

		
		if($column){
			$this->deleteFile($channel_id);
			$column->delete();
		}

		switch($type){
			case 'file':
				$contents = $this->readCsv($request->file('schedule'));
				$extension = $request->file('schedule')->getClientOriginalExtension();
            	$fileName = rand(1000,9999).".".$extension;

            	$request->file('schedule')->move('epgs',$fileName);
				
				$epgChannel = $this->channel->find($channel_id)
				->appChannelEpg()
				->create([
					'app_channel_epg_type'=>'file',
					'app_channel_epg_value'=>$fileName
					]);
				foreach($contents as $content){
                   $epgChannel->appChannelEpgDetail()->create(['day'=>$content[0],
                   		'program_name'=>$content[1],
                       'start_time'=>$content[2],
                       'end_time'=>$content[3]
                       ]);
            	}

			break;

			case 'url':
				$this->channel->find($channel_id)
				->appChannelEpg()
				->create([
					'app_channel_epg_type'=>'url',
					'app_channel_epg_value'=>$request->input('url')
					]);
			break;
		}
	}

	/*

	*/
	public function deleteFile($channel_id){
		\File::delete('epgs/'.$this->channel->find($channel_id)->appChannelEpg->app_channel_epg_value);

	}

	public function getSelectedEpg($request,$epgsLists)
	{
		$epgDetailIds = $request->get('epgDetailId');
		if($epgDetailIds == '')
		{
			$epgDetailIds = [];
			$selectedEpglists = [];
		}
		//dd($epgDetailIds);
		//dd($epgsLists->app_channel_epg_detail_id);
		try {
			foreach($epgsLists as $i=>$epgList)
			{
				
				if(in_array($epgList->app_channel_epg_detail_id,$epgDetailIds))
				{
					$selectedEpglists[] = $epgList;
				}
			}
			
			return $selectedEpglists;
			
		} catch (Exception $e) {

		}
	}




}
