<?php
namespace Broadcasters\Dashboard\Controllers;
use App\Http\Controllers\Controller;


class DashboardController extends Controller {

	public function __construct(){
		//$this->middleware('service');
	}

	public function dashboard($id){

		$app = \App\App::findOrFail($id);



		if (\Gate::denies('update', $app)) {
			abort(403);
		}
		return view('apps.dashboard',compact('id'));
	}

}