<?php namespace Broadcasters\Country\Providers;

use Broadcasters\Country\Models\Country as Model;


class CountryServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
/**
 * get the list of country 
 */
public function getLists($param1,$param2=null)
{
	return $this->model->lists($param1, $param2);
}


}
