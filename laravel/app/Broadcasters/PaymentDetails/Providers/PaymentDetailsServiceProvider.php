<?php namespace Broadcasters\PaymentDetails\Providers;

use Broadcasters\PaymentDetails\Models\PaymentDetail as Model;


class PaymentDetailsServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	
	public function getPaymentDetailById($appId)
	{
		return $this->model->where('app_id',$appId)->with('app')->get();
	}
public function totalEarning()
	{
		try {
			$earning = $this->model->get();
			$total = $earning->sum('total');
			return $total;

		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	
}
