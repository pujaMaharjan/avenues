<?php

namespace Broadcasters\PaymentDetails\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
	protected $primaryKey = 'payment_detail_id';
	protected $fillable = ['user_id','app_id','total','payment_status','paid_date','payment_gateway','payment_description','device_ip','created_by','updated_by'];

	protected $table = 'payment_details';

	 public function app(){
        return $this->belongsTo(\App\App::class);
    }
}
