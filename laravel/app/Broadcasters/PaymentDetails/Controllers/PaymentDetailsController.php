<?php

namespace Broadcasters\PaymentDetails\Controllers;

use Illuminate\Http\Request as SimpleRequest;
use App\Http\Requests;
use Broadcasters\BaseController;
use Broadcasters\PaymentDetails\Providers\PaymentDetailsServiceProvider as PaymentDetailService;



class PaymentDetailsController extends BaseController
{
	protected $service;

  function __construct(PaymentDetailService $service)
  {
    parent::__construct();
    $this->service = $service;



  }

  public function index($appId)
  {

    $paymentDetails = $this->service->getPaymentDetailById($appId);
    //dd($paymentDetails);
    return $this->view("apps.paymentDetails",compact('paymentDetails'));

  }


}