<?php

namespace Broadcasters\Movie;

use App\Http\Requests\Request;

class MovieRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'movie_name'=>'required'

        ];
    }

    public function messages(){
        return [
            'movie_name.required' => 'We need to know your movie name',
        ];
    }

}
