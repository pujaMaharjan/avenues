<?php

namespace Broadcasters\Movie;

use Illuminate\Http\Request;
use Broadcasters\Movie\AppMovie;
use App\App;
use File;

class MovieServiceProvider {

	protected $app;
	protected $movie;

	public function __construct(AppMovie $movie,App $app){
		$this->movie = $movie;
		$this->app = $app;
		
	}	

	public function find($id){
		return  $this->movie->find($id);
	}

	public function all($app_id){
		if($this->app->find($app_id))
			return $this->app->find($app_id)->movie->all();
		return $this->app->find($app_id);
	}


	public function save($request=[],$app_id,$id=null){	
		if($id){
			$this->app->find($app_id)->movie()->find($id)->update($request);
		}else {
			$this->app->find($app_id)->movie()->create($request);
		}
		

	}

	public function delete($id){

		$this->movie->find($id)->delete();
	}

	public function photo($id){
		return $this->movie->find($id)->movie_photo;
	}

	public function deletePhoto($file){

		File::delete('uploads/movies/'.$file);
	}

	public function getStreamUrl($id,$type)
	{		
		$movie = $this->movie->find($id);

		//dd($channel->validTime);
		//dd($movie);
		if(!$movie){
			return null;
		}
		$type = \Config::get('site.streamAuthType');
		//dd($type);
		//$type = "wms";
		if($type="wms")
		{
			$authSign = getUrlSignature($movie->url_token_key,$movie->valid_time);
			//dd($authSign);
		}
		else
			$authSign = getPpvUrlSignature($movie->url_token_key,$movie->valid_time);
		//return $channel->local_source;
		
		return $movie->movie_local_url.'?wmsAuthSign='.$authSign;
	}
}