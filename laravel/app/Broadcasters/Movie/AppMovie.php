<?php

namespace Broadcasters\Movie;

use Illuminate\Database\Eloquent\Model;

class AppMovie extends Model
{
	protected $primaryKey = 'app_movie_id';

	protected $fillable = ['movie_name','movie_local_url','movie_cdn_url','youtube_id','description','country','language','url_token_key','valid_time','app_id','movie_photo'];

    /**
     * Get the app relationship
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

   


}