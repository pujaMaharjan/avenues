<?php

namespace Broadcasters\Movie;

// use Illuminate\Http\Request;
use Broadcasters\Movie\MovieRequest as Request;
use App\Http\Requests;
use Broadcasters\BaseController;
use Broadcasters\Movie\AppMovie as Movie;
use Broadcasters\Movie\MovieServiceProvider as Provider;
use App\App;
//use Broadcasters\Channel\Requests\ChannelRequest as Request;

class MovieController extends BaseController
{
    protected $provider;
    public function __construct(Provider $provider){
        $this->provider = $provider;
    }

    
    public function index($app_id){
        $movies = $this->provider->all($app_id);

        return view('movies.index',compact('movies','app_id'));
    }

    public function create($app_id){

      $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
      return view('movies.form',compact('app_id','countries'));
  }

  public function store($app_id,Request $request){
    $input = $request->all();
    if($request->hasFile('movie_photo')){
        $input['movie_photo'] =  $request
        ->file('movie_photo')
        ->getClientOriginalName();
        $request->file('movie_photo')->move('uploads/movies',$input['movie_photo']);
    }

    $this->provider->save($input,$app_id);
    return redirect("broadcaster/app/$app_id/services/movies");
}


public function edit($app_id,$id){
    $movie = $this->provider->find($id);

    $countries = \Broadcasters\Models\Country::lists('country_name','country_id');
    return view('movies.form',compact('movie','app_id','countries','id'));
}


public function update($app_id,$id,Request $request){
    $input = $request->all();
    if($request->hasFile('movie_photo')){
        $this->provider->deletePhoto($this->provider->photo($id));
        $input['movie_photo'] =  $request
        ->file('movie_photo')
        ->getClientOriginalName();

        $request->file('movie_photo')->move('uploads/movies',$input['movie_photo']);
    }
    $this->provider->save($input,$app_id,$id);
    return redirect("broadcaster/app/$app_id/services/movies");
}

public function delete($app_id,$id)
{
    $this->provider->delete($id);
    return redirect("broadcaster/app/$app_id/services/movies");

}
public function show($app_id,$id)
{
    $movie =  $this->provider->find($id);
    return view('movies.show',compact('movie'));
    

}

}
