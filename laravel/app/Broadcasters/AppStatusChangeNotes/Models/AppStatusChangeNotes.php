<?php

namespace Broadcasters\AppStatusChangeNotes\Models;

use Illuminate\Database\Eloquent\Model;

class AppStatusChangeNotes extends Model
{
	protected $primaryKey = 'app_status_change_notes_id';
	protected $fillable = ['title_notes','app_status_change_notes','app_id','user_id','device_ip','published_date'];
	protected $table = 'app_status_change_notes';

	public static function boot()
	{
		parent::boot();

		AppStatusChangeNotes::creating(function($appStatusChangeNotes){
			$appStatusChangeNotes->device_ip = $_SERVER['REMOTE_ADDR'];
		});
	}
	
}