<?php namespace Broadcasters\AppStatusChangeNotes\Providers;

use Broadcasters\AppStatusChangeNotes\Models\AppStatusChangeNotes as Model;
use Broadcasters\App\Models\App;


class AppStatusChangeNotesServiceProvider{

	protected $model;
	protected $app;
	/**
	 * constructer
	 */
	public function __construct(Model $model,App $app)
	{
		$this->model = $model;
		$this->app = $app;
	}
	

	/**
	 * 
	 */
	public function create($request,$app)
	{
		try {
			
			$this->app->find($app->app_id)->appNotes()->create([
				'title_notes' => $request->get('noteTitle'),
				'app_status_change_notes' => $request->get('noteDetails'),
				'user_id' => $app->user_id
				]);

		} catch (Exception $e) {
			
		}
	}

	public function getByAppId($appId)
	{
		return $this->model->where('app_id',$appId)->get();
	}





}
