<?php

namespace Broadcasters\Radios;

use Illuminate\Database\Eloquent\Model;

class Radio extends Model
{
	protected $primaryKey = 'radio_id';

	protected $fillable = ['name','sources_local_url','sources_cdn_url','description','countryid','language','url_token_key','valid_time','app_id','logo','genere'];

    /**
     * Get the app relationship
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

   


}