<?php

namespace Broadcasters\Radios;

use Illuminate\Http\Request;
use Broadcasters\Radios\Radio;
use App\App;
use File;

class RadioServiceProvider {

	protected $app;
	protected $radio;

	public function __construct(Radio $radio,App $app){
		$this->radio = $radio;
		$this->app = $app;
		
	}	

	public function find($id){
		return  $this->radio->find($id);
	}

	public function all($app_id){
		if($this->app->find($app_id))
			return $this->app->find($app_id)->radios->all();
	}


	public function getRadioByAppId($appId)
	{
		return $this->radio->where('app_id',$appId)->first();
	}

	public function save($request=[],$app_id,$id=null){	
		if($id){
			// dd($request);
			return $this->app->find($app_id)->radios()->find($id)->update($request);
		}else {
			return $this->app->find($app_id)->radios()->create($request);
		}
		

	}

	public function delete($id){
		$this->radio->find($id)->delete();
	}

	public function photo($id){
		return $this->radio->find($id)->radio_photo;
	}

	public function deletePhoto($file){

		File::delete('uploads/radios/'.$file);
	}

	public function getStreamUrl($id,$type)
	{		
		$radio = $this->radio->find($id);
		
		//dd($channel->validTime);

		if(!$radio){
			return null;
		}
		$type = \Config::get('site.streamAuthType');
		//dd($type);
		//$type = "wms";
		if($type="wms")
		{
			$authSign = getUrlSignature($radio->url_token_key,$radio->valid_time);
			//dd($authSign);
		}
		else
			$authSign = getPpvUrlSignature($radio->url_token_key,$radio->valid_time);
		//return $channel->local_source;
		
		return $radio->sources_local_url.'?wmsAuthSign='.$authSign;
	}
}