<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class AppNotification extends Model
{
	protected $primaryKey = 'app_notification_id';
	protected $fillable = ['type','message','time','data','exclude_days','app_id'];

    /**
     * app notification belongs to app
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

}