<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
	protected $primaryKey = 'app_id';
	protected $fillable = ['app_name','description','status','platform','user_id','app_code'];
    protected $table = 'app';

    /**
     * Get the user relationship
     */
    public function user()
    {
    	return $this->belongsTo('Broadcasters\Models\User','user_id');
    }


    /**
     * relationship with app_news
     */

    public function appNews()
    {
    	return $this->hasmany('Broadcasters\Models\AppNews','app_id');
    }

    /**
     * relationship with app channels
     */
    public function appChannels()
    {
    	return $this->hasmany('Broadcasters\Models\AppChannels','app_id');
    }


    /**
     * relationship with app Vod
     */
    public function appVod()
    {
    	return $this->hasmany('Broadcasters\Models\AppVod','app_id');
    }

    /**
     * relationship with app services
     */
    public function appServices()
    {
    	return $this->hasmany('Broadcasters\Models\AppServices','app_id');
    }

    /**
     * app has many app external api
     */
    public function appExternalApi()
    {
    	return $this->hasmany('Broadcasters\NewsApi\Models\AppExternalApi','app_id');
    }

    /**
     * app has many app notification key 
     */
    public function appNotificationKey()
    {
    	return $this->hasmany('Broadcasters\Models\AppNotificationKey','app_id');
    }

    /**
     * app has many aoo notification
     */
    public function appNotification()
    {
    	return $this->hasmany('Broadcasters\Models\AppNotification','app_id');
    }

    public function appNotes()
    {
        return $this->hasmany('Broadcasters\AppStatusChangeNotes\Models\AppStatusChangeNotes','app_id');
    }

    public function scopeUnPublished($query){
        return $query->where('status',0);
    }


    public function nitvAddKeys()
    {
        return $this->hasmany('Broadcasters\Admanage\NitvAdKey','app_id');
    }


}