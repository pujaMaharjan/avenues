<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class AppServices extends Model
{
	//protected $primaryKey = 'app_id';
	protected $fillable = ['app_id','service_id'];

    /**
     * app services belongs to app
     */
    public function app()
    {
    	return $this->belongsToMany('Broadcasters\Models\App','app_services','service_id','app_id');
    }

    /**
     * app services has many services 
     */
    public function services()
    {
    	return $this->belongsToMany('Broadcasters\Models\Services','service_id');
    }

}