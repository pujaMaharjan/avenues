<?php

namespace Broadcasters\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword,Billable;

	protected $primaryKey = 'user_id';

	protected $fillable = ['username','email','password','type','stripe_id','card_brand','card_last_four','trials_end_at'];

	protected $table = 'users';

	
	/*
	 * relationship to show which broadcaster belongs to which user
	 */

	public function broadcaster()
	{

		return $this->hasOne('Broadcasters\Models\Broadcaster','user_id');
	}

	/**
	 * relationship to show which app belong to which user
	 */

	public function apps()
	{
		return $this->hasMany('Broadcasters\Models\App','user_id');
	}

	public function countApp($id){
		//dd($id);	
		return $this->find($id)->apps()->unPublished()->count();
	}
	
	public function paymentRegisterDetail()
	{
		return $this->hasOne('App\PaymentRegisterDetail','user_id');
	}

	//nitv_payment_register_step
	public function paymentRegisterStep()
	{
		return $this->hasOne('App\PaymentRegisterStep','user_id');
	}

	public function isSubscriber($id){
		return true;
		if($this->find($id)->type=='admin'){
			return true;
		}
		//return $this->find($id)->paymentRegisterDetail->payment_status;	
	}
}