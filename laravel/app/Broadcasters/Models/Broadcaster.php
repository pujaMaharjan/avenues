<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class Broadcaster extends Model
{
	protected $primaryKey = 'broadcaster_id';
	protected $fillable = ['company_name','display_name','user_id','phone','country_code','is_oem','is_worldtv'];

    protected $casts = [
    'approved'=>'boolean',
    'is_oem'=>'boolean',
    'is_worldtv'=>'boolean'
    ];
    protected $table = 'broadcasters';
    /**
     * Get the user relationship
     */
    public function user()
    {
        // return $this->hasMany('App\User');
    	 return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->hasMany('App\User','user_id');
    }

    public function country()
    {
    	return $this->belongsTo('Broadcasters\Models\Country','country_id');
    }

    public function addUnitSet()
    { 
        return $this->hasOne('Broadcasters\AddUnitSet\Models\AddUnitSet','broadcaster_id');
    }

    public function worldTv(){
        return $this->hasMany(\App\Models\WorldTv::class,'broadcaster_id');
    }

    public function companyInfo()
    {
        return $this->hasOne('Broadcasters\Broadcaster\Models\CompanyInfo','broadcaster_id');
    }
}