<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class AppNews extends Model
{
	protected $primaryKey = 'app_news_id';
	protected $fillable = ['name','app_id','logo','web_link'];

    /**
     * Get the user relationship
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }


   public function appExternalApi()
    {
    	return $this->hasmany('Broadcasters\NewsApi\Models\AppExternalApi','app_news_id');
    }


}