<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class AppNotificationKey extends Model
{
	protected $primaryKey = 'app_notification_key_id';
	protected $fillable = ['app_notification_config_key','app_id'];

    /**
     * app notification key belongs to app
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

}