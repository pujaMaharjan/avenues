<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $primaryKey = 'country_id';
	protected $fillable = ['country_name'];
    protected $table = 'country';

    /**
     * Get the relationship
     */
    public function broadcaster()
    {
        return $this->hasMany('Broadcasters\Models\Broadcaster','country_id');
    }
}