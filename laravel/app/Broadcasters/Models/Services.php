<?php

namespace Broadcasters\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
	protected $primaryKey = 'service_id';
	protected $fillable = ['service_name','description','status','price'];

    /**
     * services has many app services
     */
    public function app()
    {
    	return $this->belongsToMany('App\App','app_services','app_id','service_id');
    }

}