<?php

namespace Broadcasters\Vod\Models;

use Illuminate\Database\Eloquent\Model;

class AppVod extends Model
{
	protected $primaryKey = 'app_vod_id';
	protected $fillable = ['youtube_channel_id','feature_playlist','videos_count','app_id'];
    protected $table = 'app_vod';

    protected $casts = [
    'videos_count' => 'json',
    ];

    /**
     * app vod belongs to app 
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\App\Models\App','app_id');
    }

}