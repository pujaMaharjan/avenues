<?php namespace Broadcasters\Vod\Providers;

use Broadcasters\Vod\Models\AppVod as Model;


class VodServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function getByAppId($appId)
	{
		try {
			//dd($appId);
			return $this->model->where("app_id",$appId)->first();	
		} catch (Exception $e) {
			
		}
		
	}
}
