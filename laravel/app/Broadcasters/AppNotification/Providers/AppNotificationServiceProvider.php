<?php namespace Broadcasters\AppNotification\Providers;

use Broadcasters\AppNotification\Models\AppNotification as Model;





class AppNotificationServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	

	/**
	 * 
	 */
	public function create($request,$appId)
	{
		try {
			$notificationDays = $request->get('notificationDays');
			$daily = $request->get('daily');
			//return $notificationDays;
			if($daily == 7)
			{
				$notificationDays = [] ;
				$notificationDays [] = $daily;
			}

			$input = [
			'message' => $request->get('message'),
			'time' => $request->get('startTime'),
			'notification_day'=> json_encode($notificationDays),
			'app_id' => $appId

			];
			$this->model->create($input);
		} catch (Exception $e) {

		}


	}

	public function getNotificationByAppId($appId)
	{
		return $this->model->where('app_id',$appId)->where('status',1)->get();
	}
	public function getAll($appId)
	{
		return $this->model->where('app_id',$appId)->get();
	}

	public function updateStatus($id)
	{
		try {
			$status = $this->model->find($id)->status;

			return	$this->model->find($id)->update(['status'=>!$status]);

		} catch (Exception $e) {
			
		}
	}

	public function createGeneralNotification($request,$appId)
	{
		//dd($request->all());
		$input = [
		'message' => $request->get('notificationMessage'),
		'data' => 'title:'.$request->get('notificationTitle'),
		'app_id' => $appId

		];
		$this->model->create($input);
	}

}
