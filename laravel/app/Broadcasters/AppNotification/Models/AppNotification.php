<?php

namespace Broadcasters\AppNotification\Models;

use Illuminate\Database\Eloquent\Model;

class AppNotification extends Model
{
	protected $primaryKey = 'app_notification_id';
	protected $fillable = ['type','message','time','data','exclude_days','app_id','notification_day','status'];
	protected $table = 'app_notification';

}