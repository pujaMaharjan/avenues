<?php

namespace Broadcasters\Platform\Models;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
	protected $primaryKey = 'platform_id';
	protected $fillable = ['platform_name'];

	protected $table = 'platform';
    /**
     * Get the user relationship
     */
    public function addUnit()
    {
        return $this->hasMany('Broadcasters\AddUnit\Models\AddUnit','platform_id'); 
    }
}