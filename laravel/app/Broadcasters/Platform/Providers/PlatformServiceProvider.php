<?php namespace Broadcasters\Platform\Providers;

use Broadcasters\Platform\Models\Platform as Model;


class PlatformServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
/**
 * get the list of platform 
 */
public function getLists()
{
	return $this->model->get();
}
public function setPlatform($platform)
{

		$platformModel = $this->model->where('platform_name',$platform)->first();
		if(!is_null($platformModel))
		return $platformModel->platform_id;
		else
			return null;

	
}

}
