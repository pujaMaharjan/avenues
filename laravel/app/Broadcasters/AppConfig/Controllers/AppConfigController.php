<?php

namespace Broadcasters\AppConfig\Controllers;
use Carbon;
use App\Http\Requests;
use Illuminate\Http\Request as Simple;

use Broadcasters\BaseController;


use Broadcasters\AppConfig\Providers\AppConfigServiceProvider as AppConfigService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
use Broadcasters\AppNotificationChannels\Providers\AppNotificationChannelsServiceProvider as AppNotificationChannelsService;

use Broadcasters\Channel\Providers\ChannelServiceProvider as ChannelsService;

use Broadcasters\Epg\Providers\EpgServiceProvider as EpgService;
use Broadcasters\AppNotification\Providers\AppNotificationServiceProvider as AppNotificationService;





class AppConfigController extends BaseController
{
	protected $service,
	$appService,
	$appNotificationChannelsService,
	$channelsService,
	$epgService,
	$appNotificationService;


	function __construct(AppConfigService $service,
		AppService $appService,
		AppNotificationChannelsService $appNotificationChannelsService,
		ChannelsService $channelsService,
		EpgService $epgService,
		AppNotificationService $appNotificationService)
	{
		parent::__construct();
		$this->service = $service; 
		$this->appService = $appService;
		$this->appNotificationChannelsService = $appNotificationChannelsService;
		$this->channelsService = $channelsService;
		$this->epgService = $epgService;
		$this->appNotificationService = $appNotificationService;
	}

/**
 * return list view with unit and broadcaster
 */
// public function index()
// {
//   $units = $this->service->getAddUnit();
//   $app = $this->service->getApp();

//   return $this->view("admin.addunits.index",compact('units','app'));

// }

/**
 * create unit and its sets 
 return create view 
 */
 public function create($appId)
 {
 	$parseKey = $this->service->getParseKey($appId);

 	return $this->view('apps.appConfig.key',compact('appId','parseKey'));
 }


 public function store(Simple $request,$appId)
 {
 	$parseKey = $this->service->getParseKey($appId);
 	if($parseKey)
 	{
 		//dd($request->all());
 		$this->service->update($request,$appId);
 	}
 	//dd($request->all());
 	$this->service->create($request,$appId);
 	return back();
 }

 // public function getKeyByAppId($appId)
 // {
 // 	$parseKeys = $this->service->getParseKey($appId);

 // 	$epg = $this->channelsService->getByAppId($appId);
 // 	//dd($epg->appEpgDetails);
 // 	//dd($subcribeChannels) 	
 // 	$pushResult = $this->appService->push($parseKeys,$appId,$epg);
 // 	//dd($pushResult);
 // 	//return redirect('admin/unit/list')->with("success","New units has been added!");
 // 	//return redirect('broadcaster/app/'.$appId)->with("success","pushed");
 // 	if($pushResult['result'] = true)
 // 	{
 // 		return redirect('broadcaster/app/'.$appId)->with("success","pushed");
 // 	}
 // 	//dd($applicationId,$clientKey,$restApiKey,$masterKey);
 // 	//return $this->view('apps.appConfig.push',compact('applicationId','applicationId')); 
 // }
 public function generalPushView($appId)
 {
 	
 	return $this->view('apps.appConfig.generalpush',compact('appId')); 

 	
 }
 public function generalPush($appId,Simple $request)
 {
 	// $mytime = Carbon\Carbon::now();
 	// $mytime->todateTimeString();
 	// dd($mytime);
 	$parseKeys = $this->service->getParseKey($appId);

 	$pushResult = $this->appService->pushGeneralNotification($parseKeys,$request);
 	if($pushResult['result'] = true)
 	{
 		//$this->appNotificationService->createGeneralNotification($request,$appId);
 		return redirect()->route('generalPushView',$appId)->with("success","pushed");
 	};
 	//$this->appNotificationService->create($request,$appId);
 	
 	

 }
 public function getEpgView($appId)
 {

 	//dd($appId);
 	$epgs = $this->channelsService->getByAppId($appId);
 	//dd($epgs);
 	$epgsLists = null;
 	$externalEpg = [];
 	if($epgs)
 	{
 		
 		if(is_object($epgs))
 		{
 			$epgsLists = $epgs->appEpgDetails;
 		}
 		else
 		{
 		//dd($epgsLists);
 			$key = 'epg_data_appid_'.$appId;
 			

 			// if (\Cache::has($key)) {
 			// 	$obj = \Cache::get($key);
 			// 	//dd($key);
 			// }
 			// else{


 			$epgsUrl =  $epgs;

 			$json = file_get_contents($epgsUrl);

 			$obj = json_decode($json,true);
 				//\Cache::forever('epg_data_appid_'.$appId, $obj);
 			//}
 	//return $obj['data']['programs']['1'];

 			//dd($obj);
 			foreach($obj['data']['programs'] as $item) {
 		//$details[] = $this->unique_multidim_array($item,'name');

 		//$externalEpg[] = $item;
 				$externalEpg[] = $this->unique_multidim_array($item,'name');


 			}
 		}
 	}
 	
 	
 	// die();
 	// dd($externalEpg);
 	// $details = $this->unique_multidim_array($externalEpg,'name');
 	// dd($details);

 	return $this->view('apps.appConfig.epgsnotification',compact('epgsLists','appId','externalEpg'));
 }

 public function epgNotification(Simple $request,$appId)
 {
 	
 	return $this->appNotificationService->create($request,$appId);
 	// $this->channelsService->getByAppId($appId);
 	// $epgsLists = $epgs->appEpgDetails;
 	// $selectedEpglists = $this->epgService->getSelectedEpg($request,$epgsLists);
 	// //dd($selectedEpglists);

 	// $parseKeys = $this->service->getParseKey($appId);

 	// //$selectedEpglists = "http://avenues.tv/api/?type=program";

 	// $pushResult = $this->appService->push($parseKeys,$appId,$selectedEpglists,$request);
 	// return back();
 }
 public function pushEpgNotification(Simple $request,$appId)
 {
 	$epgLists = $this->appNotificationService->getAll($appId);

 	// $json = json_decode($epgLists[0]->notification_day,true);


 	return $this->view('apps.appConfig.pushegpsnotification',compact('epgLists','appId'));

 }
 public function pushNotification(Simple $request,$appId)
 {
 	return "here";
 	//$id = $request->get('id');

 	//$status = $this->appNotificationService->updateStatus($id);
 	$parseKeys = $this->service->getParseKey($appId);
 	$pushResult = $this->appService->push($parseKeys,$appId,$request);
 	return $pushResult;
 	//return back();
 }
 public function postUushNotification($appId)
 {
 	
 	//$status = $this->appNotificationService->updateStatus($id);
 	$notificationLists = $this->appNotificationService->getNotificationByAppId($appId);


 	$parseKeys = $this->service->getParseKey($appId);
 	$pushResult = $this->appService->push($parseKeys,$appId,$notificationLists);
 	//dd($pushResult);
 	if($pushResult != null)
 	{
 		
 		return redirect()->route('pushEpgNotification',$appId)->with("success","pushed");

 	}
 	else
 	{
 		
 		return redirect()->route('pushEpgNotification',$appId)->with("failure","plz subcribe channels");

 	}
 }

 public function updateNotificationStatus(Simple $request)
 {
 	$id = $request->get('id');

 	$status = $this->appNotificationService->updateStatus($id);
 	//return back();
 }


 public function unique_multidim_array($array,$key)
 {

 	$temp_array = array(); 
 	$i = 0; 
 	$key_array = array(); 

 	foreach($array as $val) { 
 		if (!in_array($val[$key], $key_array)) { 
 			$key_array[$i] = $val[$key]; 
 			$temp_array[$i] = $val; 
 		} 
 		$i++; 
 	} 
 	return $temp_array; 

 }

}