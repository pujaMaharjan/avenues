<?php namespace Broadcasters\AppConfig\Providers;

use Broadcasters\AppConfig\Models\AppConfig as Model;





class AppConfigServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	

	/**
	 * 
	 */
	public function create($request,$appId)
	{
		try{
			$applicationId = $request->get('applicationId');
			$clientKey = $request->get('clientKey');
			$restApiKey = $request->get('restApiKey');
			$masterKey = $request->get('masterKey');

			$key = ['applicationId','clientKey','restApiKey','masterKey'];
			$value = [$applicationId,$clientKey,$restApiKey,$masterKey];

			//dd(count($key));
			for($i=0;$i<count($key);$i++)
			{
				$input = [
				'app_id' => $appId,
				'key' => $key[$i],
				'value' => $value[$i]

				];
				$this->model->create($input);
			}
			
		}

		catch(\Exception $e){
			return $e->getMessage();
		}

	}

	public function getParseKey($appId)
	{
		return	$this->model->where('app_id',$appId)->get();
	}

	public function update($request,$appId)
	{
		try{
			$applicationId = $request->get('applicationId');
			$clientKey = $request->get('clientKey');
			$restApiKey = $request->get('restApiKey');
			$masterKey = $request->get('masterKey');

			$key = ['applicationId','clientKey','restApiKey','masterKey'];
			$value = [$applicationId,$clientKey,$restApiKey,$masterKey];

			//dd($value);
			//$input = [];
			for($i=0;$i<count($key);$i++)
			{
				$input = [				
				'value' => $value[$i]
				];
				
				$this->model->where(['app_id'=>$appId,'key'=>$key[$i]])->update($input);
			}
			
			
		}

		catch(\Exception $e){
			return $e->getMessage();
		}
	}


	public function getAllKey($appIds)
	{
		//dd($appId);
		foreach ($appIds as $key => $appId) {
			if($appId == 0)
			{
				$keys['all'][]= $this->model->get();

			}
			else
			{
				$keys[] = $this->model->where('app_id',$appId)->get();
			}
			
			
		}
		return $keys;
	}
}
