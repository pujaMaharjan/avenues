<?php

namespace Broadcasters\AppConfig\Models;

use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
	protected $primaryKey = 'app_config_id';
	protected $fillable = ['app_id','key','value'];
    protected $table = 'app_config';

}