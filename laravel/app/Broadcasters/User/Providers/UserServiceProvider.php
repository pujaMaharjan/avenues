<?php 
namespace Broadcasters\User\Providers;

use Broadcasters\User\Models\User;

use Illuminate\Contracts\Auth\Guard as Auth;

//use App\User;
use Hash;

class UserServiceProvider{

	protected $user;
	
	/**
	 * constructer
	 */
	public function __construct(User $user,Auth $auth)
	{
		$this->user = $user;
		$this->auth = $auth;
	}
	public function create($request)
	{

		// dd($request->all());
		try{

			$this->user = $this->user->create([
				'username' => $request['username'],
				'email' => $request['email'],
				'password' => bcrypt($request['password']),
				'type'=>'broadcaster',
				]);
			$data['approved'] = 1;
			$data['status'] = 1;
			$this->user->broadcaster()->create($data);
			
			//$this->user->paymentRegisterDetail()->create([
				//'total_charge'=>24,
				//'payment_status'=>0,
				//]);

			//$this->user->paymentRegisterStep()->create([
				//'name'=>'cart',
				//'description'=>'on cart page',
			//	'status'=>0,
				//]);
			//before login redirect to cart

			$this->auth->loginUsingId($this->user->user_id);
			

			
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function updatePassword($request)
	{
		$currentPassword = $request->get('oldPassword');
		$newPassword = $request->get('password');
		$oldPassword = $this->authUser()->password;
		$userId = $this->auth->id();
		if (Hash::check($currentPassword, $oldPassword)) {
			return $this->model->where('user_id',$userId)->update(['password'=>bcrypt($newPassword)]);

		}

	}

	public function authUser()
	{
		try {
			$userId = $this->auth->id();
			return $this->model->where('user_id',$userId)->first();
		} catch (Exception $e) {
			
		}
	}

	public function availableUser($userName)
	{
		$user = $this->model->where('username',$userName)->first();
		if(count($user)>0)
		{
			return ['message'=>'user alredy registerd','flag'=>false];
		}
		else
		{
			return ['message'=>'user aviliable','flag'=>true];
		}
	}

	public function getRegisterStepByUserId($userId)
	{
		return $this->user->find($userId)->paymentRegisterStep()->first();
	}

	
}