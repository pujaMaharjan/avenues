<?php
namespace Broadcasters\User\Controllers;

use Broadcasters\BaseController;
use Broadcasters\User\Providers\UserServiceProvider as UserService;
use Illuminate\Http\Request as SimpleRequest;

class UserController extends BaseController
{
	function __construct(UserService $userService)
	{
		parent::__construct();
		$this->userService = $userService;
		
	}

	public function checkUserAvailable(SimpleRequest $request)
	{
		
		$userName = $request->get('username');
		return $this->userService->availableUser($userName);



	}
}
