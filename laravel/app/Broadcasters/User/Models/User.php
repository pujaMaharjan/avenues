<?php

namespace Broadcasters\User\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword;

	protected $primaryKey = 'user_id';

	protected $fillable = ['username','email','password','type'];

	protected $table = 'users';
	/*
	 * relationship to show which broadcaster belongs to which user
	 */

	public function broadcaster()
	{

		return $this->hasOne('Broadcasters\Broadcaster\Models\Broadcaster','user_id');
	}

	/**
	 * relationship to show which app belong to which user
	 */

	public function apps()
	{
		return $this->hasMany('Broadcasters\App\Models\App','user_id');
	}


	/**
	 * relationship to show which payment belong to which user
	 */
	public function paymentRegisterDetail()
	{
		return $this->hasOne('App\PaymentRegisterDetail','user_id');
	}


	public function paymentRegisterStep()
	{
		return $this->hasOne('App\PaymentRegisterStep','user_id');
	}

	


}