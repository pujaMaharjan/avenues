<?php
namespace Broadcasters\Templates\Controllers;

use Broadcasters\BaseController;
use Illuminate\Http\Request;
use Broadcasters\Templates\Models\Layout;
class LayoutController extends BaseController	 {

	protected $layout;

	public function __construct(Layout $layout){
		$this->layout = $layout;
	}
	
	public function index(){
		$layouts = $this->layout->all();
		return view('layouts.index',compact('layouts'));
	}

	public function create(){
		return view('layouts.form');
	}
	public function store(Request $request){
		$data = $request->all();
		$file = $request->file('screen_shot');
		$destinationPath = 'templates';
		$fileName = strtolower($file->getClientOriginalExtension());
		$name = $request->get('layout_name');

		$name = 'templates/'.$name.'.'.$fileName;
		//dd($name);
		$request->file('screen_shot')->move($destinationPath, $name);
		$data['screen_shot'] = $name;
		$this->layout->create($data);

		return redirect('admin/layouts');
	}
	public function edit($id){
		$layout = $this->layout->find($id);
		return view('layouts.form',compact('layout'));
	}
	public function update($id,Request $request){
		$data = $request->all();
		$file = $request->file('screen_shot');
		$destinationPath = 'templates';
		$fileName = strtolower($file->getClientOriginalExtension());
		$name = $request->get('layout_name');

		$name = 'templates/'.$name.'.'.$fileName;
		//dd($name);
		$request->file('screen_shot')->move($destinationPath, $name);
		$data['screen_shot'] = $name;
		$this->layout->find($id)->update($data);
		return redirect('admin/layouts');
	}
	public function delete($id){
		
		$this->layout->find($id)->delete();
		return redirect('admin/layouts');
	}

	public function getLayout($id){
		return $this->layout->find($id)->screen_shot;
	}

}