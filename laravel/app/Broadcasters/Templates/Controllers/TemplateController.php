<?php
namespace Broadcasters\Templates\Controllers;

use Broadcasters\BaseController;
use Broadcasters\Templates\Providers\TemplateProvider;
use Broadcasters\Templates\Providers\LayoutProvider;
use Illuminate\Http\Request as Request;
use App\App;
use Broadcasters\Channel\Providers\ChannelServiceProvider;
use Broadcasters\App\Providers\AppServiceProvider;

class TemplateController extends BaseController	 {

	protected $service;
	protected $layout;
	protected $channelServiceProvider;
	protected $appServiceProvider;


	public function __construct(TemplateProvider $service,
		LayoutProvider $layout,
		ChannelServiceProvider $channelServiceProvider){
		parent::__construct();
		$this->service = $service;
		$this->layout = $layout;
		$this->channelServiceProvider = $channelServiceProvider;

	}

	public function index($app_id,Request $request){
		//$menus = \Config::get('sample_api.new_api.recent_news');
		//$menus = json_decode(file_get_contents($menus));

		$data = [];
		$template = $this->service->get($app_id);
		$id = $app_id;

		$layouts = $this->layout->lists();
		$channel = App::find($app_id)->channels;
		

		// dd($channel);
		if(count($template)){			
			return $this->view('templates.index',compact('template','layouts','app_id','id','channel','menus'));

		}
		return $this->view('templates.create',compact('layouts','app_id','id','channel'));
	}
	
	public function edit($app_id,$tampleteId)
	{
		//dd($tampleteId);
		
		$template = $this->service->getById($tampleteId);
		$layouts = $this->layout->lists();

		//dd($template);
		return $this->view('templates.edit',compact('template','layouts','app_id'));


	}

	public function preview(Request $request){
		//$recent_news = \Config::get('sample_api.new_api.recent_news');

		//$recent_news = json_decode(file_get_contents($recent_news));
		$data = $request->all();
		$channels = \App\App::find($data['app_id'])->channels;
		$secretKey = \Config::get('site.appHashSecretKey');
		$utc = (int)trim(\Request::get('utc'));
		$apphash = trim(\Request::get('hash'));
		$type="wms";
		foreach($channels as $channel){
			$url = $this->channelServiceProvider->getStreamUrl($channel->app_channel_id,'wms');
		}
		
		return view('templates.detail',compact('data','channels','url'));

	}

	public function save($app_id,Request $request){
		

		$this->service->save($app_id,$request->all());
		return redirect('broadcaster/app/'.$app_id.'/template');
	}

	
	public function update($app_id,Request $request){
		$data = $request->all();
		// dd($data);
		$this->service->update($app_id,$data);
		return redirect('broadcaster/app/'.$app_id.'/template');
	}

}