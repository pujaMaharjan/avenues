<?php

namespace Broadcasters\Templates\Models;

use Illuminate\Database\Eloquent\Model;


class Layout extends Model {

	protected $primaryKey = 'layout_id';
	protected $fillable = ['layout_name','layout_status','screen_shot'];
	
}

