<?php

namespace Broadcasters\Templates\Models;

use Illuminate\Database\Eloquent\Model;
use Broadcasters\Templates\Models\Layout;

class AppTemplate extends Model {

	protected $primaryKey = 'id';
	protected $table = 'app_templates';
	protected $fillable = ['app_id','layout_id','app_template_primary_color','app_template_secondary_color','app_template_tertiary_color','approvred','approvred'];
	public $timestamps = false;

	public function app()
	{
		return $this->belongsTo('Broadcasters\App\Models\App','app_id');
	}

	public function layout()
	{
		return $this->belongsTo(Layout::class);
	}

}

