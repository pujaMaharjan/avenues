<?php
namespace Broadcasters\Templates\Providers;

use Broadcasters\Templates\Models\AppTemplate as Template;
use Broadcasters\Templates\Models\Layout;


class LayoutProvider{

	protected $layout;
	public function __construct(Layout $layout){
		$this->layout = $layout;
	}	

	public function lists(){
		return $this->layout->lists('layout_name','layout_id')->toArray();
	}

}


