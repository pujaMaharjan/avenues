<?php
namespace Broadcasters\Templates\Providers;

use Broadcasters\Templates\Models\AppTemplate as Template;
use Broadcasters\Templates\Models\Layout;
use App\App;

class TemplateProvider{

	protected $template;
	protected $app;
	public function __construct(Template  $template,App $app){
		$this->template = $template;
		$this->app = $app;
	}	

	public function all(){

		return $this->template->all();

	}


	public function save($id,$request){
		
		return $this->app->find($id)->templates()->create($request);
	}

	public function update($id,$request){
		$template = $this->template->where('app_id',$id);

		return $template->update($request);
	}

	public function get($data){

		return 	$this->template->where('app_id',$data)->first();
	}

	public function getwithTemplate($data){

		$template = $this->template->where('app_id',$data)->with('layout')->first();
		// return $template;
		return [
			'app_id'=>$template->app_id,
			'app_template_primary_color'=>$template->app_template_primary_color,
			'app_template_secondary_color'=>$template->app_template_secondary_color,
			'app_template_tertiary_color'=>$template->app_template_tertiary_color,
			'layout_name'=>$template->layout->layout_name,
			'layout_type'=>$template->layout->layout_type,
		];
	}

	public function getById($tampleteId)
	{
		return $this->template->find($tampleteId);
	}

	public function getLayouts()
	{
		$results =  Layout::all();
		$data = [];
		foreach($results as $i=>$res){
			$data[$i] = [
				'layout_name'=>$res->layout_name,
				'screen_shot'=>asset($res->screen_shot),
				'layout_id'=>$res->layout_id,
				'layout_type'=>$res->layout_type
			];
		}
		return $data;
	}

}


