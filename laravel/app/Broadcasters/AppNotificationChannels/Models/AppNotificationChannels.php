<?php

namespace Broadcasters\AppNotificationChannels\Models;

use Illuminate\Database\Eloquent\Model;

class AppNotificationChannels extends Model
{
	protected $primaryKey = 'app_notification_channels_id';
	protected $fillable = ['notification_channels_id','app_id'];
	protected $table = 'app_notification_channels';


	public function notificationChannel()
	{
		return $this->hasmany('Broadcasters\ParseChannels\Models\ParseChannels','notification_channels_id');
	}
}