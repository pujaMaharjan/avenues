<?php namespace Broadcasters\AppNotificationChannels\Providers;

use Broadcasters\AppNotificationChannels\Models\AppNotificationChannels as Model;



class AppNotificationChannelsServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	

	/**
	 * 
	 */
	public function create($request,$appId)
	{
		try {
			$channelsId = $request->get('channelsId');

			for($i=0; $i<count($channelsId); $i++)
			{
				$input = [
				'notification_channels_id' => $channelsId[$i],
				'app_id' => $appId
				];
				$this->model->create($input);
			}
		} catch (Exception $e) {
			
		}
	}

	public function getSubscribeChannels($appId)
	{
		try {
			return $this->model->where('app_id',$appId)->lists('notification_channels_id');
		} catch (Exception $e) {

		}
	}
	// public function getSubcribe($appId)
	// {
	// 	try {
	// 		//
	// 		return 
	// 		return $this->model->where('app_id',$appId)->get();
	// 	} catch (Exception $e) {

	// 	}	}

	// public function sync($request,$appId)
	// {
	// 	try {
	// 		return $this->model->where('app_id')
	// 	} catch (Exception $e) {

	// 	}
	// }




	}
