<?php

namespace Broadcasters\Channel\Requests;

use App\Http\Requests\Request;

class ChannelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'logo'=>'mimes:jpeg,bmp,png,gif,jpg'
        ];
    }
}
