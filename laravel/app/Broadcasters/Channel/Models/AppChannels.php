<?php

namespace Broadcasters\Channel\Models;

use Illuminate\Database\Eloquent\Model;

class AppChannels extends Model
{
	protected $primaryKey = 'app_channel_id';
	protected $fillable = ['name','sources_local_url','sources_cdn_url','logo','description','country','language','url_token_key','valid_time','app_id','created_by','updated_by','youtube_url','rmtp_url','pid_no','polarization','frequency','sat_name'];

    /**
     * Get the app relationship
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

    /**
     * app channnels has many channel epg
     */
    public function appChannelEpg()
    {
    	return $this->hasone('Broadcasters\Epg\Models\AppChannelEpg','app_channel_id');
    }


    public function appEpgDetails(){
        return $this->hasManyThrough('App\AppChannelEpgDetail','Broadcasters\Epg\Models\AppChannelEpg','app_channel_id','app_channel_epg_id');
    }


}