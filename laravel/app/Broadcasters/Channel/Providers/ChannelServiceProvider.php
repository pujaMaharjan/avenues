<?php namespace Broadcasters\Channel\Providers;


use Broadcasters\Channel\Models\AppChannels as Model;
use Broadcasters\Country\Models\Country;

class ChannelServiceProvider
{

    protected $model;

    /**
     * constructer
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getChannelByid($id)
    {
        // return $this->model->find($id);
        return $this->model->where('app_id', $id)->first();

    }

    public function getChannel($id)
    {
        return $this->model->find($id);

    }

    public function getByAppId($appId)
    {


        if ($this->model->where('app_id', $appId)->first()) {
            $data = $this->model->find($this->model->where('app_id', $appId)->get()->app_channel_id)->appChannelEpg;
            $result = $data->app_channel_epg_value;
            if ($data->app_channel_epg_type == 'file') {
                $result = $this->model->with('appEpgDetails')->where('app_id', $appId)->get();
            }

            return $result;
        }

        return $this->model->with('appEpgDetails')->where('app_id', $appId)->get();
        //return $this->model->appEpgDetails()->toSql();

    }


    public function getChannelByAppId($appId)
    {
        //dd($this->model->where('app_id',$appId)->first());
        if ($this->model->where('app_id', $appId)->first()) {
            $data = $this->model->with('appChannelEpg')->where('app_id', $appId)->get();
            $result = $data;
            // //dd($result);
            // if($data->app_channel_epg_type=='file'){
            // 	$result = 	$this->model->with('appEpgDetails')->where('app_id',$appId)->first();
            // 	//dd($result);
            // }

            //dd($result);
            return $result;
        }
    }

    public function getStreamUrl($id, $type)
    {

        $channel = $this->model->find($id);
        //dd($channel->validTime);

        if (!$channel) {
            return null;
        }
        $type = \Config::get('site.streamAuthType');

        if ($type = "wms") {

            $authSign = getUrlSignature($channel->url_token_key, $channel->valid_time);
            //dd($authSign);
        } else {

            $authSign = getPpvUrlSignature($channel->url_token_key, $channel->valid_time);
        }
        //return $channel->local_source;
        //env prod
        //nepal

        if (env("APP_ENV") == 'local') {
            return $channel->sources_local_url . '?wmsAuthSign=' . $authSign;
        } else {
            $country = geoip_country_code_by_name(\Request::ip());
            if ($country == 'np') {
                return $channel->sources_local_url . '?wmsAuthSign=' . $authSign;
            } else {
                return $channel->sources_cdn_url . '?wmsAuthSign=' . $authSign;
            }
        }

        //excepty global url
        //excepty global url
    }
}