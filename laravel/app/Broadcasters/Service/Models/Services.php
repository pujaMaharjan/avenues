<?php

namespace Broadcasters\Service\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
	protected $primaryKey = 'service_id';
	protected $fillable = ['service_name','description','status','price'];

    /**
     * services has many app services
     */
    public function appServices()
    {
    	return $this->belongsToMany('Broadcasters\Models\AppServices','service_id');
    }

}