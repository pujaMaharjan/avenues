<?php

namespace Broadcasters\Service\Controllers;

//use Illuminate\Http\Request;
// use App\Http\Requests;
use Broadcasters\BaseController;
use App\Http\Requests\ServiceRequest as Request;

use Broadcasters\Service\Providers\ServicesServiceProvider as Service;



class ServiceController extends BaseController
{
    protected $service;
    
    public function __construct(Service $service){
        parent::__construct();
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('her');
        $services = $this->service->all();
        
        return view('services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());

     $this->service->create($request);
     return redirect('admin/services/list');
 }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $service = $this->service->getById($id);
        return view('services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $service = $this->service->update($request,$id);
       //$service->update($request->all());

        return redirect('admin/services/list');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $this->service->delete($id);
        return redirect('admin/services/list');
    }
}
