<?php namespace Broadcasters\Service\Providers;

use Broadcasters\Service\Models\Services as Model;


class ServicesServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	public function create($request)
	{
		try{
			$this->model->create($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}
	/**
	 * Update application route
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}

	public function all()
	{
		return $this->model->all();
	}
	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			dd($e);
		}
		
	}
	public function getById($id)
	{
		return $this->model->find($id);
	}


}
