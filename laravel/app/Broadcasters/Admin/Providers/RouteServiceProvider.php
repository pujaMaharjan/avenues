<?php namespace Broadcasters\Admin\Providers;


use Broadcasters\Admin\Models\Route as Model;

class RouteServiceProvider{


	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Create application route
	 */
	public function create($request)
	{
		try{
			$this->model->create($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}
	/**
	 * Update application route
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}

	/**
	 * Get all routes
	 */
	public function getWithGroup()
	{
		return $this->model->with('group')->get();
	}

	/**
	 * Get all routes
	 */
	public function all()
	{
		return $this->model->all();
	}
	/**
	 * Get route based on id
	 */
	public function getById($id)
	{
		return $this->model->with('group')->find($id);
	}

	/**
	 * delete route
	 */
	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			dd($e);
		}
		
	}

	/**
	 * get each route 
	 */
	public function getAllSingleRoutes()
	{
		try {
			return $this->model->where('route_group_id',null)->orderBy('order', 'asc')->get();
		} catch (Exception $e) {
			dd($e);
		}
	}

	/**
	 * update order of route without group 
	 */
	public function updateRouteOrder($orders)
	{
		try{
			
			foreach ($orders as $key => $value) {
				
				$model = $this->model->find($value);
				$model->order = $key+1;
				$model->save();
				//$model->update(['order' => $key+1]);
				
			}
		} catch (Exception $e) {
			dd($e);
		}
	}
	/**
	 * update order of route within group 
	 */
	public function updateGroupRouteOrder($groupRoutes)
	{
		
		try{
			
			foreach ($groupRoutes as $key => $value) {
				
				$model = $this->model->find($value);
				$model->order = $key+1;
				$model->save();
				//$model->update(['order' => $key+1]);
				
			}
		} catch (Exception $e) {
			dd($e);
		}
	}

	/**
	 * update order of child route of group 
	 */
	public function updateChildOrder($childRoutes)
	{
		
		try{
			
			foreach ($childRoutes as $key => $value) {
				
				$model = $this->model->find($value);
				$model->order = $key+1;
				$model->save();
				//$model->update(['order' => $key+1]);
				
			}
		} catch (Exception $e) {
			dd($e);
		}
	}


	
}