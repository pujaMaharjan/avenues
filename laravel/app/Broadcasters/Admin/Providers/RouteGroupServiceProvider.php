<?php namespace Broadcasters\Admin\Providers;

use Broadcasters\Admin\Models\RouteGroup as Model;

class RouteGroupServiceProvider{


	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Create application route
	 */
	public function create($request)
	{
		try{
			
			$this->model->create($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}

	public function update($request,$id)
	{
		try{

			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}
	/**
	 * Get all route groups
	 */
	public function all()
	{
		return $this->model->all();
	}
	/**
	 * Get all routes groups as list
	 */
	public function getLists($param1,$param2=null)
	{
		return $this->model->lists($param1, $param2);
	}	

	/**
	 * Get route group based on id
	 */
	public function getById($id)
	{
		return $this->model->find($id);
	}

	public function countRoutes($id)
	{
		return $this->model->find($id)->routes->count();
	}

	/**
	 * delete route group
	 */
	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			dd($e);
		}
		
	}
	/**
	 * 	get route groups with routes
	 */

	public 	function getGroupWithRoute()
	{
		return $this->model->where('parent_id',null)
		->with(['routes'=>function($query){
			return $query->orderBy('order', 'asc')->get();
		},'children'=>function($query){
			return $query->with(['routes'=>function($query){
				return $query->orderBy('order', 'asc')->get(); 
			}])->orderBy('order','asc')->get();
		}])->orderBy('order', 'asc')->get();
	}

	/**
	 * get each route 
	 */
	public function getAllParentGroup($param1, $param2)
	{
		try {
			return $this->model->where('parent_id',null)->lists($param1, $param2);
			//return $this->model->where('parent_id',null)->get();
		} catch (Exception $e) {
			dd($e);
		}
	}

	/**
	 * update order of group 
	 */
	public function updateGroupOrder($GroupOrders)
	{
		try{
			
			foreach ($GroupOrders as $key => $value) {
				
				$model = $this->model->find($value);
				//dd($model->order = $key+1);
				$model->order = $key+1;
				$model->save();
				//$model->update(['order' => $key+1]);
				
			}
		} catch (Exception $e) {
			dd($e);
		}
	}

	/**
	 * update order of sub route group 
	 */
	public function childGroupOrder($childGroupOrders)
	{
		try{
			
			foreach ($childGroupOrders as $key => $value) {
				
				$model = $this->model->find($value);
				//dd($model->order = $key+1);
				$model->order = $key+1;
				$model->save();
				//$model->update(['order' => $key+1]);
				
			}
		} catch (Exception $e) {
			dd($e);
		}
	}
}