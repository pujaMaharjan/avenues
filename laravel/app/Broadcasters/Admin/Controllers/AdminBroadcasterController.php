<?php

namespace Broadcasters\Admin\Controllers;

use Illuminate\Http\Request as SimpleRequest;
use App\Http\Requests;
use Broadcasters\BaseController;
use Broadcasters\Broadcaster\Providers\BroadcasterServiceProvider as BroadcasterService;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
// use Broadcasters\Admin\Providers\AdminBroadcasterServiceProvider as AdminBroadcasterService;
use Broadcasters\AppStatusChangeNotes\Providers\AppStatusChangeNotesServiceProvider as AppStatusChangeNotes;

use Broadcasters\AppConfig\Providers\AppConfigServiceProvider as AppConfigService;
class AdminBroadcasterController extends BaseController
{
	protected $service;
  protected $broadcaster;
  public $broadcasterId;
  protected $appService;
  protected $appStatusChangeNotes;
  protected $appConfigService;
  function __construct(BroadcasterService $broadcasterService,
    SimpleRequest $request,
    AppService $appService,
    AppStatusChangeNotes $appStatusChangeNotes,
    AppConfigService $appConfigService)
  {
    parent::__construct();
  //$this->service = $service;
    $this->broadcasterId = $request->segment(3);
    $this->broadcasterService = $broadcasterService;
    $this->broadcaster = $broadcasterService->getById($request->segment(3));
    $this->appService = $appService;
    $this->appStatusChangeNotes = $appStatusChangeNotes;
    $this->appConfigService = $appConfigService;


  }

  public function broadcaster($broadcasterId)
  {
    // dd(\Auth::id());
    // return \Broadcasters\Models\Broadcaster::find($broadcasterId);
    $new_user = \Broadcasters\Models\Broadcaster::find($broadcasterId)->user;

    // dd($new_user);
    \Session::put('orig_user', \Auth::id() );
    \Auth::login($new_user);
    $adminAsBroadcaster = $this->broadcaster;
    $id = $broadcasterId;
    $userId = $this->broadcasterService->getUserId($broadcasterId);
    $apps = $this->appService->getAppByUserId($userId);
    return $this->view("broadcaster.dashboard",compact('apps','id','adminAsBroadcaster'));

  }

  public function updateStatus($appId)
  {

    $status = $this->appService->updateStatus($appId);

    if($status)
    {
      return back();
    }
  }

  public function appNotes($appId)
  {
    return $this->view("apps.statusnotes",compact('appId'));
  }

  public function appNotePost(SimpleRequest $request,$appId)
  {

    // dd($request->all());

        $app = $this->appService->getById($appId);
    
    

    $this->appStatusChangeNotes->create($request,$app);
    $this->appService->changeAppPaymentStatus($app);
   
      $this->updateStatus($appId);

 \Event::fire(new \App\Events\AppServicePaid(\Broadcasters\App\Models\App::find($appId)));
 
    return back();
  }

  public function viewNotes($appId)
  {
    $notes = $this->appStatusChangeNotes->getByAppId($appId);

    return $this->view('apps.viewnotes',compact('notes','appId'));
  }

  public function pdfNotes($appId)
  {
    $notes = $this->appStatusChangeNotes->getByAppId($appId);

    return $this->view('broadcaster.note-pfd',compact('notes'));
    
  }

  public function pdfPost($appId)
  {
     $content = file_get_contents(route('pdfNotes',$appId));

   
    $html2pdf = new \HTML2PDF('P','A4','fr');
    $html2pdf->WriteHTML($content);
    $html2pdf->Output('exemple.pdf');
  }

  public function adminPushView()
  {
   // $adminAsBroadcaster = $this->broadcaster;
   // dd($adminAsBroadcaster);
   // // $id = $broadcasterId;
   // $userId = $this->broadcasterService->getUserId($broadcasterId);
    $apps = $this->appService->getAllApp();



    return $this->view("push.adminpush",compact('apps'));
  }

  public function adminPushPost(SimpleRequest $request)
  {

    //dd($request->all());
    $appId = $request->get('appId');

    //if request contains 0 
    if(in_array(0,$appId)){
      $appId = [];
      $appId [] = 0;
    }


    //$parseKeys = collect($this->appConfigService->getAllKey($appId));
    $parseKeys = $this->appConfigService->getAllKey($appId);
    //dd($parseKeys);
    $chunks = $parseKeys;
    if(isset($parseKeys['all']))
    {
      $chunks = [];
      $parseKeys = collect($parseKeys);
      foreach ($parseKeys['all'] as $parseKey){
        foreach($parseKey->chunk(4) as $chunk){
          $chunks[] = $chunk;
        }  

      }
    }




    //dd($chunks);

    $pushResult = $this->appService->pushAllApp($chunks,$request);
    if($pushResult['result'] = true)
    {

      return redirect()->route('adminPushView')->with("success","pushed");
    };
  }

 

 public function user_switch_stop()
  {

    $id = \Session::pull( 'orig_user' );
    // dd($id);
    $orig_user = \App\User::find( $id );
    // dd($orig_user);
    \Auth::login( $orig_user );
    return redirect('/admin');
  }

}