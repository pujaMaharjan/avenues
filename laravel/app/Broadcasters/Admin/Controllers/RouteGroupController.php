<?php
namespace Broadcasters\Admin\Controllers;

use Broadcasters\BaseController;
use App\Http\Requests\CreateRouteGroupRequest as CreateRouteGroupRequest;
use Broadcasters\Admin\Providers\RouteGroupServiceProvider as RouteGroupService;
class RouteGroupController extends BaseController
{
	protected $service;
	function __construct(RouteGroupService $service) {
		$this->service = $service;
	}
     /**
         * create route
         */
     public function create()
     {
        $routeGroups = $this->service->getAllParentGroup('prefix','route_group_id')->all();
        return view('admin.routegroups.create')->with(compact(['routeGroups']));
    }
    /**
     * Store application routes
     */
    public function store(CreateRouteGroupRequest $request)
    {
        $this->service->create($request);
        return redirect(url('admin/route/group'))->with("success","Route Group has been added!");;
    }

        /**
     * List application routes
     */
        public function listRouteGroup()
        {
            $routegroups = $this->service->all();
            //return $routegroups;
            return view('admin.routegroups.index')->with(['routegroups' => $routegroups]);
        }

        /**
         * edit route
         */
        public function edit($id)
        {
            $routeGroup = $this->service->getById($id);
            //$selectedGroup = $this->service->getAllParentGroup('groups_as','parent_id')->all();
            
            $selectedGroups = $this->service->getLists('prefix','route_group_id')->all();
            //dd($selectedGroup);
            return view('admin.routegroups.edit')->with(compact(['routeGroup','selectedGroups']));
        }
        public function update(CreateRouteGroupRequest $request,$id)
        {

            $this->service->update($request,$id);

            return redirect(url('admin/routegroup'))->with("success","Route Group has been updated!");
        }

        /**
         * delete route group
         */
        public function delete($id)
        {
            if($this->service->countRoutes($id)>0){
                return redirect(url('admin/routegroup'))->with("failure","Cannot Delete Because it content routes!");
            }
            $this->service->delete($id);
            return redirect(url('admin/routegroup'))->with("success","Route Group has been deleted!");

        }
        /**
         * order route group 
         */

        public function order()
        {
            $routeGroups = $this->routeGroupService->getGroupWithRoute();
            
            return view('admin.routes.orderRoute')->with(compact(['routeGroups']));
        }

        public function test()
        {
            return 'testing......';
            # code...
        }
    }
