<?php
namespace Broadcasters\Admin\Controllers;

use Broadcasters\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\CreateRouteRequest as CreateRouteRequest;
use Broadcasters\Admin\Providers\RouteServiceProvider as RouteService;
use Broadcasters\Admin\Providers\RouteGroupServiceProvider as RouteGroupService;
class RouteController extends BaseController
{
	protected $service;
    protected $routeGroupService;
    function __construct(RouteService $service, RouteGroupService $routeGroupService) {
        $this->service = $service;
        $this->routeGroupService = $routeGroupService;
    }
    /**
     * Store application routes
     */
    public function store(CreateRouteRequest $request)
    {
        $this->service->create($request);
        return redirect(url('admin/route/list'));
    }

        /**
     * List application routes
     */
        public function listRoute()

        {
            $routes = $this->service->getWithGroup();
            return view('admin.routes.index')->with(['routes' => $routes]);
        }

        /**
         * create route
         */
        public function create()
        {
            $routeGroups = $this->routeGroupService->getLists('prefix','route_group_id')->all();
            return view('admin.routes.create')->with(compact(['routeGroups']));
        }
        /**
         * edit route
         */
        public function edit($id)
        {
            $route = $this->service->getById($id);
            $routeGroups = $this->routeGroupService->getLists('prefix','route_group_id')->all();
            return view('admin.routes.edit')->with(compact(['route','routeGroups']));
        }
        public function update(CreateRouteRequest $request,$id)
        {
            $this->service->update($request,$id);
            return redirect(url('admin/route/list'))->with("success","Route has been updated!");

        }

        /**
         * delete route 
         */
        public function delete($id)
        {
            $this->service->delete($id);
            return redirect(url('admin/route/list'))->with("success","Route has been deleted!");

        }

        public function order()
        {
            $routes = $this->service->getAllSingleRoutes();
            $routeGroups = $this->routeGroupService->getGroupWithRoute();
            //return view('admin.routes.index')->with(['routes' => $routes]);
            return view('admin.routes.orderRoute')->with(compact(['routes','routeGroups']));
        }

        /**
         * update ordering of all routes
         */
        public function updateOrder(Request $request){

            $orders = $request->get('routeOrder');
            $groupOrders = $request->get('groupOrder');
            $groupRouteOrders = $request->get('groupRouteOrder');
            $groupChildOrders = $request->get('groupChildOrder');
            $childGroupOrders = $request->get('childGroupOrder');

            $this->routeGroupService->updateGroupOrder($groupOrders);

            $this->service->updateChildOrder($groupChildOrders);
            $this->routeGroupService->childGroupOrder($childGroupOrders);
            $this->service->updateGroupRouteOrder($groupRouteOrders);

            $this->service->updateRouteOrder($orders);
            return redirect(url('admin/route/order'))->with("success","Route has been order!");
            //return updateOrder($orders);
        }
    }
