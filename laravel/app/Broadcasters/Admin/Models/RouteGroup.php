<?php

namespace Broadcasters\Admin\Models;

use Illuminate\Database\Eloquent\Model;


class RouteGroup extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'route_group';
    protected $primaryKey = 'route_group_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'groups_as', 
    'middleware', 
    'weight',
    'namespace',
    'prefix',
    'before',
    'parent_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    /**
     * Get the comments for the blog post.
     */
    public function routes()
    {
        return $this->hasMany('Broadcasters\Admin\Models\Route','route_group_id');
    }

    public function children()
    {
        return $this->hasMany('Broadcasters\Admin\Models\RouteGroup','parent_id','route_group_id');
    }

    public function parent()
    {
        return $this->belongsTo('Broadcasters\Admin\Models\RouteGroup','parent_id');
    }

    /**
     * set group id to assign in parent id 
     */
    public function setParentIdAttribute($value)
    {

        $this->attributes['parent_id'] = $value?$value:null;
    }
}
