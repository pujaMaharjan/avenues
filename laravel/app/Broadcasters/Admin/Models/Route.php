<?php

namespace Broadcasters\Admin\Models;

use Illuminate\Database\Eloquent\Model;


class Route extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'route';
    protected $primaryKey = 'route_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['route_name', 'route_verb', 'route_as','route_uses','middleware','route_group_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    /**
     * Get the group of route
     */
    public function group()
    {
       return $this->belongsTo('Broadcasters\Admin\Models\RouteGroup','route_group_id');
   }
    /**
     * Get route_group_id empty if null
     */
    public function getRouteGroupIdAttribute($value)
    {
        if($value==null)
            return "";
        return $value;
    }

    /**
     * set group id to assign in route_group_id in route table 
     */
    public function setRouteGroupIdAttribute($value)
    {

        $this->attributes['route_group_id'] = $value?$value:null;
    }
    
}
