<?php namespace Broadcasters\App\Providers;

use Broadcasters\App\Models\App as Model;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Carbon;

class AppServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;



	}
	
	// public function 

	/**
	 * 
	 */
	public function create($request)
	{
		try{

			$this->model->create($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	/**
	 * 
	 */
	public function getAll()
	{
		try {
			return $this->model->all();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		try {
			return $this->model->find($id);
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$this->model->find($id)->update($request->all());
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}
	public function getAppByUserId($userId)
	{
		try {
			return $this->model->where('user_id',$userId)
			->get();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	public function approveAppByUserId($userId)
	{
		try {
			return $this->model->where('user_id',$userId)
			->where('status',1)
			->get();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getAppList($param1,$param2)
	{

		return $this->model->lists($param1, $param2);

	}
	public function getWithUnits($appId)
	{
		try {

			//return $this->model->with('config')->find($id)
			return $this->model->with('adUnit')->find($appId);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	public function getApp($appId)
	{
		try {
			return $this->model->where('app_id',$appId)->first();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	public function count($value='')
	{
		try {
			return $this->model->count();
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
	public function getService($appId)
	{
		
		try {
			
			return $this->model->find($appId)->services()->get();
		} catch (Exception $e) {
			
		}
	}

	public function updateStatus($appId)
	{
		
		try {
			$status = $this->model->find($appId)->status;

			if(!$status){
				return $this->model->find($appId)->update(['status'=>!$status,'expire_at'=>date('Y-m-d H:i:s',strtotime("next month"))]);
			}
			return $this->model->find($appId)->update(['status'=>!$status]);
		} catch (Exception $e) {
			
		}
	}

	public function sync($request,$appId)
	{
		try {
			$notificationChannelId = $request->get('channelsId');
			if(!$request->get('channelsId'))
				$notificationChannelId = [];
			return $this->model->find($appId)->notificationChannel()->sync($notificationChannelId);
		} catch (Exception $e) {

		}
	}
	public function deattch($request,$appId)
	{
		try {
			$notificationChannelId = $request->get('channelsId');
			if(!$request->get('channelsId'))
				$notificationChannelId = [];
			return $this->model->find($appId)->notificationChannel()->sync($notificationChannelId);
		} catch (Exception $e) {

		}
	}

	public function getAllApp()
	{
		return $this->model->get();
	}


	public function push($parseKeys,$appId,$notificationLists)
	{
		
		$mytime = Carbon\Carbon::now();
		echo $mytime->todateTimeString();
		$today = $mytime->toDateString();


		$subcribeChannels = $this->getSubcribeChannels($appId); //get app subcribedchannels

		foreach ($parseKeys as $key => $parseKey) {
			if($parseKey->key == 'applicationId')
			{
				$applicationId = $parseKey->value;
			}
			elseif($parseKey->key == 'clientKey')
			{
				$clientKey = $parseKey->value;
			}
			elseif ($parseKey->key == 'restApiKey') {
				$restApiKey = $parseKey->value;
			}
			else
			{
				$masterKey = $parseKey->value;
			}
		}
		//dd(count($subcribeChannels));
		if(count($subcribeChannels)>0)
		{
			//dd('here');
			foreach ($subcribeChannels as $key => $subcribeChannel) {
				$channels[] = $subcribeChannel->channels;
			}


		//return $channels;
			ParseClient::initialize($applicationId, $restApiKey, $masterKey );
			$queryAndroid = ParseInstallation::query();
			$queryAndroid->equalTo('deviceType', 'android');
			//dd($channels);
			foreach ($channels as $key => $channel) {

				$queryAndroid->equalTo('channels',$channel);
			//dd($channel);
				if($channel == 'A')
				{

					foreach ($notificationLists as $key => $list) {

						$match = ':';
						$startHour = $list->time;

						if(!strlen(stristr($startHour,$match))>0)
						{
							$startHour = $startHour.":"."00";
							return $startHour;
						}

						$message = $list->message;
						$day = json_decode($list->notification_day);



					//when we hit from our local server we need to - 5 hour to match current local time 




						$data = $data = array("alert" => $message);

					//$date =  new \DateTime($today.$starthour);

						if(strlen(stristr($startHour,$match))>0)
						{
							list($hr,$min) = explode(":",$startHour);
							$startHour = $hr;
						}

						$total  =  $min+$hr*60-(5*60+(45));

						if($total<0){
							$total = $total+(24*60);
						}


						$startHour =(int) floor($total/60);
					//dd($startHour);

						$minute = $total - ($startHour*60)-5;

					//$startHour = $startHour - 5;

						$startHour = $startHour.':'.$minute;
					//dd($startHour);
						$date = new \DateTime($today.$startHour);
					// print_r($day);
					// dd('here');
					//print_r($date);
					//dd($startHour);

					// //if today is in $day message send
					// dd();
					//dd($date);
						if(in_array(date("w"),$day)){

							$push = ParsePush::send(array(

								"where" => $queryAndroid,
								"push_time" => $date,

								"data" => $data
								));
						} elseif(in_array(7,$day)) {

							$push = ParsePush::send(array(

								"where" => $queryAndroid,
								"push_time" => $date,

								"data" => $data
								));
						} else {

							return "else";
						}

					}
				//$status = $request->get()
				//return $startHour;


				}
				elseif ($channel == 'B') {
					$push = ParsePush::send(array(

						"where" => $queryAndroid,
						"data" => array(
							"alert" => 'b'
							)
						));

				}
				elseif ($channel == 'C')
				{
					$push = ParsePush::send(array(

						"where" => $queryAndroid,
						"data" => array(
							"alert" => 'c'
							)
						));
				}
				elseif ($channel == 'D')
				{
					$push = ParsePush::send(array(

						"where" => $queryAndroid,
						"data" => array(
							"alert" => 'd'
							)
						));
				}
				elseif ($channel == 'E')
				{
					$push = ParsePush::send(array(

						"where" => $queryAndroid,
						"data" => array(
							"alert" => 'e'
							)
						));
				}
				else
				{
					$push = ParsePush::send(array(

						"where" => $queryAndroid,
						"data" => array(
							"alert" => 'general'
							)
						));
				}
				return $push;

			}
		}
	}

	public function getSubcribeChannels($appId)
	{
		try {
			if($this->model->find($appId))
				return $this->model->find($appId)->notificationChannel()->get();
			return $this->model->find($appId);

		} catch (Exception $e) {

		}
	}

	public function pushNotification($appNotificationId)
	{
		return $this->model->find($appNotificationId);

	}


	public function pushGeneralNotification($parseKeys,$request)
	{
		 //get app subcribedchannels
		foreach ($parseKeys as $key => $parseKey) {
			if($parseKey->key == 'applicationId')
			{
				$applicationId = $parseKey->value;
			}
			elseif($parseKey->key == 'clientKey')
			{
				$clientKey = $parseKey->value;
			}
			elseif ($parseKey->key == 'restApiKey') {
				$restApiKey = $parseKey->value;
			}
			else
			{
				$masterKey = $parseKey->value;
			}
		}
		

		ParseClient::initialize($applicationId, $restApiKey, $masterKey );
		$queryAndroid = ParseInstallation::query();
		$queryAndroid->equalTo('deviceType', 'android');
		$message = $request->get('notificationMessage');
		$title = $request->get('notificationTitle');
		$data = array(
			"alert" => $message,
			"title" => $title
			);
		$push = ParsePush::send(array(

			"where" => $queryAndroid,
			"data" => $data
			));

		return $push;
	}

	public function pushAllApp($parseKeysArray,$request)
	{


		foreach ($parseKeysArray as $j => $parseKeys) {
			foreach ($parseKeys as $key => $parseKey) {
				if($parseKey->key == 'applicationId')
				{
					$applicationIds[] = $parseKey->value;
				}
				elseif($parseKey->key == 'clientKey')
				{
					$clientKeys[] = $parseKey->value;
				}
				elseif ($parseKey->key == 'restApiKey') {
					$restApiKeys[] = $parseKey->value;
				}
				else
				{
					$masterKeys[] = $parseKey->value;
				}
			}

		}
		//dd($masterKeys);
		foreach ($applicationIds as $i => $applicationId) {
			
			ParseClient::initialize($applicationId, $restApiKeys[$i], $masterKeys[$i] );
			$queryAndroid = ParseInstallation::query();
			$queryAndroid->equalTo('deviceType', 'android');
			$message = $request->get('notificationMessage');
			$title = $request->get('notificationTitle');
			$data = array(
				"alert" => $message,
				"title" => $title
				);
			$push = ParsePush::send(array(

				"where" => $queryAndroid,
				"data" => $data
				));


		}
		return $push;

	}



	public function changeAppPaymentStatus($app){
		// dd();
		$expire_at = date('Y-m-d',strtotime($app->expire_at));
		$appPaymentStatus = $app->paymentDetail()->where('paid_for',$expire_at)->get();
		// dd($app->paymentDetail()->where('paid_for',date('Y-m-d',strtotime($app->expire_at)))->first());
		 $this->model->find($app->app_id)
		 ->paymentDetail()
		 ->where('paid_for',$expire_at)
		 
		 ->update(['payment_status'=> !$appPaymentStatus]);

	}

	public function getIdByAppCode($app_code)
	{
		return $this->model->where('app_code',$app_code)->first()->app_id;
	}
}
