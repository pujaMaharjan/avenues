<?php

namespace Broadcasters\App\Models;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
	protected $primaryKey = 'app_id';
	protected $fillable = ['app_name','description','status','platform','user_id','expire_at'];
    protected $table = 'app';

    /**
     * Get the user relationship
     */
    public function user()
    {
    	return $this->belongsTo('Broadcasters\Models\User','user_id');
    }


    /**
     * relationship with app_news
     */

    public function appNews()
    {
    	return $this->hasmany('Broadcasters\Models\AppNews','app_id');
    }

    /**
     * relationship with app channels
     */
    public function appChannels()
    {
    	return $this->hasmany('Broadcasters\Models\AppChannels','app_id');
    }


    /**
     * relationship with app Vod
     */
    public function appVod()
    {
    	return $this->hasmany('Broadcasters\Models\AppVod','app_id');
    }

    /**
     * relationship with app services
     */
    public function services()
    {
    	return $this->belongsToMany('Broadcasters\Service\Models\Services','app_services','app_id','service_id');
    }

    /**
     * app has many app external api
     */
    public function appExternalApi()
    {
    	return $this->hasmany('Broadcasters\NewsApi\Models\AppExternalApi','app_id');
    }

    /**
     * app has many app notification key 
     */
    public function appNotificationKey()
    {
    	return $this->hasmany('Broadcasters\Models\AppNotificationKey','app_id');
    }

    /**
     * app has many aoo notification
     */
    public function appNotification()
    {
    	return $this->hasmany('Broadcasters\Models\AppNotification','app_id');
    }

    public function adUnit()
    {
        return $this->hasmany('Broadcasters\AddUnitSet\Models\AddUnitSet','app_id');
    }

 public function paymentDetail(){
        return $this->hasMany(\App\PaymentDetail::class);
    }

    public function appNotes()
    {
        return $this->hasmany('Broadcasters\AppStatusChangeNotes\Models\AppStatusChangeNotes','app_id');
    }

    public function notificationChannel()
    {
        return $this->belongsToMany('Broadcasters\ParseChannels\Models\ParseChannels','app_notification_channels','app_id','notification_channels_id'); 
    }

    public function adSetings()
    {
        return $this->hasMany(\App\Models\AdSetting::class);
    }


    public function menus()
    {
       return $this->hasMany('App\Models\Menu','app_id');
    }

    public function nitvAddKeys()
    {
        return $this->hasmany('App\Models\NitvAdKey','app_id');
    }
}
