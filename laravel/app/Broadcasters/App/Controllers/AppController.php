<?php

namespace Broadcasters\App\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\App;
use Auth;
use App\Http\Requests\AppRequest;
use Broadcasters\App\Providers\AppServiceProvider as AppService;
class AppController extends Controller
{
    protected $app;
    protected $AppService;

    public function __construct(App $app,AppService $appService){
        $this->app = $app;
        $this->appService = $appService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $app = $this->app->all();
        return "";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('apps.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppRequest $request)
    {
        $this->app->app_name = $request->input('app_name');
        $this->app->description = $request->input('description');
        dd($this->app->user_id = Auth::id());
        $this->app->save();
        return redirect('broadcaster');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('apps.new');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appService($appId)
    {
        return $appService = $this->appService->getService($appId);
    }
}
