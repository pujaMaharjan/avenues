<?php

namespace Broadcasters\AddUnit\Models;

use Illuminate\Database\Eloquent\Model;

class AddUnit extends Model
{
	protected $primaryKey = 'add_unit_id';
	protected $fillable = ['unit_name','type','add_unit_set_id','platform_id','status'];
    protected $casts = [
        'status'=>'boolean'
    ];

	protected $table = 'add_unit';
    /**
     * Get the user relationship
     */
    public function addUnitSet()
    {
        return $this->belongsTo('Broadcasters\AddUnitSet\Models\AddUnitSet','add_unit_set_id'); 
    }

    public function platform()
    {
        return $this->belongsTo('Broadcasters\Platform\Models\Platform','platform_id'); 
    }

    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }

}