<?php namespace Broadcasters\AddUnit\Providers;

use Broadcasters\AddUnit\Models\AddUnit as Model;



class AddUnitServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
	

	/**
	 * 
	 */
	public function saveAddUnit($request)
	{
		try{
			$this->model->create($request);
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function update($request,$id)
	{

		try{
			
			$this->model->find($id)->update($request);
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}
	public function delete($id)
	{
		$this->model->where('add_unit_set_id',$id)->delete();
	}

	public function updateVersion($request)
	{
		
		$adUnitSetId = $request->get('adUnitSetId');
		$platformId = $request->get('platformId');
		$appVersion = $request->get('appVersion');
		$appLastVersion = $request->get('appLastVersion');
		$version = '';
		for($i=0;$i<count($platformId);$i++)
		{
			// echo filter_var($appVersion[$i], FILTER_SANITIZE_NUMBER_INT);
			// dd(filter_var($appLastVersion[$i], FILTER_SANITIZE_NUMBER_INT));
			if(filter_var($appVersion[$i], FILTER_SANITIZE_NUMBER_INT) >= filter_var($appLastVersion[$i], FILTER_SANITIZE_NUMBER_INT))
			{
				//dd('here');
				$version = $this->model
				->where(['platform_id'=>$platformId[$i],'add_unit_set_id'=>$adUnitSetId[$i]])->update(['app_version'=>$appVersion[$i]]);

			}

		}
		return $version;

	}







}
