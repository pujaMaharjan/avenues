<?php

namespace Broadcasters\UnitEarning\Models;

use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
	//protected $primaryKey = 'id';
	//protected $fillable = ['name','broadcaster_id'];

	//protected $table = 'earnings';
    /**
     * Get the user relationship
     */

    public function scopeInDate($query,$date)
    {
    	if(isset($date)) {
    		return $query->whereBetween('date',$date);
    	}
    }

}