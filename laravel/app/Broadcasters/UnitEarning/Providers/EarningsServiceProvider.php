<?php namespace Broadcasters\UnitEarning\Providers;

use Broadcasters\UnitEarning\Models\Earning as Model;



class EarningsServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function getEarningByUnits($units)
	{
		try {

			$total = [];

			foreach ($units->addUnit as $adUnit) {
			//dd(str_replace('/', ':', $adUnit->unit_name));
			//dd($adUnit->unit_name);

				$earning = $this->model->where('ad_unit_name',str_replace('/', ':', $adUnit->unit_name))->get();
				$total[] = $earning->sum('earning');

			}
			return $total;
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	public function getEarningDetailsByUnits($units)
	{
		$details = [];
		try {
			foreach ($units->addUnit as $adUnit)
			{
				$details[] = $this->model->where('ad_unit_name',str_replace('/', ':', $adUnit->unit_name))->get();

			}
			return $details;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getFilterEarning($request,$units)
	{
		try {
			$date ['start'] = $request->get('from');
			$date ['end'] = $request->get('to');

			
			$total = [];

			foreach ($units->addUnit as $adUnit) {
			//dd(str_replace('/', ':', $adUnit->unit_name));
			//dd($adUnit->unit_name);

				$earning = $this->model->where('ad_unit_name',str_replace('/', ':', $adUnit->unit_name))->inDate($date)->get();
				
				$total[] = $earning->sum('earning');

			}
			return $total;
		} catch (Exception $e) {
			return $e->getMessage();
		}
		
	}

	public function totalEarning()
	{
		try {
			$earning = $this->model->get();
			$total[] = $earning->sum('earning');
			return $total;

		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

}
