<?php

namespace Broadcasters\NewsApi\Models;

use Illuminate\Database\Eloquent\Model;

class AppExternalApi extends Model
{
	protected $primaryKey = 'app_external_api_id';
	protected $fillable = ['app_external_news','app_id'];

	protected $table = 'app_external_api';
    protected $casts = [
    'app_external_news'=>'array'
    ];

    /**
     * app extrenal api belogns to app
     */
    public function app()
    {
    	return $this->belongsTo('Broadcasters\Models\App','app_id');
    }

}