<?php

namespace Broadcasters\NewsApi\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Broadcasters\NewsApi\Models\AppExternalApi as Api;
use Broadcasters\Models\App;
use App\Http\Requests\NewsApiRequest as Request;

use Broadcasters\NewsApi\Providers\NewsApiServiceProvider as NewsApiService;



class NewsApiController extends Controller
{
    protected $service;
    function __construct(NewsApiService $service) {
        $this->service = $service;
        $this->middleware('service',['only' => ['index','create','edit']]);
    }

    // protected $api;

    // public function __construct(Api $api){

    //     $this->api = $api;

    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($app_id,$news_id)
    {
        $newsApis = $this->service->getNewsApi($news_id);
        return view("newsapi.index",compact('newsApis','news_id','app_id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($app_id,$news_id)
    {
        return view('newsapi.new',compact('app_id','news_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($app_id,$news_id,Request $request)
    {
       // $data =  $request->input('app_external_news');
        $this->service->create($request,$news_id);
        return redirect("broadcaster/app/$app_id/services/news/$news_id/newsapi")->with("success","New External news Apis has been added!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($app_id,$news_id,$app_channel_id)
    {
        $channel = Channel::find($app_channel_id);
        return view('channel.show',compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($app_id,$news_id,$app_external_api_id)
    {
        $newsApi = $this->service->getById($app_external_api_id);
        return view('newsapi.edit',compact('news_id','app_external_api_id','newsApi','app_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($app_id,$news_id,Request $request,$app_external_api_id)
    {
      
        $this->service->update($request,$news_id);
        return redirect("broadcaster/app/$app_id/services/news/$news_id/newsapi")->with("success","External news Apis has been updated!");

    }


    public function delete($id,$app_external_api_id)
    {
        $this->service->delete($app_external_api_id);
        return redirect("broadcaster/app/$id/services/newsapi")->with("success","External news Apis has been deleted!");
    }

}
