<?php namespace Broadcasters\NewsApi\Providers;

use Broadcasters\NewsApi\Models\AppExternalApi as Model;

use Broadcasters\Models\App;
use Broadcasters\Models\AppNews;



class NewsApiServiceProvider{

	protected $model;
	/**
	 * constructer
	 */
	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function getNews($id)
	{
		
		$data = App::find($id);
		if($data)
        {
            return $data->appNews;
        }
	}

	/**
	* List External news api 
 	*/
	public function getNewsApi($id)
	{
        $data = AppNews::find($id);
        if($data)
        {
            return $data->appExternalApi;
        }
	}

	/**
	 * Create New External News Api
	 */
	public function create($request,$id)
	{
		try{
			//dd($this->model = App::find($id));
			$news = AppNews::find($id);

			$data= $news->appExternalApi()->create($request->all());
			// dd($data);
		}
		catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function getById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Update news api 
	 */
	public function update($request,$id)
	{
		try{
			$news = AppNews::find($id);

			// $this->model->find($id)->update($request->all());
			$news->appExternalApi()->delete();
			$data= $news->appExternalApi()->create($request->all());
		}
		catch(\Exception $e){
			dd($e);
		}
	}

	public function delete($id)
	{
		try {
			return $this->model->find($id)->delete();
		} catch (Exception $e) {
			dd($e);
		}
		
	}
	public function getNewsApp($appId)
	{
		try {
			return $this->model->where('app_id',$appId)->first();
		} catch (Exception $e) {
			$e->message();
		}
	}

}
