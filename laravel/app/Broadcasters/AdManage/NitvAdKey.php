<?php

namespace Broadcasters\AdManage;

use Illuminate\Database\Eloquent\Model;

class NitvAdKey extends Model{

	protected $table = 'nitv_ad_keys';
	protected $fillable = ['app_id','ios_banneradunitid','ios_interestialadunitid','android_banneradunitid',
		'android_interestialadunitid','status','ios_banneradunitid_status','ios_interestialadunitid_status','android_banneradunitid_status','android_interestialadunitid_status'];

	protected $casts = [
		'status'=>'boolean',
		'ios_banneradunitid_status'=>'boolean',
		'ios_interestialadunitid_status'=>'boolean',
		'android_banneradunitid_status'=>'boolean',
		'android_interestialadunitid_status'=>'boolean',
	];
}