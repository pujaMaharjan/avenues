-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2016 at 03:29 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eserp`
--

-- --------------------------------------------------------

--
-- Table structure for table `credit_memo_details`
--

CREATE TABLE IF NOT EXISTS `credit_memo_details` (
  `credit_memo_details_id` int(11) NOT NULL,
  `credit_memo_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(10,3) NOT NULL,
  `unit_amount` decimal(10,3) NOT NULL,
  `disc_percent` decimal(10,3) NOT NULL,
  `apply_tax` int(11) NOT NULL,
  `product_location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credit_memo_details`
--
ALTER TABLE `credit_memo_details`
  ADD PRIMARY KEY (`credit_memo_details_id`),
  ADD KEY `credit_memo_id` (`credit_memo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credit_memo_details`
--
ALTER TABLE `credit_memo_details`
  MODIFY `credit_memo_details_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `credit_memo_details`
--
ALTER TABLE `credit_memo_details`
  ADD CONSTRAINT `credit_memo_details_ibfk_1` FOREIGN KEY (`credit_memo_id`) REFERENCES `credit_memo` (`credit_memo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
